import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import com.intap.ESG.Models.InvoiceNews;
import com.intap.ESG.Models.Invoices;
import com.intap.ESG.Models.InvoiceNews.InvoiceNewsBuilder;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class InvoicesNewsDTO {

	private Long id;
	
	private String tipo;
	

	public String name;
	

	private Double taxes;
	

	private Double unitPrice;
	

	private int amount;
	

	public String Observations;
	
	private Invoices invoices;
	
	
	private String newType;
}
