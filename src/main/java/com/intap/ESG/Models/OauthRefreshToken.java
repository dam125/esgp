/**
 * 
 */
package com.intap.ESG.Models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Intap-pc
 *
 */
@Entity
@Table(name="oauth_refresh_token")
public class OauthRefreshToken {
	 
	 @Id
	 @Column(name="token_id", length=256 )
	 private String tokenId;
	 
	 @Column(length=20000)
	 private String token;

	 @Column(length=20000)
	 private String authentication;
}
