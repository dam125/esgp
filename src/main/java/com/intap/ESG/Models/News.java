package com.intap.ESG.Models;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "news")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class News implements Serializable {
	

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "news_seq")
	@SequenceGenerator(name = "news_seq", sequenceName = "news_sequence", allocationSize = 1)
	private Long id;
	
	private String tipo;
	
	private String name;
	
	private Double valor;
	
	private Boolean porcentaje;
	
	@Column(columnDefinition = "decimal(20,5) DEFAULT 0")
	private Double valorPorcentaje;
	
	@Column(name = "enabled")
	public boolean enabled;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	

}
