package com.intap.ESG.Models.DTO;



import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@SuppressWarnings("serial")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class NewPageDTO {
	private Integer page;
	private Integer limit;

    private String tipo;
	
	private String name;
	
	private Double valor;
	
	private Double valorMax;
	
	private Boolean porcentaje;

	private Double valorPorcentaje;
	
	private Boolean estado;
	
}
