package com.intap.ESG.Models.DTO;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@SuppressWarnings("serial")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductsDTO implements Serializable {

	private String name;
	private String code;
	private String reference;
	private String type;
	private String creationDate;
	private String Category;
	private Boolean inventory;
	private Boolean enabled;
	private Double taxes;
	private Double unitPrice;
}
