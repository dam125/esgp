package com.intap.ESG.Models.DTO;

import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 
 * @author Leonardo
 *
 */
@SuppressWarnings("serial")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AssocietedPageData implements Serializable {

	
	private Integer limit;	
	private Integer page;	
	private String names;
	private String identificationNumber;
	private String CellPhoneNumber;
	private String neighborhood;
	private String city;
	private String email;
	private List<TypeAssociatedFilter> typeAssociated;
	private Integer userId;
	
}
