/**
 * 
 */
package com.intap.ESG.Models.DTO;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Leonardo
 *
 */
@SuppressWarnings("serial")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductsPageDto implements Serializable {

	private Integer page;
	private Integer limit;
	private String name;
	private String reference;
	private String type;
	private Boolean inventory;
	private Boolean enabled;
	private Integer maxvalue;
	private Integer minvalue;
	private Long categoryProductsId;
}
