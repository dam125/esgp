package com.intap.ESG.Models.DTO;


import java.time.LocalDate;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ContractsDto {
	
	public Long id;
	public String consecutive;
	public String address;
	public String startDate;
	public String endingDate;
	public String statusDate;
	public String billingStarDate;
	public int cuttingDay;
	public int expirationDay;
	public int startPeriod;
	public String periodicity;
	public String ipClient;
	public String macClient;
	public String serialEquipment;
	public int associateId;
	public int clientId;
	public String associateName;
	public String associateDocumento;
	public String associatePhone;
	public String celphone2;
	public String associateEmail;
	public Boolean activo;
	public int factClose;
	public int facOpen;
	public double contractValue;
	public Boolean deliveredProduct;
	public String retirementReason;
	public String retirementObservation;
	public String city;
	public String neighborhood;
	public String type;
	public boolean noBill;
	public String estado;
	public int diasMora;
	public int saldo;
	public boolean datacredit;
	public String urldc;
	public LocalDate dateNotification;
	public LocalDate dateReport;
	public LocalDate dateDeleteReport;
	public Integer idDataCredit;


}

