package com.intap.ESG.Models.DTO;


import java.io.Serializable;

import javax.persistence.Entity;


import com.intap.ESG.Models.Invoices;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class InvoicesNewsDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;


	private Long id;
	
	private String tipo;
	

	public String name;
	

	private Double taxes;
	

	private Double unitPrice;
	

	private int amount;

	public String Observations;
	
	// llaves 
	private Invoices invoices;
	
	private String newsType;

}
