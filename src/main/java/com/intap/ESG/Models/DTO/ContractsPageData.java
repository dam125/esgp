package com.intap.ESG.Models.DTO;

import java.io.Serializable;
import java.time.LocalDate;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 
 * @author Jose A
 *
 */

@SuppressWarnings("serial")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ContractsPageData  implements Serializable {

	private Integer limit;	
	private Integer page;
	private String nameCliente;
	private String consecutive;
	private String cc;
	private String ip;
	private Integer fopen;
	private Integer fclose;
	private String mac;
	private LocalDate fechaAfiliacion;
	private String barrio;
	private String telefono;
	private String direccion;
	private Number valor;
	private Boolean activo;
	private String estado;
	private String subsidiado;
	private LocalDate fechaFac;
	private String serialEquipo;
	private Integer saldomin;
	private Integer saldomax;
	
}
