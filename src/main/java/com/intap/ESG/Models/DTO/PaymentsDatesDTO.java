package com.intap.ESG.Models.DTO;

import java.time.LocalDate;
import java.util.List;

import com.intap.ESG.Models.DTO.RoleDataDto.RoleDataDtoBuilder;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PaymentsDatesDTO {

	private String desde;
	private String hasta;
	
}
