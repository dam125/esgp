package com.intap.ESG.Models.DTO;

import lombok.Builder;
import lombok.Data;

/**
 *
 * @author Leonardo Castro
 *
 */
@Data
@Builder
public class ErrorData {
	private final String property;
	private final String message;
}
