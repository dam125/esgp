package com.intap.ESG.Models.DTO;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 
 * @author Leonardo
 *
 */
@SuppressWarnings("serial")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class InvoicesPageDto implements Serializable {

	private Integer page;
	private Integer limit;
	

	private String startDate; 
	

	private String endDate; 
	
	private String consecutive; 
	private String consecutivemin;
	private String names; 
	private String envoicesState; 
	private Integer minTotal; 
	private Integer maxTotal;
	
}

