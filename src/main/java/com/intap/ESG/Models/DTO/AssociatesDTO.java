package com.intap.ESG.Models.DTO;

import java.time.LocalDate;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonInclude(Include.NON_NULL)
public class AssociatesDTO {

	public Integer id; //
	public String fullName; //
	public String firstName; //
	public String lastName; //
	public boolean enabled; //
	public String city; //
	public String phoneNumber; //
	public String address; //
	public String identificationNumber; //
	public String email; //
	public Long associate_type_id; //
	public String birthdate; //
	public String identificationType;
	public Long idRol;
	public List<Object> type;
	public LocalDate creationDateAssociated;

	// Adicional info Client
	public Integer idClient;
	public boolean enabledClient; //
	public List<String> associate_type_name; //
	//public String ip_Client;
	public Long id_info; //
	public String neighborhood_Client; //
	public String creationDate_Client; //
	public String CellPhoneNumber2_Client; //
	public String CellPhoneNumber; //
	public String ReferenceFamily_Client; //
	public String ContacNumber_Client; //

	// info Employee
	public Integer idEmployee;
	public boolean enabledEmployee; //
	public String PositionEmployee; //
	public String branchEmployee; //
	public String ObservationsEmployee; //
	public String FileEmployee; //
	public String businessName; //
	public String dependencyEmployee;
	public LocalDate  creationDataEmployee;

	// Info provider
	public Integer idProvider;
	public boolean enabledProvider; //
	public String economicActivityProvider;
	public String PaymentConditionProvider;
	public LocalDate creationDateProvider;
	public String TypeServiceProvider;
	public String CIIUCode;
	public String Country;
	public String providerContactName;
	public String providerContactCellphone;
	//public Associates associate_id;
	
	//socio
	
	public Integer idPartner;
	public boolean enabledPartner; //
	public String rolPartner;
	public LocalDate  creationDataPartner;

	// User Info

	public Long idUser;
	

}
