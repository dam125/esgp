/**
 * 
 */
package com.intap.ESG.Models.DTO;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Leonardo
 *
 */
@SuppressWarnings("serial")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TypeAssociatedFilter implements Serializable {
	private Integer type; //0: general, 1:empleado, 2:cliente, 3:proveedor, 4:socio,
	private Integer status; // 0:all, 1:active, 2:inactive
}
