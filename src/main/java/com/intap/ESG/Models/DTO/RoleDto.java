package com.intap.ESG.Models.DTO;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.intap.ESG.Models.DTO.AuthorityDto.AuthorityDtoBuilder;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonInclude(Include.NON_NULL)
public class RoleDto {
	
	public Long id;
	public String name;
	public List<String> authorities;
	
	
	

}
