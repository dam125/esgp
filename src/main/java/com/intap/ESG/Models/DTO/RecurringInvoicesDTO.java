package com.intap.ESG.Models.DTO;

import java.io.Serializable;
import java.time.LocalDate;

import com.intap.ESG.Models.DTO.InvoicesDTO.InvoicesDTOBuilder;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class RecurringInvoicesDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer page;
	private Integer limit;
	private String year;
	private String file;
	private Long id;
	
	private String month;
	
	private Integer invoicesGenerated;
	private Integer invoicesGeneratedMax;
}
