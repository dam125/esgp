package com.intap.ESG.Models.DTO;

import java.time.LocalDate;

import javax.persistence.Column;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class InvoicesDTO {
	
	public Long id;
	public String consecutive;
	public String referenciaPago;
	public Integer total;
	public String envoicesState;
	public String envoicesMonth;
	public LocalDate invoiceDate;
	public LocalDate expirationDay;
	public String associateName;
	public String celPhone;
	public String celPhone2;
	public Long contract;
	public Long associate;
	public Integer jsonTotales;
	public String email;
	public String file;
	public Double pago;
	public boolean cancel;
	public String reasonCancel;
	public String observationCancel;
	public double saldo;
	
}
