package com.intap.ESG.Models;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "filesEmployee")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class FilesEmployee implements Serializable{
	

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "files_employee_seq")
	@SequenceGenerator(name = "files_employee_seq", sequenceName = "files_employee_sequence", initialValue = 1)
	public Integer id;
	
	public String nombre;
	
	public String url;
	
	//Llaves 
	@JsonIgnore
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "idEmployee", referencedColumnName = "id", foreignKey = @ForeignKey(name="fk_Employee_id__FileEmployee_id"))
	private Employee idEmployee;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
