package com.intap.ESG.Models;


import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "employee")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Employee implements Serializable{
	

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "employee_seq")
	@SequenceGenerator(name = "employee_seq", sequenceName = "employee_sequence", initialValue = 1)
	public Integer id;
	
	public String Position;

	public String branch;

	public String Observations;
	
	public String dependency;

	public String File;
	
	public boolean enabled;
	public LocalDate dateEnabled;
	private String enabledObservation;
	
	private String enabledReason;

	/*@OneToOne
	@JoinColumn(name = "enabledReasonId", referencedColumnName = "id", foreignKey = @ForeignKey(name="fk_Associated_id__enabledReason"))
	public EnabledReason enabledReason;*/
	
	//Llaves 
	@JsonIgnore
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "idAssociated", referencedColumnName = "id", foreignKey = @ForeignKey(name="fk_Associated_id__Employee"))
	private Associated idAssociated;
	
	
	@OneToOne
	@JoinColumn(name = "userId", referencedColumnName = "id", foreignKey = @ForeignKey(name="fk_UserModel_id__Employee"))
	public UserModel userId;
	
	private LocalDate creationDate;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
