package com.intap.ESG.Models;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "products")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Products implements Serializable{

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "products_seq")
	@SequenceGenerator(name = "products_seq", sequenceName = "products_sequence", allocationSize = 1)
	private Long id;
	
	
	public Long code;
	
	@Column(name = "name")
	public String name;
	
	@Column(name = "reference")
	public String reference;
	
	@Column(name = "type")
	public String type;
	
	@Column(name = "inventory")
	public boolean inventory;
	
	//@Column(name = "stock")
	//public int stock;
	
	@Column(name = "enabled")
	public boolean enabled;
	
	public Double unitPrice;
	
	@Column(name = "taxes")
	public Double taxes;
	
	//Llaves
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "category_products_id", referencedColumnName = "id")
	private CategoryProducts categoryProducts;
	
	//Control de actualizacioness
	
	@Column(name="updated_at", columnDefinition="TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP")	
	@Temporal(TemporalType.TIMESTAMP)	
	private Date updatedAt;

	@Column(name="create_at", columnDefinition="TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP")
	@Temporal(TemporalType.TIMESTAMP)	
	private Date createAt;
	
	@PrePersist
	public void beforeCreating() {
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeZone(TimeZone.getTimeZone("UTC"));

		this.setCreateAt(calendar.getTime());	
	}
	
	@PreUpdate
	public void beforeUpdate() {
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeZone(TimeZone.getTimeZone("UTC"));

		this.setUpdatedAt(calendar.getTime());		
	}
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
