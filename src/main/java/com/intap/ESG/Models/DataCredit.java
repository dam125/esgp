package com.intap.ESG.Models;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "datacredit")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class DataCredit implements Serializable{

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "datacredit_seq")
	@SequenceGenerator(name = "datacredit_seq", sequenceName = "datacredit_seq", initialValue = 1)
	public Integer id;
	
	public LocalDate reporDate;
	
	public LocalDate notificationDate;
	
	public LocalDate reportDeleteDate;
	
	public String file;
	
	@JsonIgnore
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "idAssociated", referencedColumnName = "id", foreignKey = @ForeignKey(name="fk_Associated_id__Employee"))
	public Associated idAssociated;
	
	private static final long serialVersionUID = 1L;
}
