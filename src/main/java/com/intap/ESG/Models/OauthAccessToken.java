/**
 * 
 */
package com.intap.ESG.Models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author
 *
 */
@Entity
@Table(name="oauth_access_token")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class OauthAccessToken {
	  	  
	  @Column(name="authentication_id", unique = true)
	  private String authenticationId;
	  
	  @Id
	  @Column(name="token_id")
	  private String tokenId;
	  	  
	  @Lob
	  @Column(columnDefinition = "BYTEA")
	  private byte[] token;
	  	  
	  @Column(name="user_name")
	  private String username;

	  @Column(name="client_id")
	  private String clientId;
	  
	  @Lob
	  @Column(columnDefinition = "BYTEA")
	  private byte[] authentication;

	  @Lob
	  @Column(name="refresh_token")
	  private String refreshToken;
}
