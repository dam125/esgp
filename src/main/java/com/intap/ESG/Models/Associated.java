package com.intap.ESG.Models;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "associated")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Associated implements Serializable{
	

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "associated_seq")
    @SequenceGenerator(sequenceName = "associated_seq", allocationSize = 1, name = "associated_seq")
	public Integer id;

	public String firstName;

	public String lastName;
	
	public String identificationType;
	
	public String identificationNumber;
	
	public String CellPhoneNumber;

	public String phoneNumber;

	public String address;
	
	public String neighborhood;
	
	public String city;

	public String email;

	public LocalDate birthdate;
	
	public boolean enabled;
	public LocalDate dateEnabled;
	private String enabledObservation;
	
	private String enabledReason;

	/*@OneToOne
	@JoinColumn(name = "enabledReasonId", referencedColumnName = "id", foreignKey = @ForeignKey(name="fk_Associated_id__enabledReason"))
	public EnabledReason enabledReason;*/
	
	private LocalDate creationDate;
	
	@OneToOne(mappedBy = "idAssociated")
	private Client client;
	
	@OneToOne(mappedBy = "idAssociated")
	private Provider provider;
	
	@OneToOne(mappedBy = "idAssociated")
	private Employee employee;
	
	@OneToOne(mappedBy = "idAssociated")
	private Partner partner;
	
	@OneToOne(mappedBy = "idAssociated")
	private DataCredit datacredit;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
