package com.intap.ESG.Models;


import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "payments")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Payments implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "payments_seq")
	@SequenceGenerator(name = "payments_seq", sequenceName = "payments_sequence", allocationSize = 1)
	private Long id;
	
	@Column(name = "invoiceDate")
	public LocalDate paymentDate;
	
	@Column(name = "wayOfPay")
	private String wayOfPay;
	
	@Column(name = "valuePaid")
	private Double valuePaid;
	
	@Column(name = "moneyReceived")
	private Double moneyReceived;
	
	@Column(name = "consecutive", nullable = false, unique = true, length = 250, columnDefinition = "varchar(250)")
	private String consecutive;
	
	@Column(name = "cancel", columnDefinition = "BOOLEAN default FALSE")
	public boolean cancel;
	
	@Column(name = "observationCancel")
	private String observationCancel;

	//Facha anulacion
	//Motivo de anulacion
	//observacion de anulacion
	//anulacion
	
	//Llaves 
	@JsonIgnore
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "idFactura", referencedColumnName = "id")
	private Invoices factura;

}
