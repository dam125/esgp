package com.intap.ESG.Models;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "categoryProducts")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CategoryProducts implements Serializable{
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "category_products_seq")
	@SequenceGenerator(name = "category_products_seq", sequenceName = "category_products_sequence", allocationSize = 1)
	private Long id;
	
	@NotEmpty(message = "El campo name es requerido")
	private String name;
	
	@JsonIgnore
	@OneToMany(mappedBy = "categoryProducts")
	private List<Products> products;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
