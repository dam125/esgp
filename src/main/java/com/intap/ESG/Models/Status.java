package com.intap.ESG.Models;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "Status")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Status implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "status_seq")
	@SequenceGenerator(name = "status_seq", sequenceName = "stratus_sequence", allocationSize = 1)
	private Long id;
	
	private String name;
	
	private String description;
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
