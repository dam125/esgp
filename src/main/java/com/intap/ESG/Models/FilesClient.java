package com.intap.ESG.Models;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "filesClient")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class FilesClient implements Serializable{
	

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "files_client_seq")
	@SequenceGenerator(name = "files_client_seq", sequenceName = "files_client_sequence", initialValue = 1)
	public Integer id;
	
	public String nombre;
	
	public String url;
	
	//Llaves 
	@JsonIgnore
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "idClient", referencedColumnName = "id", foreignKey = @ForeignKey(name="fk_Client_id__FileClient_id"))
	private Client idClient;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
