package com.intap.ESG.Models;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "contractStatus")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ContractStatus implements Serializable {
	


	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Contract_status_seq")
	@SequenceGenerator(name = "Contract_status_seq", sequenceName = "Contract_status_sequence", allocationSize = 1)
	@Column(name = "id")
	private Long id;
	
	@OneToOne
	@JoinColumn(name = "contract", referencedColumnName = "id", foreignKey = @ForeignKey(name="fk_contractStatus_id__contract"))
	private Contracts contract;
	
	@OneToOne
	@JoinColumn(name = "status", referencedColumnName = "id", foreignKey = @ForeignKey(name="fk_contractStatus_id__status"))
	private Status status;
	
	private String observation;
	
	//Control de actualizacioness
	
	@OneToOne
	@JoinColumn(name = "userCrea", referencedColumnName = "id", foreignKey = @ForeignKey(name="fk_contractStatus_id__Employee"))
	private Employee userCrea;


	@Column(name = "createAt")
	public LocalDate createAt;
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


}
