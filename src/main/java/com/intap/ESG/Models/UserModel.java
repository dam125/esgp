package com.intap.ESG.Models;

import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.TimeZone;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "users", uniqueConstraints = @UniqueConstraint(columnNames = "user_name", name = "uniquie_user_name_users"))
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserModel implements UserDetails{

	
	private static final long serialVersionUID = -3687396829902318542L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "users_seq")
	@SequenceGenerator(name = "users_seq", sequenceName = "users_sequence", allocationSize = 1)
	private Long id;

	@Column(name = "user_name")
	private String username;

	@JsonIgnore
	@Column(name = "password")
	private String password;

	@Column(name = "account_expired")
	private Boolean accountExpired;

	@Column(name = "account_locked")
	private Boolean accountLocked;

	@Column(name = "credenttials_expired")
	private Boolean credentialsExpired;

	@Column(name = "enabled")
	private Boolean enabled;

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "role_id", foreignKey = @ForeignKey(name = "fk_role_id_users"))
	@NotNull(message = "el campo role es requerido")
	private Role role;
	
	@Column(name="updated_at", columnDefinition="TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedAt;

	@Column(name="create_at", columnDefinition="TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createAt;
	
	@JsonIgnore
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return (Collection<? extends GrantedAuthority>) role.getAuthorities();
	}
	
	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getUsername() {
		return username;
	}


	public void setUsername(String username) {
		this.username = username;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}


	public Boolean getAccountExpired() {
		return accountExpired;
	}


	public void setAccountExpired(Boolean accountExpired) {
		this.accountExpired = accountExpired;
	}


	public Boolean getAccountLocked() {
		return accountLocked;
	}


	public void setAccountLocked(Boolean accountLocked) {
		this.accountLocked = accountLocked;
	}


	public Boolean getCredentialsExpired() {
		return credentialsExpired;
	}


	public void setCredentialsExpired(Boolean credentialsExpired) {
		this.credentialsExpired = credentialsExpired;
	}


	public Boolean getEnabled() {
		return enabled;
	}


	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}


	
	public Role getRole() {
		return role;
	}


	public void setRole(Role role) {
		this.role = role;
	}


	public Date getUpdatedAt() {
		return updatedAt;
	}


	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}


	public Date getCreateAt() {
		return createAt;
	}


	public void setCreateAt(Date createAt) {
		this.createAt = createAt;
	}


	@Override
	public boolean isAccountNonExpired() {
		return this.getAccountExpired();
	}

	@Override
	public boolean isAccountNonLocked() {
		return this.getAccountLocked();
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return this.getCredentialsExpired();
	}

	@Override
	public boolean isEnabled() {
		return this.getEnabled();
	}
	
	@PrePersist
	public void beforeCreating() {
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeZone(TimeZone.getTimeZone("UTC"));

		this.setCreateAt(calendar.getTime());
	}

	@PreUpdate
	public void beforeUpdate() {
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeZone(TimeZone.getTimeZone("UTC"));

		this.setUpdatedAt(calendar.getTime());
	}

	  
	  
	
}
