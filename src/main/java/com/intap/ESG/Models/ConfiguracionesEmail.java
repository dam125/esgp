package com.intap.ESG.Models;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "configuracionesEmail")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ConfiguracionesEmail implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "configurarionEmail_seq")
	@SequenceGenerator(name = "configurarionEmail_seq", sequenceName = "configurarionEmail_sequence", allocationSize = 1)
	private Long id;
	
	String defaultEncoding;
	String host;
	Integer port;
	String username;
	String password;
	Boolean mailSmtpAuth;
	Boolean mailSmtpStarttlsEnable;
	Boolean mailSmtpStarttlsRequired;
	String mailSmtpSslTrust;
	Integer mailSmtpConnectiontimeout;
	Integer mailSmtpTimeout;
	Integer mailSmtpWritetimeout;
	
	

}
