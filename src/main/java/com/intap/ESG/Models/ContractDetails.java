package com.intap.ESG.Models;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "contractDetails")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ContractDetails implements Serializable{
	

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Contract_detail_seq")
	@SequenceGenerator(name = "Contract_detail_seq", sequenceName = "Contract_detail_sequence", allocationSize = 1)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "taxes", columnDefinition = "decimal(4,2) DEFAULT 0")
	private Double taxes;
	
	@Column(name = "unitPrice")
	private Double unitPrice;
	
	@Column(name = "amount")
	private int amount;
	
	@Column(name = "Observations")
	public String Observations;
	
	
	// llaves 
	@JsonIgnore
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "contracts_id", referencedColumnName = "id")
	private Contracts contracts;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "products_id", referencedColumnName = "id")
	private Products products;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	

}
