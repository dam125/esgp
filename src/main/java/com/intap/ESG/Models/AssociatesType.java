package com.intap.ESG.Models;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "associatesType")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AssociatesType {

	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")	
	public Long id;
	   

	  @Column(name = "name")
	  public String name; 
	  
}
