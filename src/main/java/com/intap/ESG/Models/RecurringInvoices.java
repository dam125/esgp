package com.intap.ESG.Models;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "RecurringInvoices")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class RecurringInvoices implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "RecurringInvoices_seq")
	@SequenceGenerator(name = "RecurringInvoices_seq", sequenceName = "RecurringInvoices_sequence", allocationSize = 1)
	private Long id;
	
	private String year;
	
	private String month;
	
	private Integer invoicesGenerated;
	
	private String file;
	
	//Llaves 
	@JsonIgnore
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "idEmployee", referencedColumnName = "id", foreignKey = @ForeignKey(name="fk_RecurringInvoices_id__Employee_id"))
	private Employee idEmployee;
	
	@Column(name="create_at", columnDefinition="TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP")
	@Temporal(TemporalType.TIMESTAMP)	
	private Date createAt;
	
	@PrePersist
	public void beforeCreating() {
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeZone(TimeZone.getTimeZone("UTC"));

		this.setCreateAt(calendar.getTime());	
	}

}
