package com.intap.ESG.Models;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "provider")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Provider implements Serializable{
	

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "provider_seq")
	@SequenceGenerator(name = "provider_seq", sequenceName = "provider_sequence", initialValue = 1)
	public Integer id;
	
	public String economicActivity;

	public String PaymentCondition;

	public LocalDate creationDate;

	public String TypeService;

	public String CIIUCode;

	public String Country;

	public String businessName;
	
	public String contactName;
	
	public String contactCellphone;
	
	public String File;
	
	public boolean enabled;
	public LocalDate dateEnabled;
	private String enabledObservation;
	private String enabledReason;

	/*@OneToOne
	@JoinColumn(name = "enabledReasonId", referencedColumnName = "id", foreignKey = @ForeignKey(name="fk_Associated_id__enabledReason"))
	public EnabledReason enabledReason;*/
	
	//Llaves 
	@JsonIgnore
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "idAssociated", referencedColumnName = "id", foreignKey = @ForeignKey(name="fk_Associated_id__Provider"))
	private Associated idAssociated;
	
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
