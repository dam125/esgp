package com.intap.ESG.Models;


import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;



import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "configurations")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Configurations implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "configuration_seq")
	@SequenceGenerator(name = "configuration_seq", sequenceName = "configuration_sequence", allocationSize = 1)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "consecutiveFactura")
	private String consecutiveFactura;
	
	@Column(name = "consecutiveFacturaStart")
	private String consecutiveFacturaStart;
	
	@Column(name = "resolutionconsecutiveInvoice")
	private String resolutionconsecutiveInvoice;
	
	@Column(name = "consecutiveFacturaEnd")
	private String consecutiveFacturaEnd;
	
	@Column(name = "consecutiveFacturaDate")
	private LocalDate consecutiveFacturaDate;
	
	@Column(name = "consecutiveContrato")
	private String consecutiveContrato;
	
	@Column(name = "consecutiveRefPago")
	private String consecutiveRefPago;
	
	@Column(name = "consecutivePaymemt")
	private String consecutivePaymemt;
	
	@Column(name = "consecutiveProducts")
	private String consecutiveProducts;
	
	@Column(name = "textoFactura")
	private String textoFactura;
	
	@Column(name = "urlPayment")
	private String urlPayment;
	
	private Double tasaUsura;

}
