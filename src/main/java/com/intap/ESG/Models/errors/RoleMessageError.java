/**
 * 
 */
package com.intap.ESG.Models.errors;

/**
 * @author Leonardo Castro 
 *
 */
public enum RoleMessageError {

	NOT_FOUND_ROLE,
	INVALID_USER_FIELDS;
	
}
