package com.intap.ESG.Models;


import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "invoices")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Invoices implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "invoices_seq")
	@SequenceGenerator(name = "invoices_seq", sequenceName = "invoices_sequence", allocationSize = 1)
	private Long id;
	
	@Column(name = "invoiceDate")
	public LocalDate invoiceDate;
	
	@Column(name = "consecutive", nullable = false, unique = true, length = 250, columnDefinition = "varchar(250)")
	private String consecutive;
	
	@Column(name = "refPago", nullable = true, unique = true, length = 250, columnDefinition = "varchar(250)")
	private String refPago;
	
	@Column(name = "envoicesMonth")
	private String envoicesMonth;
	
	@Column(name = "envoicesState")
	private String envoicesState;
	
	@Column(name = "cancel", columnDefinition = "BOOLEAN default FALSE")
	public boolean cancel;
	
	@Column(name = "reasonCancel")
	private String reasonCancel;
	
	@Column(name = "observationCancel")
	private String observationCancel;
	
	//Campo motivo de anulacion
	//Campo observacion de la anulacion
	
	@Column(name = "total")
	private Integer total;
	
	@Column(name = "file")
	public String file;
	
	@Column(name = "expirationDay")
	public LocalDate expirationDay;
	
	@JsonIgnore
	@OneToMany(mappedBy = "invoices")
	private List<InvoiceNews> invoiceNews; 
	
	@Column(name = "recurringInvoice", columnDefinition = "BOOLEAN default FALSE")
	public boolean recurringInvoice;	

	//Llaves 
	@JsonIgnore
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "idContrato", referencedColumnName = "id")
	private Contracts contrato;
	
	@JsonIgnore
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "idAsociate", referencedColumnName = "id")
	private Associated associate;
	
	//private List<Payments> 
	
}
