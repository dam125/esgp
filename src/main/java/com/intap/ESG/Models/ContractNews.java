package com.intap.ESG.Models;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "contractNews")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ContractNews implements Serializable{
	

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Contract_news_seq")
	@SequenceGenerator(name = "Contract_news_seq", sequenceName = "Contract_news_sequence", allocationSize = 1)
	private Long id;
	
	@Column(name = "taxes", columnDefinition = "decimal(4,2) DEFAULT 0")
	private Double taxes;
	
	@Column(name = "unitPrice")
	private Double unitPrice;
	
	@Column(name = "amount")
	private int amount;
	
	@Column(name = "observations")
	public String Observations;
	
	@Column(name = "numberOfMonths")
	private Integer numberOfMonths;
	
	@Column(name = "billedMonths")
	private Integer billedMonths;
	
	// llaves 
	@JsonIgnore
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "contracts_id", referencedColumnName = "id", foreignKey = @ForeignKey(name="fk_contracts_id__contract_news"))
	private Contracts contracts;
	
	// llaves 
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "news_id", referencedColumnName = "id", foreignKey = @ForeignKey(name="fk_news_id__contract_news"))
	private News news;
	
	//Control de actualizacioness
	
	@Column(name="create_at", columnDefinition="TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP")
	@Temporal(TemporalType.TIMESTAMP)	
	private Date createAt;
	
	@PrePersist
	public void beforeCreating() {
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeZone(TimeZone.getTimeZone("UTC"));

		this.setCreateAt(calendar.getTime());	
	}
	
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
