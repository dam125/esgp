package com.intap.ESG.Models;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.intap.ESG.Models.Contracts.ContractsBuilder;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Entity
@Table(name = "contracts_view")
public class ViewModel implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Contracts_seq")
	@SequenceGenerator(name = "Contracts_seq", sequenceName = "Contracts_sequence", allocationSize = 1)
	@Column(name = "id",insertable = false, updatable = false)
	private Long id;
	
	@Column(name = "consecutive", nullable = false, unique = true, length = 250, columnDefinition = "varchar(250)" ,insertable = false, updatable = false)
	private String consecutive;
	

	@Column(name = "address", nullable = false, length = 250, columnDefinition = "varchar(250)" ,insertable = false, updatable = false)
	public String address;
	
	@Column(name = "city" ,insertable = false, updatable = false)
	public String city;
	
	@Column(name = "neighborhood" ,insertable = false, updatable = false)
	public String neighborhood;
	
	@Column(name = "startDate" ,insertable = false, updatable = false)
	public LocalDate startDate;
	
	@Column(name = "endingDate",insertable = false, updatable = false)
	public LocalDate endingDate;
	
	@Column(name = "billingStarDate",insertable = false, updatable = false)
	public LocalDate billingStarDate;
	
	@Column(name = "cuttingDay",insertable = false, updatable = false)
	public int cuttingDay;
	
	@Column(name = "expirationDay",insertable = false, updatable = false)
	public int expirationDay;
	
	@Column(name = "startPeriod",insertable = false, updatable = false)
	public int startPeriod;
	
	@Column(name = "periodicity",insertable = false, updatable = false)
	public String periodicity;
	
	@Column(name = "ipClient",insertable = false, updatable = false)
	public String ipClient;
	
	@Column(name = "macClient",insertable = false, updatable = false)
	public String macClient;
	
	@Column(name = "serialEquipment",insertable = false, updatable = false)
	public String serialEquipment;
	
	@Column(name = "enabled",insertable = false, updatable = false)
	public boolean enabled;
	
	@Column(name = "type",insertable = false, updatable = false)
	public String type;
	
	@Column(name = "deliveredProduct", columnDefinition = "BOOLEAN default FALSE",insertable = false, updatable = false)
	public boolean deliveredProduct;
	
	@Column(name = "dateDeliveredProduct",insertable = false, updatable = false)
	public LocalDate dateDeliveredProduct;
	
	@Column(name = "noBill", columnDefinition = "BOOLEAN default FALSE",insertable = false, updatable = false)
	public boolean noBill;
	
	@Column(name = "monthsWithoutBillSoFar", columnDefinition = "int2 default 0",insertable = false, updatable = false)
	private Integer monthsWithoutBillSoFar;
	
	@Column(name = "retirementReason",insertable = false, updatable = false)
	public String retirementReason;
	
	@Column(name = "retirementObservation", nullable = false, columnDefinition = "text default 'N/A'",insertable = false, updatable = false)
	private String retirementObservation;
	
	@Column(name = "daysPastDue", nullable = false, columnDefinition = "text default 0",insertable = false, updatable = false)
	private int daysPastDue;
	
	
	//Control de actualizacioness
	
	@Column(name="updated_at", columnDefinition="TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP",insertable = false, updatable = false)	
	@Temporal(TemporalType.TIMESTAMP)	
	private Date updatedAt;

	@Column(name="create_at", columnDefinition="TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP",insertable = false, updatable = false)
	@Temporal(TemporalType.TIMESTAMP)	
	private Date createAt;
		
	//Llaves 
	private Client id_client;
		
	//listar las coneciones Many to One
	@JsonIgnore
	@OneToMany(mappedBy = "contracts")
	private List<ContractDetails> contractDetails;
	
	@JsonIgnore
	@OneToMany(mappedBy = "contracts")
	private List<ContractNews> contractNews; 
	
	@JsonIgnore
	@OneToMany(mappedBy = "contrato")
	private List<Invoices> invoices;
	
	@JsonIgnore
	@ManyToMany(mappedBy = "contract")
	private List<ContractStatus> contractStatus;
		
}
