package com.intap.ESG.Models;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "filesPartner")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class FilesPartner implements Serializable{
	

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "files_partner_seq")
	@SequenceGenerator(name = "files_partner_seq", sequenceName = "files_partner_sequence", initialValue = 1)
	public Integer id;
	
	public String nombre;
	
	public String url;
	
	//Llaves 
	@JsonIgnore
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "idPartner", referencedColumnName = "id", foreignKey = @ForeignKey(name="fk_Partner_id__FilePartner_id"))
	private Partner idPartner;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
