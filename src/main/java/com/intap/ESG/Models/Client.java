package com.intap.ESG.Models;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "client")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Client implements Serializable{
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "client_seq")
	@SequenceGenerator(name = "client_seq", sequenceName = "client_sequence", initialValue = 1)
	public Integer id;
	
	public LocalDate creationDate;

	public String CellPhoneNumber2;

	public String ReferenceFamily;

	public String ContacNumber;
	
	public boolean enabled;
	public LocalDate dateEnabled;
	private String enabledObservation;
	
	private String enabledReason;
	
	public String File;

	/*@OneToOne
	@JoinColumn(name = "enabledReasonId", referencedColumnName = "id", foreignKey = @ForeignKey(name="fk_Associated_id__enabledReason"))
	public EnabledReason enabledReason;*/
	
	//Llaves 
	@JsonIgnore
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "idAssociated", referencedColumnName = "id", foreignKey = @ForeignKey(name="fk_Associated_id__Client"))
	private Associated idAssociated;
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
