package com.intap.ESG.Models;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "retirementReason")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class RetirementReason implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "retirement_reason_seq")
	@SequenceGenerator(name = "retirement_reason_seq", sequenceName = "retirement_reason_sequence", allocationSize = 1)
	private Long id;
	

	private String name;

}
