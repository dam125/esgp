package com.intap.ESG.Models;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "enabledReason")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class EnabledReason implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "enabled_reason_seq")
	@SequenceGenerator(name = "enabled_reason_seq", sequenceName = "enabled_reason_sequence", allocationSize = 1)
	private Long id;
	
	private String name;
	
	@JsonIgnore
	@ManyToOne()
	@JoinColumn(name = "associateType", referencedColumnName = "id")
	public AssociatesType associateType;

}
