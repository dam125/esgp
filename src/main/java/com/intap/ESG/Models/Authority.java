/**
 * 
 */
package com.intap.ESG.Models;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.springframework.security.core.GrantedAuthority;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Intap-pc
 *
 */
@Entity
@Table(name = "authorities", 
uniqueConstraints = @UniqueConstraint(columnNames = "description", name = "unique_description_authorities" ))
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Authority implements GrantedAuthority {


	@Id
	private String authority;

	private String description;

	@ManyToMany(mappedBy = "authorities", fetch = FetchType.EAGER)
	private List<Role> roles;

	@Override
	public String getAuthority() {
		return authority;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (obj instanceof Authority) {
			return this.authority.equals(((Authority) obj).authority);
		}

		return false;
	}

	@Override
	public int hashCode() {
		return this.authority.hashCode();
	}
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2359193422499438180L;

}
