package com.intap.ESG.Models;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.intap.ESG.Models.RecurringInvoices.RecurringInvoicesBuilder;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "RecoverPassword")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class RecoverPassword implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "IDRECU", unique = true, nullable = false, length = 50, columnDefinition = "varchar(50) NOT NULL")
	private String idrecu;

	@Column(name = "CODIGO", nullable = true, columnDefinition = "INT NOT NULL")
	private Integer codigo;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHACREA", length = 19, updatable = false, columnDefinition = " timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP")
	private Date fechacrea;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHAUSO", length = 19, insertable = false, columnDefinition = "timestamp NULL DEFAULT NULL")
	private Date fechauso;

	
	@OneToOne
	@JoinColumn(name = "userId", referencedColumnName = "id", foreignKey = @ForeignKey(name="fk_UserModel_id__recover_password"))
	private UserModel idusuario;

}
