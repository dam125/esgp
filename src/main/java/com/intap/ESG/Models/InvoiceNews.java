package com.intap.ESG.Models;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Entity
@Table(name = "invoicesNews")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class InvoiceNews implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "invoice_news_seq")
	@SequenceGenerator(name = "invoice_news_seq", sequenceName = "invoice_news_sequence", allocationSize = 1)
	private Long id;
	
	private String tipo;
	
	@Column(name = "name")
	public String name;
	
	@Column(name = "taxes", columnDefinition = "decimal(4,2) DEFAULT 0")
	private Double taxes;
	
	@Column(name = "unitPrice")
	private Double unitPrice;
	
	@Column(name = "amount")
	private int amount;
	
	@Column(name = "Observations")
	public String Observations;
	
	// llaves 
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "invoices_id", referencedColumnName = "id")
	private Invoices invoices;
}
