package com.intap.ESG.Models;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotEmpty;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "roles", uniqueConstraints = @UniqueConstraint(columnNames = "name", name = "uniquie_name_roles"))
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Role implements Serializable{
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "roles_seq")
	@SequenceGenerator(name = "roles_seq", sequenceName = "roles_sequence", allocationSize = 1)
	private Long id;
	
	@NotEmpty(message = "El campo name es requerido")
	private String name;

	

	@ManyToMany(cascade = {CascadeType.MERGE},fetch = FetchType.EAGER)
	@JoinTable(name = "roles_authorities", joinColumns = { @JoinColumn(name = "role_id") }, inverseJoinColumns = {
			@JoinColumn(name = "authority_id") }, foreignKey = @ForeignKey(name = "fk_role_roles_authorities"), inverseForeignKey = @ForeignKey(name = "fk_authority_roles_authorities"))
	private List<Authority> authorities;
	
	@Column(name="updated_at", columnDefinition="TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP")	
	@Temporal(TemporalType.TIMESTAMP)	
	private Date updatedAt;

	@Column(name="create_at", columnDefinition="TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP")
	@Temporal(TemporalType.TIMESTAMP)	
	private Date createAt;
	
	

	@PrePersist
	public void beforeCreating() {
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeZone(TimeZone.getTimeZone("UTC"));

		this.setCreateAt(calendar.getTime());	
	}
	
	@PreUpdate
	public void beforeUpdate() {
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeZone(TimeZone.getTimeZone("UTC"));

		this.setUpdatedAt(calendar.getTime());		
	}
	
	
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	

}
