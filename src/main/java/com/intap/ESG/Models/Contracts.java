package com.intap.ESG.Models;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "contracts")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Contracts implements Serializable{

	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Contracts_seq")
	@SequenceGenerator(name = "Contracts_seq", sequenceName = "Contracts_sequence", allocationSize = 1)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "consecutive", nullable = false, unique = true, length = 250, columnDefinition = "varchar(250)")
	private String consecutive;
	

	@Column(name = "address", nullable = false, length = 250, columnDefinition = "varchar(250)")
	public String address;
	
	public String city;
	
	public String neighborhood;
	
	@Column(name = "startDate")
	public LocalDate startDate;
	
	@Column(name = "endingDate")
	public LocalDate endingDate;
	
	@Column(name = "billingStarDate")
	public LocalDate billingStarDate;
	
	@Column(name = "cuttingDay")
	public int cuttingDay;
	
	@Column(name = "expirationDay")
	public int expirationDay;
	
	@Column(name = "startPeriod")
	public int startPeriod;
	
	@Column(name = "periodicity")
	public String periodicity;
	
	@Column(name = "ipClient")
	public String ipClient;
	
	@Column(name = "macClient")
	public String macClient;
	
	@Column(name = "serialEquipment")
	public String serialEquipment;
	
	@Column(name = "enabled")
	public boolean enabled;
	
	@Column(name = "type")
	public String type;
	
	@Column(name = "deliveredProduct", columnDefinition = "BOOLEAN default FALSE")
	public boolean deliveredProduct;
	
	@Column(name = "dateDeliveredProduct")
	public LocalDate dateDeliveredProduct;
	
	@Column(name = "noBill", columnDefinition = "BOOLEAN default FALSE")
	public boolean noBill;
	
	@Column(name = "monthsWithoutBillSoFar", columnDefinition = "int2 default 0")
	private Integer monthsWithoutBillSoFar;
	
	@Column(name = "retirementReason")
	public String retirementReason;
	
	@Column(name = "retirementObservation", nullable = false, columnDefinition = "text default 'N/A'")
	private String retirementObservation;
	
	@Column(name = "daysPastDue", nullable = false, columnDefinition = "text default 0")
	private int daysPastDue;
	
	
	//Control de actualizacioness
	
	@Column(name="updated_at", columnDefinition="TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP")	
	@Temporal(TemporalType.TIMESTAMP)	
	private Date updatedAt;

	@Column(name="create_at", columnDefinition="TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP")
	@Temporal(TemporalType.TIMESTAMP)	
	private Date createAt;
	
	
	//Llaves 
	@JsonIgnore
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "idClient", referencedColumnName = "id", foreignKey = @ForeignKey(name="fk_Contract_id__Client_id"))
	private Client idClient;
	
	
	//listar las coneciones Many to One
	@JsonIgnore
	@OneToMany(mappedBy = "contracts")
	private List<ContractDetails> contractDetails;
	
	@JsonIgnore
	@OneToMany(mappedBy = "contracts")
	private List<ContractNews> contractNews; 
	
	@JsonIgnore
	@OneToMany(mappedBy = "contrato")
	private List<Invoices> invoices;
	
	@JsonIgnore
	@ManyToMany(mappedBy = "contract")
	private List<ContractStatus> contractStatus;
	

	@PrePersist
	public void beforeCreating() {
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeZone(TimeZone.getTimeZone("UTC"));

		this.setCreateAt(calendar.getTime());	
	}
	
	@PreUpdate
	public void beforeUpdate() {
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeZone(TimeZone.getTimeZone("UTC"));

		this.setUpdatedAt(calendar.getTime());		
	}
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	
}
