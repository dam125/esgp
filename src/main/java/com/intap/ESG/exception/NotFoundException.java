/**
 * 
 */
package com.intap.ESG.exception;

/**
 * @author Leonardo Castro
 *
 */
public class NotFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1574130063213699144L;

	public NotFoundException(String message) {
		super(message);
	}

}
