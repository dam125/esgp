package com.intap.ESG.Service;

import java.util.List;

import com.intap.ESG.Models.Associated;
import com.intap.ESG.Models.Employee;
import com.intap.ESG.Models.UserModel;

public interface IEmployeeService {
	
	public List<Employee> findAll();
	
	public List<Employee> findAllEnabled();
	
	public Employee findById(Integer id);
	
	public Employee save(Employee employee);
	
	public void delete(Integer id);
	
	public Employee  findByUserId (UserModel user);
	
	public Employee findByIdAssociated(Associated associated);

}
