package com.intap.ESG.Service;

import java.util.List;

import com.intap.ESG.Models.ContractDetails;
import com.intap.ESG.Models.Contracts;
import com.intap.ESG.Models.Products;


public interface IContractDetailsService {
	
	public List<ContractDetails> findAll();
	
	public ContractDetails findById(Long id);
	
	public ContractDetails save(ContractDetails contractDetails);
	
	public void delete(Long id);
	
	List<ContractDetails> findByContracts(Contracts id);
	
	void deleteAll(List<ContractDetails>  contractDetails);
	
	double contractValue(Contracts id);
	
	List<ContractDetails> findByProduct(Products products);
	
	public void UpdatePriceByProduct(Products products, Double price);

}
