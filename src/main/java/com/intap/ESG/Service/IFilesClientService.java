package com.intap.ESG.Service;

import java.util.List;

import com.intap.ESG.Models.Client;
import com.intap.ESG.Models.FilesClient;



public interface IFilesClientService {
	
	public List<FilesClient> findAll();
	
	public List<FilesClient> findByIdClient(Client client);
	
	public FilesClient findById(Integer id);
	
	public FilesClient save(FilesClient filesClient);
	
	public void delete(Integer id);

}
