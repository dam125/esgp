package com.intap.ESG.Service;

import java.util.List;

import com.intap.ESG.Models.Employee;
import com.intap.ESG.Models.FilesEmployee;

public interface IFilesEmployeeService {
	
	public List<FilesEmployee> findAll();
	
	public List<FilesEmployee> findByIdEmployee(Employee employee);
	
	public FilesEmployee findById(Integer id);
	
	public FilesEmployee save(FilesEmployee filesEmployee);
	
	public void delete(Integer id);

}
