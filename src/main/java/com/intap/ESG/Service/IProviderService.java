package com.intap.ESG.Service;

import java.util.List;

import com.intap.ESG.Models.Associated;
import com.intap.ESG.Models.Provider;



public interface IProviderService {
	
	public List<Provider> findAll();
	
	public List<Provider> findAllEnabled();
	
	public Provider findByIdAssociated(Associated associated);
	
	public Provider findById(Integer id);
	
	public Provider save(Provider provider);
	
	public void delete(Integer id);


}
