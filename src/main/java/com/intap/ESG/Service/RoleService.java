package com.intap.ESG.Service;

import java.util.List;

import com.intap.ESG.Models.Role;
import com.intap.ESG.Models.DTO.PaginationData;
import com.intap.ESG.Models.DTO.RoleDataDto;
import com.intap.ESG.Models.DTO.RoleDto;

public interface RoleService {
	
	PaginationData findAll(Integer page, Integer limit, String sort, String search);
	
	List<Role> findAll();
	
	RoleDto findById(Long id);
	
	RoleDto create(RoleDataDto roleData);
	
	RoleDto update(Long id, RoleDataDto roleData);
	
	void delete(Long id);
	
	Role findByRol (Long id);

	List<RoleDto> findAllExport();

}
