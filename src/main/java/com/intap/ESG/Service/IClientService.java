package com.intap.ESG.Service;

import java.util.List;

import com.intap.ESG.Models.Associated;
import com.intap.ESG.Models.Client;

public interface IClientService {
	
	public List<Client> findAll();
	
	public List<Associated> findAllInfo();
	
	public List<Client> findAllEnabled();
	
	public Client findByIdAssociated(Associated associated);
	
	public Client findById(Integer id);
	
	public Client save(Client client);
	
	public void delete(Integer id);


}
