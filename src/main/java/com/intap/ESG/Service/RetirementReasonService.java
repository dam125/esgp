package com.intap.ESG.Service;

import java.util.List;

import com.intap.ESG.Models.RetirementReason;




public interface RetirementReasonService {
	
	public List<RetirementReason> findAll();
	
	public RetirementReason findById(Long id);
	
	public RetirementReason save(RetirementReason retirementReason);
	
	public void delete(Long id);

}
