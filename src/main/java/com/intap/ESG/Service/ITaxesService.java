package com.intap.ESG.Service;

import java.util.List;


import com.intap.ESG.Models.Taxes;

public interface ITaxesService {
	
	public List<Taxes> findAll();
	
	public Taxes findById(Long id);
	
	public Taxes save(Taxes taxes);
	
	public void delete(Long id);

}
