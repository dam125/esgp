package com.intap.ESG.Service;

import java.util.List;

import org.springframework.data.domain.Page;

import com.intap.ESG.Models.News;




public interface INewsService {
	
	public List<News> findAll();
	
	public News findById(Long id);
	
	public News save(News news);
	
	public void delete(Long id);
	
	Page<News> findAll(Integer page, Integer limit, String name, String tipo, Double valor,Double ValorMax, Boolean porcentaje,
			Double valorPorcentaje, Boolean estado);

	Iterable<News> findAll(String name, String tipo, Double valor,Double ValorMax, Boolean porcentaje,
			Double valorPorcentaje, Boolean estado);


}
