package com.intap.ESG.Service;

import java.util.List;
import java.util.Optional;

import com.intap.ESG.Models.Role;
import com.intap.ESG.Models.UserModel;

public interface IUserService {
	
	
	public List<UserModel> GetAllUser();
	
	public Optional<UserModel> findUserByUser(String User);
	
	public UserModel SaveUser(UserModel user);
	
	Optional<UserModel> findById(Long id);
	
	public void delete(UserModel userModel);
	
	List<UserModel> findByRol(Role role);
}
