package com.intap.ESG.Service;

import java.util.List;


import com.intap.ESG.Models.ContractNews;
import com.intap.ESG.Models.Contracts;
import com.intap.ESG.Models.News;


public interface IContractNewsService {
	
	public List<ContractNews> findAll();
	
	public ContractNews findById(Long id);
	
	public ContractNews save(ContractNews contactNews);
	
	public void delete(Long id);
	
	List<ContractNews> findByContracts(Contracts id);
	
	List<ContractNews> findByContractsEnabled(Contracts id);
	
	void deleteAll(List<ContractNews>  contractNews);
	
	List<ContractNews> findByContractNewss (News news);

}
