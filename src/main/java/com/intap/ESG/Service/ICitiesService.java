package com.intap.ESG.Service;





import com.intap.ESG.Models.Cities;

public interface ICitiesService {
	
	Iterable<Cities> findAll();
	
	public Cities findById(Long id);
	
	public Cities save(Cities cities);
	
	public void delete(Long id);

}
