package com.intap.ESG.Service;

import java.util.List;

import com.intap.ESG.Models.Status;

public interface IStatusService {
	
	public List<Status> findAll();

	public Status findById(Long id);
	
	Status findByName(String name);

	public Status save(Status status);

	public void delete(Long id);

}
