package com.intap.ESG.Service;

import java.util.List;

import com.intap.ESG.Models.CategoryProducts;



public interface ICategoryProductsService {
	
	public List<CategoryProducts> findAll();
	
	public CategoryProducts findById(Long id);
	
	public CategoryProducts save(CategoryProducts categoryProducts);
	
	public void delete(Long id);

}
