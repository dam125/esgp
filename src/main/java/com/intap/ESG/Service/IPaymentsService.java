package com.intap.ESG.Service;

import java.time.LocalDate;
import java.util.List;

import com.intap.ESG.Models.Invoices;
import com.intap.ESG.Models.Payments;

public interface IPaymentsService {
	
	public List<Payments> findAll();

	public Payments findById(Long id);

	public Payments save(Payments invoices);

	public void delete(Long id);
	
	List<Payments> findByInvoices (Invoices factura);
	
	
	
	List<Payments> findByDates (LocalDate startDate,LocalDate endDate);
}
