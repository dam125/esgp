package com.intap.ESG.Service;

import java.util.List;

import org.springframework.data.domain.Page;

import com.intap.ESG.Models.Employee;

import com.intap.ESG.Models.RecurringInvoices;

public interface IRecurringInvoicesService {
	
	public List<RecurringInvoices> findAll();
	
	public RecurringInvoices findById(Long id);
	
	public RecurringInvoices save(RecurringInvoices recurringInvoices);
	
	public void delete(Long id);
	
	RecurringInvoices findByInvoices (Employee employee, String year, String month);
	
	Page<RecurringInvoices> findAll(Integer page, Integer limit, String year, String month, Integer invoicesGenerated, Integer invoicesGeneratedMax);

	Iterable<RecurringInvoices> findAll(String year, String month, Integer invoicesGenerated, Integer invoicesGeneratedMax);
	
}
