package com.intap.ESG.Service;

import java.io.File;

public interface ISendEmailService {
	
	
	public void sendEmail(String to, String subject, String text);
	
	public void sendEmail(String to, String subject, String text, String attachments, String fileName);

}
