package com.intap.ESG.Service;

import com.intap.ESG.Models.DataCredit;

public interface IDataCreditService {

	public DataCredit save(DataCredit dc);
	
	public DataCredit findById(Integer id);
}
