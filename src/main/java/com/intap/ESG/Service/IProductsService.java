package com.intap.ESG.Service;

import java.util.List;

import org.springframework.data.domain.Page;

import com.intap.ESG.Models.CategoryProducts;
import com.intap.ESG.Models.Products;


public interface IProductsService {
	
	public List<Products> findAll();
	
	public Products findById(Long id);
	
	public Products save(Products products);
	
	public void delete(Long id);
	
	List<Products> findByategoryProducts (CategoryProducts categoryProducts);

	Page<Products> findAll(Integer page, Integer limit, String name, String reference, String type, Boolean inventory,
			Boolean enabled,Integer maxvalue, Integer minvalue, Long categoryProductsId);

	Iterable<Products> findAll(String name, String reference, String type, Boolean inventory, Boolean enabled, Integer maxvalue, Integer minvalue,
			Long categoryProductsId);
	
	Products finByCode(Long code);

}
