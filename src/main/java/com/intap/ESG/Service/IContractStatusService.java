package com.intap.ESG.Service;

import java.util.List;

import com.intap.ESG.Models.ContractStatus;
import com.intap.ESG.Models.Contracts;
import com.intap.ESG.Models.DTO.ContractsStatusDto;


public interface IContractStatusService {
	
	public List<ContractStatus> findAll();

	public ContractStatus findById(Long id);

	public ContractStatus save(ContractStatus contractStatus);

	public void delete(Long id);
	
	public void deleteAll(List<ContractStatus> contracts);
	
	List<ContractStatus> lastStateInvoice(Long id);
	
	List<ContractStatus> findByContract(Contracts contract);

}
