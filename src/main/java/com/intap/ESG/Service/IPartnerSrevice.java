package com.intap.ESG.Service;

import java.util.List;

import com.intap.ESG.Models.Associated;
import com.intap.ESG.Models.Partner;

public interface IPartnerSrevice {
	
	public List<Partner> findAll();
	
	public List<Partner> findAllEnabled();
	
	public Partner findByIdAssociated(Associated associated);
	
	public Partner findById(Integer id);
	
	public Partner save(Partner provider);
	
	public void delete(Integer id);

}
