package com.intap.ESG.Service;

import java.util.List;

import com.intap.ESG.Models.Associated;
import com.intap.ESG.Models.InvoiceNews;
import com.intap.ESG.Models.Invoices;

public interface IInvoiceNewsService {

	public List<InvoiceNews> findAll();
	
	public InvoiceNews findById(Long id);
	
	
	public InvoiceNews save(InvoiceNews invoiceNews);
	
	public void delete(Long id);
	
	List<InvoiceNews> findByInvoices (Invoices factura);
}
