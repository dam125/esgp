package com.intap.ESG.Service;

import com.intap.ESG.Models.RecoverPassword;
import com.intap.ESG.Models.UserModel;


public interface IRecoverPasswordService {
	
	RecoverPassword findByCodigo (Integer codigo);
	
	public RecoverPassword save(RecoverPassword recoverPassword);

	Integer generateTokenUsuario(UserModel usuario) throws Exception;
}
