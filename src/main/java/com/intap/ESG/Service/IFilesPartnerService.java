package com.intap.ESG.Service;

import java.util.List;

import com.intap.ESG.Models.FilesPartner;
import com.intap.ESG.Models.Partner;

public interface IFilesPartnerService {
	
	public List<FilesPartner> findAll();
	
	public List<FilesPartner> findByIdPartner(Partner partner);
	
	public FilesPartner findById(Integer id);
	
	public FilesPartner save(FilesPartner filesPartner);
	
	public void delete(Integer id);

}
