package com.intap.ESG.Service;

import java.util.List;

import org.springframework.data.domain.Page;

import com.intap.ESG.Models.Associated;
import com.intap.ESG.Models.Client;
import com.intap.ESG.Models.DTO.TypeAssociatedFilter;

public interface IAssociatedService {
	
	public List<Associated> findAll();
	
	List<Associated> findWithoutActualUser(Integer id);
	
	public Associated findById(Integer id);
	
	public Associated save(Associated associated);
	
	public void delete(Integer id);
	
	Associated  findByIdentificationNumber (String identification);
	
	Associated  findByIdentificationNumberUpdate (String identification, Integer id);
	
	
	String findAdress(Integer id);

	Page<Associated> findAll(Integer page, Integer limit, Integer userId, String names, String identificationNumber,
			String CellPhoneNumber, String neighborhood, String city, String email,
			List<TypeAssociatedFilter> typeAssociated);

	Iterable<Associated> findAll(Integer userId, String names, String identificationNumber, String CellPhoneNumber,
			String neighborhood, String city, String email, List<TypeAssociatedFilter> typeAssociated);

	public List<Associated> getUsersFilter(String value);


}
