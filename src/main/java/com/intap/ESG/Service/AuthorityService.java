package com.intap.ESG.Service;

import com.intap.ESG.Models.DTO.AuthorityDto;

public interface AuthorityService {
	
	Iterable<AuthorityDto> findAll();

}
