package com.intap.ESG.Service;

import java.util.List;


import com.intap.ESG.Models.FilesProvider;
import com.intap.ESG.Models.Provider;

public interface IFilesProviderService {
	
	public List<FilesProvider> findAll();
	
	public List<FilesProvider> findByIdProvider(Provider provider);
	
	public FilesProvider findById(Integer id);
	
	public FilesProvider save(FilesProvider filesProvider);
	
	public void delete(Integer id);

}
