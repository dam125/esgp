package com.intap.ESG.Service;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;

import com.intap.ESG.Models.Contracts;
import com.intap.ESG.Models.Invoices;
import com.intap.ESG.Models.Products;

public interface IInvoicesService {
	
	public List<Invoices> findAll();

	public Invoices findById(Long id);

	public Invoices save(Invoices invoices);

	public void delete(Long id);
	
	public Invoices findByContractAndMonth(Contracts contract, String month);
	
	Invoices findByInvoiceNumber(String invoiceNumber);
	
	Integer countInvoicesGenerated(Contracts contrato);
	
	Integer countInvoicesOpenOrClose(Contracts contrato, String state);
	
	Integer sumInvoicesOpenOrClose(Contracts contrato, String state);
	
	List<Invoices> findAllInvoicesOpenOrClose(Contracts contrato, String state);

	List<Invoices> findAllInvoicesRecurrentes(LocalDate fechaGeneracion, String envoicesMonth);
	
	Invoices findByRefernce(String reference,String estado);
	
	List<Invoices> findInvoicesOpen(Contracts contrato,String state, Long idInvoice);

	Page<Invoices> findAll(Integer page, Integer limit, String startDate, String endDate, String consecutive, String names,
			String envoicesState, Integer minTotal, Integer maxTotal);
	
	Iterable<Invoices> findAll(String startDate, String endDate, String consecutive, String names,
			String envoicesState, Integer minTotal, Integer maxTotal);
	
	List<Invoices> findByfechaAndRecurrence(String fecha);
	
}
