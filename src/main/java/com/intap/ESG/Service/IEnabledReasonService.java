package com.intap.ESG.Service;

import java.util.List;

import com.intap.ESG.Models.AssociatesType;
import com.intap.ESG.Models.EnabledReason;


public interface IEnabledReasonService {

	public List<EnabledReason> findAll();

	public EnabledReason findById(Long id);

	public EnabledReason save(EnabledReason enabledReason);

	public void delete(Long id);
	
	List<EnabledReason> findByAssociatesType (AssociatesType asociateType);
}
