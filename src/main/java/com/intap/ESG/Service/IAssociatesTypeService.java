package com.intap.ESG.Service;

import com.intap.ESG.Models.AssociatesType;

public interface IAssociatesTypeService {
	
	public AssociatesType findById(Long id);
	
	AssociatesType  findByName (String name);

}
