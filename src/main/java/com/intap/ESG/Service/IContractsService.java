package com.intap.ESG.Service;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.domain.Page;

import com.intap.ESG.Models.Client;
import com.intap.ESG.Models.Contracts;



public interface IContractsService {
	
	public List<Contracts> findAll();
	
	public Contracts findById(Long id);
	
	public Contracts save(Contracts contracts);
	
	public void delete(Long id);
	
	int countConsecutive(String consecutive);
	
	int countConsecutiveUpdate(String consecutive, Long id);
	
	List<Contracts> findByAssociates(Client associates);
	
	Contracts findByIp(String ipClient);
	
	Contracts findByMac(String ipClient);
	
	Contracts findBySerial(String Serial);
	
	Contracts findByIpUpdate(String ipClient, Long id);
	
	Contracts findBymacUpdate(String macClient, Long id);

	Contracts findByserialUpdate(String serialClient, Long id);
	
	Contracts findByAssociate(Client associate);
	
	List<Contracts> findContracsEnabled ();
	
	List<Contracts> findContracsRecurrente (int cuttingDay, String periodicity);
	
	Page<Contracts> findAll(Integer page, Integer limit, String name, String consecutive, String ip, String cc, Integer fopen, Integer fclose, String mac, LocalDate fechaAfiliacion, String barrio, String telefono, String direccio, Number valor, Boolean activo, String estado, String subsidiado, LocalDate fechaFac, String serialEquipo,Integer saldomin,Integer saldomax);
	
	Iterable<Contracts> findAll(String name, String consecutive, String ip, String cc, Integer fopen, Integer fclose, String mac, LocalDate fechaAfiliacion, String barrio, String telefono, String direccio, Number valor, Boolean activo, String estado, String subsidiado, LocalDate fechaFac, String serialEquipo,Integer saldomin,Integer saldomax);
}
