package com.intap.ESG.Service;

import java.util.List;

import com.intap.ESG.Models.Configurations;



public interface IConfigurationsService {

	public List<Configurations> findAll();

	public Configurations findById(Long id);
	
	public Configurations findOne();

	public Configurations save(Configurations configurations);

	public void delete(Long id);
}
