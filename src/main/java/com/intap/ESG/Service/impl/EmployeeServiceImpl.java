package com.intap.ESG.Service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.intap.ESG.Dao.IEmployeeDao;
import com.intap.ESG.Models.Associated;
import com.intap.ESG.Models.Employee;
import com.intap.ESG.Models.UserModel;
import com.intap.ESG.Service.IEmployeeService;

@Service
public class EmployeeServiceImpl implements IEmployeeService{
	@Autowired
	private IEmployeeDao employeeDao;

	@Override
	public List<Employee> findAll() {
		return employeeDao.findAll();
	}

	@Override
	public List<Employee> findAllEnabled() {
		return employeeDao.findAllEnabled();
	}

	@Override
	public Employee findById(Integer id) {
		return employeeDao.findById(id).orElse(null);
	}

	@Override
	public Employee save(Employee employee) {
		return employeeDao.save(employee);
	}

	@Override
	public void delete(Integer id) {
		employeeDao.deleteById(id);
		
	}

	@Override
	public Employee findByUserId(UserModel user) {
		return employeeDao.findByUserId(user);
	}

	@Override
	public Employee findByIdAssociated(Associated associated) {
		return employeeDao.findByIdAssociated(associated);
	}

}
