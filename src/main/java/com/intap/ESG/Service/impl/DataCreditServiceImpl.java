package com.intap.ESG.Service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.intap.ESG.Dao.IDataCreditDao;
import com.intap.ESG.Models.DataCredit;
import com.intap.ESG.Service.IDataCreditService;

@Service
public class DataCreditServiceImpl implements IDataCreditService {
	@Autowired
	private IDataCreditDao datacreditDao;
	
	@Override
	public DataCredit save(DataCredit dc) {
		return datacreditDao.save(dc);
	}
	
	@Override
	public DataCredit findById(Integer id) {
		return datacreditDao.findById(id).orElse(null);
	}
}
