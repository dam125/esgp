package com.intap.ESG.Service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.intap.ESG.Dao.IContractDetailsDao;
import com.intap.ESG.Models.ContractDetails;
import com.intap.ESG.Models.Contracts;
import com.intap.ESG.Models.Products;
import com.intap.ESG.Service.IContractDetailsService;

@Service
public class ContractDetailsServiceImpl implements IContractDetailsService{
	
	@Autowired
	private IContractDetailsDao contractDetailsDao;

	@Override
	public List<ContractDetails> findAll() {
		return (List<ContractDetails>) contractDetailsDao.findAll();
	}

	@Override
	public ContractDetails findById(Long id) {
		return contractDetailsDao.findById(id).orElse(null);
	}

	@Override
	public ContractDetails save(ContractDetails contractDetails) {
		return contractDetailsDao.save(contractDetails);
	}

	@Override
	public void delete(Long id) {
		contractDetailsDao.deleteById(id);
		
	}

	@Override
	public List<ContractDetails> findByContracts(Contracts id) {
		
		return contractDetailsDao.findByContracts(id);
		
		
	}

	@Override
	public void deleteAll(List<ContractDetails> contractDetails) {
		contractDetailsDao.deleteAll(contractDetails);
		
	}

	@Override
	public double contractValue(Contracts id) {
		return contractDetailsDao.contractValue(id);
	}

	@Override
	public List<ContractDetails> findByProduct(Products products) {
		return contractDetailsDao.findByProduct(products);
	}
	
	@Override
	public void UpdatePriceByProduct(Products products, Double price) {
		contractDetailsDao.updatePriceByProduct(products, price);
	}

}
