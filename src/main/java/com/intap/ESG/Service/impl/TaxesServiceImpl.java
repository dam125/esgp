package com.intap.ESG.Service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.intap.ESG.Dao.ITaxesDao;
import com.intap.ESG.Models.Taxes;
import com.intap.ESG.Service.ITaxesService;

@Service
public class TaxesServiceImpl implements ITaxesService{
	
	@Autowired
	private ITaxesDao taxesDao;
	
	

	@Override
	public List<Taxes> findAll() {
		return (List<Taxes>) taxesDao.findAll();
	}



	@Override
	public Taxes findById(Long id) {
		// TODO Auto-generated method stub
		return taxesDao.findById(id).orElse(null);
	}



	@Override
	public Taxes save(Taxes taxes) {
		return taxesDao.save(taxes);
	}



	@Override
	public void delete(Long id) {
		taxesDao.deleteById(id);
		
	}

}
