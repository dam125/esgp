package com.intap.ESG.Service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.intap.ESG.Dao.IFilesPartnerDao;
import com.intap.ESG.Models.FilesPartner;
import com.intap.ESG.Models.Partner;
import com.intap.ESG.Service.IFilesPartnerService;



@Service
public class FilesPartnerServiceImpl implements IFilesPartnerService{
	@Autowired
	private IFilesPartnerDao filesPartnerDao;

	@Override
	public List<FilesPartner> findAll() {
		return (List<FilesPartner>) filesPartnerDao.findAll();
	}

	@Override
	public FilesPartner findById(Integer id) {
		return filesPartnerDao.findById(id).orElse(null);
	}

	@Override
	public FilesPartner save(FilesPartner filesPartner) {
		return filesPartnerDao.save(filesPartner);
	}

	@Override
	public void delete(Integer id) {
		filesPartnerDao.deleteById(id);
		
	}

	@Override
	public List<FilesPartner> findByIdPartner(Partner partner) {
		return filesPartnerDao.findByIdPartner(partner);
	}




}
