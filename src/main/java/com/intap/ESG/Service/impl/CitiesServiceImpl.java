package com.intap.ESG.Service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.intap.ESG.Dao.ICitiesDao;
import com.intap.ESG.Models.Cities;
import com.intap.ESG.Service.ICitiesService;

@Service
public class CitiesServiceImpl implements ICitiesService{
	@Autowired
	private ICitiesDao citiesDao;

	@Override
	public Iterable<Cities> findAll() {
		// TODO Auto-generated method stub
		return citiesDao.findAll();
	}

	@Override
	public Cities findById(Long id) {
		// TODO Auto-generated method stub
		return citiesDao.findById(id).orElse(null);
	}

	@Override
	public Cities save(Cities cities) {
		// TODO Auto-generated method stub
		return citiesDao.save(cities);
	}

	@Override
	public void delete(Long id) {
		citiesDao.deleteById(id);
		
	}

}
