package com.intap.ESG.Service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.intap.ESG.Dao.IAssociatedDao;
import com.intap.ESG.Dao.IClientDao;
import com.intap.ESG.Models.Associated;
import com.intap.ESG.Models.Client;
import com.intap.ESG.Service.IClientService;

@Service
public class ClientServiceImpl implements IClientService{
	
	@Autowired
	private IClientDao clientDao;
	
	@Autowired
	private IAssociatedDao associateDao;

	@Override
	public List<Client> findAll() {
		return clientDao.findAll();
	}

	@Override
	public List<Client> findAllEnabled() {
		return clientDao.findAllEnabled();
	}

	@Override
	public Client findByIdAssociated(Associated associated) {
		return clientDao.findByIdAssociated(associated);
	}

	@Override
	public Client findById(Integer id) {
		return clientDao.findById(id).orElse(null);
	}

	@Override
	public Client save(Client client) {
		return clientDao.save(client);
	}

	@Override
	public void delete(Integer id) {
		clientDao.deleteById(id);
		
	}

	@Override
	public List<Associated> findAllInfo() {
		// TODO Auto-generated method stub
		return associateDao.findAll();
	}

}
