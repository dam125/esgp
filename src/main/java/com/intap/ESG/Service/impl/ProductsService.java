package com.intap.ESG.Service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.intap.ESG.Dao.IProductsDao;
import com.intap.ESG.Models.CategoryProducts;
import com.intap.ESG.Models.Products;
import com.intap.ESG.Service.IProductsService;
import com.intap.ESG.util.specification.ProductsSpecification;

@Service
public class ProductsService implements IProductsService {

	@Autowired
	private IProductsDao productsDao;

	@Override
	public List<Products> findAll() {
		return (List<Products>) productsDao.findAll();
	}

	@Override
	public Products findById(Long id) {
		return productsDao.findById(id).orElse(null);
	}

	@Override
	public Products save(Products products) {
		return productsDao.save(products);
	}

	@Override
	public void delete(Long id) {
		productsDao.deleteById(id);

	}

	@Override
	public List<Products> findByategoryProducts(CategoryProducts categoryProducts) {
		return productsDao.findByategoryProducts(categoryProducts);
	}

	@Override
	@Transactional(readOnly = true)
	public Page<Products> findAll(Integer page, Integer limit, String name, String reference, String type,
			Boolean	 inventory, Boolean enabled, Integer maxvalue, Integer minvalue, Long categoryProductsId) {

		Pageable pageable = PageRequest.of(page, limit, Sort.by(Direction.ASC, "createAt"));

		return this.productsDao.findAll(
				new ProductsSpecification(name, reference, type, inventory, enabled, minvalue, maxvalue, categoryProductsId), pageable);
	}
	
	@Override
	@Transactional(readOnly = true)
	public Iterable<Products> findAll(String name, String reference, String type,
			Boolean	 inventory, Boolean enabled, Integer maxvalue, Integer minvalue, Long categoryProductsId) {

		
		return (Iterable<Products>) this.productsDao.findAll(new ProductsSpecification(name, reference, type, inventory, enabled, minvalue, maxvalue, categoryProductsId),
				Sort.by(Direction.ASC, "createAt"));
	}

	@Override
	public Products finByCode(Long code) {
		return productsDao.findByode(code);
	}
	
} 
