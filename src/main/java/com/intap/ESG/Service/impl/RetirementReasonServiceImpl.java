package com.intap.ESG.Service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.intap.ESG.Dao.RetirementReasonDao;
import com.intap.ESG.Models.RetirementReason;
import com.intap.ESG.Service.RetirementReasonService;

@Service
public class RetirementReasonServiceImpl implements RetirementReasonService{
	
	@Autowired
	private RetirementReasonDao retirementReasonDao;
	
	@Override
	public List<RetirementReason> findAll() {
		// TODO Auto-generated method stub
		return (List<RetirementReason>) retirementReasonDao.findAll();
	}

	@Override
	public RetirementReason findById(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RetirementReason save(RetirementReason retirementReason) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		
	}

}
