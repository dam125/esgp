package com.intap.ESG.Service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.intap.ESG.Dao.IUserDao;
import com.intap.ESG.Models.Role;
import com.intap.ESG.Models.UserModel;
import com.intap.ESG.Service.IUserService;

@Service
public class UserServiceImpl implements UserDetailsService, IUserService {
	

	@Autowired
	private IUserDao userdao;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Optional<UserModel> optional = this.userdao.findByUsername(username);

		if (optional.isPresent()) {
			

			return optional.get();
			
		}
		
		throw new UsernameNotFoundException("el usuario " + username + " no se encuemtra registrodo");
	}

	@Override
	public List<UserModel> GetAllUser() {		
		return (List<UserModel>) this.userdao.findAll();
	}

	@Override
	public Optional<UserModel> findUserByUser(String User) {
		return  userdao.findByUsername(User);
	}

	@Override
	public UserModel SaveUser(UserModel user) {
		return userdao.save(user);
	}

	@Override
	public Optional<UserModel> findById(Long id) {
		return userdao.findById(id);
	}

	@Override
	public void delete(UserModel userModel) {
		userdao.delete(userModel);
		
	}

	@Override
	public List<UserModel> findByRol(Role role) {
		return userdao.findByRol(role);
	}

}
