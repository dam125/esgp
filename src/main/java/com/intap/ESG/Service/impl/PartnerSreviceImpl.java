package com.intap.ESG.Service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.intap.ESG.Dao.IPartnerDao;
import com.intap.ESG.Models.Associated;
import com.intap.ESG.Models.Partner;
import com.intap.ESG.Service.IPartnerSrevice;

@Service
public class PartnerSreviceImpl implements IPartnerSrevice{
	
	@Autowired
	private IPartnerDao partnerDao;

	@Override
	public List<Partner> findAll() {
		return partnerDao.findAll();
	}

	@Override
	public List<Partner> findAllEnabled() {
		return partnerDao.findAllEnabled();
	}

	@Override
	public Partner findByIdAssociated(Associated associated) {
		return partnerDao.findByIdAssociated(associated);
	}

	@Override
	public Partner findById(Integer id) {
		return partnerDao.findById(id).orElse(null);
	}

	@Override
	public Partner save(Partner provider) {
		return partnerDao.save(provider);
	}

	@Override
	public void delete(Integer id) {
		partnerDao.deleteById(id);
		
	}

}
