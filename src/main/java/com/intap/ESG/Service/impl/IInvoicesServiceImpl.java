package com.intap.ESG.Service.impl;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.intap.ESG.Dao.IInvoicesDao;
import com.intap.ESG.Models.Contracts;
import com.intap.ESG.Models.Invoices;
import com.intap.ESG.Models.Products;
import com.intap.ESG.Service.IInvoicesService;
import com.intap.ESG.util.specification.InvoicesSpecification;
import com.intap.ESG.util.specification.ProductsSpecification;

@Service
public class IInvoicesServiceImpl implements IInvoicesService {

	@Autowired
	private IInvoicesDao invoicesDao;

	@Override
	public List<Invoices> findAll() {
		return (List<Invoices>) invoicesDao.findAll();
	}

	@Override
	public Invoices findById(Long id) {
		return invoicesDao.findById(id).orElse(null);
	}

	@Override
	public Invoices save(Invoices invoices) {
		return invoicesDao.save(invoices);
	}

	@Override
	public void delete(Long id) {
		invoicesDao.deleteById(id);
	}

	@Override
	public Invoices findByContractAndMonth(Contracts contract, String month) {
		return invoicesDao.findByContractAndMonth(contract, month);
	}

	@Override
	public Invoices findByInvoiceNumber(String invoiceNumber) {
		return invoicesDao.findByInvoiceNumber(invoiceNumber);
	}

	@Override
	public Integer countInvoicesGenerated(Contracts contrato) {
		return invoicesDao.countInvoicesGenerated(contrato);
	}

	@Override
	public Integer countInvoicesOpenOrClose(Contracts contrato, String state) {
		// TODO Auto-generated method stub
		return invoicesDao.countInvoicesOpenOrClose(contrato, state);
	}

	@Override
	public List<Invoices> findAllInvoicesOpenOrClose(Contracts contrato, String state) {
		return invoicesDao.findAllInvoicesOpenOrClose(contrato, state);
	}

	@Override
	public List<Invoices> findAllInvoicesRecurrentes(LocalDate fechaGeneracion, String envoicesMonth) {
		return invoicesDao.findAllInvoicesRecurrentes(fechaGeneracion, envoicesMonth);
	}

	@Override
	public Invoices findByRefernce(String reference, String estado) {
		return invoicesDao.findByRefernce(reference, estado);
	}

	@Override
	public List<Invoices> findInvoicesOpen(Contracts contrato, String state, Long idInvoice) {
		return invoicesDao.findInvoicesOpen(contrato, state, idInvoice);
	}

	@Override
	@Transactional(readOnly = true)
	public Page<Invoices> findAll(Integer page, Integer limit, String startDate, String endDate, String consecutive,
			String names, String envoicesState, Integer minTotal, Integer maxTotal) {
		
		Pageable pageable = PageRequest.of(page, limit, Sort.by(Direction.DESC, "id"));
		
		return invoicesDao.findAll(
				new InvoicesSpecification(startDate, endDate, consecutive, names, envoicesState,minTotal, maxTotal), pageable);
		
	}

	@Override
	public Iterable<Invoices> findAll(String startDate, String endDate, String consecutive, String names,
			String envoicesState, Integer minTotal, Integer maxTotal) {
		
		return (Iterable<Invoices>) this.invoicesDao.findAll(new InvoicesSpecification(startDate, endDate, consecutive, names, envoicesState,minTotal, maxTotal),
				Sort.by(Direction.DESC, "id"));
	}

	@Override
	public List<Invoices> findByfechaAndRecurrence(String fecha) {
		return invoicesDao.findByfechaAndRecurrence(fecha);
	}

	@Override
	public Integer sumInvoicesOpenOrClose(Contracts contrato, String state) {
	
		return invoicesDao.sumInvoicesOpenOrClose(contrato, state);
	}

}
