package com.intap.ESG.Service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.intap.ESG.Dao.IFilesProviderDao;
import com.intap.ESG.Models.FilesProvider;
import com.intap.ESG.Models.Provider;
import com.intap.ESG.Service.IFilesProviderService;

@Service
public class FilesProviderServiceImpl implements IFilesProviderService{
	@Autowired
	private IFilesProviderDao filesProviderDao;

	@Override
	public List<FilesProvider> findAll() {
		return (List<FilesProvider>) filesProviderDao.findAll();
	}

	@Override
	public FilesProvider findById(Integer id) {
		return filesProviderDao.findById(id).orElse(null);
	}

	@Override
	public FilesProvider save(FilesProvider filesProvider) {
		return filesProviderDao.save(filesProvider);
	}

	@Override
	public void delete(Integer id) {
		filesProviderDao.deleteById(id);
		
	}

	@Override
	public List<FilesProvider> findByIdProvider(Provider provider) {
		return filesProviderDao.findByIdProvider(provider);
	}
	


}
