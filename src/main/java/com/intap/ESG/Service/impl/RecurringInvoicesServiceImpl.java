package com.intap.ESG.Service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.intap.ESG.Dao.IRecurringInvoicesDao;
import com.intap.ESG.Models.Employee;
import com.intap.ESG.Models.Invoices;
import com.intap.ESG.Models.RecurringInvoices;
import com.intap.ESG.Service.IRecurringInvoicesService;
import com.intap.ESG.util.specification.InvoicesSpecification;
import com.intap.ESG.util.specification.RecurinInvoicesSpecification;

@Service
public class RecurringInvoicesServiceImpl implements IRecurringInvoicesService{
	
	@Autowired
	private IRecurringInvoicesDao recurringInvoicesDao;

	@Override
	public List<RecurringInvoices> findAll() {
		return (List<RecurringInvoices>) recurringInvoicesDao.findAll();
	}

	@Override
	public RecurringInvoices findById(Long id) {
		return recurringInvoicesDao.findById(id).orElse(null);
	}

	@Override
	public RecurringInvoices save(RecurringInvoices recurringInvoices) {
		return recurringInvoicesDao.save(recurringInvoices);
	}

	@Override
	public void delete(Long id) {
		recurringInvoicesDao.deleteById(id);
	}

	@Override
	public RecurringInvoices findByInvoices(Employee employee, String year, String month) {
		return recurringInvoicesDao.findByInvoices(employee, year, month);
	}

	@Override
	public Page<RecurringInvoices> findAll(Integer page, Integer limit, String year, String month,
			Integer invoicesGenerated, Integer invoicesGeneratedMax) {
		Pageable pageable = PageRequest.of(page, limit, Sort.by(Direction.ASC, "createAt"));
		
		return recurringInvoicesDao.findAll(
				new RecurinInvoicesSpecification(year, month, invoicesGenerated, invoicesGeneratedMax), pageable);
		
	}

	@Override
	public Iterable<RecurringInvoices> findAll(String year, String month, Integer invoicesGenerated,
			Integer invoicesGeneratedMax) {
		return (Iterable<RecurringInvoices>) this.recurringInvoicesDao.findAll(new RecurinInvoicesSpecification(year, month, invoicesGenerated, invoicesGeneratedMax),Sort.by(Direction.ASC, "createAt"));
	}

}
