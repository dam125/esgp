package com.intap.ESG.Service.impl;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.intap.ESG.Dao.IRecoverPasswordDao;
import com.intap.ESG.Dao.IRecurringInvoicesDao;
import com.intap.ESG.Models.Employee;
import com.intap.ESG.Models.RecoverPassword;
import com.intap.ESG.Models.RecurringInvoices;
import com.intap.ESG.Models.UserModel;
import com.intap.ESG.Service.IRecoverPasswordService;
import com.intap.ESG.Service.IRecurringInvoicesService;
import com.intap.ESG.util.RandomStringGenerator;


@Service
public class RecoverPasswordServiceImpl implements IRecoverPasswordService{
	
	@Autowired
	IRecoverPasswordDao recoverPasswordDao;

	@Override
	public RecoverPassword findByCodigo(Integer codigo) {
		return recoverPasswordDao.findByCodigo(codigo);
	}

	@Override
	public RecoverPassword save(RecoverPassword recoverPassword) {
		return recoverPasswordDao.save(recoverPassword);
	}

	@Override
	public Integer generateTokenUsuario(UserModel usuario) throws Exception {
		
		Integer random = (int)(10000000 * Math.random());
		String randomtring = new RandomStringGenerator(45).getRandomStringAppendTime();
		
		RecoverPassword recuperarPass = new RecoverPassword();

		recuperarPass.setIdrecu(randomtring);
		recuperarPass.setIdusuario(usuario);
		recuperarPass.setCodigo(random);
		Date fechaActual=Calendar.getInstance().getTime();
		recuperarPass.setFechacrea(fechaActual);
		
		recoverPasswordDao.save(recuperarPass);
		
		return recuperarPass.getCodigo();
	}

	

}
