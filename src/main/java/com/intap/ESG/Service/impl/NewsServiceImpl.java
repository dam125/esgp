package com.intap.ESG.Service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.intap.ESG.Dao.INewsDao;
import com.intap.ESG.Models.News;
import com.intap.ESG.Models.Products;
import com.intap.ESG.Service.INewsService;
import com.intap.ESG.util.specification.NewsSpecification;
import com.intap.ESG.util.specification.ProductsSpecification;

@Service
public class NewsServiceImpl implements INewsService{
	
	@Autowired
	private INewsDao newsDao;

	@Override
	public List<News> findAll() {
		return (List<News>) newsDao.findAll();
	}

	@Override
	public News findById(Long id) {
		return newsDao.findById(id).orElse(null);
	}

	@Override
	public News save(News news) {
		return newsDao.save(news);
	}

	@Override
	public void delete(Long id) {
		newsDao.deleteById(id);
		
	}

	@Override
	public Page<News> findAll(Integer page, Integer limit, String name, String tipo, Double valor,Double valorMax, Boolean porcentaje,
			Double valorPorcentaje,Boolean estado) {
				
		Pageable pageable = PageRequest.of(page, limit, Sort.by(Direction.ASC, "name"));
		return this.newsDao.findAll(
				new NewsSpecification(name, tipo, valor,valorMax, porcentaje,valorPorcentaje,estado), pageable);
	}

	@Override
	public Iterable<News> findAll(String name, String tipo, Double valor,Double valorMax, Boolean porcentaje, Double valorPorcentaje,Boolean estado) {
		return (Iterable<News>) this.newsDao.findAll(new NewsSpecification(name, tipo, valor,valorMax, porcentaje,valorPorcentaje,estado),
				Sort.by(Direction.ASC, "name"));
	}

}
