package com.intap.ESG.Service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.intap.ESG.Dao.IAssociatesTypeDao;
import com.intap.ESG.Models.AssociatesType;
import com.intap.ESG.Service.IAssociatesTypeService;

@Service
public class AssociatesTypeServiceImpl implements IAssociatesTypeService{
	@Autowired
	private IAssociatesTypeDao associatesTypeDao;

	@Override
	public AssociatesType findById(Long id) {
		return associatesTypeDao.findById(id).orElse(null);
	}

	@Override
	public AssociatesType findByName(String name) {
		return associatesTypeDao.findByName(name);
	}

}
