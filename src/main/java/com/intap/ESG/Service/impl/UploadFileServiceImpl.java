package com.intap.ESG.Service.impl;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.intap.ESG.util.Constant;

@Service
public class UploadFileServiceImpl implements IUploadFileService{
	
	private final Logger log = LoggerFactory.getLogger(UploadFileServiceImpl.class);
	
	

	@Override
	public Resource cargar(String nombreFoto,  String carpeta) throws MalformedURLException {
		
		Path rutaArchivo = getPath(nombreFoto,carpeta);
		log.info(rutaArchivo.toString());
		
		Resource recurso = new UrlResource(rutaArchivo.toUri());
		
		if(!recurso.exists() && !recurso.isReadable()) {		
			log.error("Error no se pudo cargar la imagen: " + nombreFoto);
			
		}
		return recurso;
	}

	@Override
	public String copiar(MultipartFile archivo, String nombreArchivo, String carpeta) throws IOException {
		String reportPath = Constant.ruta;

	    File dir = new File(reportPath+carpeta);
	    if (!dir.exists()) {
			dir.mkdirs();
	    }
	    
	    String nombreArchi = archivo.getOriginalFilename().replace(".", "/");
		String[] array = nombreArchi.split("/");
		int cat = array.length;
		String ext = array[cat-1];


	    
	    String nombreArchivo2 =  nombreArchivo+"."+ext;
		
		Path rutaArchivo = getPath(nombreArchivo2,carpeta);

		eliminar(nombreArchivo2,carpeta);
		
		Files.copy(archivo.getInputStream(), rutaArchivo);


		return (carpeta+File.separator+nombreArchivo2);
	}

	@Override
	public boolean eliminar(String nombreFoto, String carpeta) {
		
		if(nombreFoto !=null && nombreFoto.length() >0) {
			Path rutaFotoAnterior = Paths.get(Constant.ruta+carpeta).resolve(nombreFoto).toAbsolutePath();
			File archivoFotoAnterior = rutaFotoAnterior.toFile();
			if(archivoFotoAnterior.exists() && archivoFotoAnterior.canRead()) {
				archivoFotoAnterior.delete();
				return true;
			}
		}
		
		return false;
	}

	@Override
	public Path getPath(String nombreFoto, String carpeta) {
		 String reportPath = Constant.ruta;
		return Paths.get(reportPath+carpeta).resolve(nombreFoto).toAbsolutePath();
	}


}
