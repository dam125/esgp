package com.intap.ESG.Service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.intap.ESG.Dao.IProviderDao;
import com.intap.ESG.Models.Associated;
import com.intap.ESG.Models.Provider;
import com.intap.ESG.Service.IProviderService;

@Service 
public class ProviderServiceImpl implements IProviderService{
	
	@Autowired
	private IProviderDao providerDao;
	

	@Override
	public List<Provider> findAll() {
		return providerDao.findAll();
	}

	@Override
	public List<Provider> findAllEnabled() {
		return providerDao.findAllEnabled();
	}
	
	@Override
	public Provider findByIdAssociated(Associated associated) {
		return providerDao.findByIdAssociated(associated);
	}

	@Override
	public Provider findById(Integer id) {
		return providerDao.findById(id).orElse(null);
	}

	@Override
	public Provider save(Provider provider) {
		return providerDao.save(provider);
	}

	@Override
	public void delete(Integer id) {
		providerDao.deleteById(id);
		
	}


}
