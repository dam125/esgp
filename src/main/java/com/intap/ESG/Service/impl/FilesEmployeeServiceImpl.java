package com.intap.ESG.Service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.intap.ESG.Dao.IFilesEmployeeDao;
import com.intap.ESG.Models.Employee;
import com.intap.ESG.Models.FilesEmployee;
import com.intap.ESG.Service.IFilesEmployeeService;

@Service
public class FilesEmployeeServiceImpl implements IFilesEmployeeService{
	@Autowired
	private IFilesEmployeeDao filesEmployeeDao;
	
	@Override
	public List<FilesEmployee> findAll() {
		return (List<FilesEmployee>) filesEmployeeDao.findAll();
	}

	@Override
	public FilesEmployee findById(Integer id) {
		return filesEmployeeDao.findById(id).orElse(null);
	}

	@Override
	public FilesEmployee save(FilesEmployee filesEmployee) {
		return filesEmployeeDao.save(filesEmployee);
	}

	@Override
	public void delete(Integer id) {
		filesEmployeeDao.deleteById(id);
		
	}

	@Override
	public List<FilesEmployee> findByIdEmployee(Employee employee) {
		return filesEmployeeDao.findByIdEmployee(employee);
	}



}
