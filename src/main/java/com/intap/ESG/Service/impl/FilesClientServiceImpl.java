package com.intap.ESG.Service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.intap.ESG.Dao.IFilesClientDao;
import com.intap.ESG.Models.Client;
import com.intap.ESG.Models.FilesClient;
import com.intap.ESG.Service.IFilesClientService;


@Service
public class FilesClientServiceImpl implements IFilesClientService{
	@Autowired
	private IFilesClientDao filesClientDao;

	@Override
	public List<FilesClient> findAll() {
		return (List<FilesClient>) filesClientDao.findAll();
	}

	@Override
	public FilesClient findById(Integer id) {
		return filesClientDao.findById(id).orElse(null);
	}

	@Override
	public FilesClient save(FilesClient filesClient) {
		return filesClientDao.save(filesClient);
	}

	@Override
	public void delete(Integer id) {
		filesClientDao.deleteById(id);
		
	}

	@Override
	public List<FilesClient> findByIdClient(Client client) {
		return filesClientDao.findByIdClient(client);
	}


	


}
