package com.intap.ESG.Service.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.intap.ESG.Dao.IContractsDao;
import com.intap.ESG.Dao.IViewsDao;
import com.intap.ESG.Models.Client;
import com.intap.ESG.Models.Contracts;
import com.intap.ESG.Models.Invoices;
import com.intap.ESG.Models.ViewModel;
import com.intap.ESG.Service.IContractsService;
import com.intap.ESG.util.specification.ContractsSpecification;

@Service
public class ContractsServiceImpl implements IContractsService{
	
	@Autowired
	private IContractsDao contractsDao;
	@Autowired
	private IViewsDao viewDao;
 
	@Autowired
    JdbcTemplate jdbcTemplate;
	

	@Override
	public List<Contracts> findAll() {
		return (List<Contracts>) contractsDao.findAll();
	}

	@Override
	public Contracts findById(Long id) {
		return contractsDao.findById(id).orElse(null);
	}

	@Override
	public Contracts save(Contracts contracts) {
		return contractsDao.save(contracts);
	}

	@Override
	public void delete(Long id) {
		contractsDao.deleteById(id);
		
	}

	@Override
	public int countConsecutive(String consecutive) {
		return contractsDao.countConsecutive(consecutive);
	}

	@Override
	public int countConsecutiveUpdate(String consecutive, Long id) {
		return contractsDao.countConsecutiveUpdate(consecutive, id);
	}

	@Override
	public List<Contracts> findByAssociates(Client associates) {
		return contractsDao.findByAssociates(associates);
	}

	@Override
	public Contracts findByIp(String ipClient) {
		return contractsDao.findByIp(ipClient);
	}

	@Override
	public Contracts findByIpUpdate(String ipClient, Long id) {
		return contractsDao.findByIpUpdate(ipClient, id);
	}

	@Override
	public Contracts findByAssociate(Client associates) {
		// TODO Auto-generated method stub
		return contractsDao.findByIdClient(associates);
	}

	@Override
	public List<Contracts> findContracsEnabled() {
		
		
		List<Contracts> rows = jdbcTemplate.query("Select * from contracts_view c WHERE c.enabled = true and c.no_bill= true"
				,new RowMapper<Contracts>(){  
				    @Override  
				    public Contracts mapRow(ResultSet rs, int rownumber) throws SQLException {  
				    	Contracts e=new Contracts();  
				    	Client client = new Client();
				    	
				        e.setId(rs.getLong("id"));  
				        e.setAddress(rs.getString("address"));  
				        e.setBillingStarDate(rs.getDate("billing_star_date").toLocalDate());  
				        e.setCity(rs.getString("city"));  
				        e.setConsecutive(rs.getString("consecutive"));  
				        e.setCreateAt(rs.getDate("create_at"));  
				        e.setCuttingDay(rs.getInt("cutting_day"));  
//				        e.setDateDeliveredProduct(rs.getDate("date_delivered_product").toLocalDate());  
				        e.setDaysPastDue(rs.getInt("days_past_due"));  
				        e.setDeliveredProduct(rs.getBoolean("delivered_product"));  
				        e.setEnabled(rs.getBoolean("enabled"));
//				        e.setEndingDate(rs.getDate("ending_date").toLocalDate());  
				        e.setExpirationDay(rs.getInt("expiration_day"));  
				        e.setIpClient(rs.getString("ip_client"));  
				        e.setMacClient(rs.getString("mac_client"));  
				        e.setMonthsWithoutBillSoFar(rs.getInt("months_without_bill_so_far"));  
				        e.setNeighborhood(rs.getString("neighborhood"));  
				        e.setNoBill(rs.getBoolean("no_bill"));  
				        e.setPeriodicity(rs.getString("periodicity"));  
				        e.setRetirementObservation(rs.getString("retirement_observation")); 
				        e.setRetirementReason(rs.getString("retirement_reason"));  
				        e.setSerialEquipment(rs.getString("serial_equipment"));  
				        e.setStartDate(rs.getDate("start_date").toLocalDate());
				        e.setStartPeriod(rs.getInt("start_period")); 
				        e.setType(rs.getString("type")); 
				        e.setUpdatedAt(rs.getDate("updated_at")); 
				        e.setIdClient((client)); 
				        return e;  
				    }  
				    });    
		
		return contractsDao.findContracsEnabled();
	}

	@Override
	public List<Contracts> findContracsRecurrente(int cuttingDay, String periodicity) {
		// TODO Auto-generated method stub
//
	   String sql = "Select * from contracts_view c WHERE c.enabled = true and c.no_bill= true AND c.cutting_day = "
	   		+ cuttingDay + "AND c.periodicity = '"+  periodicity +"' ORDER BY c.neighborhood ASC";
		
		List<Contracts> rows = jdbcTemplate.query(sql
				,new RowMapper<Contracts>(){  
				    @Override  
				    public Contracts mapRow(ResultSet rs, int rownumber) throws SQLException {  
				    	Contracts e = new Contracts();  
				    	Client client = new Client();
				        e.setId(rs.getLong("id"));  
				        e.setAddress(rs.getString("address"));  
				        e.setBillingStarDate(rs.getDate("billing_star_date").toLocalDate());  
				        e.setCity(rs.getString("city"));  
				        e.setConsecutive(rs.getString("consecutive"));  
				        e.setCreateAt(rs.getDate("create_at"));  
				        e.setCuttingDay(rs.getInt("cutting_day"));  
//				        e.setDateDeliveredProduct(rs.getDate("date_delivered_product").toLocalDate());  
				        e.setDaysPastDue(rs.getInt("days_past_due"));  
				        e.setDeliveredProduct(rs.getBoolean("delivered_product"));  
				        e.setEnabled(rs.getBoolean("enabled"));
//				        e.setEndingDate(rs.getDate("ending_date").toLocalDate());  
				        e.setExpirationDay(rs.getInt("expiration_day"));  
				        e.setIpClient(rs.getString("ip_client"));  
				        e.setMacClient(rs.getString("mac_client"));  
				        e.setMonthsWithoutBillSoFar(rs.getInt("months_without_bill_so_far"));  
				        e.setNeighborhood(rs.getString("neighborhood"));  
				        e.setNoBill(rs.getBoolean("no_bill"));  
				        e.setPeriodicity(rs.getString("periodicity"));  
				        e.setRetirementObservation(rs.getString("retirement_observation")); 
				        e.setRetirementReason(rs.getString("retirement_reason"));  
				        e.setSerialEquipment(rs.getString("serial_equipment"));  
				        e.setStartDate(rs.getDate("start_date").toLocalDate());
				        e.setStartPeriod(rs.getInt("start_period")); 
				        e.setType(rs.getString("type")); 
				        e.setUpdatedAt(rs.getDate("updated_at")); 
				        e.setIdClient((client)); 
				        return e;  
				    }  
				    });  
		
		return contractsDao.findContracsRecurrente(cuttingDay, periodicity);
	}
	
	@Override
	@Transactional(readOnly = true)
	public Page<Contracts> findAll(Integer page, Integer limit, String name, String consecutive, String ip, String cc, Integer fopen, Integer fclose, String mac, LocalDate fechaAfiliacion, String barrio, String telefono, String direccio, Number valor, Boolean activo, String estado, String subsidiado, LocalDate fechaFac, String serialEquipo,Integer saldomin,Integer saldomax){
		Pageable pageable = PageRequest.of(page, limit, Sort.by(Direction.ASC, "createAt"));
		
		return contractsDao.findAll(new ContractsSpecification(name, consecutive, cc, ip, fopen, fclose, mac, fechaAfiliacion, barrio, telefono, direccio, valor, activo, estado, subsidiado, fechaFac, serialEquipo, saldomin,saldomax), pageable);
	}
	
	@Override
	public Iterable<Contracts> findAll(String name, String consecutive, String ip, String cc, Integer fopen, Integer fclose, String mac, LocalDate fechaAfiliacion, String barrio, String telefono, String direccio, Number valor, Boolean activo, String estado, String subsidiado, LocalDate fechaFac, String serialEquipo,Integer saldomin,Integer saldomax){
		
		return (Iterable<Contracts>) contractsDao.findAll(new ContractsSpecification(name, consecutive, cc, ip, fopen, fclose, mac, fechaAfiliacion, barrio, telefono, direccio, valor, activo, estado, subsidiado, fechaFac, serialEquipo, saldomin,saldomax), Sort.by(Direction.DESC, "id"));
	}

	@Override
	public Contracts findByMac(String mac) {
		// TODO Auto-generated method stub
		return contractsDao.findByMac(mac);
	}

	@Override
	public Contracts findBySerial(String Serial) {
		// TODO Auto-generated method stub
		return contractsDao.findBySerial(Serial);
	}

	@Override
	public Contracts findBymacUpdate(String macClient, Long id) {
		// TODO Auto-generated method stub
		return contractsDao.findBmacUpdate(macClient,id);
	}

	@Override
	public Contracts findByserialUpdate(String serialClient, Long id) {
		// TODO Auto-generated method stub
		return contractsDao.findBserialUpdate(serialClient,id);
	}

}
