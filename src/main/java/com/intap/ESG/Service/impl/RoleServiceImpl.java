package com.intap.ESG.Service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.intap.ESG.Dao.IAuthorityDao;
import com.intap.ESG.Dao.IRoleDao;
import com.intap.ESG.Models.Authority;
import com.intap.ESG.Models.Role;
import com.intap.ESG.Models.DTO.PaginationData;
import com.intap.ESG.Models.DTO.RoleDataDto;
import com.intap.ESG.Models.DTO.RoleDto;
import com.intap.ESG.Models.errors.RoleMessageError;
import com.intap.ESG.Service.RoleService;
import com.intap.ESG.util.SortUtil;
import com.intap.ESG.util.ValidationFields;

import javassist.NotFoundException;






@Service
public class RoleServiceImpl implements RoleService{
	
	@Autowired
	private IRoleDao roleDao;

	@Autowired
	IAuthorityDao authorityDao;

	@Override
	@Transactional(readOnly = true)
	public PaginationData findAll(Integer page, Integer limit, String sort, String search) {
		List<RoleDto> roleDtos = new ArrayList<RoleDto>();

		Pageable pageable = PageRequest.of(page, limit, Sort.by(SortUtil.sort(sort)));

		Page<Role> pages = null;

		if (!search.isEmpty()) {
			pages = this.roleDao.findAllByNameContaining(search, pageable);
		} else {
			pages = this.roleDao.findAll(pageable);
		}

		pages.forEach(role -> {
			roleDtos.add(RoleDto.builder().id(role.getId()).name(role.getName()).build());
		});

		return PaginationData.builder().total(pages.getTotalElements()).pages(pages.getTotalPages()).values(roleDtos)
				.build();
	}

	@Override
	@Transactional(readOnly = true)
	public RoleDto findById(Long id) {
		Optional<Role> optional = this.roleDao.findById(id);
		if (!optional.isPresent()) {
			try {
				throw new NotFoundException(RoleMessageError.NOT_FOUND_ROLE.toString());
			} catch (NotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		Role role = optional.get();

		List<String> authorityDtos = new ArrayList<String>();
		Iterable<Authority> authorities = this.authorityDao.findAllByRoles(role);
		authorities.forEach(authority -> {
			authorityDtos.add(authority.getAuthority());
		});

		return RoleDto.builder().id(role.getId()).name(role.getName()).authorities(authorityDtos).build();
	}

	@Override
	@Transactional
	public RoleDto create(RoleDataDto roleData) {
		
		List<Authority> authorities = null;
		final List<String> authorityDtos = new ArrayList<String>();
		ValidationFields<Role> validation = new ValidationFields<Role>();

		if (roleData.getAuthority() != null) {
			authorities = (List<Authority>) this.authorityDao.findAllById(roleData.getAuthority());
		}

		Role role = Role.builder().name(roleData.getName()).authorities(authorities)
				.build();

		validation.validate(role, RoleMessageError.INVALID_USER_FIELDS.toString());

		this.roleDao.save(role);

		if (roleData.getAuthority() != null) {
			role.getAuthorities().forEach(authority -> {
				authorityDtos.add(authority.getAuthority());
			});
		}

		return RoleDto.builder().id(role.getId()).name(role.getName()).authorities(authorityDtos).build();
	}

	@Override
	@Transactional
	public RoleDto update(Long id, RoleDataDto roleData) {
		ValidationFields<Role> validation = new ValidationFields<Role>();
		List<Authority> authorities = null;
		final List<String> authorityDtos = new ArrayList<String>();
		Optional<Role> optional = this.roleDao.findById(id);

		if (!optional.isPresent()) {
			try {
				throw new NotFoundException(RoleMessageError.NOT_FOUND_ROLE.toString());
			} catch (NotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		if (roleData.getAuthority() != null) {
			authorities = (List<Authority>) this.authorityDao.findAllById(roleData.getAuthority());
		}

		Role role = optional.get();

		role.setAuthorities(authorities);
		role.setName(roleData.getName());

		validation.validate(role, RoleMessageError.INVALID_USER_FIELDS.toString());

		this.roleDao.save(role);
		
		if (role.getAuthorities() != null) {
			role.getAuthorities().forEach(authority -> {
				authorityDtos.add(authority.getAuthority());
			});
		}

		return RoleDto.builder().id(role.getId()).name(role.getName()).authorities(authorityDtos).build();
	}

	@Override
	@Transactional
	public void delete(Long id) {
	roleDao.deleteById(id);
		
	}
	
	@Override
	@Transactional(readOnly = true)
	public List<RoleDto> findAllExport() {
		Iterable<Role> optional = this.roleDao.findAll();
		List<RoleDto> RolesDto = new ArrayList<RoleDto>();
		optional.forEach(authority -> {
			RoleDto rol = new RoleDto();
			rol.setId(authority.getId());
			rol.setName(authority.getName());
			List<String> authorityDtos = new ArrayList<String>();
			Iterable<Authority> authorities = this.authorityDao.findAllByRoles(authority);
			authorities.forEach(authoriti -> {
				authorityDtos.add(authoriti.getDescription());
			});
			rol.setAuthorities(authorityDtos);
			RolesDto.add(rol);
		});
		
		return RolesDto;
	}

	@Override
	@Transactional(readOnly = true)
	public List<Role> findAll() {
		return (List<Role>) roleDao.findAll(Sort.by(Direction.ASC, "name"));
	}

	@Override
	public Role findByRol(Long id) {
		return roleDao.findByRol(id);
	}

}
