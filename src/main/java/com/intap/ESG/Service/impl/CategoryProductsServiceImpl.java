package com.intap.ESG.Service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.intap.ESG.Dao.ICategoryProductsDao;
import com.intap.ESG.Models.CategoryProducts;
import com.intap.ESG.Service.ICategoryProductsService;

@Service
public class CategoryProductsServiceImpl implements ICategoryProductsService{
	
	@Autowired
	private ICategoryProductsDao categoryProductsDao;

	@Override
	public List<CategoryProducts> findAll() {
		return (List<CategoryProducts>) categoryProductsDao.findAllByOrderByName();
	}

	@Override
	public CategoryProducts findById(Long id) {
		return categoryProductsDao.findById(id).orElse(null);
	}

	@Override
	public CategoryProducts save(CategoryProducts categoryProducts) {
		return categoryProductsDao.save(categoryProducts);
	}

	@Override
	public void delete(Long id) {
		categoryProductsDao.deleteById(id);
		
	}

}
