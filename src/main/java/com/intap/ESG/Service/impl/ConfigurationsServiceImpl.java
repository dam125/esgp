package com.intap.ESG.Service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.intap.ESG.Dao.IConfigurationsDao;
import com.intap.ESG.Models.Configurations;
import com.intap.ESG.Service.IConfigurationsService;
@Service
public class ConfigurationsServiceImpl implements IConfigurationsService{
	@Autowired
	private IConfigurationsDao configurationsDao;
	@Override
	public List<Configurations> findAll() {
		return (List<Configurations>) configurationsDao.findAll();
	}

	@Override
	public Configurations findById(Long id) {
		return configurationsDao.findById(id).orElse(null);
	}
	
	@Override
	public Configurations findOne() {
		return configurationsDao.findOne();
	}

	@Override
	public Configurations save(Configurations configurations) {
		return configurationsDao.save(configurations);
	}

	@Override
	public void delete(Long id) {
		configurationsDao.deleteById(id);
		
	}

}
