package com.intap.ESG.Service.impl;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.intap.ESG.Dao.IPaymentsDao;
import com.intap.ESG.Models.Invoices;
import com.intap.ESG.Models.Payments;
import com.intap.ESG.Service.IPaymentsService;

@Service
public class PaymentsServiceImpl implements IPaymentsService{
	
	@Autowired
	private IPaymentsDao paymentsDao;

	@Override
	public List<Payments> findAll() {
		return (List<Payments>) paymentsDao.findAll();
	}

	@Override
	public Payments findById(Long id) {
		return paymentsDao.findById(id).orElse(null);
	}

	@Override
	public Payments save(Payments invoices) {
		return paymentsDao.save(invoices);
	}

	@Override
	public void delete(Long id) {
		paymentsDao.deleteById(id);
		
	}

	@Override
	public List<Payments> findByInvoices(Invoices factura) {
		return paymentsDao.findByInvoices(factura);
	}

	@Override
	public List<Payments> findByDates(LocalDate startDate, LocalDate endDate) {
		return paymentsDao.findAllDates(startDate,endDate);
	}

	

}
