package com.intap.ESG.Service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.intap.ESG.Dao.IStatusDao;
import com.intap.ESG.Models.Status;
import com.intap.ESG.Service.IStatusService;

@Service
public class StatusServiceImpl implements IStatusService {
	
	@Autowired
	private IStatusDao statusDao;

	@Override
	public List<Status> findAll() {
		return (List<Status>) statusDao.findAll();
	}

	@Override
	public Status findById(Long id) {
		return statusDao.findById(id).orElse(null);
	}

	@Override
	public Status save(Status status) {
		return statusDao.save(status);
	}

	@Override
	public void delete(Long id) {
		statusDao.deleteById(id);
		
	}

	@Override
	public Status findByName(String name) {
		return statusDao.findByName(name);
	}

}
