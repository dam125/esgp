package com.intap.ESG.Service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.intap.ESG.Dao.IEnabledReasonDao;
import com.intap.ESG.Models.AssociatesType;
import com.intap.ESG.Models.EnabledReason;
import com.intap.ESG.Service.IEnabledReasonService;

@Service
public class IEnabledReasonServiceImpl  implements IEnabledReasonService {
	
	@Autowired
	private IEnabledReasonDao enabledReasonDao;

	@Override
	public List<EnabledReason> findAll() {
		return (List<EnabledReason>) enabledReasonDao.findAll();
	}

	@Override
	public EnabledReason findById(Long id) {
		return enabledReasonDao.findById(id).orElse(null);
	}
	
	@Override
	public List<EnabledReason> findByAssociatesType(AssociatesType asociateType) {
		return enabledReasonDao.findByAssociateType(asociateType);
	}

	@Override
	public EnabledReason save(EnabledReason enabledReason) {
		return enabledReasonDao.save(enabledReason);
	}

	@Override
	public void delete(Long id) {
		enabledReasonDao.deleteById(id);
		
	}



}
