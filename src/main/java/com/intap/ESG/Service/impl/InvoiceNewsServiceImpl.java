package com.intap.ESG.Service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.intap.ESG.Dao.IInvoiceNewsDao;
import com.intap.ESG.Models.Associated;
import com.intap.ESG.Models.InvoiceNews;
import com.intap.ESG.Models.Invoices;
import com.intap.ESG.Service.IInvoiceNewsService;

@Service
public class InvoiceNewsServiceImpl implements IInvoiceNewsService{
	
	@Autowired
	private IInvoiceNewsDao invoiceNewsDao;
	
	@Override
	public List<InvoiceNews> findAll(){
		return (List<InvoiceNews>) invoiceNewsDao.findAll();
	}
	
	@Override
	public InvoiceNews findById(Long id){
		return invoiceNewsDao.findById(id).orElse(null);
	}
	
	@Override
	public InvoiceNews save(InvoiceNews invoiceNews){
		return invoiceNewsDao.save(invoiceNews);
	}
	
	@Override
	public void delete(Long id){
		invoiceNewsDao.deleteById(id);
	}

	@Override
	public List<InvoiceNews> findByInvoices(Invoices factura) {
		return invoiceNewsDao.findByInvoices(factura);
	}


}
