package com.intap.ESG.Service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.intap.ESG.Dao.IContractNewsDao;
import com.intap.ESG.Models.ContractNews;
import com.intap.ESG.Models.Contracts;
import com.intap.ESG.Models.News;
import com.intap.ESG.Service.IContractNewsService;

@Service
public class ContractNewsServiceImpl implements IContractNewsService{
	
	@Autowired
	private IContractNewsDao contractNewsDao;

	@Override
	public List<ContractNews> findAll() {
		return (List<ContractNews>) contractNewsDao.findAll();
	}

	@Override
	public ContractNews findById(Long id) {
		return contractNewsDao.findById(id).orElse(null);
	}

	@Override
	public ContractNews save(ContractNews contractNew) {
		// TODO Auto-generated method stub
		return contractNewsDao.save(contractNew);
	}

	@Override
	public void delete(Long id) {
		contractNewsDao.deleteById(id);
		
	}

	@Override
	public List<ContractNews>  findByContracts(Contracts id) {
		return contractNewsDao.findByContracts(id);
		
	}
	
	@Override
	public List<ContractNews> findByContractsEnabled(Contracts id) {
		return contractNewsDao.findByContractsEnabled(id);
		
	}

	@Override
	public void deleteAll(List<ContractNews> contractNews) {
		contractNewsDao.deleteAll(contractNews);
		
	}

	@Override
	public List<ContractNews> findByContractNewss(News news) {
		return contractNewsDao.findByContractNewss(news);
	}

}
