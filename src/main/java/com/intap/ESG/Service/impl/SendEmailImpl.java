package com.intap.ESG.Service.impl;

import java.io.File;
import java.util.Optional;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Address;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.intap.ESG.Dao.IConfiguracionesEmailDao;
import com.intap.ESG.Models.ConfiguracionesEmail;
import com.intap.ESG.Service.ISendEmailService;
import com.intap.ESG.util.PropertiesEmail;

@Service
public class SendEmailImpl implements ISendEmailService{
	
	@Autowired
	private IConfiguracionesEmailDao configuracionesEmailDao;
	
	
	@Override
	public void sendEmail(String to, String subject, String text) {
		
		ConfiguracionesEmail confEmail = configuracionesEmailDao.findById2();
		
		Properties props = PropertiesEmail.properties(confEmail); 

	    Session session = Session.getDefaultInstance(props);
	    MimeMessage message = new MimeMessage(session);
	    
	    try {
	    	
	        message.setFrom(new InternetAddress(confEmail.getUsername()));
	        message.addRecipients(Message.RecipientType.TO, to);   
	        message.setSubject(subject);
	        message.setContent(text, "text/html; charset=utf-8");
	        Transport transport = session.getTransport("smtp");
	        transport.connect(confEmail.getHost(), confEmail.getUsername(), confEmail.getPassword());
	        transport.sendMessage(message, message.getAllRecipients());
	        transport.close();
	        
	    }
	    catch (MessagingException me) {
	        me.printStackTrace();   //Si se produce un error
	    }
		
	}

	@Override
	public void sendEmail(String to, String subject, String text, String attachments, String fileName) {
		
		ConfiguracionesEmail confEmail = configuracionesEmailDao.findById2();
		
		Properties props = PropertiesEmail.properties(confEmail); 

	    Session session = Session.getDefaultInstance(props);
	    // Create a default MimeMessage object.
        Message message = new MimeMessage(session);
	    
	    try {
	    		    	
	        message.setFrom(new InternetAddress(confEmail.getUsername()));
	        message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));   //Se podrían añadir varios de la misma manera
	        message.setSubject(subject);
	        //adjunto 
			BodyPart messageBodyPart = new MimeBodyPart();
			// Now set the actual message
			messageBodyPart.setContent(text, "text/html; charset=utf-8");
			// Create a multipar message
			Multipart multipart = new MimeMultipart();
			// Set text message part
			multipart.addBodyPart(messageBodyPart);
			// Part two is attachment
	         messageBodyPart = new MimeBodyPart();
	         String filename = attachments;
	         
	         DataSource source = new FileDataSource(filename);
	         messageBodyPart.setDataHandler(new DataHandler(source));
	         messageBodyPart.setFileName(fileName);
	         multipart.addBodyPart(messageBodyPart);

	         // Send the complete message parts
	         message.setContent(multipart);
			
	        
	        
	        
	        Transport transport = session.getTransport("smtp");
	        transport.connect(confEmail.getHost(), confEmail.getUsername(), confEmail.getPassword());
	        //Transport.send(message);
	        transport.sendMessage(message, message.getAllRecipients());
	        transport.close();
	        
	    }
	    catch (MessagingException me) {
	        me.printStackTrace();   //Si se produce un error
	    }
		
	}
	

}
