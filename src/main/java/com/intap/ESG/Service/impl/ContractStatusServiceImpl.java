package com.intap.ESG.Service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.intap.ESG.Dao.IContractStatusDao;
import com.intap.ESG.Models.ContractStatus;
import com.intap.ESG.Models.Contracts;
import com.intap.ESG.Models.DTO.ContractsStatusDto;
import com.intap.ESG.Service.IContractStatusService;

@Service
public class ContractStatusServiceImpl implements IContractStatusService{
	
	@Autowired
	private IContractStatusDao contractStatusDao;

	@Override
	public List<ContractStatus> findAll() {
		return (List<ContractStatus>) contractStatusDao.findAll();
	}

	@Override
	public ContractStatus findById(Long id) {
		return contractStatusDao.findById(id).orElse(null);
	}

	@Override
	public ContractStatus save(ContractStatus contractStatus) {
		return contractStatusDao.save(contractStatus);
	}

	@Override
	public void delete(Long id) {
		contractStatusDao.deleteById(id);
		
	}

	@Override
	public List<ContractStatus> lastStateInvoice(Long id) {
		return contractStatusDao.lastStateInvoice(id);
	}

	@Override
	public List<ContractStatus> findByContract(Contracts contract) {
		return contractStatusDao.findByContract(contract);
	}

	@Override
	public void deleteAll(List<ContractStatus> contracts) {
		contractStatusDao.deleteAll(contracts);
		
	}

}
