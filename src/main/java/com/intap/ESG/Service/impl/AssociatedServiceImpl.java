package com.intap.ESG.Service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.intap.ESG.Dao.IAssociatedDao;
import com.intap.ESG.Models.Associated;
import com.intap.ESG.Models.DTO.TypeAssociatedFilter;
import com.intap.ESG.Service.IAssociatedService;
import com.intap.ESG.util.specification.AssocietedSpecification;

@Service
public class AssociatedServiceImpl implements IAssociatedService {

	@Autowired
	private IAssociatedDao associatedDao;

	@Override
	public List<Associated> findAll() {
		return associatedDao.findAll();
	}

	@Override
	public List<Associated> findWithoutActualUser(Integer id) {
		return associatedDao.findWithoutActualUser(id);
	}

	@Override
	public Associated findById(Integer id) {
		return associatedDao.findById(id).orElse(null);
	}

	@Override
	public Associated save(Associated associated) {
		return associatedDao.save(associated);
	}

	@Override
	public void delete(Integer id) {
		associatedDao.deleteById(id);
	}

	@Override
	public Associated findByIdentificationNumber(String identification) {
		return associatedDao.findByIdentificationNumber(identification);
	}

	@Override
	public Associated findByIdentificationNumberUpdate(String identification, Integer id) {
		return associatedDao.findByIdentificationNumberUpdate(identification, id);
	}

	@Override
	public String findAdress(Integer id) {
		return associatedDao.findAdress(id);
	}

	@Override
	@Transactional(readOnly = true)
	public Page<Associated> findAll(Integer page, Integer limit, Integer userId, String names,
			String identificationNumber, String CellPhoneNumber, String neighborhood, String city, String email,
			List<TypeAssociatedFilter> typeAssociated) {

		Pageable pageable = PageRequest.of(page, limit, Sort.by(Direction.DESC, "id"));

		return associatedDao.findAll(new AssocietedSpecification(names, identificationNumber, CellPhoneNumber,
				neighborhood, city, email, typeAssociated, userId), pageable);
	}

	@Override
	@Transactional(readOnly = true)
	public Iterable<Associated> findAll(Integer userId, String names, String identificationNumber,
			String CellPhoneNumber, String neighborhood, String city, String email,
			List<TypeAssociatedFilter> typeAssociated) {

		return associatedDao.findAll(new AssocietedSpecification(names, identificationNumber, CellPhoneNumber,
				neighborhood, city, email, typeAssociated, userId), Sort.by(Direction.DESC, "id"));
	}

	@Override
	public List<Associated> getUsersFilter(String value) {
		// TODO Auto-generated method stub
		return this.associatedDao.GetUsersFilter(value);
	}

}
