package com.intap.ESG.Service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.intap.ESG.Dao.IAuthorityDao;
import com.intap.ESG.Models.Authority;
import com.intap.ESG.Models.DTO.AuthorityDto;
import com.intap.ESG.Service.AuthorityService;





@Service
public class AuthorityServiceImpl implements AuthorityService{
	
	@Autowired
	private IAuthorityDao authorityDao;

	@Override
	@Transactional(readOnly = true)
	public Iterable<AuthorityDto> findAll() {
		List<AuthorityDto> authorityDtos = new ArrayList<AuthorityDto>();

		Iterable<Authority> authorities = this.authorityDao.findAll();
		
		authorities.forEach(authority -> {
			authorityDtos.add(AuthorityDto.builder()
					.authority(authority.getAuthority())
					.description(authority.getDescription())
					.build());
		});

		return authorityDtos;
	}

}
