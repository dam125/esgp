package com.intap.ESG.Dao;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.intap.ESG.Models.Contracts;
import com.intap.ESG.Models.Invoices;
import com.intap.ESG.Models.Products;

public interface IInvoicesDao extends PagingAndSortingRepository<Invoices, Long> {

	@Query("SELECT i FROM Invoices i WHERE i.contrato = ?1 AND i.envoicesMonth = ?2")
	Invoices findByContractAndMonth(Contracts contract, String month);

	@Query("SELECT i FROM Invoices i WHERE i.consecutive = ?1 ")
	Invoices findByInvoiceNumber(String invoiceNumber);

	@Query("SELECT count(i) FROM Invoices i WHERE i.contrato = ?1 ")
	Integer countInvoicesGenerated(Contracts contrato);

	@Query("SELECT count(i) FROM Invoices i WHERE i.contrato = ?1 AND i.envoicesState = ?2")
	Integer countInvoicesOpenOrClose(Contracts contrato, String state);

	@Query("SELECT sum(i.total) FROM Invoices i WHERE i.contrato = ?1 AND i.envoicesState = ?2")
	Integer sumInvoicesOpenOrClose(Contracts contrato, String state);
	
	@Query("SELECT i FROM Invoices i WHERE i.contrato = ?1 AND i.envoicesState = ?2 ORDER BY i.envoicesMonth DESC")
	List<Invoices> findAllInvoicesOpenOrClose(Contracts contrato, String state);

	@Query("SELECT i FROM Invoices i WHERE i.invoiceDate = ?1 AND i.envoicesMonth = ?2 ORDER BY i.id ASC")
	List<Invoices> findAllInvoicesRecurrentes(LocalDate fechaGeneracion, String envoicesMonth);
	
	@Query("SELECT i FROM Invoices i WHERE i.envoicesMonth = ?1 AND i.recurringInvoice = true ORDER BY i.id ASC")
	List<Invoices> findByfechaAndRecurrence(String fecha);

	@Query("SELECT i FROM Invoices i WHERE i.refPago = ?1 AND i.envoicesState = ?2 ")
	Invoices findByRefernce(String reference, String estado);

	@Query("SELECT i FROM Invoices i WHERE i.contrato = ?1 AND i.envoicesState = ?2 AND i.id <> ?3 ")
	List<Invoices> findInvoicesOpen(Contracts contrato, String state, Long idInvoice);

	Page<Invoices> findAll(Specification<Invoices> spec, Pageable pageable);
	
	Iterable<Invoices> findAll(Specification<Invoices> spec, org.springframework.data.domain.Sort sort);

}
