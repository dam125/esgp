package com.intap.ESG.Dao;


import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.intap.ESG.Models.Invoices;
import com.intap.ESG.Models.Payments;

public interface IPaymentsDao extends CrudRepository<Payments, Long>{
	
	@Query("SELECT p FROM Payments p WHERE p.factura = ?1 AND cancel = false ORDER BY paymentDate DESC, consecutive DESC")
	List<Payments> findByInvoices (Invoices factura);

	
	@Query("SELECT p FROM Payments p WHERE p.paymentDate BETWEEN ?1  AND ?2 ")
	List<Payments> findAllDates (LocalDate dateStart,LocalDate dateEnd);
}
