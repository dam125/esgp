package com.intap.ESG.Dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.intap.ESG.Models.Associated;
import com.intap.ESG.Models.Provider;

public interface IProviderDao extends CrudRepository<Provider, Integer>{
	
	@Query("SELECT c FROM Provider c ORDER BY c.id DESC")
	public List<Provider> findAll();
	
	@Query("SELECT a from Provider a WHERE a.enabled=true ORDER BY a.id DESC")
	public List<Provider> findAllEnabled();
		
	public Provider findByIdAssociated(Associated associated);

}
