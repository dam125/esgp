package com.intap.ESG.Dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.intap.ESG.Models.Associated;
import com.intap.ESG.Models.Client;
import com.intap.ESG.Models.Contracts;

public interface IContractsDao extends CrudRepository<Contracts, Long> {
	
	//public Iterable<Contracts> findAllOrderByIdDesc();
	@Query("SELECT c FROM Contracts c ORDER BY c.id DESC")
	public List<Contracts> findAll();

	@Query("SELECT COUNT(c.id) FROM Contracts c WHERE c.consecutive = ?1")
	int countConsecutive(String consecutive);
	
	@Query("SELECT COUNT(c.id) FROM Contracts c WHERE c.consecutive = ?1 AND c.id <> ?2")
	int countConsecutiveUpdate(String consecutive, Long id);
	
	@Query("SELECT c FROM Contracts c WHERE c.idClient = ?1 ")
	List<Contracts> findByAssociates(Client associates);
	
	@Query("SELECT c FROM Contracts c WHERE c.ipClient = ?1 ")
	Contracts findByIp(String ipClient);
	
	@Query("SELECT c FROM Contracts c WHERE c.macClient = ?1 ")
	Contracts findByMac(String mac_client);

	@Query("SELECT c FROM Contracts c WHERE c.serialEquipment = ?1 ")
	Contracts findBySerial(String Serial);
	
	@Query("SELECT c FROM Contracts c WHERE c.ipClient = ?1 AND c.id <> ?2 ")
	Contracts findByIpUpdate(String ipClient, Long id);
	
	@Query("SELECT c FROM Contracts c WHERE c.macClient = ?1 AND c.id <> ?2 ")
	Contracts findBmacUpdate(String macClient, Long id);
	
	@Query("SELECT c FROM Contracts c WHERE c.serialEquipment = ?1 AND c.id <> ?2 ")
	Contracts findBserialUpdate(String macClient, Long id);
	
	Contracts findByIdClient(Client client);
	
	@Query("SELECT c FROM Contracts c WHERE c.enabled = true and c.noBill= true")
	List<Contracts> findContracsEnabled ();
	
	@Query("SELECT c FROM Contracts c WHERE c.enabled = true AND c.noBill= true AND c.cuttingDay = ?1 "
			+ "AND c.periodicity = ?2 ORDER BY c.neighborhood ASC")
	List<Contracts> findContracsRecurrente (int cuttingDay, String periodicity);
	
	Page<Contracts> findAll(Specification<Contracts> spec, Pageable pageable);
	
	Iterable<Contracts> findAll(Specification<Contracts> spec, org.springframework.data.domain.Sort sort);
	
}
