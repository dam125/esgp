package com.intap.ESG.Dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.intap.ESG.Models.Associated;
import com.intap.ESG.Models.Employee;
import com.intap.ESG.Models.UserModel;

public interface IEmployeeDao extends CrudRepository<Employee, Integer>{
	
	@Query("SELECT c FROM Employee c ORDER BY c.id DESC")
	public List<Employee> findAll();
	
	@Query("SELECT a from Employee a WHERE a.enabled=true ORDER BY a.id DESC")
	public List<Employee> findAllEnabled();
	
	public Employee  findByUserId (UserModel user);
	
	public Employee findByIdAssociated(Associated associated);
	

}
