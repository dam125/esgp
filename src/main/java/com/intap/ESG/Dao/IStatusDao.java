package com.intap.ESG.Dao;

import org.springframework.data.repository.CrudRepository;

import com.intap.ESG.Models.Status;

public interface IStatusDao extends CrudRepository<Status, Long>{
	

	Status findByName(String name);

}
