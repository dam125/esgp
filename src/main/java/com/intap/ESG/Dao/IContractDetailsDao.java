package com.intap.ESG.Dao;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import com.intap.ESG.Models.ContractDetails;
import com.intap.ESG.Models.Contracts;
import com.intap.ESG.Models.Products;

public interface IContractDetailsDao extends CrudRepository<ContractDetails, Long>{
	
	@Query("SELECT u  FROM ContractDetails u WHERE u.contracts = ?1")
	List<ContractDetails> findByContracts(Contracts id);
	
	@Query("SELECT SUM((a.amount*a.unitPrice)+((a.amount*a.unitPrice)*a.taxes)/100) as cantTotal "
			+ "FROM ContractDetails as a WHERE a.contracts =?1")
	double contractValue(Contracts id);
	
	@Query("SELECT c FROM ContractDetails c WHERE c.products = ?1 ")
	List<ContractDetails> findByProduct(Products products);
	
	@Transactional
	@Modifying
	@Query("UPDATE ContractDetails c SET c.unitPrice = ?2  WHERE c.products = ?1 ")
	void updatePriceByProduct(Products products, Double price);
		


}
