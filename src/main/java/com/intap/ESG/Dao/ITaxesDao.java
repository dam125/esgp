package com.intap.ESG.Dao;

import org.springframework.data.repository.CrudRepository;

import com.intap.ESG.Models.Taxes;

public interface ITaxesDao extends CrudRepository<Taxes, Long>{

}
