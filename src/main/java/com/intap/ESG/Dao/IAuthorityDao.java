package com.intap.ESG.Dao;



import org.springframework.data.repository.CrudRepository;


import com.intap.ESG.Models.Authority;
import com.intap.ESG.Models.Role;



public interface IAuthorityDao extends CrudRepository<Authority, String>  {
	
	Iterable<Authority> findAllByRoles(Role roles);

}
