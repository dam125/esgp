package com.intap.ESG.Dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.intap.ESG.Models.Employee;

import com.intap.ESG.Models.RecurringInvoices;

public interface IRecurringInvoicesDao extends CrudRepository<RecurringInvoices, Long>{
	
	@Query("SELECT p FROM RecurringInvoices p WHERE p.idEmployee = ?1 AND p.year = ?2 AND p.month = ?3")
	RecurringInvoices findByInvoices (Employee employee, String year, String month);

	
	Page<RecurringInvoices> findAll(Specification<RecurringInvoices> spec, Pageable pageable);
	
	Iterable<RecurringInvoices> findAll(Specification<RecurringInvoices> spec, org.springframework.data.domain.Sort sort);
}
