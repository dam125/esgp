package com.intap.ESG.Dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.intap.ESG.Models.Configurations;

public interface IConfigurationsDao extends CrudRepository<Configurations, Long>{
	@Query("SELECT c FROM Configurations c WHERE c.id = 1")
	Configurations findOne();
}
