package com.intap.ESG.Dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.intap.ESG.Models.RecoverPassword;

public interface IRecoverPasswordDao extends CrudRepository<RecoverPassword, Integer>{
	
	@Query( value = "SELECT r FROM RecoverPassword r  WHERE r.codigo = ?1")
	RecoverPassword findByCodigo (Integer codigo);
	

}
