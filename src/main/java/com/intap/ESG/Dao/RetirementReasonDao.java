package com.intap.ESG.Dao;

import org.springframework.data.repository.CrudRepository;

import com.intap.ESG.Models.RetirementReason;

public interface RetirementReasonDao extends CrudRepository<RetirementReason, Long>{

}
