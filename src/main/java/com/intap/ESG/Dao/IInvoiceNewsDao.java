package com.intap.ESG.Dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.intap.ESG.Models.Associated;
import com.intap.ESG.Models.InvoiceNews;
import com.intap.ESG.Models.Invoices;
import com.intap.ESG.Models.Payments;

public interface IInvoiceNewsDao  extends CrudRepository <InvoiceNews, Long> {

	@Query("SELECT p FROM InvoiceNews p WHERE p.invoices = ?1 ")
	List<InvoiceNews> findByInvoices (Invoices factura);
	

	
}
