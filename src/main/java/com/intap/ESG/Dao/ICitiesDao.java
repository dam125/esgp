package com.intap.ESG.Dao;

import org.springframework.data.repository.CrudRepository;

import com.intap.ESG.Models.Cities;

public interface ICitiesDao extends CrudRepository<Cities, Long>{

}
