package com.intap.ESG.Dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.intap.ESG.Models.CategoryProducts;

public interface ICategoryProductsDao extends CrudRepository<CategoryProducts, Long>{
	
	public List<CategoryProducts> findAllByOrderByName();

}
