package com.intap.ESG.Dao;

import org.springframework.data.repository.CrudRepository;

import com.intap.ESG.Models.AssociatesType;

public interface IAssociatesTypeDao extends CrudRepository<AssociatesType, Long> {
	

	AssociatesType  findByName (String name);

}
