package com.intap.ESG.Dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.intap.ESG.Models.FilesPartner;
import com.intap.ESG.Models.Partner;

public interface IFilesPartnerDao extends CrudRepository<FilesPartner, Integer> {
	
	public List<FilesPartner> findByIdPartner(Partner partner);

}
