package com.intap.ESG.Dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.intap.ESG.Models.ConfiguracionesEmail;

public interface IConfiguracionesEmailDao extends CrudRepository<ConfiguracionesEmail, Long>{
	
	@Query("select a from ConfiguracionesEmail a where a.id = 1 ")
	public ConfiguracionesEmail findById2();

}
