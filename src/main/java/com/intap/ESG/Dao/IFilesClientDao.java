package com.intap.ESG.Dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.intap.ESG.Models.Client;
import com.intap.ESG.Models.FilesClient;

public interface IFilesClientDao extends CrudRepository<FilesClient, Integer> {
	
	public List<FilesClient> findByIdClient(Client client);

}
