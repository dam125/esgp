package com.intap.ESG.Dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.intap.ESG.Models.Contracts;
import com.intap.ESG.Models.ViewModel;

public interface IViewsDao extends CrudRepository<ViewModel, Long> {

}
