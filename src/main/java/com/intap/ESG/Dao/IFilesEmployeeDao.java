package com.intap.ESG.Dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.intap.ESG.Models.Employee;
import com.intap.ESG.Models.FilesEmployee;

public interface IFilesEmployeeDao extends CrudRepository<FilesEmployee, Integer> {
	
	public List<FilesEmployee> findByIdEmployee(Employee employee);

}
