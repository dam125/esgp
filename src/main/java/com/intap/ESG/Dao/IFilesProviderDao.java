package com.intap.ESG.Dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.intap.ESG.Models.FilesProvider;
import com.intap.ESG.Models.Provider;

public interface IFilesProviderDao extends CrudRepository<FilesProvider, Integer> {
	
	public List<FilesProvider> findByIdProvider(Provider provider);

}
