package com.intap.ESG.Dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.repository.CrudRepository;

import com.intap.ESG.Models.News;
import com.intap.ESG.Models.Products;
import com.intap.ESG.util.specification.NewsSpecification;

public interface INewsDao extends CrudRepository<News, Long>{

	Page<News> findAll(Specification<News> spec, Pageable pageable);

	Iterable<News> findAll(Specification<News> spec, org.springframework.data.domain.Sort sort);

}
