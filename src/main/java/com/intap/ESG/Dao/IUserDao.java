package com.intap.ESG.Dao;


import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.intap.ESG.Models.Role;
import com.intap.ESG.Models.UserModel;



public  interface IUserDao extends PagingAndSortingRepository<UserModel, Integer> {
	
	@Query("SELECT DISTINCT u FROM UserModel u INNER JOIN FETCH u.role AS r INNER JOIN FETCH r.authorities AS a "
			+ " WHERE u.username = :username")
	Optional<UserModel> findByUsername(@Param("username") String username);
	
	@Query("SELECT  u FROM UserModel u WHERE u.id = ?1")
	Optional<UserModel> findById(Long id);
	

	@Query("SELECT  u FROM UserModel u WHERE u.role = ?1")
	List<UserModel> findByRol(Role role);
	

}
