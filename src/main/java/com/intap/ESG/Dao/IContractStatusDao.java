package com.intap.ESG.Dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.intap.ESG.Models.ContractStatus;
import com.intap.ESG.Models.Contracts;
import com.intap.ESG.Models.DTO.ContractsStatusDto;

public interface IContractStatusDao extends CrudRepository<ContractStatus, Long>{
	
	@Query("SELECT cs FROM Status s INNER JOIN ContractStatus cs ON cs.status = s.id " + 
			"INNER JOIN Contracts c ON c.id = cs.contract " + 
			"WHERE c.id = ?1 ORDER BY cs.id DESC ")
	List<ContractStatus> lastStateInvoice(Long id);
	

	List<ContractStatus> findByContract(Contracts contract);
	

}
