package com.intap.ESG.Dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.intap.ESG.Models.Associated;
import com.intap.ESG.Models.CategoryProducts;
import com.intap.ESG.Models.Products;
import com.intap.ESG.util.specification.ProductsSpecification;

public interface IProductsDao extends PagingAndSortingRepository<Products, Long> {

	@Query("SELECT c FROM Products c WHERE c.categoryProducts = ?1 ")
	List<Products> findByategoryProducts(CategoryProducts categoryProducts);

	Page<Products> findAll(Specification<Products> spec, Pageable pageable);
	
	Iterable<Products> findAll(Specification<Products> spec, org.springframework.data.domain.Sort sort);
	
	@Query("SELECT c FROM Products c WHERE c.code = ?1 ")
	Products findByode(Long Code);



}
