package com.intap.ESG.Dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.intap.ESG.Models.Associated;

public interface IAssociatedDao extends PagingAndSortingRepository<Associated, Integer>{
	
	@Query("SELECT c FROM Associated c ORDER BY c.id DESC")
	public List<Associated> findAll();
	
	@Query("select a from Associated a  where a.id <> ?1 ORDER BY a.id DESC")
	List<Associated> findWithoutActualUser(Integer id);
	
	Associated  findByIdentificationNumber (String identification);
	
	@Query("select a from Associated a  where a.identificationNumber = ?1 and a.id <> ?2")
	Associated  findByIdentificationNumberUpdate (String identification, Integer id);
	
	@Query("select a.address from Associated a  WHERE a.id = ?1")
	String findAdress(Integer id);
	
	Page<Associated> findAll(Specification<Associated> spec, Pageable pageable);
	
	Iterable<Associated> findAll(Specification<Associated> spec, org.springframework.data.domain.Sort sort);
	
	
	@Query("SELECT c FROM Associated c where UPPER(CONCAT(c.firstName,' ', c.lastName)) LIKE CONCAT(UPPER(:value),'%')")
	public List<Associated> GetUsersFilter(@Param("value")String value);
	

}
