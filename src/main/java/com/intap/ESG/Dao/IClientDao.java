package com.intap.ESG.Dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.intap.ESG.Models.Associated;
import com.intap.ESG.Models.Client;

public interface IClientDao extends CrudRepository<Client, Integer>{
	
	@Query("SELECT c FROM Client c ORDER BY c.id DESC")
	public List<Client> findAll();
	
	@Query("SELECT a from Client a WHERE a.enabled=true ORDER BY a.id DESC")
	public List<Client> findAllEnabled();
		
	public Client findByIdAssociated(Associated associated);

}
