package com.intap.ESG.Dao;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.intap.ESG.Models.DataCredit;

public interface IDataCreditDao extends PagingAndSortingRepository<DataCredit, Integer> {

}
