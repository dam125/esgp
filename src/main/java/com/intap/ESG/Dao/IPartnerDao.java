package com.intap.ESG.Dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.intap.ESG.Models.Associated;
import com.intap.ESG.Models.Partner;

public interface IPartnerDao extends CrudRepository<Partner, Integer>{
	
	@Query("SELECT c FROM Partner c ORDER BY c.id DESC")
	public List<Partner> findAll();
	
	@Query("SELECT a from Partner a WHERE a.enabled=true ORDER BY a.id DESC")
	public List<Partner> findAllEnabled();
		
	public Partner findByIdAssociated(Associated associated);

}
