package com.intap.ESG.Dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.intap.ESG.Models.Role;



public interface IRoleDao extends PagingAndSortingRepository<Role, Long> {
	
	Page<Role> findAllByNameContaining(String name, Pageable pageable);
	
	@Query("SELECT  u FROM Role u WHERE u.id = ?1")
	Role findByRol (Long id);

}
