package com.intap.ESG.Dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.intap.ESG.Models.ContractNews;
import com.intap.ESG.Models.Contracts;
import com.intap.ESG.Models.News;

public interface IContractNewsDao extends CrudRepository<ContractNews, Long>{
	
	@Query("SELECT u FROM ContractNews u WHERE u.contracts = ?1 ORDER BY u.news.name")
	List<ContractNews>  findByContracts(Contracts id);
	
	@Query("SELECT u FROM ContractNews u WHERE u.contracts = ?1 AND u.numberOfMonths > u.billedMonths")
	List<ContractNews>  findByContractsEnabled(Contracts id);
	
	@Query("SELECT c FROM ContractNews c WHERE c.news = ?1 ")
	List<ContractNews> findByContractNewss (News news);



}
