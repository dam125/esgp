package com.intap.ESG.Dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.intap.ESG.Models.AssociatesType;
import com.intap.ESG.Models.EnabledReason;

public interface IEnabledReasonDao extends CrudRepository<EnabledReason, Long> {
	
	List<EnabledReason> findByAssociateType (AssociatesType asociateType);

}
