package com.intap.ESG.Controller;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.intap.ESG.Models.Associated;
import com.intap.ESG.Models.Client;
import com.intap.ESG.Models.Configurations;
import com.intap.ESG.Models.ContractDetails;
import com.intap.ESG.Models.ContractNews;
import com.intap.ESG.Models.ContractStatus;
import com.intap.ESG.Models.Contracts;
import com.intap.ESG.Models.DataCredit;
import com.intap.ESG.Models.Employee;
import com.intap.ESG.Models.Invoices;
import com.intap.ESG.Models.News;
import com.intap.ESG.Models.Products;
import com.intap.ESG.Models.Status;
import com.intap.ESG.Models.DTO.ContractsDto;
import com.intap.ESG.Models.DTO.ContractsPageData;
import com.intap.ESG.Models.DTO.ContractsStatusDto;
import com.intap.ESG.Models.DTO.PaginationData;
import com.intap.ESG.Service.IAssociatedService;
import com.intap.ESG.Service.IClientService;
import com.intap.ESG.Service.IConfigurationsService;
import com.intap.ESG.Service.IContractDetailsService;
import com.intap.ESG.Service.IContractNewsService;
import com.intap.ESG.Service.IContractStatusService;
import com.intap.ESG.Service.IContractsService;
import com.intap.ESG.Service.IDataCreditService;
import com.intap.ESG.Service.IEmployeeService;
import com.intap.ESG.Service.IInvoicesService;
import com.intap.ESG.Service.INewsService;
import com.intap.ESG.Service.IProductsService;
import com.intap.ESG.Service.IStatusService;
import com.intap.ESG.Service.impl.UploadFileServiceImpl;
import com.intap.ESG.util.Constant;


@RestController
@RequestMapping(path = "contracts")
public class ContractsRestController {

	@Autowired
	private IContractsService contractsService;

	@Autowired
	private IContractDetailsService contractDetailsService;

	@Autowired
	private IContractNewsService contractNewsService;

	@Autowired
	private IClientService clientService;;

	@Autowired
	private IProductsService productsService;

	@Autowired
	private INewsService newsService;

	@Autowired
	private IConfigurationsService configurationService;

	@Autowired
	private IInvoicesService invoicesService;

	@Autowired
	private IStatusService statusService;

	@Autowired
	private IContractStatusService contractStatusService;
	
	@Autowired
	private IEmployeeService employeeService;
	
	@Autowired
	private InvoicesRestController invoicesRestController;
	
	@Autowired
	private UploadFileServiceImpl upload;
	
	@Autowired
	private IAssociatedService associateService;
	
	@Autowired
	private IDataCreditService datacreditService;
	
	

	@PreAuthorize("hasAuthority('CONTRACTS_READ')")
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> findAll() {
		Map<String, Object> response = new HashMap<>();
		
		
		//Consultamos si hay facturas para cambiarles los estados del contrato
		invoicesRestController.changeStatusAuto();

		List<Contracts> contracts = contractsService.findAll();
		List<ContractsDto> contractDtos = new ArrayList<>();

		contracts.forEach(cont -> {

			double contracValue = contractDetailsService.contractValue(cont);
			Integer invoicesOpen = invoicesService.countInvoicesOpenOrClose(cont, "Abierta");
			Integer invoicesClose = invoicesService.countInvoicesOpenOrClose(cont, "Cerrada");

			LocalDate fecha = cont.getEndingDate();
			String ffin = null;
			if (fecha == null) {
				ffin = "";
			} else {
				ffin = fecha.toString();
			}

			// saber el estado de cada contrato
			String estado = "";
			String fechaEstado = "";
			boolean datacredit = false;
			String urldc = "";
			LocalDate dateNotification = null;
			LocalDate dateReport = null;
			LocalDate dateDeleteReport = null;
			Integer idDataCredit = null;
			
			if(cont.getIdClient().getIdAssociated().getDatacredit() != null) {
				datacredit = true;
				urldc = cont.getIdClient().getIdAssociated().getDatacredit().getFile();
				dateNotification = cont.getIdClient().getIdAssociated().getDatacredit().getNotificationDate();
				dateReport = cont.getIdClient().getIdAssociated().getDatacredit().getReporDate();
				dateDeleteReport = cont.getIdClient().getIdAssociated().getDatacredit().getReportDeleteDate();
				idDataCredit = cont.getIdClient().getIdAssociated().getDatacredit().getId();
			}
			
			
			// se consulta el ultimo estado del contrato para ser asignado al DTO
			List<ContractStatus> listEstados = contractStatusService.lastStateInvoice(cont.getId());

			if (listEstados.size() > 0) {
				estado = listEstados.get(0).getStatus().getName();
				fechaEstado = listEstados.get(0).getCreateAt().toString();
			} else {
				estado = "No tiene estado asignado";
			}

			// se arma el objeto DTO con la informacion de cada contrato
			contractDtos.add(
					ContractsDto.builder()
					.id(cont.getId())
					.consecutive(cont.getConsecutive())
					.address(cont.getAddress())
					.startDate(cont.getStartDate().toString())
					.endingDate(ffin)
					.billingStarDate(cont.getBillingStarDate().toString())
					.cuttingDay(cont.getCuttingDay())
					.expirationDay(cont.getExpirationDay())
					.startPeriod(cont.getStartPeriod())
					.periodicity(cont.getPeriodicity())
					.ipClient(cont.getIpClient())
					.macClient(cont.getMacClient())
					.serialEquipment(cont.getSerialEquipment())
					.clientId(cont.getIdClient().getId())
					.associateId(cont.getIdClient().getIdAssociated().getId())
					.associateName(cont.getIdClient().getIdAssociated().getFirstName() + " "+ cont.getIdClient().getIdAssociated().getLastName())
					.associateDocumento(cont.getIdClient().getIdAssociated().getIdentificationNumber())
					.associatePhone(cont.getIdClient().getIdAssociated().getCellPhoneNumber())
					.celphone2(cont.getIdClient().getCellPhoneNumber2())
					.associateEmail(cont.getIdClient().getIdAssociated().getEmail())
					.activo(cont.enabled)
					.facOpen(invoicesOpen)
					.factClose(invoicesClose).contractValue(contracValue)
					.retirementReason(cont.getRetirementReason())
					.retirementObservation(cont.getRetirementObservation())
					.deliveredProduct(cont.isDeliveredProduct())
					.city(cont.getCity())
					.neighborhood(cont.getNeighborhood())
					.type(cont.getType())
					.type(cont.getType())
					.noBill(cont.noBill)
					.estado(estado)
					.statusDate(fechaEstado)
					.datacredit(datacredit)
					.urldc(urldc)
					.dateNotification(dateNotification)
					.dateReport(dateReport)
					.dateDeleteReport(dateDeleteReport)
					.idDataCredit(idDataCredit)
					.build());
		});

		response.put("mensaje", "Listado de contratos.");
		response.put("data", contractDtos);

		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}

	@PreAuthorize("hasAuthority('CONTRACTS_READ')")
	@GetMapping(path = "/enabled", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> findAllEnabled() {
		Map<String, Object> response = new HashMap<>();
		// se consultan los contratos activos
		List<Contracts> contracts = contractsService.findContracsEnabled();
		List<ContractsDto> contractDtos = new ArrayList<>();

		contracts.forEach(cont -> {

			double contracValue = contractDetailsService.contractValue(cont);
			Integer invoicesOpen = invoicesService.countInvoicesOpenOrClose(cont, "Abierta");
			Integer invoicesClose = invoicesService.countInvoicesOpenOrClose(cont, "Cerrada");

			LocalDate fecha = cont.getEndingDate();
			String ffin = null;
			if (fecha == null) {
				ffin = "";
			} else {
				ffin = fecha.toString();
			}

			String estado = "";
			String fechaEstado = "";
			boolean datacredit = false;
			String urldc = "";
			LocalDate dateNotification = null;
			LocalDate dateReport = null;
			LocalDate dateDeleteReport = null;
			Integer idDataCredit = null;
			
			if(cont.getIdClient().getIdAssociated().getDatacredit() != null) {
				datacredit = true;
				urldc = cont.getIdClient().getIdAssociated().getDatacredit().getFile();
				dateNotification = cont.getIdClient().getIdAssociated().getDatacredit().getNotificationDate();
				dateReport = cont.getIdClient().getIdAssociated().getDatacredit().getReporDate();
				dateDeleteReport = cont.getIdClient().getIdAssociated().getDatacredit().getReportDeleteDate();
				idDataCredit = cont.getIdClient().getIdAssociated().getDatacredit().getId();
			}
			
			// se consulta el ultimo estado asignado al contrato
			List<ContractStatus> listEstados = contractStatusService.lastStateInvoice(cont.getId());

			if (listEstados.size() > 0) {
				estado = listEstados.get(0).getStatus().getName();
				fechaEstado = listEstados.get(0).getCreateAt().toString();
			} else {
				estado = "No tiene estado asignado";
			}
			
			// se agregan a la lista solo los contratos conectados
			if (estado.equals("CONECTADO")) {
				contractDtos.add(ContractsDto.builder()
						.id(cont.getId())
						.consecutive(cont.getConsecutive())
						.address(cont.getAddress())
						.startDate(cont.getStartDate().toString())
						.endingDate(ffin)
						.billingStarDate(cont.getBillingStarDate().toString())
						.cuttingDay(cont.getCuttingDay())
						.expirationDay(cont.getExpirationDay())
						.startPeriod(cont.getStartPeriod())
						.periodicity(cont.getPeriodicity())
						.ipClient(cont.getIpClient())
						.macClient(cont.getMacClient())
						.serialEquipment(cont.getSerialEquipment())
						.clientId(cont.getIdClient().getId())
						.associateId(cont.getIdClient().getIdAssociated().getId())
						.associateName(cont.getIdClient().getIdAssociated().getFirstName() + " "+ cont.getIdClient().getIdAssociated().getLastName())
						.associateDocumento(cont.getIdClient().getIdAssociated().getIdentificationNumber())
						.activo(cont.enabled).facOpen(invoicesOpen)
						.associatePhone(cont.getIdClient().getIdAssociated().getCellPhoneNumber())
						.celphone2(cont.getIdClient().getCellPhoneNumber2())
						.factClose(invoicesClose).contractValue(contracValue)
						.retirementReason(cont.getRetirementReason())
						.retirementObservation(cont.getRetirementObservation())
						.city(cont.getCity())
						.neighborhood(cont.getNeighborhood())
						.type(cont.getType())
						.noBill(cont.noBill)
						.diasMora(cont.getDaysPastDue())
						.datacredit(datacredit)
						.urldc(urldc)
						.dateNotification(dateNotification)
						.dateReport(dateReport)
						.dateDeleteReport(dateDeleteReport)
						.idDataCredit(idDataCredit)
						.build());
			}

		});

		response.put("mensaje", "Listado de contratos Activos.");
		response.put("data", contractDtos);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}

	
	@PreAuthorize("hasAuthority('CONTRACTS_READ')")
	@GetMapping(path = "/enabledAll", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> findAllEnabledAll() {
		Map<String, Object> response = new HashMap<>();

		List<Contracts> contracts = contractsService.findContracsEnabled();
		List<ContractsDto> contractDtos = new ArrayList<>();

		contracts.forEach(cont -> {

			double contracValue = contractDetailsService.contractValue(cont);
			Integer invoicesOpen = invoicesService.countInvoicesOpenOrClose(cont, "Abierta");
			Integer invoicesClose = invoicesService.countInvoicesOpenOrClose(cont, "Cerrada");

			LocalDate fecha = cont.getEndingDate();
			String ffin = null;
			if (fecha == null) {
				ffin = "";
			} else {
				ffin = fecha.toString();
			}

			@SuppressWarnings("unused")
			String estado = "";
			String fechaEstado = "";
			
			boolean datacredit = false;
			String urldc = "";
			LocalDate dateNotification = null;
			LocalDate dateReport = null;
			LocalDate dateDeleteReport = null;
			Integer idDataCredit = null;
			
			if(cont.getIdClient().getIdAssociated().getDatacredit() != null) {
				datacredit = true;
				urldc = cont.getIdClient().getIdAssociated().getDatacredit().getFile();
				dateNotification = cont.getIdClient().getIdAssociated().getDatacredit().getNotificationDate();
				dateReport = cont.getIdClient().getIdAssociated().getDatacredit().getReporDate();
				dateDeleteReport = cont.getIdClient().getIdAssociated().getDatacredit().getReportDeleteDate();
				idDataCredit = cont.getIdClient().getIdAssociated().getDatacredit().getId();
			}
			
			List<ContractStatus> listEstados = contractStatusService.lastStateInvoice(cont.getId());

			if (listEstados.size() > 0) {
				estado = listEstados.get(0).getStatus().getName();
				fechaEstado = listEstados.get(0).getCreateAt().toString();
			} else {
				estado = "No tiene estado asignado";
			}

		
				contractDtos.add(ContractsDto.builder()
						.id(cont.getId())
						.consecutive(cont.getConsecutive())
						.address(cont.getAddress())
						.startDate(cont.getStartDate().toString())
						.endingDate(ffin)
						.billingStarDate(cont.getBillingStarDate().toString())
						.cuttingDay(cont.getCuttingDay())
						.expirationDay(cont.getExpirationDay())
						.startPeriod(cont.getStartPeriod())
						.periodicity(cont.getPeriodicity())
						.ipClient(cont.getIpClient())
						.macClient(cont.getMacClient())
						.serialEquipment(cont.getSerialEquipment())
						.clientId(cont.getIdClient().getId())
						.associateId(cont.getIdClient().getIdAssociated().getId())
						.associateName(cont.getIdClient().getIdAssociated().getFirstName() + " "+ cont.getIdClient().getIdAssociated().getLastName())
						.associateDocumento(cont.getIdClient().getIdAssociated().getIdentificationNumber())
						.activo(cont.enabled).facOpen(invoicesOpen)
						.factClose(invoicesClose).contractValue(contracValue)
						.retirementReason(cont.getRetirementReason())
						.retirementObservation(cont.getRetirementObservation())
						.city(cont.getCity())
						.neighborhood(cont.getNeighborhood())
						.type(cont.getType())
						.noBill(cont.noBill)
						.diasMora(cont.getDaysPastDue())
						.associatePhone(cont.getIdClient().getIdAssociated().getCellPhoneNumber())
						.celphone2(cont.getIdClient().getCellPhoneNumber2())
						.datacredit(datacredit)
						.urldc(urldc)
						.dateNotification(dateNotification)
						.dateReport(dateReport)
						.dateDeleteReport(dateDeleteReport)
						.idDataCredit(idDataCredit)
						.build());

		});

		response.put("mensaje", "Listado de contratos Activos.");
		response.put("data", contractDtos);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}
	
	@PreAuthorize("hasAuthority('CONTRACTS_READ')")
	@GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> findById(@PathVariable Long id) {

		Contracts cont = null;
		Map<String, Object> response = new HashMap<>();
		ContractsDto contractDtos = new ContractsDto();

		try {

			cont = contractsService.findById(id);

		} catch (DataAccessException e) {
			response.put("mensaje", "Error al realizar la consulta en la base de datos");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		if (cont == null) {
			response.put("mensaje",
					"¡El producto ID: ".concat(id.toString().concat(" no existe en la base de datos!")));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}

		double contracValue = contractDetailsService.contractValue(cont);
		LocalDate fecha = cont.getEndingDate();
		String ffin = null;
		if (fecha == null) {
			ffin = "";
		} else {
			ffin = fecha.toString();
		}
		
		boolean datacredit = false;
		String urldc = "";
		LocalDate dateNotification = null;
		LocalDate dateReport = null;
		LocalDate dateDeleteReport = null;
		Integer idDataCredit = null;
		
		if(cont.getIdClient().getIdAssociated().getDatacredit() != null) {
			datacredit = true;
			urldc = cont.getIdClient().getIdAssociated().getDatacredit().getFile();
			dateNotification = cont.getIdClient().getIdAssociated().getDatacredit().getNotificationDate();
			dateReport = cont.getIdClient().getIdAssociated().getDatacredit().getReporDate();
			dateDeleteReport = cont.getIdClient().getIdAssociated().getDatacredit().getReportDeleteDate();
			idDataCredit = cont.getIdClient().getIdAssociated().getDatacredit().getId();
		}

		Integer invoicesOpen = invoicesService.countInvoicesOpenOrClose(cont, "Abierta");
		Integer invoicesClose = invoicesService.countInvoicesOpenOrClose(cont, "Cerrada");

		
		// se arma el objeto con toda la informacion de contrato
		contractDtos = ContractsDto.builder()
				.id(cont.getId())
				.consecutive(cont.getConsecutive())
				.address(cont.getAddress())
				.startDate(cont.getStartDate().toString())
				.endingDate(ffin)
				.billingStarDate(cont.getBillingStarDate().toString())
				.cuttingDay(cont.getCuttingDay())
				.expirationDay(cont.getExpirationDay())
				.startPeriod(cont.getStartPeriod())
				.periodicity(cont.getPeriodicity())
				.ipClient(cont.getIpClient())
				.macClient(cont.getMacClient())
				.serialEquipment(cont.getSerialEquipment())	
				.clientId(cont.getIdClient().getId())
				.associateId(cont.getIdClient().getIdAssociated().getId())
				.associateName(cont.getIdClient().getIdAssociated().getFirstName() + " "+ cont.getIdClient().getIdAssociated().getLastName())
				.associateDocumento(cont.getIdClient().getIdAssociated().getIdentificationNumber())
				.activo(cont.enabled)
				.facOpen(invoicesOpen)
				.factClose(invoicesClose)
				.contractValue(contracValue)
				.retirementReason(cont.getRetirementReason())
				.retirementObservation(cont.getRetirementObservation())
				.city(cont.getCity())
				.neighborhood(cont.getNeighborhood())
				.type(cont.getType())
				.associatePhone(cont.getIdClient().getIdAssociated().getCellPhoneNumber())
				.celphone2(cont.getIdClient().getCellPhoneNumber2())
				.noBill(cont.noBill)
				.datacredit(datacredit)
				.urldc(urldc)
				.dateNotification(dateNotification)
				.dateReport(dateReport)
				.dateDeleteReport(dateDeleteReport)
				.idDataCredit(idDataCredit)
				.build();

		List<ContractDetails> cd = contractDetailsService.findByContracts(cont);
		List<ContractNews> cn = contractNewsService.findByContracts(cont);

		response.put("contract", contractDtos);
		response.put("details", cd);
		response.put("news", cn);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}

	@PreAuthorize("hasAuthority('CONTRACTS_READ')")
	@GetMapping(path = "FindNewsByIdContract/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?>FindNewsByIdContract(@PathVariable Long id){
		
		
		Contracts cont = null;
		Map<String, Object> response = new HashMap<>();

		try {

			cont = contractsService.findById(id);

		} catch (DataAccessException e) {
			response.put("mensaje", "Error al realizar la consulta en la base de datos");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		List<ContractNews> cn = contractNewsService.findByContracts(cont);
		
		response.put("news", cn);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);

	}
	
	
	@PreAuthorize("hasAuthority('CONTRACTS_READ')")
	@GetMapping(path = "/consecutives", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> Configuration() {

		Map<String, Object> response = new HashMap<>();

		// se busnca la informacion de configuracion de los consecutivos que se manejan en la app
		Configurations config = configurationService.findById((long) 1);
		Integer consecuContrato = Integer.parseInt(config.getConsecutiveContrato()) + 1;
		Integer consecuFactura = Integer.parseInt(config.getConsecutiveFactura()) + 1;
		Integer consecutiveRefPago = Integer.parseInt(config.getConsecutiveRefPago()) + 1;
		Integer consecutivePayment = Integer.parseInt(config.getConsecutivePaymemt()) + 1;
		Integer consecutiveProducts = Integer.parseInt(config.getConsecutiveProducts()) + 1;

		
		// se arma el objeto con la configuracion de los consecutivos
		Configurations conf = Configurations.builder().id(config.getId())
				.consecutiveContrato(consecuContrato.toString())
				.consecutiveFactura(consecuFactura.toString())
				.consecutivePaymemt(consecutivePayment.toString())
				.consecutiveRefPago(consecutiveRefPago.toString())
				.consecutiveProducts(consecutiveProducts.toString())
				.textoFactura(config.getTextoFactura()).build();

		response.put("data", conf);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}

	@PreAuthorize("hasAuthority('CONTRACTS_READ')")
	@GetMapping(path = "GetByAssociate/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> findContractByAssociate(@PathVariable int id) {

		Contracts cont = null;
		Map<String, Object> response = new HashMap<>();
		ContractsDto contractDtos = new ContractsDto();

		try {

			cont = contractsService.findById((long) id);

			if (cont == null) {
				response.put("mensaje",
						"¡El contrato ID: ".concat(Integer.toString(id).concat(" no existe en la base de datos!")));
				return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
			}

		} catch (DataAccessException e) {
			response.put("mensaje", "Error al realizar la consulta en la base de datos");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		LocalDate fecha = cont.getEndingDate();
		String ffin = null;
		boolean datacredit = false;
		String urldc = "";
		LocalDate dateNotification = null;
		LocalDate dateReport = null;
		LocalDate dateDeleteReport = null;
		Integer idDataCredit = null;
		
		if(cont.getIdClient().getIdAssociated().getDatacredit() != null) {
			datacredit = true;
			urldc = cont.getIdClient().getIdAssociated().getDatacredit().getFile();
			dateNotification = cont.getIdClient().getIdAssociated().getDatacredit().getNotificationDate();
			dateReport = cont.getIdClient().getIdAssociated().getDatacredit().getReporDate();
			dateDeleteReport = cont.getIdClient().getIdAssociated().getDatacredit().getReportDeleteDate();
			idDataCredit = cont.getIdClient().getIdAssociated().getDatacredit().getId();
		}
		if (fecha == null) {
			ffin = "";
		} else {
			ffin = fecha.toString();
		}
		double contracValue = contractDetailsService.contractValue(cont);
		Integer invoicesOpen = invoicesService.countInvoicesOpenOrClose(cont, "Abierta");
		Integer invoicesClose = invoicesService.countInvoicesOpenOrClose(cont, "Cerrada");

		contractDtos = ContractsDto.builder()
				.id(cont.getId()).consecutive(cont.getConsecutive())
				.address(cont.getAddress())
				.startDate(cont.getStartDate().toString())
				.endingDate(ffin)
				.billingStarDate(cont.getBillingStarDate().toString())
				.cuttingDay(cont.getCuttingDay())
				.expirationDay(cont.getExpirationDay())
				.startPeriod(cont.getStartPeriod())
				.periodicity(cont.getPeriodicity())
				.ipClient(cont.getIpClient())
				.macClient(cont.getMacClient())
				.serialEquipment(cont.getSerialEquipment())
				.clientId(cont.getIdClient().getId())
				.associateId(cont.getIdClient().getIdAssociated().getId())
				.clientId(cont.getIdClient().getId())
				.associateName(cont.getIdClient().getIdAssociated().getFirstName() + " "+ cont.getIdClient().getIdAssociated().getLastName())
				.associateDocumento(cont.getIdClient().getIdAssociated().getIdentificationNumber())
				.activo(cont.enabled)
				.facOpen(invoicesOpen)
				.factClose(invoicesClose)
				.contractValue(contracValue)
				.retirementReason(cont.getRetirementReason())
				.retirementObservation(cont.getRetirementObservation())
				.city(cont.getCity())
				.neighborhood(cont.getNeighborhood())
				.type(cont.getType())
				.associatePhone(cont.getIdClient().getIdAssociated().getCellPhoneNumber())
				.celphone2(cont.getIdClient().getCellPhoneNumber2())
				.noBill(cont.noBill)
				.diasMora(cont.getDaysPastDue())
				.datacredit(datacredit)
				.urldc(urldc)
				.dateNotification(dateNotification)
				.dateReport(dateReport)
				.dateDeleteReport(dateDeleteReport)
				.idDataCredit(idDataCredit)
				.build();

		List<ContractDetails> cd = contractDetailsService.findByContracts(cont);
		List<ContractNews> cn = contractNewsService.findByContractsEnabled(cont);
		Integer facturasGeneradas = invoicesService.countInvoicesGenerated(cont);

		response.put("contract", contractDtos);
		response.put("details", cd);
		response.put("news", cn);
		response.put("invoicesGenerated", facturasGeneradas);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}

	@SuppressWarnings("unchecked")
	@Transactional
	@PreAuthorize("hasAuthority('CONTRACTS_WRITE')")
	@PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> create(@RequestBody Map<String, Object> data) {

		Map<String, Object> response = new HashMap<>();

		Map<String, Object> contracts2 = null;
		ArrayList<?> contractDetail2 = null;
		ArrayList<?> contractNews2 = null;
		Client client = null;
		Products product = null;

		//// ---------------- INICIO DE LAS VALIDACIONES ---------------- ////

		contracts2 = (Map<String, Object>) data.get("contract");
		JSONObject contracts = new JSONObject(contracts2);

		contractDetail2 = (ArrayList<?>) data.get("contractDetail");
		JSONArray contractDetail = new JSONArray(contractDetail2);

		contractNews2 = (ArrayList<?>) data.get("contractNews");
		JSONArray contractNews = new JSONArray(contractNews2);

		// Validar que exista un Json con el nombre DatosBasicos
		if (contracts2 == null || contracts.length() == 0) {
			response.put("mensaje", "Error: no existe un array con el nombre 'contract'");
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}

		if (contractDetail2 == null || contractDetail.length() == 0) {
			response.put("mensaje", "Error: no existe un array con el nombre 'contractDetail'");
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}


		client = clientService.findById(Integer.parseInt(contracts.get("idClient").toString()));

		if (client == null) {
			response.put("mensaje",
					"Error: no existe un cliente asignado al assicado con el ID: " + contracts.get("idClient").toString());
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}

		// validar que no este duplicado el consecutivo
		int countConsecu = contractsService.countConsecutive(contracts.get("consecutive").toString());
		if (countConsecu >= 1) {

			response.put("mensaje", "Error: El consecutivo número '" + contracts.get("consecutive").toString()
					+ "' ya se encuentra asignado a un contrato");
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);

		}

		// Validamos que la IP no esta repetida en otro contrato
		Contracts contract = null;
		contract = contractsService.findByIp(contracts.get("ipClient").toString());

		if (contract != null) {

			response.put("contract",
					"Error: La IP '" + contracts.get("ipClient").toString()
							+ "' se encuentra asignada al contrato con consecutivo No. '" + contract.getConsecutive()
							+ "' asignado al cliente '" + contract.getIdClient().getIdAssociated().getFirstName() + " "
							+ contract.getIdClient().getIdAssociated().getLastName() + "'.");
			
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}

		//// ---------------- FIN DE LAS VALIDACIONES ---------------- ////

		// Guardo el contrato

		// actualizamos el consecutivo del contrato
		Configurations config = configurationService.findById((long) 1);
		config.setConsecutiveContrato(contracts.get("consecutive").toString());
		configurationService.save(config);

		// Guardamos el contrato
		Contracts cont = new Contracts();
		cont.setConsecutive(contracts.get("consecutive").toString());
		cont.setAddress(contracts.get("address").toString());
		cont.setCity(contracts.get("city").toString());
		cont.setNeighborhood(contracts.get("neighborhood").toString());
		cont.setStartDate(LocalDate.parse(contracts.get("startDate").toString(), DateTimeFormatter.BASIC_ISO_DATE));
		cont.setBillingStarDate(
				LocalDate.parse(contracts.get("billingStarDate").toString(), DateTimeFormatter.BASIC_ISO_DATE));
		cont.setCuttingDay(Integer.parseInt(contracts.get("cuttingDay").toString()));
		cont.setExpirationDay(Integer.parseInt(contracts.get("expirationDay").toString()));
		cont.setStartPeriod(Integer.parseInt(contracts.get("startPeriod").toString()));
		cont.setPeriodicity(contracts.get("periodicity").toString());
		cont.setIpClient(contracts.get("ipClient").toString());
		cont.setMacClient(contracts.get("macClient").toString());
		cont.setSerialEquipment(contracts.get("serialEquipment").toString());
		cont.setEnabled(Boolean.parseBoolean(contracts.get("enabled").toString()));
		cont.setRetirementObservation("N/A");
		cont.setIdClient(client);
		cont.setType(contracts.get("type").toString());
		cont.setNoBill(Boolean.parseBoolean(contracts.get("noBill").toString()));

		Contracts contractSave = contractsService.save(cont);

		// se guarda el primer estado de la factura
		Status status = statusService.findById((long) 1);
		
		
		
		Employee userCrea = employeeService.findById(Integer.parseInt(contracts.get("idUserCrea").toString()));

		ContractStatus contracStatus = new ContractStatus();
		//Se toma la fecha actual para colombia
		LocalDate fecha = LocalDate.now(ZoneId.of("UTC-5"));
		contracStatus.setCreateAt(fecha);
		
		contracStatus.setContract(contractSave);
		contracStatus.setStatus(status);
		contracStatus.setObservation("N/A");
		contracStatus.setUserCrea(userCrea);

		contractStatusService.save(contracStatus);

		// Guardamos el detalle del Contrato

		for (int i = 0; i < contractDetail.length(); i++) {

			product = null;

			ContractDetails details = new ContractDetails();

			JSONObject dato = contractDetail.getJSONObject(i);

			product = productsService.findById(Long.parseLong(dato.get("productId").toString()));

			details.setTaxes(Double.parseDouble(dato.get("taxes").toString()));
			details.setUnitPrice(Double.parseDouble(dato.get("unitPrice").toString()));
			details.setAmount(Integer.parseInt(dato.get("amount").toString()));
			details.setObservations(dato.get("Observations").toString());
			details.setContracts(contractSave);
			details.setProducts(product);

			contractDetailsService.save(details);

		}

		// Guardar las novedades si las envian
		if (contractNews != null) {

			for (int i = 0; i < contractNews.length(); i++) {

				News news = null;

				ContractNews cNews = new ContractNews();

				JSONObject dato = contractNews.getJSONObject(i);

				news = newsService.findById(Long.parseLong(dato.get("newsId").toString()));

				cNews.setTaxes(Double.parseDouble(dato.get("taxes").toString()));
				cNews.setUnitPrice(Double.parseDouble(dato.get("unitPrice").toString()));
				cNews.setAmount(Integer.parseInt(dato.get("amount").toString()));
				cNews.setObservations(dato.get("Observations").toString());
				cNews.setNumberOfMonths(Integer.parseInt(dato.get("numberOfMonths").toString()));
				cNews.setContracts(contractSave);
				cNews.setBilledMonths(0);
				cNews.setNews(news);

				contractNewsService.save(cNews);

			}

		}

		response.put("mensaje", "¡El contrato ha sido creado con éxito!");
		response.put("data", contractSave);

		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);

	}

	@SuppressWarnings("unchecked")
	@PreAuthorize("hasAuthority('CONTRACTS_WRITE')")
	@PutMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> update(@PathVariable Long id, @RequestBody Map<String, Object> data) {

		Map<String, Object> response = new HashMap<>();

		Map<String, Object> contracts2 = null;
		ArrayList<?> contractDetail2 = null;
		ArrayList<?> contractNews2 = null;
		Client client = null;
		Products product = null;

		//// ---------------- INICIO DE LAS VALIDACIONES ---------------- ////

		contracts2 = (Map<String, Object>) data.get("contract");
		JSONObject contracts = new JSONObject(contracts2);

		contractDetail2 = (ArrayList<?>) data.get("contractDetail");
		JSONArray contractDetail = new JSONArray(contractDetail2);

		contractNews2 = (ArrayList<?>) data.get("contractNews");
		JSONArray contractNews = new JSONArray(contractNews2);
		
		// Validar que exista un Json con el nombre DatosBasicos
		if (contracts2 == null || contracts.length() == 0) {
			response.put("mensaje", "Error: no existe un array con el nombre 'contract'");
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}

		if (contractDetail2 == null || contractDetail.length() == 0) {
			response.put("mensaje", "Error: no existe un array con el nombre 'contractDetail'");
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}
		

		client = clientService.findById(Integer.parseInt(contracts.get("idClient").toString()));

		if (client == null) {
			response.put("mensaje",
					"Error: no existe un asociado con el ID: " + contracts.get("idClient").toString());
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}

		// validar que no este duplicado elconsecutivo
		int countConsecu = contractsService.countConsecutiveUpdate(contracts.get("consecutive").toString(), id);
		if (countConsecu >= 1) {

			response.put("mensaje", "Error: El consecutivo Número '" + contracts.get("consecutive").toString()
					+ "' ya se encuentra asignado a un contrato");
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);

		}

		// Validamos que la ipe no esta repetida en otro contrato
		Contracts contract = null;
		contract = contractsService.findByIpUpdate(contracts.get("ipClient").toString(), id);

		if (contract != null) {

			response.put("contract",
					"Error: La IP '" + contracts.get("ipClient").toString()
							+ "' se encuentra asignada al contrato con consecutivo No. '" + contract.getConsecutive()
							+ "' asignado al cliente '" + contract.getIdClient().getIdAssociated().getFirstName() + " "
							+ contract.getIdClient().getIdAssociated().getLastName() + "'.");
			
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}

		//// ---------------- FIN DE LAS VALIDACIONES ---------------- ////

		// Guardo el contrato

		Contracts cont = new Contracts();
		// 1. Consultamos el contrato
		cont = contractsService.findById(id);
		// 2. Eliminamos los detalles de ese contrato
		List<ContractDetails> cd = contractDetailsService.findByContracts(cont);
		if (cd.size() > 0) {
			contractDetailsService.deleteAll(cd);
		}
		// 3. Eiminamos las novedades del contratos
		List<ContractNews> cn = contractNewsService.findByContracts(cont);
		if (cn.size() > 0) {
			contractNewsService.deleteAll(cn);
		}

		// Actualizamos la informacion basica del contrato
		cont.setConsecutive(contracts.get("consecutive").toString());
		cont.setAddress(contracts.get("address").toString());
		cont.setCity(contracts.get("city").toString());
		cont.setNeighborhood(contracts.get("neighborhood").toString());
		cont.setStartDate(LocalDate.parse(contracts.get("startDate").toString(), DateTimeFormatter.BASIC_ISO_DATE));
		cont.setBillingStarDate(
				LocalDate.parse(contracts.get("billingStarDate").toString(), DateTimeFormatter.BASIC_ISO_DATE));
		cont.setCuttingDay(Integer.parseInt(contracts.get("cuttingDay").toString()));
		cont.setExpirationDay(Integer.parseInt(contracts.get("expirationDay").toString()));
		cont.setStartPeriod(Integer.parseInt(contracts.get("startPeriod").toString()));
		cont.setPeriodicity(contracts.get("periodicity").toString());
		cont.setIpClient(contracts.get("ipClient").toString());
		cont.setMacClient(contracts.get("macClient").toString());
		cont.setSerialEquipment(contracts.get("serialEquipment").toString());
		cont.setEnabled(Boolean.parseBoolean(contracts.get("enabled").toString()));
		cont.setRetirementObservation("N/A");
		cont.setIdClient(client);
		cont.setType(contracts.get("type").toString());
		cont.setNoBill(Boolean.parseBoolean(contracts.get("noBill").toString()));

		Contracts contractSave = contractsService.save(cont);

		// Guardamos el detalle del Contrato

		for (int i = 0; i < contractDetail.length(); i++) {

			product = null;

			ContractDetails details = new ContractDetails();

			JSONObject dato = contractDetail.getJSONObject(i);

			product = productsService.findById(Long.parseLong(dato.get("productId").toString()));

			details.setTaxes(Double.parseDouble(dato.get("taxes").toString()));
			details.setUnitPrice(Double.parseDouble(dato.get("unitPrice").toString()));
			details.setAmount(Integer.parseInt(dato.get("amount").toString()));
			details.setObservations(dato.get("Observations").toString());
			details.setContracts(contractSave);
			details.setProducts(product);

			contractDetailsService.save(details);

		}

		// Guardar las novedades si las envian
		if (contractNews != null) {

			for (int i = 0; i < contractNews.length(); i++) {

				News news = null;

				ContractNews cNews = new ContractNews();

				JSONObject dato = contractNews.getJSONObject(i);

				news = newsService.findById(Long.parseLong(dato.get("newsId").toString()));

				cNews.setTaxes(Double.parseDouble(dato.get("taxes").toString()));
				cNews.setUnitPrice(Double.parseDouble(dato.get("unitPrice").toString()));
				cNews.setAmount(Integer.parseInt(dato.get("amount").toString()));
				cNews.setObservations(dato.get("Observations").toString());
				cNews.setNumberOfMonths(Integer.parseInt(dato.get("numberOfMonths").toString()));
				cNews.setBilledMonths(Integer.parseInt(dato.get("billedMonths").toString()));
				cNews.setContracts(contractSave);
				cNews.setNews(news);

				contractNewsService.save(cNews);

			}

		}

		response.put("mensaje", "¡El contrato ha sido actualizado con éxito!");
		response.put("data", contractSave);

		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);

	}

	@PreAuthorize("hasAuthority('CONTRACTS_DELETE')")
	@DeleteMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> delete(@PathVariable Long id) {
		Map<String, Object> response = new HashMap<>();

		Contracts cont = new Contracts();

		// 1. Consultamos el contrato
		cont = contractsService.findById(id);

		// se debe verificar quie el contrato a eliminar no tenga facturas, cuando se

		int invoices = invoicesService.countInvoicesGenerated(cont);

		if (invoices > 0) {
			response.put("mensaje", "No se puede eliminar el contrato ya que tiene facturas asignadas.");
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}

		// 2. Elimonamos los detalles de ese contrato
		List<ContractDetails> cd = contractDetailsService.findByContracts(cont);
		if (cd.size() > 0) {
			contractDetailsService.deleteAll(cd);
		}

		// 3. Eiminamos las novedades del contratos
		List<ContractNews> cn = contractNewsService.findByContracts(cont);
		if (cn.size() > 0) {
			contractNewsService.deleteAll(cn);
		}

		// 4. Eliminamos los estados del contrato
		List<ContractStatus> contractStatus = contractStatusService.findByContract(cont);
		contractStatusService.deleteAll(contractStatus);

		// 5. Eliminamos el contrato
		contractsService.delete(id);

		response.put("data", "Contrato eliminado exitosamente");
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}

	@PreAuthorize("hasAuthority('CONTRACTS_WRITE')")
	@PutMapping(path = "ChangeEnabled/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> ChageStatus(@PathVariable Long id, @RequestBody Map<String, Object> prod) {
		Map<String, Object> response = new HashMap<>();

		// buscamos el contrato
		Contracts contract = contractsService.findById(id);

		Boolean estado = Boolean.parseBoolean(prod.get("enabled").toString());
		
		String e = null;

		// caudo se cambia el estado a true se borra la info de razon de retiro
		if (estado == true) {
			contract.setEnabled(estado);
			contract.setEndingDate(null);
			contract.setRetirementReason(null);
			contract.setDeliveredProduct(false);
			contract.setRetirementObservation("N/A");
			e = "Activado";

		} else {

			// se agregan la razon de retiro y las observaciones
			Boolean equipo = Boolean.parseBoolean(prod.get("retirementProduct").toString());
			
			contract.setEnabled(estado);
			contract.setEndingDate(
					LocalDate.parse(prod.get("retirementDate").toString(), DateTimeFormatter.BASIC_ISO_DATE));
			contract.setDeliveredProduct(equipo);
			contract.setRetirementReason(prod.get("retirementReason").toString());
			contract.setRetirementObservation(prod.get("retirementObservation").toString());
			e = "Inactivado";
			
			// se buscan las facturas abiertas que tenga el contrato
			List<Invoices> invoices = null;
			invoices = invoicesService.findAllInvoicesOpenOrClose(contract, "Abierta");
			
			ContractStatus contracStatus = new ContractStatus();
			
			// se valida que no tenga facturas abiertas y que haya entregado el equipo para cambiar el esatdo a "AL DIA"
			if(invoices.size() == 0 && equipo) {
				Status status = statusService.findByName("AL DIA");
				Employee userCrea = employeeService.findById(1);
				
				contract.setDateDeliveredProduct(
						LocalDate.parse(prod.get("retirementDate").toString(), DateTimeFormatter.BASIC_ISO_DATE));
				
				//Se toma la fecha actual para colombia
				LocalDate fecha = LocalDate.now(ZoneId.of("UTC-5"));
				contracStatus.setCreateAt(fecha);
				
				contracStatus.setContract(contract);
				contracStatus.setStatus(status);
				contracStatus.setObservation("N/A");
				contracStatus.setUserCrea(userCrea);
			}else {
				
				// SI TIENE FACTURAS ABIERTAS O NO SE HA REALIZADO LA ENTREGA DE EQUIPO QUEDA EN ESTADO "CARTERA"
				Status status = statusService.findByName("CARTERA");
				Employee userCrea = employeeService.findById(1);
				
				//Se toma la fecha actual para colombia
				LocalDate fecha = LocalDate.now(ZoneId.of("UTC-5"));
				contracStatus.setCreateAt(fecha);
				
				contracStatus.setContract(contract);
				contracStatus.setStatus(status);
				contracStatus.setObservation("N/A");
				contracStatus.setUserCrea(userCrea);
			}
			
			contractStatusService.save(contracStatus);

		}

		// SE GUARDA LA ACTUALIZACION DEL CONTRATO
		contractsService.save(contract);

		response.put("mensaje", "¡El contrato ha sido " + e + " con éxito!");
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}
	
	@PreAuthorize("hasAuthority('CONTRACTS_WRITE')")
	@PutMapping(path = "ChangeDeliveredProduct/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> ChangeDeliveredProduct(@PathVariable Long id, @RequestBody Map<String, Object> prod) {
		Map<String, Object> response = new HashMap<>();

		Contracts contract = contractsService.findById(id);

		Boolean equipo = Boolean.parseBoolean(prod.get("retirementProduct").toString());
			
		contract.setDateDeliveredProduct(
			LocalDate.parse(prod.get("retirementDate").toString(), DateTimeFormatter.BASIC_ISO_DATE));
		contract.setDeliveredProduct(equipo);
			
		List<Invoices> invoices = null;
		
		// SE BUSCAN LAS FACTURAS ABIERTAS DEL CONTRATO
		invoices = invoicesService.findAllInvoicesOpenOrClose(contract, "Abierta");
			 
		ContractStatus contracStatus = new ContractStatus();
		if(invoices.size() == 0 && equipo) {
			
			// SI NO TIENE FACTURAS ABIERTAS Y ENTRGO EL EQUIPO QUEDA EN ESTADO "AL DIA"
			Status status = statusService.findByName("AL DIA");
			Employee userCrea = employeeService.findById(1);
			//Se toma la fecha actual para colombia
			LocalDate fecha = LocalDate.now(ZoneId.of("UTC-5"));
			contracStatus.setCreateAt(fecha);
			
			contracStatus.setContract(contract);
			contracStatus.setStatus(status);
			contracStatus.setObservation("N/A");
			contracStatus.setUserCrea(userCrea);
		}else {
			// SI TIENE FACTURAS ABIERTAS QUEDA EN ESATDO "CARTERA"
			Status status = statusService.findByName("CARTERA");
			Employee userCrea = employeeService.findById(1);
			//Se toma la fecha actual para colombia
			LocalDate fecha = LocalDate.now(ZoneId.of("UTC-5"));
			contracStatus.setCreateAt(fecha);
			
			contracStatus.setContract(contract);
			contracStatus.setStatus(status);
			contracStatus.setObservation("N/A");
			contracStatus.setUserCrea(userCrea);
		}
			
		contractStatusService.save(contracStatus);

		contractsService.save(contract);

		response.put("mensaje", "¡El contrato ha sido actualizado con éxito!");
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}

	@PreAuthorize("hasAuthority('CONTRACTS_READ')")
	@GetMapping(path = "/ip/{ip}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> findById(@PathVariable String ip) {
		Map<String, Object> response = new HashMap<>();
		Contracts contract = null;
		contract = contractsService.findByIp(ip);
		
		
		// SE VALIDA QUE LA IP QUE SE VA A REGISTRAR EN UN CONTRATO NO SE ENCUENTRE ASIGNADA EN OTRO
		if (contract != null) {

			response.put("contract",
					"La IP '" + ip + "' se encuentra asignada al contrato con consecutivo No. '"
							+ contract.getConsecutive() + "' asignado al cliente '"
							+ contract.getIdClient().getIdAssociated().getFirstName() + " "
							+ contract.getIdClient().getIdAssociated().getLastName() + "'.");
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);

		} else {
			response.put("contract", "La IP digitada está disponible para ser usada");
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
		}

	}

	@PreAuthorize("hasAuthority('CONTRACTS_READ')")
	@GetMapping(path = "/mac/{mac}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> findByMac(@PathVariable String mac) {
		Map<String, Object> response = new HashMap<>();
		Contracts contract = null;
		contract = contractsService.findByMac(mac);

		// SE VALIDA QUE LA MAC QUE SE VA A REGISTRAR EN UN CONTRATO NO SE ENCUENTRE ASIGNADA EN OTRO
		if (contract != null) {

			response.put("contract",
					"La Mac '" + mac + "' se encuentra asignada al contrato con consecutivo No. '"
							+ contract.getConsecutive() + "' asignado al cliente '"
							+ contract.getIdClient().getIdAssociated().getFirstName() + " "
							+ contract.getIdClient().getIdAssociated().getLastName() + "'.");
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);

		} else {
			response.put("contract", "La Mac digitada está disponible para ser usada");
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
		}

	}
	
	@PreAuthorize("hasAuthority('CONTRACTS_READ')")
	@GetMapping(path = "/serial/{serial}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> findBySerial(@PathVariable String serial) {
		Map<String, Object> response = new HashMap<>();
		Contracts contract = null;
		contract = contractsService.findBySerial(serial);

		// SE VALIDA QUE EL SERIAL QUE SE VA A REGISTRAR EN UN CONTRATO NO SE ENCUENTRE ASIGNADO EN OTRO
		if (contract != null) {

			response.put("contract",
					"El serial '" + serial + "' se encuentra asignada al contrato con consecutivo No. '"
							+ contract.getConsecutive() + "' asignado al cliente '"
							+ contract.getIdClient().getIdAssociated().getFirstName() + " "
							+ contract.getIdClient().getIdAssociated().getLastName() + "'.");
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);

		} else {
			response.put("contract", "El serial digitado está disponible para ser usado");
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
		}

	}
	
	
	@PreAuthorize("hasAuthority('CONTRACTS_READ')")
	@GetMapping(path = "/ipUpdate/{ip}/{idContract}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> findByIpUpdate(@PathVariable String ip, @PathVariable Long idContract) {
		Map<String, Object> response = new HashMap<>();
		Contracts contract = null;
		contract = contractsService.findByIpUpdate(ip, idContract);
		// SE VALIDA QUE LA IP QUE SE VA A ACTUALIZAR EN UN CONTRATO NO SE ENCUENTRE ASIGNADA EN OTRO
		if (contract != null) {

			response.put("contract",
					"La IP '" + ip + "' se encuentra asignada al contrato con consecutivo No. '"
							+ contract.getConsecutive() + "' asignado al cliente '"
							+ contract.getIdClient().getIdAssociated().getFirstName() + " "
							+ contract.getIdClient().getIdAssociated().getLastName() + "'.");
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);

		} else {
			response.put("contract", "La IP digitada está disponible para ser usada");
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
		}

	}

	@PreAuthorize("hasAuthority('CONTRACTS_READ')")
	@GetMapping(path = "/macUpdate/{mac}/{idContract}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> findByMacUpdate(@PathVariable String mac, @PathVariable Long idContract) {
		Map<String, Object> response = new HashMap<>();
		Contracts contract = null;
		contract = contractsService.findBymacUpdate(mac, idContract);
		// SE VALIDA QUE LA MAC QUE SE VA A ACTUALIZAR EN UN CONTRATO NO SE ENCUENTRE ASIGNADA EN OTRO
		if (contract != null) {

			response.put("contract",
					"La Mac '" + mac + "' se encuentra asignada al contrato con consecutivo No. '"
							+ contract.getConsecutive() + "' asignado al cliente '"
							+ contract.getIdClient().getIdAssociated().getFirstName() + " "
							+ contract.getIdClient().getIdAssociated().getLastName() + "'.");
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);

		} else {
			response.put("contract", "La Mac digitada está disponible para ser usada");
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
		}

	}

	
	@PreAuthorize("hasAuthority('CONTRACTS_READ')")
	@GetMapping(path = "/serialUpdate/{serial}/{idContract}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> findBySerialUpdate(@PathVariable String serial, @PathVariable Long idContract) {
		Map<String, Object> response = new HashMap<>();
		Contracts contract = null;
		contract = contractsService.findByserialUpdate(serial, idContract);
		// SE VALIDA QUE EL SERIAL QUE SE VA A ACTUALIZAR EN UN CONTRATO NO SE ENCUENTRE ASIGNADO EN OTRO
		if (contract != null) {

			response.put("contract",
					"El serial '" + serial + "' se encuentra asignada al contrato con consecutivo No. '"
							+ contract.getConsecutive() + "' asignado al cliente '"
							+ contract.getIdClient().getIdAssociated().getFirstName() + " "
							+ contract.getIdClient().getIdAssociated().getLastName() + "'.");
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);

		} else {
			response.put("contract", "El serial digitado está disponible para ser usado");
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
		}

	}
	
	// Estados de los contratos

	@PreAuthorize("hasAuthority('CONTRACTS_READ')")
	@GetMapping(path = "/statusList", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> findAllStatus() {
		Map<String, Object> response = new HashMap<>();

		//SE BUSCA LA LISTA DE ESTADOS DE LOS CONTRATOS
		List<Status> status = statusService.findAll();

		response.put("mensaje", "Listado de Estados de contrato");
		response.put("data", status);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}

	// cambiar el estado del contrato

	@Transactional
	@PreAuthorize("hasAuthority('CONTRACTS_WRITE')")
	@PostMapping(path = "/crateStatus", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> createStatus(@RequestBody List<Map<String, Object>> datas) {

		Map<String, Object> response = new HashMap<>();

		Status status = null;
		Contracts contract = null;
		Employee employee = null;

		// SE RECORRE LA LISTA DE CONTRATOS A LOS QUE SE LE HARAN CAMBIO DE ESTADO
		for (Map<String, Object> data : datas) {
			ContractStatus contractStatus = new ContractStatus();

			employee = employeeService.findById(Integer.parseInt(data.get("idUserCrea").toString()));
			
			// SE VALIDA QUE EL USUARIO EXISTA
			if (employee == null) {
				response.put("mensaje", "Error: no existe un empleado con el ID: " + data.get("idUserCrea").toString());
				return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
			}

			contract = contractsService.findById(Long.parseLong(data.get("idContract").toString()));

			// SE VALIDA QUE EL CONTRATO EXISTA
			if (contract == null) {
				response.put("mensaje", "Error: no existe un contrato con el ID: " + data.get("idContract").toString());
				return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
			}

			status = statusService.findById(Long.parseLong(data.get("idStatus").toString()));

			// SE VALIDA QUE EL ESTADO EXISTA
			if (status == null) {
				response.put("mensaje", "Error: no existe un estado con el ID: " + data.get("idStatus").toString());
				return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
			}
			
			if(data.get("fechacambio").toString() != null) {
				contractStatus.setCreateAt(
						LocalDate.parse(data.get("fechacambio").toString(), DateTimeFormatter.BASIC_ISO_DATE));
			}else {
				//Se toma la fecha actual para colombia
				LocalDate fecha = LocalDate.now(ZoneId.of("UTC-5"));
				contractStatus.setCreateAt(fecha);
			}
				
			// SI ES ESTADO ES "SUSPENDIDO SE INACTIVA EL CONTRATO"
			if(status.getName().equals("SUSPENDIDO")) {
				contract.setEnabled(false);
				contractsService.save(contract);
			}
			
			// SE GUARDAN LOS CAMBIOS DEL CAMBIO DE ESTADO
			contractStatus.setContract(contract);
			contractStatus.setStatus(status);
			contractStatus.setUserCrea(employee);
			contractStatus.setObservation(data.get("observation").toString());

			contractStatusService.save(contractStatus);
			
			
		}	

		response.put("data", "Estado asignado exitosamente al contrato.");
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}
	
	@Transactional
	@PreAuthorize("hasAuthority('CONTRACTS_WRITE')")
	@PostMapping(path = "/UpdateObservations", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> UpdateObservations(@RequestBody Map<String, Object> data) {
 
		Map<String, Object> response = new HashMap<>();
		Contracts contract = null;
		
		contract = contractsService.findById(Long.parseLong(data.get("id").toString()));

		if (contract == null) {
			response.put("mensaje", "Error: no existe un contrato con el ID: " + data.get("id").toString());
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}

		contract.setRetirementObservation(data.get("retirementObservation").toString());
		
		contractsService.save(contract);
		
		response.put("data", "Observación actualizada existosamente");
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}
	
	@Transactional(readOnly = true)
	@PreAuthorize("hasAuthority('CONTRACTS_READ')")
	@PostMapping(path = "/pages", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> GetContracs(@RequestBody ContractsPageData contractsPageData){
		Page<Contracts> Lista = contractsService.findAll(contractsPageData.getPage() - 1, contractsPageData.getLimit(), contractsPageData.getNameCliente(), contractsPageData.getConsecutive(), contractsPageData.getIp(), contractsPageData.getCc(), contractsPageData.getFopen(), contractsPageData.getFclose(), contractsPageData.getMac(), contractsPageData.getFechaAfiliacion(), contractsPageData.getBarrio(), contractsPageData.getTelefono(),contractsPageData.getDireccion(), contractsPageData.getValor(), contractsPageData.getActivo(), contractsPageData.getEstado(), contractsPageData.getSubsidiado(), contractsPageData.getFechaFac(), contractsPageData.getSerialEquipo(),
				contractsPageData.getSaldomin(),contractsPageData.getSaldomax());
		ArrayList<ContractsDto> contractslist = new ArrayList<ContractsDto>();
		
		Lista.forEach(cont -> {
			
			double contracValue = contractDetailsService.contractValue(cont);
			Integer invoicesSum = invoicesService.sumInvoicesOpenOrClose(cont, "Abierta");
			
			
			if(invoicesSum == null) {
				invoicesSum = 0;
			}
			
			Integer invoicesOpen = invoicesService.countInvoicesOpenOrClose(cont, "Abierta");
			Integer invoicesClose = invoicesService.countInvoicesOpenOrClose(cont, "Cerrada");

			LocalDate fecha = cont.getEndingDate();
			String ffin = null;
			if (fecha == null) {
				ffin = "";
			} else {
				ffin = fecha.toString();
			}

			// saber el estado de cada contrato
			String estado = "";
			String fechaEstado = "";
			boolean datacredit = false;
			String urldc = "";
			LocalDate dateNotification = null;
			LocalDate dateReport = null;
			LocalDate dateDeleteReport = null;
			Integer idDataCredit = null;
			
			if(cont.getIdClient().getIdAssociated().getDatacredit() != null) {
				datacredit = true;
				urldc = cont.getIdClient().getIdAssociated().getDatacredit().getFile();
				dateNotification = cont.getIdClient().getIdAssociated().getDatacredit().getNotificationDate();
				dateReport = cont.getIdClient().getIdAssociated().getDatacredit().getReporDate();
				dateDeleteReport = cont.getIdClient().getIdAssociated().getDatacredit().getReportDeleteDate();
				idDataCredit = cont.getIdClient().getIdAssociated().getDatacredit().getId();
			}

			List<ContractStatus> listEstados = contractStatusService.lastStateInvoice(cont.getId());

			if (listEstados.size() > 0) {
				estado = listEstados.get(0).getStatus().getName();
				fechaEstado = listEstados.get(0).getCreateAt().toString();
			} else {
				estado = "No tiene estado asignado";
			}
			
			contractslist.add(
					ContractsDto.builder()
					.id(cont.getId())
					.consecutive(cont.getConsecutive())
					.address(cont.getAddress())
					.startDate(cont.getStartDate().toString())
					.endingDate(ffin)
					.billingStarDate(cont.getBillingStarDate().toString())
					.cuttingDay(cont.getCuttingDay())
					.expirationDay(cont.getExpirationDay())
					.startPeriod(cont.getStartPeriod())
					.periodicity(cont.getPeriodicity())
					.ipClient(cont.getIpClient())
					.macClient(cont.getMacClient())
					.serialEquipment(cont.getSerialEquipment())
					.clientId(cont.getIdClient().getId())
					.associateId(cont.getIdClient().getIdAssociated().getId())
					.associateName(cont.getIdClient().getIdAssociated().getFirstName() + " "+ cont.getIdClient().getIdAssociated().getLastName())
					.associateDocumento(cont.getIdClient().getIdAssociated().getIdentificationNumber())
					.associatePhone(cont.getIdClient().getIdAssociated().getCellPhoneNumber())
					.celphone2(cont.getIdClient().getCellPhoneNumber2())
					.associateEmail(cont.getIdClient().getIdAssociated().getEmail())
					.activo(cont.enabled)
					.facOpen(invoicesOpen)
					.factClose(invoicesClose).contractValue(contracValue)
					.retirementReason(cont.getRetirementReason())
					.deliveredProduct(cont.isDeliveredProduct())
					.retirementObservation(cont.getRetirementObservation())
					.city(cont.getCity())
					.neighborhood(cont.getNeighborhood())
					.type(cont.getType())
					.type(cont.getType())
					.noBill(cont.noBill)
					.estado(estado)
					.statusDate(fechaEstado)
					.saldo(invoicesSum)
					.datacredit(datacredit)
					.urldc(urldc)
					.dateNotification(dateNotification)
					.dateReport(dateReport)
					.dateDeleteReport(dateDeleteReport)
					.idDataCredit(idDataCredit)
					.build());
		});
		
		PaginationData data = PaginationData.builder().pages(Lista.getTotalPages()).total(Lista.getTotalElements()).values(contractslist).build();
		return new ResponseEntity<PaginationData>(data, HttpStatus.OK);
	}
	
	@Transactional(readOnly = true)
	@PreAuthorize("hasAuthority('CONTRACTS_READ')")
	@PostMapping(path = "/pagesAll", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> GetContracsAll(@RequestBody ContractsPageData contractsPageData){
		Map<String, Object> response = new HashMap<>();
		List<Contracts> Lista = (List<Contracts>)contractsService.findAll(contractsPageData.getNameCliente(), contractsPageData.getConsecutive(), contractsPageData.getIp(), contractsPageData.getCc(), contractsPageData.getFopen(), contractsPageData.getFclose(), contractsPageData.getMac(), contractsPageData.getFechaAfiliacion(), contractsPageData.getBarrio(), contractsPageData.getTelefono(),contractsPageData.getDireccion(), contractsPageData.getValor(), contractsPageData.getActivo(), contractsPageData.getEstado(), contractsPageData.getSubsidiado(), contractsPageData.getFechaFac(), contractsPageData.getSerialEquipo(),
				contractsPageData.getSaldomin(),contractsPageData.getSaldomax());
		ArrayList<ContractsDto> contractslist = new ArrayList<ContractsDto>();
		
		Lista.forEach(cont -> {
			
			double contracValue = contractDetailsService.contractValue(cont);
			Integer invoicesSum = invoicesService.sumInvoicesOpenOrClose(cont, "Abierta");
			
			
			if(invoicesSum == null) {
				invoicesSum = 0;
			}
			
			Integer invoicesOpen = invoicesService.countInvoicesOpenOrClose(cont, "Abierta");
			Integer invoicesClose = invoicesService.countInvoicesOpenOrClose(cont, "Cerrada");

			LocalDate fecha = cont.getEndingDate();
			String ffin = null;
			if (fecha == null) {
				ffin = "";
			} else {
				ffin = fecha.toString();
			}

			// saber el estado de cada contrato
			String estado = "";
			String fechaEstado = "";
			boolean datacredit = false;
			String urldc = "";
			
			if(cont.getIdClient().getIdAssociated().getDatacredit() != null) {
				datacredit = true;
				urldc = cont.getIdClient().getIdAssociated().getDatacredit().getFile();
			}

			List<ContractStatus> listEstados = contractStatusService.lastStateInvoice(cont.getId());

			if (listEstados.size() > 0) {
				estado = listEstados.get(0).getStatus().getName();
				fechaEstado = listEstados.get(0).getCreateAt().toString();
			} else {
				estado = "No tiene estado asignado";
			}
			
			contractslist.add(
					ContractsDto.builder()
					.id(cont.getId())
					.consecutive(cont.getConsecutive())
					.address(cont.getAddress())
					.startDate(cont.getStartDate().toString())
					.endingDate(ffin)
					.billingStarDate(cont.getBillingStarDate().toString())
					.cuttingDay(cont.getCuttingDay())
					.expirationDay(cont.getExpirationDay())
					.startPeriod(cont.getStartPeriod())
					.periodicity(cont.getPeriodicity())
					.ipClient(cont.getIpClient())
					.macClient(cont.getMacClient())
					.serialEquipment(cont.getSerialEquipment())
					.clientId(cont.getIdClient().getId())
					.associateId(cont.getIdClient().getIdAssociated().getId())
					.associateName(cont.getIdClient().getIdAssociated().getFirstName() + " "+ cont.getIdClient().getIdAssociated().getLastName())
					.associateDocumento(cont.getIdClient().getIdAssociated().getIdentificationNumber())
					.associatePhone(cont.getIdClient().getIdAssociated().getCellPhoneNumber())
					.celphone2(cont.getIdClient().getCellPhoneNumber2())
					.associateEmail(cont.getIdClient().getIdAssociated().getEmail())
					.activo(cont.enabled)
					.facOpen(invoicesOpen)
					.factClose(invoicesClose).contractValue(contracValue)
					.retirementReason(cont.getRetirementReason())
					.deliveredProduct(cont.isDeliveredProduct())
					.retirementObservation(cont.getRetirementObservation())
					.city(cont.getCity())
					.neighborhood(cont.getNeighborhood())
					.type(cont.getType())
					.type(cont.getType())
					.noBill(cont.noBill)
					.estado(estado)
					.statusDate(fechaEstado)
					.saldo(invoicesSum)
					.datacredit(datacredit)
					.urldc(urldc)
					.build());
		});
		
		response.put("values", contractslist);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}
	
	@PreAuthorize("hasAuthority('CONTRACTS_READ')")
	@GetMapping(path = "/getTraceability/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> GetTraceability(@PathVariable Long id) {
		Map<String, Object> response = new HashMap<>();
		Contracts contract = contractsService.findById(id);
		List<ContractStatus> contractStatus = null;
		contractStatus = contractStatusService.findByContract(contract);

		if (contractStatus != null) {

			response.put("data", contractStatus);
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);

		} else {
			response.put("dataError", "No se encontraron resultados");
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}

	}
	
	@PreAuthorize("hasAuthority('CONTRACTS_READ')")
	@PostMapping(path = "/datacredit", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> ReportDataCredit(@RequestParam(value = "file") MultipartFile file,
			@RequestParam(value = "idAsociado") Integer idAssociate,
			@RequestParam(value = "fechareporte") String fechaReporte,
			@RequestParam(value = "fechanotificacion") String fechaNotificacion) throws IOException {
		Map<String, Object> response = new HashMap<>();
		DataCredit dc = new DataCredit();
		String reportPath = Constant.ruta + File.separator;
		
		try {
			File dir = new File(reportPath + "ReportDC");
			if (!dir.exists()) {
				dir.mkdirs();
			}
			if (file != null) {
				// enviamos a guardar elarchivo al servidor
				String nombreArchivo =  "DataCredit-" + idAssociate;
				dc.setFile(upload.copiar(file, nombreArchivo, "ReportDC"));
			}
			
			Associated associated =  associateService.findById(idAssociate);
			
			dc.setIdAssociated(associated);
			dc.setNotificationDate(LocalDate.parse(fechaNotificacion, DateTimeFormatter.BASIC_ISO_DATE));
			dc.setReporDate(LocalDate.parse(fechaReporte, DateTimeFormatter.BASIC_ISO_DATE));
			
			datacreditService.save(dc);
			
			response.put("data", "Reporte guardado correctamente");
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);

		} catch(DataAccessException e) {
			response.put("mensaje", "Error al realizar la consulta en la base de datos");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}
	
	@PreAuthorize("hasAuthority('CONTRACTS_READ')")
	@PostMapping(path = "/deletedatacredit", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> DeleteReportDataCredit(@RequestParam(value = "idreport") Integer idreport,
			@RequestParam(value = "dateDelete") String dateDelete) throws IOException {
		Map<String, Object> response = new HashMap<>();
		DataCredit dc = datacreditService.findById(idreport);
		
		try {
			
			
			dc.setReportDeleteDate(LocalDate.parse(dateDelete, DateTimeFormatter.BASIC_ISO_DATE));
			
			datacreditService.save(dc);
			
			response.put("data", "Reporte guardado correctamente");
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);

		} catch(DataAccessException e) {
			response.put("mensaje", "Error al realizar la consulta en la base de datos");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

}
