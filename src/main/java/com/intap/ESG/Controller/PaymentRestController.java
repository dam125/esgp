package com.intap.ESG.Controller;

import java.awt.print.Book;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.time.temporal.ChronoUnit;

import org.apache.poi.hpsf.Decimal;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.intap.ESG.Models.Configurations;
import com.intap.ESG.Models.ContractNews;
import com.intap.ESG.Models.ContractStatus;
import com.intap.ESG.Models.Contracts;
import com.intap.ESG.Models.Employee;
import com.intap.ESG.Models.InvoiceNews;
import com.intap.ESG.Models.Invoices;
import com.intap.ESG.Models.News;
import com.intap.ESG.Models.Payments;
import com.intap.ESG.Models.Status;
import com.intap.ESG.Models.DTO.AssociatesDTO;
import com.intap.ESG.Models.DTO.InvoicesNewsDTO;
import com.intap.ESG.Models.DTO.PaymentsDatesDTO;
import com.intap.ESG.Service.IConfigurationsService;
import com.intap.ESG.Service.IContractStatusService;
import com.intap.ESG.Service.IContractsService;
import com.intap.ESG.Service.IEmployeeService;
import com.intap.ESG.Service.IInvoiceNewsService;
import com.intap.ESG.Service.IInvoicesService;
import com.intap.ESG.Service.IPaymentsService;
import com.intap.ESG.Service.IStatusService;
import com.intap.ESG.Service.impl.UploadFileServiceImpl;
import com.intap.ESG.util.Constant;

import net.sf.jasperreports.engine.JRException;

@RestController
@RequestMapping(path = "payment")
public class PaymentRestController {

	@Autowired
	private IPaymentsService paymentsService;

	@Autowired
	private IInvoicesService invoiceServices;
	
	@Autowired
	private IInvoiceNewsService invoiceNewsServices;

	@Autowired
	private IContractStatusService contractStatusService;
	
	@Autowired
	private IContractsService contractsService;

	@Autowired
	private IEmployeeService employeeService;
	
	@Autowired
	private IConfigurationsService configurationService;

	@Autowired
	private IStatusService statusService;
	
	@Autowired
	private UploadFileServiceImpl upload;

	@PreAuthorize("hasAuthority('PAYMENTS_READ')")
	@GetMapping(path = "/{idInvoices}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> findById(@PathVariable Long idInvoices) {

		List<Payments> pagos = null;
		Invoices factura = null;
		Map<String, Object> response = new HashMap<>();

		try {

			factura = invoiceServices.findById(idInvoices);

			pagos = paymentsService.findByInvoices(factura);

		} catch (DataAccessException e) {
			response.put("mensaje", "Error al realizar la consulta en la base de datos");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		if (pagos == null) {
			response.put("mensaje",
					"No existen pagos asociados a la factura Número: ".concat(factura.getConsecutive().toString()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}

		response.put("data", pagos);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}
	

	@PreAuthorize("hasAuthority('PAYMENTS_READ')")
	@PostMapping(path = "/findByDates", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> findByDates(@RequestBody PaymentsDatesDTO datesDTO) {

		List<Payments> pagos = null;
		Invoices factura = null;
		Map<String, Object> response = new HashMap<>();

		try {
			pagos = paymentsService.findByDates(LocalDate.parse(datesDTO.getDesde(),DateTimeFormatter.BASIC_ISO_DATE),LocalDate.parse(datesDTO.getHasta(),DateTimeFormatter.BASIC_ISO_DATE));

		} catch (DataAccessException e) {
			response.put("mensaje", "Error al realizar la consulta en la base de datos");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		response.put("data", pagos);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}

	
	

	@PreAuthorize("hasAuthority('INVOICES_WRITE')")
	@PutMapping(path = "/UpdateStatus/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> UpdateStatus(@PathVariable Long id, @RequestBody Map<String, Object> prod) throws JRException, SQLException {

		Map<String, Object> response = new HashMap<>();
		
		Payments Payment = paymentsService.findById(id);
		
		Invoices invoice = invoiceServices.findById(Payment.getFactura().getId());
		
		Payment.setCancel(true);
		Payment.setObservationCancel(prod.get("retirementObservation").toString());
		
		paymentsService.save(Payment);
		
		List<InvoiceNews> abonos = invoiceNewsServices.findByInvoices(invoice);
		
		InvoiceNews InvoiceNews = new InvoiceNews();
		
		if(invoice.getEnvoicesState().equals("Cerrada")) {
			List<Payments> Payments = paymentsService.findByInvoices(invoice);
			Double Abonos = new Double("0");
			for(Payments Pay: Payments) {
				Abonos += Pay.getValuePaid();
			}
			
			Boolean AbonoExits = true;
			for(InvoiceNews abono: abonos) {
				if(abono.getName().equals("Abono a la factura")) {
					AbonoExits = false;
					abono.setUnitPrice(Double.parseDouble("-" + Abonos));
					InvoiceNews = abono;
				}
			}
			
			if(AbonoExits) {
				InvoiceNews.setObservations("Abono a la factura");
				InvoiceNews.setAmount(1);
				InvoiceNews.setName("Abono a la factura");
				InvoiceNews.setTaxes(Double.parseDouble("0"));
				InvoiceNews.setTipo("Novedad");
				InvoiceNews.setUnitPrice(Double.parseDouble("-" + Abonos));
				InvoiceNews.setInvoices(invoice);
			}
			
		}else {
			for(InvoiceNews abono: abonos) {
				if(abono.getName().equals("Abono a la factura")) {
					abono.setUnitPrice(abono.getUnitPrice() + Payment.getValuePaid());
					InvoiceNews = abono;
				}
			}
		}
		
		invoiceNewsServices.save(InvoiceNews);
		
		invoice.setEnvoicesState("Abierta");
		
		invoiceServices.save(invoice);
		
		response.put("mensaje", "¡El pago ha sido anulado con éxito!");
		response.put("data", Payment);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
	}
	
	@PreAuthorize("hasAuthority('PAYMENTS_WRITE')")
	@PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> create(@RequestBody Map<String, Object> data) throws ParseException {

		Map<String, Object> response = new HashMap<>();

		Payments pago = new Payments();
		List<Payments> pagoAnterior = null;
		Invoices factura = null;

		try {

			factura = invoiceServices.findById(Long.parseLong(data.get("idFactura").toString()));

			if (factura == null) {
				response.put("mensaje", "No existe factura con el ID: " + data.get("idFactura").toString());
				return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
			}

			pagoAnterior = paymentsService.findByInvoices(factura);
			double total = 0;
			if (!pagoAnterior.isEmpty()) {
				for(Payments pagos:pagoAnterior) {
					total += pagos.getValuePaid();
				}
				if(total == Double.parseDouble(factura.getTotal().toString())) {
					response.put("mensaje",
							"La factura No " + factura.getConsecutive() + " ya tiene un pago registrado en el sistema.");
					return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
				}
			}

			Configurations config = configurationService.findById((long) 1);

			if((Double.parseDouble(data.get("valuePaid").toString()) + total) >= Double.parseDouble(factura.getTotal().toString())) {
				// Cambiamos es estado de la factura
				factura.setEnvoicesState("Cerrada");
				invoiceServices.save(factura);
				pago.setValuePaid(Double.parseDouble(factura.getTotal().toString()) - total);
				List<InvoiceNews> InvoiceNew = new ArrayList<InvoiceNews>();
				InvoiceNew = invoiceNewsServices.findByInvoices(factura);
				for(InvoiceNews News: InvoiceNew ) {
					if(News.getName().equals("Abono a la factura")) {
						invoiceNewsServices.delete(News.getId());
					}
				}
			}else {
				InvoiceNews InvoiceNews = null;
				List<InvoiceNews> InvoiceNew = new ArrayList<InvoiceNews>();
				InvoiceNew = invoiceNewsServices.findByInvoices(factura);
				for(InvoiceNews News: InvoiceNew ) {
					if(News.getName().equals("Abono a la factura")) {
						News.setUnitPrice(Double.parseDouble( "-" + (Double.parseDouble(data.get("valuePaid").toString()) - News.getUnitPrice())));
						InvoiceNews = News;
					}
				}
				
				if(InvoiceNews == null) {
					InvoiceNews = new InvoiceNews();
					InvoiceNews.setObservations("Abono a la factura");
					InvoiceNews.setAmount(1);
					InvoiceNews.setName("Abono a la factura");
					InvoiceNews.setTaxes(Double.parseDouble("0"));
					InvoiceNews.setTipo("Novedad");
					InvoiceNews.setUnitPrice(Double.parseDouble("-" + data.get("valuePaid").toString()));
					InvoiceNews.setInvoices(factura);
				}
				
				invoiceNewsServices.save(InvoiceNews);
				
				pago.setValuePaid(Double.parseDouble(data.get("valuePaid").toString()));
			}
			
			pago.setFactura(factura);
			pago.setConsecutive(Integer.toString(Integer.parseInt(config.getConsecutivePaymemt()) + 1));
			pago.setPaymentDate(LocalDate.parse(data.get("paymentDate").toString(), DateTimeFormatter.BASIC_ISO_DATE));
			pago.setWayOfPay(data.get("wayOfPay").toString());
			pago.setMoneyReceived(Double.parseDouble(data.get("moneyReceived").toString()));

			config.setConsecutivePaymemt(Integer.toString(Integer.parseInt(config.getConsecutivePaymemt()) + 1));
			configurationService.save(config);

			paymentsService.save(pago);
			if(factura.getContrato() != null) {
				if(!factura.getContrato().isEnabled()) {
					List<Invoices> invoices = null;
					invoices = invoiceServices.findAllInvoicesOpenOrClose(factura.getContrato(), "Abierta");
					
					ContractStatus contracStatus = new ContractStatus();
					
					if(invoices.size() == 0) {
						Status status = statusService.findByName("AL DIA");
						Employee userCrea = employeeService.findById(1);
							
						//Se toma la fecha actual para colombia
						LocalDate fecha = LocalDate.now(ZoneId.of("UTC-5"));
						contracStatus.setCreateAt(fecha);
						
						contracStatus.setContract(factura.getContrato());
						contracStatus.setStatus(status);
						contracStatus.setObservation("N/A");
						contracStatus.setUserCrea(userCrea);
					}else {
						Status status = statusService.findByName("CARTERA");
						Employee userCrea = employeeService.findById(1);
		
						//Se toma la fecha actual para colombia
						LocalDate fecha = LocalDate.now(ZoneId.of("UTC-5"));
						contracStatus.setCreateAt(fecha);
						
						contracStatus.setContract(factura.getContrato());
						contracStatus.setStatus(status);
						contracStatus.setObservation("N/A");
						contracStatus.setUserCrea(userCrea);
					}
						
					contractStatusService.save(contracStatus);
				}
			}
			// sacar los dias de mora de la factura

			LocalDate fechaVencimiento = factura.getExpirationDay();
			LocalDate fechaPago = LocalDate.parse(data.get("paymentDate").toString(), DateTimeFormatter.BASIC_ISO_DATE);

			long diasMora = ChronoUnit.DAYS.between(fechaVencimiento, fechaPago);

			if (diasMora > 0) {

				// Actualizamos el contrato con los dias de mora que se deben cobrar.
				Contracts contr = factura.getContrato();

				int diasMoraActual = contr.getDaysPastDue();

				contr.setDaysPastDue((int) (diasMoraActual + diasMora));
				contractsService.save(contr);
			}

		} catch (DataAccessException e) {
			response.put("mensaje", "Error al realizar la consulta en la base de datos");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		response.put("mensaje", "¡El pago ha sido registrado con éxito!");
		response.put("data", pago);

		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);

	}

	// Logica para los pagos masivos
	@Transactional
	@PreAuthorize("hasAuthority('PAYMENTS_WRITE')")
	@PostMapping(path = "/masivo", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> masivo(@RequestParam(value = "masivo", required = true) MultipartFile masivo)
			throws IOException, ParseException {
		Map<String, Object> response = new HashMap<>();

		String archivo = null;
		if (masivo != null) {
			Date ahora = new Date();
			SimpleDateFormat formateador = new SimpleDateFormat("yyyy-MM-dd");
			int numero = (int) (Math.random() * 999) + 1; // para que el archivo siempre tenga un nombre diferente
			// enviamos a guardar elarchivo al servidor
	
			Calendar calendar = Calendar.getInstance();
			int year = calendar.get(Calendar.YEAR);
			int mes = calendar.get(Calendar.MONTH) + 1;
			
			String carpeta = "PagosMasivos" +File.separator+year + File.separator + mes ;

			String nombreArchivo = formateador.format(ahora) + "-" + numero;
			archivo = (upload.copiar(masivo, nombreArchivo, carpeta));
		} else {

			response.put("mensaje", "Por favor adjunte el archivo de los pagos para poder gestionarlo.");
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);

		}
		
		String reportPath = Constant.ruta + File.separator;

				
		String excelFilePath = reportPath + archivo; // can be .xls or .xlsx

		

		FileInputStream inputStream = new FileInputStream(new File(excelFilePath));

		Workbook workbook = getWorkbook(inputStream, excelFilePath);

		Sheet firstSheet = workbook.getSheetAt(0);

		// Create a DataFormatter to format and get each cell's value as String
		DataFormatter dataFormatter = new DataFormatter();
		int catPagos = 0;
		for (Row row : firstSheet) {

		
			
			String fecha = dataFormatter.formatCellValue(row.getCell(0));
			String valorPagado =dataFormatter.formatCellValue(row.getCell(1));
			String referenciaPago = dataFormatter.formatCellValue(row.getCell(2));
			if(!fecha.equals("FECHA") && !fecha.equals("")) {
				if(DateUtil.isCellDateFormatted(row.getCell(0))) {
					SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
					fecha = sdf.format(row.getCell(0).getDateCellValue());
				}
			}
			
			if(!validarFecha(fecha)) {
				
				if(!fecha.equals("FECHA")) {
					Cell cell2 =  row.createCell(3);
					// Update the cell's value
					cell2.setCellValue("La fecha no es valida o no tienen el formato correcto : dd/MM/YYYY");

				}
			}else {
				Invoices invoice = null;
				DecimalFormat df = new DecimalFormat("0000000");
				String reference = df.format(Integer.parseInt(referenciaPago));
				invoice = invoiceServices.findByRefernce(reference,"Abierta");
				
				
				if(invoice != null) {

					DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
					LocalDate localDate = LocalDate.parse(fecha, formatter);
					Configurations config = configurationService.findById((long) 1);
					Payments pago = new Payments();
					pago.setFactura(invoice);
					pago.setConsecutive(Integer.toString(Integer.parseInt(config.getConsecutivePaymemt()) + 1));
					pago.setPaymentDate(localDate);
					pago.setValuePaid(Double.parseDouble(valorPagado));
					pago.setWayOfPay("Consignacion");
					
					paymentsService.save(pago);
					
					//cambiamos el estado a la factura
					
					invoice.setEnvoicesState("Cerrada");
					invoiceServices.save(invoice);
					
					//Actualizar el consecutivo de los pagos
					config.setConsecutivePaymemt(Integer.toString(Integer.parseInt(config.getConsecutivePaymemt()) + 1));
					configurationService.save(config);
					
					catPagos++;
					
					// Get the Cell at index 2 from the above row
					Cell cell3 = row.createCell(3);
					// Update the cell's value
					cell3.setCellValue("PAGO REGISTRADO");
					Cell cell4 = row.createCell(4);
					cell4.setCellValue("Factura No: (Consecutivo)");
					Cell cell5 = row.createCell(5);
					cell5.setCellValue(invoice.getConsecutive());
					
					
				}else {
					// Get the Cell at index 2 from the above row
					Cell cell2 =  row.createCell(3);
					// Update the cell's value
					cell2.setCellValue("Referencia no encontrada ó pago registrado anteriormente");

				}
			}
			/// hacemos la validaciones

			
			//Escribimos en el archivo pdf
			FileOutputStream fileOut = new FileOutputStream(excelFilePath);
		    workbook.write(fileOut);
		    fileOut.close();
		}

		workbook.close();
		inputStream.close();


		response.put("mensaje", catPagos+" Pagos registrados en el sistema");
		response.put("data", catPagos);
		response.put("file", archivo);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);

	}
	
	
	public static boolean validarFecha(String fecha) {
        try {
        	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        	LocalDate localDate = LocalDate.parse(fecha, formatter);
        } catch (DateTimeParseException e) {
            return false;
        }
        return true;
    }

	private Workbook getWorkbook(FileInputStream inputStream, String excelFilePath) throws IOException {
		Workbook workbook = null;

		if (excelFilePath.endsWith("xlsx")) {
			workbook = new XSSFWorkbook(inputStream);
		} else if (excelFilePath.endsWith("xls")) {
			workbook = new HSSFWorkbook(inputStream);
		} else {
			throw new IllegalArgumentException("El archivo debe ser un excel (.xlsx ó .xls)");
		}

		return workbook;
	}

}
