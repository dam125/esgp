package com.intap.ESG.Controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.intap.ESG.Models.Role;
import com.intap.ESG.Models.UserModel;
import com.intap.ESG.Models.DTO.RoleDataDto;
import com.intap.ESG.Models.DTO.RoleDto;
import com.intap.ESG.Service.IUserService;
import com.intap.ESG.Service.RoleService;


@RestController
@RequestMapping(path = "roles")
public class RoleRestController {
	
	@Autowired
	private RoleService roleService;
	
	@Autowired
	private IUserService userService;
	
	@PreAuthorize("hasAuthority('SECURITY2_READ')")
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(code = HttpStatus.OK)
	public ResponseEntity<?> findAll() {
		
		List<Role> roles =new ArrayList<Role>();
		List<RoleDto> roleDtos = new ArrayList<RoleDto>();
		Map<String, Object> response = new HashMap<>();
		
		roles = roleService.findAll();
		
		roles.forEach(role -> {
			roleDtos.add(RoleDto.builder().id(role.getId()).name(role.getName()).build());
		});

		
		response.put("data", roleDtos);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);

	}
	
	@PreAuthorize("hasAuthority('SECURITY2_READ')")
	@GetMapping(path = "/findAllExport", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(code = HttpStatus.OK)
	public ResponseEntity<?> findAllExport() {
		
		List<RoleDto> data = this.roleService.findAllExport();
		return ResponseEntity.ok(data);
	}
	
	
	@PreAuthorize("hasAuthority('SECURITY2_READ')")
	@GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(code = HttpStatus.OK)
	public ResponseEntity<RoleDto> findById(@PathVariable(value = "id") Long id) {
		RoleDto data = this.roleService.findById(id);
		return ResponseEntity.ok(data);
	}
	
	@PreAuthorize("hasAuthority('SECURITY2_WRITE')")
	@PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(code = HttpStatus.CREATED)
	public ResponseEntity<RoleDto> create(@RequestBody RoleDataDto roleData) {
		RoleDto data = this.roleService.create(roleData);
		return ResponseEntity.ok(data);
	}
	
	@PreAuthorize("hasAuthority('SECURITY2_DELETE')")
	@PutMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(code = HttpStatus.OK)
	public ResponseEntity<RoleDto> update(@RequestBody RoleDataDto roleData, @PathVariable(value = "id") Long id) {
		RoleDto data = this.roleService.update(id, roleData);
		return ResponseEntity.ok(data);
	}

	@PreAuthorize("hasAuthority('SECURITY2_WRITE')")
	@DeleteMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	public ResponseEntity<?> delete(@PathVariable(value = "id") Long id) {
		Map<String, Object> response = new HashMap<>();
		
		Role rol = roleService.findByRol(id);
		
		List<UserModel> user= userService.findByRol(rol);
		
		Integer cant = user.size();
		
		if (cant > 0) {
			
			response.put("mensaje", "El rol '"+rol.getName()+"' no se puede eliminar ya que está asociado a un usuario.");
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
			
		} else {
			
			this.roleService.delete(id);
			response.put("mensaje", "¡Rol eliminado exitosamente!");
		}
		
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
		

	}
	
	@PreAuthorize("hasAuthority('USER_READ')")
	@GetMapping(path = "/findAll", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(code = HttpStatus.OK)
	public ResponseEntity<?> findAll2() {
		
		List<Role> roles =new ArrayList<Role>();
		List<RoleDto> roleDtos = new ArrayList<RoleDto>();
		Map<String, Object> response = new HashMap<>();
		
		roles = roleService.findAll();
		
		roles.forEach(role -> {
			roleDtos.add(RoleDto.builder().id(role.getId()).name(role.getName()).build());
		});

		
		response.put("data", roleDtos);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);

	}

}
