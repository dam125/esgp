package com.intap.ESG.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.intap.ESG.Models.Cities;
import com.intap.ESG.Service.ICitiesService;

@RestController
@RequestMapping(path = "cities")
public class CitiesRestController {
	
	@Autowired
	private ICitiesService citiesService; 
	
	@PreAuthorize("hasAuthority('USER_READ')")
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(code = HttpStatus.OK)
	public ResponseEntity<Iterable<Cities>> findAll() {
		Iterable<Cities> authorityDtos = citiesService.findAll();
		return ResponseEntity.ok(authorityDtos);
	}

}
