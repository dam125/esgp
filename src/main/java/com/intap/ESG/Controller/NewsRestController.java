package com.intap.ESG.Controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.intap.ESG.Models.ContractNews;
import com.intap.ESG.Models.News;
import com.intap.ESG.Models.Products;
import com.intap.ESG.Models.DTO.NewPageDTO;
import com.intap.ESG.Models.DTO.PaginationData;
import com.intap.ESG.Service.IContractNewsService;
import com.intap.ESG.Service.INewsService;

@RestController
@RequestMapping(path = "news")
public class NewsRestController {
	
	@Autowired
	private INewsService newsService;
	
	@Autowired
	private IContractNewsService contractNewsService;
	
	@PreAuthorize("hasAuthority('NEWS_READ')")
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?>  findAll(){
		Map<String, Object> response = new HashMap<>();
		
		List<News> news =  newsService.findAll();
		
		response.put("mensaje", "¡Listado de Novedades!");
		response.put("data", news);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}
	
	
	@PreAuthorize("hasAuthority('NEWS_READ')")
	@PostMapping(path = "/findPager", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> findPager(@RequestBody NewPageDTO newPagesDTO) {
		
		List<News> pages = (List<News>) this.newsService.findAll(newPagesDTO.getName(), newPagesDTO.getTipo(), 
				newPagesDTO.getValor(),
				newPagesDTO.getValorMax(),
				newPagesDTO.getPorcentaje(), 
				newPagesDTO.getValorPorcentaje(),newPagesDTO.getEstado());
		
		List<Map<String, Object>> data = pages.stream().map(products -> {

			Map<String, Object> d = new HashMap<>();	
			
			d.put("news", products);
			
			return d;
		}).collect(Collectors.toList());
		
	

		return new ResponseEntity<Object>(data, HttpStatus.OK);

	}
	
	@PreAuthorize("hasAuthority('NEWS_READ')")
	@PostMapping(path = "pages", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<PaginationData>  findAll(@RequestBody NewPageDTO newPagesDTO){
		
		Page<News> page =  this.newsService.findAll(newPagesDTO.getPage() -1, newPagesDTO.getLimit(), 
				newPagesDTO.getName(), newPagesDTO.getTipo(), newPagesDTO.getValor(),newPagesDTO.getValorMax(), newPagesDTO.getPorcentaje(), 
				newPagesDTO.getValorPorcentaje(),newPagesDTO.getEstado());
		
		
		PaginationData data = PaginationData.builder()
				.pages(page.getTotalPages())
				.total(page.getTotalElements())
				.values(page.getContent())
				.build();
		
		
		return new ResponseEntity<PaginationData>(data, HttpStatus.OK);
}
	
	
	@PreAuthorize("hasAuthority('NEWS_READ')")
	@GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> findById(@PathVariable Long id) {
		
		News news = null;
		Map<String, Object> response = new HashMap<>();
		
		try {
			
			news = newsService.findById(id);	
			
		} catch(DataAccessException e) {
			response.put("mensaje", "Error al realizar la consulta en la base de datos");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		if(news == null) {
			response.put("mensaje", "¡la novedad ID: ".concat(id.toString().concat(" no existe en la base de datos!")));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}

			response.put("data", news);
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}
	
	
	@PreAuthorize("hasAuthority('NEWS_WRITE')")
	@PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> create(@Valid @RequestBody News news) {
		
		Map<String, Object> response = new HashMap<>();
		
		news.setEnabled(true);
		News news2 = newsService.save(news);
		response.put("mensaje", "¡Novedad creada con éxito!");
		response.put("data", news2);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);

	}
	
	@PreAuthorize("hasAuthority('NEWS_WRITE')")
	@PutMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> update(@PathVariable Long id, @RequestBody News ne) {
		Map<String, Object> response = new HashMap<>();
		
		News news = newsService.findById(id);
		
		news.setTipo(ne.getTipo());
		news.setName(ne.getName());
		news.setValor(ne.getValor());
		news.setPorcentaje(ne.getPorcentaje());
		news.setValorPorcentaje(ne.getValorPorcentaje());
		
		newsService.save(news);
		
		response.put("mensaje", "¡Novedad actualizada con éxito!");
		response.put("data", news);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}
	
	@PreAuthorize("hasAuthority('NEWS_DELETE')")
	@DeleteMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> delete(@PathVariable Long id) {
		Map<String, Object> response = new HashMap<>();
		
		List<ContractNews> contr = null;
		
		News news = newsService.findById(id);
		
		contr = contractNewsService.findByContractNewss(news);
		Integer cant = contr.size();
		if (cant > 0) {
			
			response.put("mensaje", "La novedad  '" + news.getName()
					+ "' no se puede eliminar ya que está asociada a un contrato");
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
			

		} else {
	
			newsService.delete(id);
			response.put("mensaje", "¡Novedad eliminada exitosamente!");

		}

		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}

	@PreAuthorize("hasAuthority('NEWS_WRITE')")
	@PutMapping(path = "ChangeStatus/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?>ChageStatus(@PathVariable Long id, @RequestBody Map<String, Object> prod){
		Map<String, Object> response = new HashMap<>();
		
		News product = newsService.findById(id);
		
		product.setEnabled(Boolean.parseBoolean(prod.get("enabled").toString()));
		
		newsService.save(product);
		
		response.put("mensaje", "¡El estado del producto ha sido actualizado con éxito!");
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}
	
	
}
