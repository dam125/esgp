package com.intap.ESG.Controller;


import java.util.HashMap;
import java.util.List;
import java.util.Map;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.intap.ESG.Models.UserModel;
import com.intap.ESG.Service.IUserService;


@RestController
@RequestMapping("/user")
public class UserRestController {

	@Autowired 
	private IUserService userService;
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@GetMapping("/GetUsers")
	public ResponseEntity<?> GetUsers() {
		Map<String, Object> response = new HashMap<>();
		
		List<UserModel> Lista = userService.GetAllUser();
		
		response.put("messaje", "User list");
		response.put("users", Lista);
		
	    return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}
	
	@GetMapping("/encode/prueva")
	public ResponseEntity<Boolean> encode() {
		String hash2 = this.passwordEncoder.encode("12345");
		return ResponseEntity.ok(this.passwordEncoder.matches("12345", hash2));
	}


}
