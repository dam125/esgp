package com.intap.ESG.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.intap.ESG.Models.DTO.AuthorityDto;
import com.intap.ESG.Service.AuthorityService;



@RestController
@RequestMapping(path = "autorities")
public class AuthorityRestController {
	
	@Autowired
	private AuthorityService authorityService; 
	
	@PreAuthorize("hasAuthority('SECURITY2_READ')")
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(code = HttpStatus.OK)
	public ResponseEntity<Iterable<AuthorityDto>> findAll() {
		Iterable<AuthorityDto> authorityDtos = this.authorityService.findAll();
		return ResponseEntity.ok(authorityDtos);
	}
}
