package com.intap.ESG.Controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.text.NumberFormat;
import java.util.Locale;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.krysalis.barcode4j.impl.code128.Code128LogicImpl;
import org.krysalis.barcode4j.impl.code128.EAN128Bean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.intap.ESG.Models.Associated;
import com.intap.ESG.Models.Configurations;
import com.intap.ESG.Models.ContractDetails;
import com.intap.ESG.Models.ContractNews;
import com.intap.ESG.Models.ContractStatus;
import com.intap.ESG.Models.Contracts;
import com.intap.ESG.Models.Employee;
import com.intap.ESG.Models.InvoiceNews;
import com.intap.ESG.Models.Invoices;
import com.intap.ESG.Models.News;
import com.intap.ESG.Models.Payments;
import com.intap.ESG.Models.RecurringInvoices;
import com.intap.ESG.Models.Status;
import com.intap.ESG.Models.DTO.ContractsStatusDto;
import com.intap.ESG.Models.DTO.InvoicesDTO;
import com.intap.ESG.Models.DTO.InvoicesNewsDTO;
import com.intap.ESG.Models.DTO.InvoicesPageDto;
import com.intap.ESG.Models.DTO.PaginationData;
import com.intap.ESG.Models.DTO.RecurringInvoicesDTO;
import com.intap.ESG.Service.IAssociatedService;
import com.intap.ESG.Service.IConfigurationsService;
import com.intap.ESG.Service.IContractDetailsService;
import com.intap.ESG.Service.IContractNewsService;
import com.intap.ESG.Service.IContractStatusService;
import com.intap.ESG.Service.IContractsService;
import com.intap.ESG.Service.IEmployeeService;
import com.intap.ESG.Service.IInvoiceNewsService;
import com.intap.ESG.Service.IInvoicesService;
import com.intap.ESG.Service.INewsService;
import com.intap.ESG.Service.IPaymentsService;
import com.intap.ESG.Service.IRecurringInvoicesService;
import com.intap.ESG.Service.ISendEmailService;
import com.intap.ESG.Service.IStatusService;
import com.intap.ESG.util.Constant;
import com.lowagie.text.Document;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfImportedPage;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfWriter;

import lombok.var;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;

@RestController
@RequestMapping(path = "invoice")
public class InvoicesRestController {

	@Autowired
	private IInvoicesService invoiceServices;
	
	@Autowired
	private INewsService newsServices;
	
	@Autowired
	private IAssociatedService associateServices;

	@Autowired
	private IContractsService contractsService;

	@Autowired
	private IContractNewsService contractsNewsService;

	@Autowired
	private IContractDetailsService contractDetailsService;

	@Autowired
	private IInvoiceNewsService invoiceNewsService;

	@Autowired
	private IConfigurationsService configurationService;

	@Autowired
	private IPaymentsService paymentsService;

	@Autowired
	private IContractStatusService contractStatusService;

	@Autowired
	private IStatusService statusService;

	@Autowired
	private IEmployeeService employeeService;

	@Autowired
	private ISendEmailService sendEmail;

	@Autowired
	private IRecurringInvoicesService recurringInvoicesService;

	@Autowired
	@Qualifier("jdbcTemplate")
	private JdbcTemplate jdbcTemplate;

	@PreAuthorize("hasAuthority('INVOICES_READ')")
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getAll() {
		Map<String, Object> response = new HashMap<>();

		// Consultamos si hay facturas para cambiarles los estados
		this.changeStatusAuto();

		List<Invoices> invoices = invoiceServices.findAll();
		
		List<InvoicesDTO> invoicesDTO = new ArrayList<>();

		
		//Se valida si la fuctura fue realizada a un contrato o a un asociado y se arma que objeto que sera devuelto.
		for (Invoices invoice : invoices) {

			List<Payments> pago = null;
			pago = paymentsService.findByInvoices(invoice);

			Double pagado = (double) 0;

			// se busca si la factura tiene pagos asociados
            if(!pago.isEmpty()) {
                for(Payments pagos: pago) {
                    pagado += pagos.getValuePaid();
                }
			}

			double saldo = invoice.getTotal() - pagado;
			
			
			// se valida si la factura pertenece a un contrato o a un asociado
			if(invoice.getContrato() != null) {
				
				invoicesDTO.add(InvoicesDTO.builder().id(invoice.getId()).consecutive(invoice.getConsecutive())
						.total(invoice.getTotal()).envoicesMonth(invoice.getEnvoicesMonth())
						.envoicesState(invoice.getEnvoicesState()).invoiceDate(invoice.getInvoiceDate())
						.associateName(invoice.getContrato().getIdClient().getIdAssociated().getFirstName()
								.concat(" ".concat(invoice.getContrato().getIdClient().getIdAssociated().getLastName())))
						.cancel(invoice.isCancel())
						.reasonCancel(invoice.getReasonCancel())
						.observationCancel(invoice.getObservationCancel())
						.contract(invoice.getContrato().getId()).jsonTotales(invoice.getTotal()).pago(pagado).saldo(saldo)
						.build());
				
			}else {
				
				invoicesDTO.add(InvoicesDTO.builder().id(invoice.getId()).consecutive(invoice.getConsecutive())
						.total(invoice.getTotal()).envoicesMonth(invoice.getEnvoicesMonth())
						.envoicesState(invoice.getEnvoicesState()).invoiceDate(invoice.getInvoiceDate())
						.associateName(invoice.getAssociate().getFirstName()
					    .concat(" ".concat(invoice.getAssociate().getLastName())))
						.cancel(invoice.isCancel())
						.reasonCancel(invoice.getReasonCancel())
						.observationCancel(invoice.getObservationCancel())
						.jsonTotales(invoice.getTotal()).pago(pagado).saldo(saldo)
						.build());
			}

			
		}

		response.put("mensaje", "Listado de Facturas.");
		response.put("data", invoicesDTO);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}

	@PreAuthorize("hasAuthority('INVOICES_READ')")
	@GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> findById(@PathVariable Long id) {

		Invoices invoices = null;
		List<InvoiceNews> detalles = null;
		InvoicesDTO invoicesDTO = new InvoicesDTO();
		Map<String, Object> response = new HashMap<>();
		List<InvoicesNewsDTO> InvoicesNewsDTOS = new ArrayList<>();

		
		double suma = 0;
		
		try {

			invoices = invoiceServices.findById(id);

			detalles = invoiceNewsService.findByInvoices(invoices);

		} catch (DataAccessException e) {
			response.put("mensaje", "Error al realizar la consulta en la base de datos");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		if (invoices == null) {
			response.put("mensaje", "La factura ID: ".concat(id.toString().concat(" no existe en la base de datos!")));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}

		for (InvoiceNews detalle : detalles) {

			InvoicesNewsDTOS.add(InvoicesNewsDTO.builder().id(detalle.getId()).tipo(detalle.getTipo())
					.name(detalle.getName()).taxes(detalle.getTaxes()).unitPrice(detalle.getUnitPrice())
					.amount(detalle.getAmount()).Observations(detalle.getObservations()).build());
			

		}

		
		invoicesDTO.setId(invoices.getId());
		invoicesDTO.setConsecutive(invoices.getConsecutive());
		invoicesDTO.setTotal(invoices.getTotal());
		
		invoicesDTO.setEnvoicesState(invoices.getEnvoicesState());
		invoicesDTO.setExpirationDay(invoices.getExpirationDay());
		invoicesDTO.setEnvoicesMonth(invoices.getEnvoicesMonth());
		invoicesDTO.setInvoiceDate(invoices.getInvoiceDate());
	
		// Se valida si factura pertenece a un contrato o a un asociado.
		if(invoices.getContrato() != null) {
			invoicesDTO.setContract(invoices.getContrato().getId());
		}else {
			invoicesDTO.setAssociate(Long.parseLong(invoices.getAssociate().getId().toString()));
		}
		
		response.put("data", invoicesDTO);
		response.put("detalle", InvoicesNewsDTOS);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}

	@Transactional
	@PreAuthorize("hasAuthority('INVOICES_WRITE')")
	@PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> create(@RequestBody Map<String, Object> invoice) throws JRException, SQLException {

		Map<String, Object> response = new HashMap<>();
		Invoices invMonth = null;
		ArrayList<?> contractDetail1 = null;
		ArrayList<?> contractDetail2 = null;
		
		String consecutiveB ;
		Boolean validateClient = Boolean.parseBoolean(invoice.get("invoiceClient").toString());
		
		
		// Se valida la factura pertenece a un contrato o a un asociado
		if(validateClient) {
			
			//En este bloque se manejan las facturas que se le realizan a un Asociado.
			
			Associated con = associateServices.findById(Integer.parseInt(invoice.get("contract").toString()));
			Date ahora = new Date();
			SimpleDateFormat formateador = new SimpleDateFormat("yyyyMMdd");

			Configurations config = configurationService.findById((long) 1);
			
			contractDetail1 = (ArrayList<?>) invoice.get("detailNes");
			JSONArray contractDetailNews = new JSONArray(contractDetail1);
			
			
			contractDetail2 = (ArrayList<?>) invoice.get("detailProducts");
			JSONArray contractDetailProducts = new JSONArray(contractDetail2);
			
			Invoices inv = new Invoices();
			
			// Se le da formato al consecturivo de factura
			DecimalFormat df = new DecimalFormat("0000000");
			String ref =  Integer.toString((Integer.parseInt(config.getConsecutiveRefPago())+ 1));
			ref = df.format(Integer.parseInt(ref));
			String cons = Integer.toString(Integer.parseInt(config.getConsecutiveFactura()) + 1);
			
			cons = df.format(Integer.parseInt(cons));
			inv.setConsecutive(cons);
			inv.setRefPago(ref);
			inv.setEnvoicesMonth(invoice.get("invoiceMonth").toString());
			inv.setEnvoicesState("Abierta");
			
			// esta fecha debe de ser la fecha actual en la que se genero la factura
			
			inv.setInvoiceDate(LocalDate.parse(formateador.format(ahora), DateTimeFormatter.BASIC_ISO_DATE));
			inv.setTotal(Integer.parseInt(invoice.get("total").toString()));
			inv.setExpirationDay(LocalDate.parse(invoice.get("invoiceDate").toString(), DateTimeFormatter.BASIC_ISO_DATE));
			inv.setAssociate(con);
			config.setConsecutiveFactura(cons);
			config.setConsecutiveRefPago(ref);

			
			configurationService.save(config);
			Invoices invo = invoiceServices.save(inv);
			consecutiveB =   invo.getId().toString();
			
			//Se guardan las novedades de la factura
			
			for (int i = 0; i < contractDetailNews.length(); i++) {
				InvoiceNews details = new InvoiceNews();	
				JSONObject dato = contractDetailNews.getJSONObject(i);
				details.setName(dato.get("productNameTable").toString());
				details.setInvoices(invo);
				details.setTaxes(Double.parseDouble(dato.get("taxes").toString()));
				details.setUnitPrice(Double.parseDouble(dato.get("unitPrice").toString()));
				details.setAmount(Integer.parseInt(dato.get("amount").toString()));
				details.setObservations(dato.get("Observations").toString());
				details.setTipo(dato.get("type").toString());				
				invoiceNewsService.save(details);
				
			}
			
			Double vContrato = (double) 0;
			
			//Se guardan los productos de las facturas
			for (int i = 0; i < contractDetailProducts.length(); i++) {
				InvoiceNews details = new InvoiceNews();
				JSONObject dato = contractDetailProducts.getJSONObject(i);
				
				// calculamos el precio unitario segun los dias a facturar
				Double precioUnitarioDiario = Double.parseDouble(dato.get("unitPrice").toString());
				details.setName(dato.get("productNameTable").toString());
				details.setInvoices(invo);
				details.setTaxes(Double.parseDouble(dato.get("taxes").toString()));
				details.setUnitPrice(Double.parseDouble(precioUnitarioDiario.toString()));
				details.setAmount(Integer.parseInt(dato.get("amount").toString()));
				details.setObservations(dato.get("Observations").toString());
				details.setTipo(dato.get("type").toString());
				
				invoiceNewsService.save(details);
				
				Double Suma = (precioUnitarioDiario * Integer.parseInt(dato.get("amount").toString()));
				
				Double impuesto = Suma * Integer.parseInt(dato.get("taxes").toString());
				
				vContrato += Suma + impuesto;
			}
			
			
		}else {
			
			//En este bloque se manejan las facturas que estan asociadas a un contrato
			
			int idContrac =   Integer.parseInt(invoice.get("contract").toString());
			long num = (idContrac);
			Contracts con = contractsService.findById(num);
			List<Invoices> invoices = null;
			invoices = invoiceServices.findAllInvoicesOpenOrClose(con, "Abierta");
			
			//Se valida si el contrato ya tiene dos facturas abiertas y deja crear mas
			if(invoices.size() >= 2) {
				response.put("mensaje","No se puede crear la factura porque ya tiene dos facturas abiertas");
				response.put("data", null);
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
			}
		
		
		con.getInvoices();

		//Se valida si el contrato ya tiene una factura generada para ese mes 
		invMonth = invoiceServices.findByContractAndMonth(con, invoice.get("invoiceMonth").toString());
		
		if (invMonth != null) {
			response.put("mensaje", "La factura del mes: "
					.concat(invoice.get("invoiceMonth").toString().concat(" ya se encuentra registrada!")));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}
		
		// se consulta y se valida si el contrato tiene facturas anteriores abiertas para calcular la mora de estas.
		
		List<Invoices> invMora = invoiceServices.findInvoicesOpen(con, "Abierta", (long) -1);
		Date ahora = new Date();
		String month = invoice.get("invoiceMonth").toString().replace("/", "");
		String dayCutting = StringUtils.leftPad(String.valueOf(con.getCuttingDay()), 2, '0');
		LocalDate fechaNuevaFacturaAux = LocalDate.parse(month.concat(dayCutting), DateTimeFormatter.BASIC_ISO_DATE);
		LocalDate fechaNuevaFactura = fechaNuevaFacturaAux.minusMonths(1);
		SimpleDateFormat formateador = new SimpleDateFormat("yyyyMMdd");
		
		if(invMora.size() > 0){
			invMora.forEach(iM ->{
				LocalDate fechaVencimiento = iM.getExpirationDay();
				
				long diasMora = ChronoUnit.DAYS.between(fechaVencimiento, fechaNuevaFactura);
				
				if (diasMora > 0) {

					// Actualizamos el contrato con los dias de mora que se deben cobrar.

					int diasMoraActual = con.getDaysPastDue();

					con.setDaysPastDue((int) (diasMoraActual + diasMora));
					contractsService.save(con);
				}
			});
		}

		Configurations config = configurationService.findById((long) 1);
		
		contractDetail1 = (ArrayList<?>) invoice.get("detailNes");
		JSONArray contractDetailNews = new JSONArray(contractDetail1);
		
		
		contractDetail2 = (ArrayList<?>) invoice.get("detailProducts");
		JSONArray contractDetailProducts = new JSONArray(contractDetail2);
			
		Invoices inv = new Invoices();

		//Se le da formato al consecutivo de la factura y se arma el objeto de la factura
		DecimalFormat df = new DecimalFormat("0000000");
		String ref =  Integer.toString((Integer.parseInt(config.getConsecutiveRefPago())+ 1));
		ref = df.format(Integer.parseInt(ref));
		String cons = Integer.toString(Integer.parseInt(config.getConsecutiveFactura()) + 1);
	
		cons = df.format(Integer.parseInt(cons));
		inv.setConsecutive(cons);
		inv.setRefPago(ref);
		inv.setEnvoicesMonth(invoice.get("invoiceMonth").toString());
		inv.setEnvoicesState("Abierta");
		// sta fecha debe de ser la fecha actual en la que se genero la factura
		inv.setInvoiceDate(LocalDate.parse(formateador.format(ahora), DateTimeFormatter.BASIC_ISO_DATE));
		inv.setTotal(Integer.parseInt(invoice.get("total").toString()));
		inv.setExpirationDay(LocalDate.parse(invoice.get("invoiceDate").toString(), DateTimeFormatter.BASIC_ISO_DATE));
		inv.setContrato(con);

		config.setConsecutiveFactura(cons);
		config.setConsecutiveRefPago(ref);
		configurationService.save(config);
		Invoices invo = invoiceServices.save(inv);
		
		consecutiveB =   invo.getId().toString();
		
		
		//Se guardan las novedades asigandas a la facturas
		
		for (int i = 0; i < contractDetailNews.length(); i++) {
			InvoiceNews details = new InvoiceNews();
			

			JSONObject dato = contractDetailNews.getJSONObject(i);
			details.setName(dato.get("productNameTable").toString());
			details.setInvoices(invo);
			details.setTaxes(Double.parseDouble(dato.get("taxes").toString()));
			details.setUnitPrice(Double.parseDouble(dato.get("unitPrice").toString()));
			details.setAmount(Integer.parseInt(dato.get("amount").toString()));
			details.setObservations(dato.get("Observations").toString());
			details.setTipo(dato.get("type").toString());
			
			invoiceNewsService.save(details);
			
		}

		List<ContractDetails> cd = contractDetailsService.findByContracts(con);
		Double vContrato = (double) 0;
		
		//Se guardan los productos asigandos a la facturas.
		
		for (int i = 0; i < contractDetailProducts.length(); i++) {
			InvoiceNews details = new InvoiceNews();
			JSONObject dato = contractDetailProducts.getJSONObject(i);
			
			// calculamos el precio unitario segun los dias a facturar
			Double precioUnitarioDiario = Double.parseDouble(dato.get("unitPrice").toString()) / 30;
			Double dias = Double.parseDouble(invoice.get("diasFacturar").toString());
			Double precioUnitario = precioUnitarioDiario * dias;
						

			details.setName(dato.get("productNameTable").toString());
			details.setInvoices(invo);
			details.setTaxes(Double.parseDouble(dato.get("taxes").toString()));
			details.setUnitPrice(Double.parseDouble(precioUnitario.toString()));
			details.setAmount(Integer.parseInt(dato.get("amount").toString()));
			details.setObservations(dato.get("Observations").toString());
			details.setTipo(dato.get("type").toString());
			
			invoiceNewsService.save(details);
			
			Double Suma = (precioUnitario * Integer.parseInt(dato.get("amount").toString()));
			
			Double impuesto = Suma * Double.parseDouble(dato.get("taxes").toString());
			
			vContrato += Suma + impuesto;
		}

		List<ContractNews> cn = contractsNewsService.findByContractsEnabled(con);

		// Guardamos las novedades 
		for (ContractNews cn2 : cn) {
			// Actualizamos las veces que se ha cobrado la novedad
			cn2.setBilledMonths(cn2.getBilledMonths() + 1);
			contractsNewsService.save(cn2);
		}

		int diasMora = con.getDaysPastDue();
		//Se calcula el valor de mora de la factura.
		
		if (diasMora > 0) {

			Double anual = calcular();
			double dias = 365;
			Double pDia = anual / dias;

			Double valorDia = (vContrato * pDia);

			LocalDate monthAux = inv.getExpirationDay().minusMonths(1);
			InvoiceNews inDetail = new InvoiceNews();
			inDetail.setAmount(diasMora);
			inDetail.setInvoices(invo);
			inDetail.setTaxes(10.5);
			inDetail.setTipo("Novedad - Cobro");
			inDetail.setUnitPrice(valorDia);
			inDetail.setName("Intereses por mora "+ monthOfYear(monthAux.getMonthValue()));
			inDetail.setObservations("Cobro de intereses por pago tardío de la factura");
			invoiceNewsService.save(inDetail);
			
			InvoiceNews inIvaMora = new InvoiceNews();
			inIvaMora.setAmount(1);
			inIvaMora.setInvoices(invo);
			inIvaMora.setTaxes(0.0);
			inIvaMora.setTipo("Novedad - Cobro");
			inIvaMora.setUnitPrice(valorDia * diasMora * 19 / 100);
			inIvaMora.setName("Iva intereses por mora " + monthOfYear(monthAux.getMonthValue()));
			inIvaMora.setObservations("Cobro de iva de intereses por pago tardío de la factura");
			invoiceNewsService.save(inIvaMora);
			
			invo.setTotal((int) (invo.getTotal() + inIvaMora.getUnitPrice() + (inDetail.getUnitPrice() * inDetail.getAmount())));
			invoiceServices.save(invo);

		}
		
	
		// Actualizamos el contrato para que quede en 0 los dias de mora
		con.setDaysPastDue(0);
		contractsService.save(con);
	}


		response.put("mensaje", consecutiveB.toString() );
		response.put("data", null);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);

	}

	@PreAuthorize("hasAuthority('INVOICES_WRITE')")
	@PutMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> update(@PathVariable Long id, @RequestBody Map<String, Object> invoice) {
		Map<String, Object> response = new HashMap<>();
		Invoices invMonth = null;
		ArrayList<?> contractDetail1 = null;
		ArrayList<?> contractDetail2 = null;
		
		
		// se valida si la factura pertenece a un contrato o a un asociado
		Boolean validateClient = Boolean.parseBoolean(invoice.get("invoiceClient").toString());
		
		if(validateClient) {
			//en este bloque se realiza el proceso para una factura a un asociado.
			
			Associated con = associateServices.findById(Integer.parseInt(invoice.get("contract").toString()));
			Invoices invos= invoiceServices.findById(id);
			
			List<InvoiceNews> news  = invoiceNewsService.findByInvoices(invos);
			
			// Se eliminan los productos y novedades de la factura para luego ser creados nuevamente.
			news.forEach(cont -> {
				invoiceNewsService.delete(cont.getId());
			});
			
	
			Date ahora = new Date();
			SimpleDateFormat formateador = new SimpleDateFormat("yyyyMMdd");

			Configurations config = configurationService.findById((long) 1);
			
			contractDetail1 = (ArrayList<?>) invoice.get("detailNes");
			JSONArray contractDetailNews = new JSONArray(contractDetail1);
			
			
			contractDetail2 = (ArrayList<?>) invoice.get("detailProducts");
			JSONArray contractDetailProducts = new JSONArray(contractDetail2);
			

			invos.setId(id);
			
			//Se arma el objeto de la factura que se va a actualizar
			
			DecimalFormat df = new DecimalFormat("0000000");
			String ref =  Integer.toString((Integer.parseInt(config.getConsecutiveRefPago())+ 1));
			ref = df.format(Integer.parseInt(ref));
			String cons = Integer.toString(Integer.parseInt(config.getConsecutiveFactura()) + 1);
			cons = df.format(Integer.parseInt(cons));
			invos.setConsecutive(cons);
			invos.setRefPago(ref);
			invos.setEnvoicesMonth(invoice.get("invoiceMonth").toString());
			invos.setEnvoicesState("Abierta");
			// esta fecha debe de ser la fecha actual en la que se genero la factura
			invos.setInvoiceDate(LocalDate.parse(formateador.format(ahora), DateTimeFormatter.BASIC_ISO_DATE));
			invos.setTotal(Integer.parseInt(invoice.get("total").toString()));
			invos.setExpirationDay(LocalDate.parse(invoice.get("invoiceDate").toString(), DateTimeFormatter.BASIC_ISO_DATE));
			

			Invoices invo = invoiceServices.save(invos);
			
			
			//Se guardan las novedades de la factura
			for (int i = 0; i < contractDetailNews.length(); i++) {
				InvoiceNews details = new InvoiceNews();
				

				JSONObject dato = contractDetailNews.getJSONObject(i);
				details.setName(dato.get("productNameTable").toString());
				details.setInvoices(invo);
				details.setTaxes(Double.parseDouble(dato.get("taxes").toString()));
				details.setUnitPrice(Double.parseDouble(dato.get("unitPrice").toString()));
				details.setAmount(Integer.parseInt(dato.get("amount").toString()));
				details.setObservations(dato.get("Observations").toString());
				details.setTipo(dato.get("type").toString());				
				invoiceNewsService.save(details);
				
			}
			
			Double vContrato = (double) 0;
			
			//Se guardan los productos de la factura
			
			for (int i = 0; i < contractDetailProducts.length(); i++) {
				InvoiceNews details = new InvoiceNews();
				JSONObject dato = contractDetailProducts.getJSONObject(i);
				
				// calculamos el precio unitario segun los dias a facturar
				Double precioUnitarioDiario = Double.parseDouble(dato.get("unitPrice").toString());
				
				
				details.setName(dato.get("productNameTable").toString());
				details.setInvoices(invo);
				details.setTaxes(Double.parseDouble(dato.get("taxes").toString()));
				details.setUnitPrice(Double.parseDouble(precioUnitarioDiario.toString()));
				details.setAmount(Integer.parseInt(dato.get("amount").toString()));
				details.setObservations(dato.get("Observations").toString());
				details.setTipo(dato.get("type").toString());
				
				invoiceNewsService.save(details);
				
				
				// Se calcula el valor de la factura.
				Double Suma = (precioUnitarioDiario * Integer.parseInt(dato.get("amount").toString()));
				
				Double impuesto = Suma * Integer.parseInt(dato.get("taxes").toString());
				
				vContrato += Suma + impuesto;
			}
			
			
		}else {
			
			// En este bloque se realiza el procedimiento a una factura cuando pertenece a un contrato

		Contracts con = contractsService.findById(Long.parseLong(invoice.get("contract").toString()));

		invMonth = invoiceServices.findByContractAndMonth(con, invoice.get("invoiceMonth").toString());
		
		Date ahora = new Date();
		SimpleDateFormat formateador = new SimpleDateFormat("yyyyMMdd");

		Configurations config = configurationService.findById((long) 1);
		
		contractDetail1 = (ArrayList<?>) invoice.get("detailNes");
		JSONArray contractDetailNews = new JSONArray(contractDetail1);
		
		
		contractDetail2 = (ArrayList<?>) invoice.get("detailProducts");
		JSONArray contractDetailProducts = new JSONArray(contractDetail2);
		
		
		
		Invoices invos= invoiceServices.findById(id);
		
		List<InvoiceNews> news  = invoiceNewsService.findByInvoices(invos);

		//Se arma el objeto de la factura que va a ser modificada
		Invoices inv = new Invoices();
		inv.setConsecutive(invos.getConsecutive());
		inv.setRefPago(invos.getRefPago());
		inv.setEnvoicesMonth(invoice.get("invoiceMonth").toString());
		inv.setEnvoicesState("Abierta");
		// sta fecha debe de ser la fecha actual en la que se genero la factura
		inv.setInvoiceDate(LocalDate.parse(formateador.format(ahora), DateTimeFormatter.BASIC_ISO_DATE));
		inv.setTotal(Integer.parseInt(invoice.get("total").toString()));
		inv.setExpirationDay(LocalDate.parse(invoice.get("invoiceDate").toString(), DateTimeFormatter.BASIC_ISO_DATE));
		inv.setContrato(con);
		inv.setId(id);
		
		//Se eliminan los productos y las novedades de la factura para luego ser creadas nuevamente
		news.forEach(cont -> {
			invoiceNewsService.delete(cont.getId());
		});
		
		// Se guardan las novedadesde la factura.
		for (int i = 0; i < contractDetailNews.length(); i++) {
			InvoiceNews details = new InvoiceNews();
			JSONObject dato = contractDetailNews.getJSONObject(i);
			details.setName(dato.get("productNameTable").toString());
			details.setInvoices(inv);
			details.setTaxes(Double.parseDouble(dato.get("taxes").toString()));
			details.setUnitPrice(Double.parseDouble(dato.get("unitPrice").toString()));
			details.setAmount(Integer.parseInt(dato.get("amount").toString()));
			details.setObservations(dato.get("Observations").toString());
			details.setTipo(dato.get("type").toString());
			
			invoiceNewsService.save(details);
			
		}

		List<ContractDetails> cd = contractDetailsService.findByContracts(con);
		Double vContrato = (double) 0;
		
		//Se guardan los productos de la factura.
	
		for (int i = 0; i < contractDetailProducts.length(); i++) {
			InvoiceNews details = new InvoiceNews();
			JSONObject dato = contractDetailProducts.getJSONObject(i);
			
			// calculamos el precio unitario segun los dias a facturar
			Double precioUnitarioDiario = Double.parseDouble(dato.get("unitPrice").toString()) / 30;
			Double dias = 30.0;
			Double precioUnitario = precioUnitarioDiario * dias;
							
			details.setName(dato.get("productNameTable").toString());
			details.setInvoices(inv);
			details.setTaxes(Double.parseDouble(dato.get("taxes").toString()));
			details.setUnitPrice(Double.parseDouble(precioUnitario.toString()));
			details.setAmount(Integer.parseInt(dato.get("amount").toString()));
			details.setObservations(dato.get("Observations").toString());
			details.setTipo(dato.get("type").toString());
			
			invoiceNewsService.save(details);
			
			// Se calcula el valor de la factura
			Double Suma = (precioUnitario * Integer.parseInt(dato.get("amount").toString()));		
			Double impuesto =  (Suma * Double.parseDouble(dato.get("taxes").toString())) / 100;			
			vContrato += Suma + impuesto;
		}

		inv.setTotal(Integer.parseInt(invoice.get("total").toString()));
		Invoices invo = invoiceServices.save(inv);
		
		// se calcula la mora de la factura.
		int diasMora = con.getDaysPastDue();
		if (diasMora > 0) {

			Double anual = calcular();
			double dias = 365;
			Double pDia = anual / dias;

			Double valorDia = (vContrato * pDia);

			InvoiceNews inDetail = new InvoiceNews();
			inDetail.setAmount(diasMora);
			inDetail.setInvoices(invo);
			inDetail.setTaxes(0.0);
			inDetail.setTipo("Novedad - Cobro");
			inDetail.setUnitPrice(valorDia);
			inDetail.setName("Intereses mora");
			inDetail.setObservations("Cobro de intereses por pago tardío de la factura");

			invoiceNewsService.save(inDetail);

		}
		
	
		// Actualizamos el contrato para que quede en 0 los dias de mora
		
		contractsService.save(con);
	}

		response.put("mensaje", "¡La factura ha sido actualizada con éxito!");
		response.put("data", null);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);

	}

	@PreAuthorize("hasAuthority('INVOICES_WRITE')")
	@PutMapping(path = "/generarPDF/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> generatePDF(@PathVariable Long id) throws JRException, SQLException {

		Map<String, Object> response = new HashMap<>();

		Invoices invo = invoiceServices.findById(id);
		
		JasperReport jasperReport;
		String pdfPath = Constant.RUTAPDF;
		
		// se valida si la factura es a un contrato o a un asociado para saber que pdf se va a usar.
		
		if( invo.getContrato() != null) {
			
			jasperReport = JasperCompileManager.compileReport(pdfPath + "invoice-copia(3).jrxml");
		}else {
			
			 jasperReport = JasperCompileManager.compileReport(pdfPath + "invoiceSalesReal.jrxml");
		}
		
		// Se llama al metodo generatePDFMeto para generar el pdf y este devuelve la ruta en donde quedo el pdf.
		String dirPath = generatePDFMeto(invo, jasperReport);

		response.put("mensaje", "¡La factura ha sido generada con éxito!");
		response.put("url", dirPath);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
	}
	
	@PreAuthorize("hasAuthority('INVOICES_WRITE')")
	@PutMapping(path = "/UpdateStatus/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> UpdateStatus(@PathVariable Long id, @RequestBody Map<String, Object> prod) throws JRException, SQLException {

		Map<String, Object> response = new HashMap<>();

		// se arma el objeto con la factura va ser anulada y se gaurda el cambio de esatdo.
		
		Invoices invoices = invoiceServices.findById(id);		
		invoices.setEnvoicesState("Anulada");
		invoices.setCancel(true);
		invoices.setReasonCancel(prod.get("retirementReason").toString());
		invoices.setObservationCancel(prod.get("retirementObservation").toString());
		
		invoiceServices.save(invoices);
		
		response.put("mensaje", "¡La factura ha sido generada con éxito!");
		response.put("data", invoices);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
	}

	public String generatePDFMeto(Invoices invoices, JasperReport jasperReport) throws JRException, SQLException {

		String pdfPath = Constant.RUTAPDF;
		String reportPath = Constant.ruta + File.separator;

		// se valida si la factura es a un contrato o a un asociado.
		Contracts contract = null;
		Associated associate = null;
		if(invoices.getContrato() != null) {
			contract = invoices.getContrato();
		    associate = contract.getIdClient().getIdAssociated();
		}else {
			associate = invoices.getAssociate();
		}
		
		
		double suma = 0;
			
		// se calcula el valor de la factura
		for (InvoiceNews detalle : invoices.getInvoiceNews()) {
		
		if(detalle.getUnitPrice() < 0) {
			
			suma  = suma + detalle.getUnitPrice();				
		  }
		}
		
		Configurations config = configurationService.findById((long) 1);
				
		Connection conn = jdbcTemplate.getDataSource().getConnection();

		LocalDate date = invoices.getExpirationDay();
		
		String formatd = invoices.getEnvoicesMonth() +"/30" ;
		
		String[] concatDAte = formatd.toString().split("/");
		
		String dates = concatDAte[0]+"-" + concatDAte[1]+"-" +"01"   +" al " + formatd.replace("/", "-") ;
		
		

		// Consultamos si debe otro contrato
		List<Invoices> deudasAnt = null;
		deudasAnt = invoiceServices.findInvoicesOpen(invoices.getContrato(), "Abierta", invoices.getId());
		int deudas = 0; 
		if (deudasAnt.size() != 0) { 

			for (int i = 0; i < deudasAnt.size(); i++) {
				Invoices fac = deudasAnt.get(i);
				deudas += fac.getTotal();
			}
		}

		// en esta parte se organiza la direccion si es dependiendo si es persona natular o nit
		Long consecutivo = Long.parseLong(invoices.getConsecutive());
		String direccion = "";
		String[] dire = null;
		if(contract != null) {
			dire = contract.getAddress().split("-");
		}else {
			dire = associate.getAddress().split("-");
		}
		 
		for (int i = 0; i < dire.length; i++) {
			int post = i +1;
			 
			if (!(dire[i].equals("Otros"))) {
				if(post <= 2) {
					if(post == 2) {
						if(!dire[i].equals("#")) 
							direccion += dire[i] + " # ";
					}else {
						if(!dire[i].equals("#")) 
							direccion += dire[i] + " ";
					}
				}else {
					if(!dire[i].equals("#")) {
						if(dire.length == 4) {
							if(i == 2) {
								direccion += dire[i] + " - ";
							}else {
								direccion += dire[i] + " ";
							}
						}else {
							if(i == 3) {
								direccion += dire[i] + " - ";
							}else {
								direccion += dire[i] + " ";
							}
						}
					}
				}
			}
  
		}

		
		Map<String, Object> parameters = new HashMap<>();
		
		String dato = associate.getIdentificationType().toString();
		String documentType = "Cedula de ciudadania(C.C)  ";
		
		//Se asigna la ruta de donde estan los recursos necesarios para el pdf.
		parameters.put("ruta_recursos", pdfPath);
		parameters.put("cliente_nombre", associate.getFirstName() + " " + associate.getLastName());
		
		
		if(dato.equals("1") ||dato.equals("2") ){	
			// Se arma la direccion y demas datos personales cuando es persona natural
			parameters.put("cliente_documento", " ");
			
			if(contract != null) {
				parameters.put("cliente_direccion", direccion);
				parameters.put("Ciudad",  contract.getNeighborhood());
				parameters.put("tipo_documento", contract.getCity());
			}else {
				parameters.put("cliente_direccion", direccion);
				parameters.put("Ciudad",  associate.getNeighborhood());
				parameters.put("tipo_documento", associate.getCity());
			}
			
		}else{
			// Se arma la direccion y demas datos personales cuando es empresa nit.
			parameters.put("cliente_documento", associate.getIdentificationNumber());
			parameters.put("tipo_documento","NIT: ");
			
			if(contract != null) {
				parameters.put("cliente_direccion", direccion + ", " + contract.getNeighborhood());
				parameters.put("Ciudad",  contract.getCity());
			}else {
				parameters.put("cliente_direccion", direccion + ", " + associate.getNeighborhood());
				parameters.put("Ciudad",  associate.getCity());
			}
			
		}		
		
		// se setenan los datos a jasper report para generar el pdf
		parameters.put("cliente_celular", associate.getCellPhoneNumber());
		parameters.put("fechanum", config.getConsecutiveFacturaDate().toString());
		parameters.put("finNum", config.getConsecutiveFacturaEnd());
		parameters.put("inicioNum", config.getConsecutiveFacturaStart());
		parameters.put("Resolution", config.getResolutionconsecutiveInvoice());
		
		parameters.put("cliente_email", associate.getEmail());
		parameters.put("factura_id", invoices.getId());
		parameters.put("factura_numero", consecutivo.toString());
		parameters.put("factura_periodo", dates);
		parameters.put("factura_fecha", invoices.getInvoiceDate().toString());
		parameters.put("factura_vencimiento", invoices.getExpirationDay().toString());
		parameters.put("factura_suspension",
				(date.getDayOfMonth() + 2) + " de " + monthOfYear(date.getMonthValue()) + " " + date.getYear());
		parameters.put("factura_ref_pago", invoices.getRefPago());

		
		Double ValorAbono = new Double(0);
		
		// se valida si factura tiene abonos para ser mostrados en el pdf
		for(InvoiceNews abono: invoices.getInvoiceNews()) {
			if(abono.getName().equals("Abono a la factura")) {
				ValorAbono = Double.parseDouble(abono.getUnitPrice().toString());
			}
		}
		
		int totalFactura = (invoices.getTotal() + deudas) + ValorAbono.intValue();
	
		
		//se valida el estado de la factura para ser mostrado como marca de agua en el pdf
		String Estado = invoices.getEnvoicesState();
	
		if(Estado.equals("Abierta")) {
			parameters.put("factura_estado", "");
		}else {
			parameters.put("factura_estado", invoices.getEnvoicesState());
		}

		parameters.put("factura_total", totalFactura);
		
		parameters.put("Descuento", (int) suma);
		parameters.put("factura_total", totalFactura);
		
		parameters.put("factura_total", totalFactura);
		
		int Ivac = (int) (totalFactura /1.19);
		
		int Iva  = Ivac * 19 /100;
		
			
		parameters.put("Subtotal_fact",(Ivac));
		parameters.put("Iva",(Iva));
		
		parameters.put("texto_adiconal", config.getTextoFactura());
		
		//se valida si la factura es a un contrato o a un asociado para mostrar el consecutivo de la factura.
		if(contract != null) {
			parameters.put("contrato_numero", contract.getConsecutive());
			parameters.put("contrato_id", contract.getId());
		}else {
			parameters.put("contrato_numero", invoices.getConsecutive());
		}
		

		// se arma el codigo de barras y el codigo QR
		String[] fecha = invoices.getExpirationDay().toString().split("-");
		String fechaExpiracion = fecha[0] + "" + fecha[1] + "" + fecha[2];

		final char FNC1 = Code128LogicImpl.FNC_1;
		
		String datos = "41577099984196988020" + invoices.getRefPago() + FNC1 + "3900" + totalFactura + FNC1 + "96" + fechaExpiracion;
		parameters.put("barCode",datos);
		parameters.put("barCode2",
				"(415)7709998419698(8020)" + invoices.getRefPago() + "(3900)" + totalFactura + "(96)" + fechaExpiracion);
		parameters.put("qrCode", "https://www.esgcomunicaciones.com/");

		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, conn);

		String dir2 = "facturas" + File.separator + invoices.getEnvoicesMonth() + File.separator;

		File dir = new File(reportPath + dir2);
		if (!dir.exists()) {
			dir.mkdirs();
		}

		//se Construye  la ruta de la factura dependiendo si es a un contrato o a un asociado
		String dirPath = "";
		if(contract != null) {
			dirPath = dir2 + contract.getIdClient().getIdAssociated().getFirstName()
					+ contract.getIdClient().getIdAssociated().getLastName() + "-" + invoices.getConsecutive() + ".pdf";
		}else {
			dirPath = dir2 + associate.getFirstName()
					+ associate.getLastName() + "-" + invoices.getConsecutive() + ".pdf";
		}
		
		
		String fileName = reportPath + dirPath;

		// se exporta el pdf
		JasperExportManager.exportReportToPdfFile(jasperPrint, fileName);

		conn.close();

		// guardamos la ruta de la factura
		invoices.setFile(dirPath);
		invoiceServices.save(invoices);

		// se retorna la ruta de la factura 
		return dirPath;

	}

	@GetMapping(path = "/changeStatusAuto", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> findByIdInvoices() {

		Map<String, Object> response = new HashMap<>();

		this.changeStatusAuto();
		response.put("mensaje", "estados actualizados con exito");
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);

	}

	public void changeStatusAuto() {

		List<Contracts> allContract = null;

		// se busacan todos los contratos disponibles para cambio de estado.
		allContract = contractsService.findContracsEnabled();
		allContract.forEach(cont -> {

			List<Invoices> invoices = null;
			
			// se traen todas la facturas que tenga abiertas el contrato para validar
			invoices =  cont.getInvoices().stream().filter(x -> x.getEnvoicesState().equals("Abierta")).collect(Collectors.toList());

			// verificamos cual es el ultimo estado de este contrato
			String estado = "";
			String fechaEstado = "";
				
			List<ContractStatus> listEstados = contractStatusService.lastStateInvoice(cont.getId());

			if (listEstados.size() > 0) {
				estado = listEstados.get(0).getStatus().getName();
				fechaEstado = listEstados.get(0).getCreateAt().toString();
			} else {
				estado = "No tiene estado asignado";
			}

			// 1. Se verifica si ya pago las facturas que debia
			if (estado.equals("SUSPENDIDO") && invoices.size() == 0) {

				Status status = statusService.findByName("RECONECTAR");
				Employee userCrea = employeeService.findById(1);

				ContractStatus contracStatus = new ContractStatus();
				//Se toma la fecha actual para colombia
				LocalDate fecha = LocalDate.now(ZoneId.of("UTC-5"));
				contracStatus.setCreateAt(fecha);
				
				contracStatus.setContract(cont);
				contracStatus.setStatus(status);
				contracStatus.setObservation("N/A");
				contracStatus.setUserCrea(userCrea);

				contractStatusService.save(contracStatus);
			}

			// Verificamos que si este contrato tenga 2 facturas vencidas,
			// si es asi se le cambia el estado del contrato, pasando de “CONECTADO” a “SUSPENDER”
			if(!estado.equals("SUSPENDIDO")) {
				if (invoices != null && invoices.size() >= 2) {
	
					// mes da la ultima factura sin pagar
					String[] envoicesMonth = (invoices.get(0).getEnvoicesMonth()).split("/");
					int yearInvoice = Integer.parseInt(envoicesMonth[0]);
					int montInvoice = Integer.parseInt(envoicesMonth[1]);
					int vencimiento = cont.getExpirationDay();
	
					// Sacamos la fecha actual
					Calendar fecha = Calendar.getInstance();
					int yearActual = fecha.get(Calendar.YEAR);
					int monthActual = fecha.get(Calendar.MONTH) + 1;
					int dayActual = fecha.get(Calendar.DAY_OF_MONTH);
	
					if (yearActual >= yearInvoice) {
	
						// verificamos que la fecha actual sea mayor o igual a la ultima factura
	
						if (((montInvoice == 12) && (monthActual >= 1)) || (monthActual >= montInvoice)) {
	
							// se valida si la fecha actual es mayor a la fecha de la factura mas los dias de vencimiento
							if (dayActual > vencimiento) {
	
								// aqui se debe cambiar el estado del contrato pasa de “CONECTADO” a “SUSPENDER”
								if (!estado.equals("SUSPENDER")) {	
									Status status = statusService.findByName("SUSPENDER");
									Employee userCrea = employeeService.findById(1);
	
									ContractStatus contracStatus = new ContractStatus();
									//Se toma la fecha actual para colombia
									LocalDate fechaActual = LocalDate.now(ZoneId.of("UTC-5"));
									contracStatus.setCreateAt(fechaActual);
									
									contracStatus.setContract(cont);
									contracStatus.setStatus(status);
									contracStatus.setObservation("N/A");
									contracStatus.setUserCrea(userCrea);
									cont.setEnabled(false);
									contractsService.save(cont);
									contractStatusService.save(contracStatus);
								}
							}
						}
	
					}
	
				}
			}
		});

	}
	
	public String monthOfYear(Integer number) {

		if (number == 1) {
			return "Enero";
		} else if (number == 2) {
			return "Febrero";
		} else if (number == 3) {
			return "Marzo";
		} else if (number == 4) {
			return "Abril";
		} else if (number == 5) {
			return "Mayo";
		} else if (number == 6) {
			return "Junio";
		} else if (number == 7) {
			return "Julio";
		} else if (number == 8) {
			return "Agosto";
		} else if (number == 9) {
			return "Septiembre";
		} else if (number == 10) {
			return "Octubre";
		} else if (number == 11) {
			return "Noviembre";
		} else if (number == 12) {
			return "Diciembre";
		} else {
			return "";
		}

	}
	
	@PreAuthorize("hasAuthority('INVOICES_WRITE')")
	@Transactional
	@GetMapping(path = "/downloadUrls/{fecha}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> downloadUrls(@PathVariable String fecha) {

		Map<String, Object> response = new HashMap<>();
		
		List<Invoices> invoice = invoiceServices.findByfechaAndRecurrence(fecha.replace('-', '/'));

		List<InvoicesDTO> InvoiceDTO = new ArrayList<InvoicesDTO>();
		for(Invoices item: invoice) {
			
			InvoiceDTO.add(InvoicesDTO.builder()
					.associateName(item.getContrato().getIdClient().getIdAssociated().getFirstName().concat(" ")
							.concat(item.getContrato().getIdClient().getIdAssociated().getLastName()))
					.referenciaPago(item.getRefPago())
					.total(item.getTotal())
					.invoiceDate(item.getInvoiceDate())
					.expirationDay(item.getExpirationDay())
					.file(item.file)
					.celPhone(item.getContrato().getIdClient().getIdAssociated().getCellPhoneNumber())
					.celPhone2(item.getContrato().getIdClient().getCellPhoneNumber2())
					.build());
		}
		
		response.put("mensaje", InvoiceDTO);

		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);

	}
	
	@PreAuthorize("hasAuthority('INVOICES_WRITE')")
	@Transactional
	@GetMapping(path = "/sendEmailMasivo/{fecha}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> sendEmailMasivo(@PathVariable String fecha) {

		Map<String, Object> response = new HashMap<>();
		
		//se buscan las facturas generadas en el periodo especificado
		List<Invoices> invoice = invoiceServices.findByfechaAndRecurrence(fecha.replace('-', '/'));
		
		invoice.forEach(invo -> {
			try {
				sendEmail(invo.getId());
			}catch (Exception e) {
				response.put("errores", e);
			}
		});

		response.put("mensaje", "Se enviaron los correos con las facturas correctamente.");

		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);

	}

	@PreAuthorize("hasAuthority('INVOICES_WRITE')")
	@Transactional
	@GetMapping(path = "/sendEmail/{idFactura}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> sendEmail(@PathVariable Long idFactura) throws JRException, SQLException {

		Map<String, Object> response = new HashMap<>();

		Invoices invoice = invoiceServices.findById(idFactura);
		Configurations config = configurationService.findOne();

		// se valida que la factura tenga un pdf generado.
		String file = null;
		file = invoice.getFile();

		if (file == null || file == "") {

			response.put("mensaje", "Para poder enviar la factura por E-mail primero debe generarla.");
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);

		}

		// se arma la configuracion del cuerpo y sujeto del correo
		String reportPath = Constant.ruta + File.separator;
		String nameFile = invoice.getRefPago() + ".pdf";
		String to ="";
		String subject = "Tu Factura ESG No. " + invoice.getRefPago();
		
		String body = "";
		
		String[] periodo = invoice.getEnvoicesMonth().split("/");
		NumberFormat format = NumberFormat.getCurrencyInstance(Locale.getDefault());
		String currency = format.format(invoice.getTotal());
		currency = currency.substring(0, currency.length() - 3);
		
		
		// se valida si la factura pertenece a un contrato o a un asociado para saber que diseño de correo utilizar.
		if(invoice.getContrato() != null) {
			body = "<body style='width: 100%; background-color: white;'>" +
					"<style>" +
					"@media only screen and (min-device-width: 375px) and (max-device-width: 413px) {" + 
					".div_container table" + 
					"	{width:100%!important}" + 
					".img-pse" +
					"{width: 80px; height: 60px}" +
					".btn-pagar" +
					"{width: 70px; height: 35px}" +
					".td-center-rigth" + 
					"{width: 55%; padding: 0px }" +
					".td-center-left" + 
					"{width: 45%}" +
					"}" +
					"</style>" +
					"<center>" +
					"<div class='div_container' style='width:100%; margin:0 auto'>" +
						"<table style='width: 60%' bgcolor='#ed7d31'>" +
							"<tr>" +
								"<td style='width: 60%; padding: 8px'>" +
									"<div style='height: 100%;'>" +
										"<div style='color: white; font-size: 17px;'>Hola,</div>" +
										"<div style='color: white; font-size: 17px;'>" + invoice.getContrato().getIdClient().getIdAssociated().getFirstName() + " " + invoice.getContrato().getIdClient().getIdAssociated().getLastName() + "</div>" +
										"<div style='color: white; font-size: 17px;'>Contrato #. " + invoice.getContrato().getConsecutive() + "</div>" +
									"</div>" +
								"</td>" +
								"<td style='width: 40%; padding: 3px' align='right'>" +
									"<img data-imagetype='External' src='http://162.241.134.243:8080/uploadsESG/logo-invoice.PNG' width='105' height='75'>" +
								"</td>" +
							"</tr>" +
						"</table>" +
					 	"<table style='width: 60%'>" +
							"<tr>" +
								"<td class='td-center-left' valign='top' style='width: 50%'>" +
									"<div style='padding: 10px'>" +
										"<p style='color: #00b0f0'><img data-imagetype='External' src='http://162.241.134.243:8080/uploadsESG/wifi.png'  width='15' height='13'> Ya está disponible la factura  <strong><span style='color: #0070c0'>No. " + invoice.getConsecutive() + "</span></strong>" +
										 " de tu servicio de internet, correspondiente al periodo <strong><span style='color: #0070c0'>" +  monthOfYear(Integer.parseInt(periodo[1])) + " del " + periodo[0] + "</span></strong></p>" +
									"</div><br>" +
									"<center>" +
							 			"<div style='color: #00b0f0'>Fecha límite de pago</div>" +
							 			"<div style='color: #0070c0;'><strong>" + invoice.getExpirationDay() + "</strong></div>" +
							 		"</center><br>" +
							 		"<center>" +
							 			"<div style='color: #00b0f0'>Referencia de pago</p>" +
							 			"<div style='color: #0070c0;'><strong>" + invoice.getRefPago() + "</strong></div>" +
							 		"</center>" +
								"</td>" +
								"<td  class='td-center-rigth' style='width: 50%; padding: 25px 5px'>" +
									"<center>" +
							 			"<div style='color: #ed7d31; font-size: 20px'>Valor a pagar</div>" +
							 			"<div style='color: #c55a11;font-size: 22px;'><strong>" + currency + "</strong></div>" +
							 			"<br>" +
							 		"</center>" +
							 		"<center>" +
							 			"<p style='color: #00b0f0'>Conoce el detalle de tu factura en el <strong>ADJUNTO</strong></p>" +
							 		"<br>" +
							 		"</center>" +
							 		"<table style='width: 100%'>" +
							 			"<tr>" +
							 				"<td style='width: 60%; padding: 10px'>" +
							 					"<a href='" + config.getUrlPayment() + "' target='_blank'><img class='btn-pagar' data-imagetype='External' src='http://162.241.134.243:8080/uploadsESG/PAGA.png'  width='120' height='50'></a>" +
							 				"</td>" +
							 				"<td style='width: 40%; padding: 10px'>" +
							 					"<img class='img-pse' data-imagetype='External' src='http://162.241.134.243:8080/uploadsESG/logofv.png'  width='140' height='105'>" +
							 				"</td>" +
							 			"</tr>" +
							 		"</table>" +
								"</td>" +
							"</tr>" +
						"</table>" +
					 	"<table style='width: 60%' bgcolor='#f2f2f2'>" +
							"<tr>" +
								"<td style='width: 50%; padding: 10px;'>" +
									"<div style='font-size: 12px; color: #606060;'>Si tienes alguna duda puedes contactarte con nosotros al número telefónico:</span></div>" +
									"<div style='font-size: 12px; color: #606060;'>(057) 3183394361 - (057) 2834070 o  al correo </div>" +
									"<div style='font-size: 12px;'><span style='color:#0563c1; text-decoration: underline;'>esgcomunicacionessas@gmail.com.</span></div>" +
								"</td>" +
								"<td style='width: 0.2%;background-color: white'></td>" +
								"<td style='width: 49.8%; padding: 12px;'>" +
									"<center>" +
							 			"<div style='font-size: 12px; color: #00b0f0;'>*Para conservar tu factura, puedes descargar y guardar en tu computadora o celular. Recuerda la solicitud de copia en físico tiene un costo.</div>" +	 
							 		"</center>" +
								"</td>" +
							"</tr>" +
						"</table>" +
				 	"</div>" +
				 	"</center>" +
				 	"</body>";

			 to = invoice.getContrato().getIdClient().getIdAssociated().getEmail();
			
		}else {
			
			body = "<body style='width: 100%; background-color: white;'>" +
					"<style>" +
					"@media only screen and (min-device-width: 375px) and (max-device-width: 413px) {" + 
					".div_container table" + 
					"	{width:100%!important}" + 
					".img-pse" +
					"{width: 80px; height: 60px}" +
					".btn-pagar" +
					"{width: 70px; height: 35px}" +
					".td-center-rigth" + 
					"{width: 55%; padding: 0px }" +
					".td-center-left" + 
					"{width: 45%}" +
					"}" +
					"</style>" +
					"<center>" +
					"<div class='div_container' style='width:100%; margin:0 auto'>" +
						"<table style='width: 60%' bgcolor='#ed7d31'>" +
							"<tr>" +
								"<td style='width: 60%; padding: 8px'>" +
									"<div style='height: 100%;'>" +
										"<div style='color: white; font-size: 17px;'>Hola,</div>" +
										"<div style='color: white; font-size: 17px;'>" + invoice.getAssociate().getFirstName() + " " + invoice.getAssociate().getLastName() + "</div>" +
									"</div>" +
								"</td>" +
								"<td style='width: 40%; padding: 3px' align='right'>" +
									"<img data-imagetype='External' src='http://162.241.134.243:8080/uploadsESG/logo-invoice.PNG' width='105' height='75'>" +
								"</td>" +
							"</tr>" +
						"</table>" +
					 	"<table style='width: 60%'>" +
							"<tr>" +
								"<td class='td-center-left' valign='top' style='width: 50%'>" +
									"<div style='padding: 10px'>" +
										"<p style='color: #00b0f0'><img data-imagetype='External' src='http://162.241.134.243:8080/uploadsESG/wifi.png'  width='15' height='13'> Ya está disponible la factura  <strong><span style='color: #0070c0'>No. " + invoice.getConsecutive() + "</span></strong>" +
										 " de tu servicio de internet, correspondiente al periodo <strong><span style='color: #0070c0'>" +  monthOfYear(Integer.parseInt(periodo[1])) + " del " + periodo[0] + "</span></strong></p>" +
									"</div><br>" +
									"<center>" +
							 			"<div style='color: #00b0f0'>Fecha límite de pago</div>" +
							 			"<div style='color: #0070c0;'><strong>" + invoice.getExpirationDay() + "</strong></div>" +
							 		"</center><br>" +
							 		"<center>" +
							 			"<div style='color: #00b0f0'>Referencia de pago</p>" +
							 			"<div style='color: #0070c0;'><strong>" + invoice.getRefPago() + "</strong></div>" +
							 		"</center>" +
								"</td>" +
								"<td class='td-center-rigth' style='width: 50%; padding: 25px 5px'>" +
									"<center>" +
							 			"<div style='color: #ed7d31; font-size: 20px'>Valor a pagar</div>" +
							 			"<div style='color: #c55a11;font-size: 22px;'><strong>" + currency + "</strong></div>" +
							 			"<br>" +
							 		"</center>" +
							 		"<center>" +
							 			"<p style='color: #00b0f0'>Conoce el detalle de tu factura en el <strong>ADJUNTO</strong></p>" +
							 		"<br>" +
							 		"</center>" +
							 		"<table style='width: 100%'>" +
							 			"<tr>" +
							 				"<td style='width: 60%; padding: 10px'>" +
							 					"<a href='" + config.getUrlPayment() + "' target='_blank'><img class='btn-pagar' data-imagetype='External' src='http://162.241.134.243:8080/uploadsESG/PAGA.png'  width='120' height='50'></a>" +
							 				"</td>" +
							 				"<td style='width: 40%; padding: 10px'>" +
							 					"<img class='img-pse' data-imagetype='External' src='http://162.241.134.243:8080/uploadsESG/logofv.png'  width='140' height='105'>" +
							 				"</td>" +
							 			"</tr>" +
							 		"</table>" +
								"</td>" +
							"</tr>" +
						"</table>" +
					 	"<table style='width: 60%' bgcolor='#f2f2f2'>" +
							"<tr>" +
								"<td style='width: 50%; padding: 10px;'>" +
									"<div style='font-size: 12px; color: #606060;'>Si tienes alguna duda puedes contactarte con nosotros al número telefónico:</span></div>" +
									"<div style='font-size: 12px; color: #606060;'>(057) 3183394361 - (057) 2834070 o  al correo </div>" +
									"<div style='font-size: 12px;'><span style='color:#0563c1; text-decoration: underline;'>esgcomunicacionessas@gmail.com.</span></div>" +
								"</td>" +
								"<td style='width: 0.2%;background-color: white'></td>" +
								"<td style='width: 49.8%; padding: 12px;'>" +
									"<center>" +
							 			"<div style='font-size: 12px; color: #00b0f0;'>*Para conservar tu factura, puedes descargar y guardar en tu computadora o celular. Recuerda la solicitud de copia en físico tiene un costo.</div>" +	 
							 		"</center>" +
								"</td>" +
							"</tr>" +
						"</table>" +
				 	"</div>" +
				 	"</center>" +
				 	"</body>";
			
			 to = invoice.getAssociate().getEmail();
		}
	
		sendEmail.sendEmail(to, subject, body, reportPath + file, nameFile);

		response.put("mensaje", "Correo enviado exitosamente");
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);

	}

	@Transactional
	@PreAuthorize("hasAuthority('INVOICES_WRITE')")
	@PostMapping(path = "/recurrente", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> createRecurrente(@RequestBody Map<String, Object> data)
			throws JRException, FileNotFoundException {

		Map<String, Object> response = new HashMap<>();

		List<Contracts> allContract = null;

		int cuttingDay = Integer.parseInt(data.get("cuttingDay").toString());
		String periodicity = data.get("periodicity").toString();

		
		// se buscan los contratos con estado "Conecatdo" y activos para ser facturados
		allContract = contractsService.findContracsRecurrente(cuttingDay, periodicity);

		allContract.forEach(con -> {
	
			// se valida que el contrato no tenga una factura para el perido seleccionado
			List<Invoices> invMonth =  con.getInvoices().stream().filter(x -> x.getEnvoicesMonth().equals(data.get("invoiceMonth").toString())).collect(Collectors.toList());

			if (invMonth.size() == 0 ) {

				// periodo donde se debe iniciar a facturar
				LocalDate fechaInicioFacturar = con.getStartDate();
				int day = fechaInicioFacturar.getDayOfMonth();
				int monthStart = fechaInicioFacturar.getMonthValue();
				int year = fechaInicioFacturar.getYear();

				// periodo que se esta facturando
				String[] periodo = data.get("invoiceMonth").toString().split("/");
				int yearActual = Integer.parseInt(periodo[0]);
				int MonthFActurar = Integer.parseInt(periodo[1]);

				LocalDate pedriodoFacturar = LocalDate.of(yearActual, MonthFActurar, 1);
				LocalDate pedriodoInicioFact = LocalDate.of(year, monthStart, day);
				LocalDate pedriodoInicioFactIgual = LocalDate.of(year, monthStart, 1);

				if ((pedriodoFacturar.isAfter(pedriodoInicioFact))
						|| (pedriodoFacturar.isEqual(pedriodoInicioFactIgual))) {

					// si la factura no se a generado ingresa aqui.

					
					// se valida si la factura anterior tiene mora
					Date ahora = new Date();
					SimpleDateFormat formateador = new SimpleDateFormat("yyyyMMdd");
					List<Invoices> invoi  =   con.getInvoices().stream().filter(x -> x.getEnvoicesState().equals("Abierta")).collect(Collectors.toList());
					LocalDate fechaNuevaFactura = LocalDate.of(yearActual, MonthFActurar, con.getCuttingDay());
					List<Invoices> invMora = (List<Invoices>) invoi;
					if(invMora.size() > 0){
						invMora.forEach(iM ->{
							LocalDate fechaVencimiento = iM.getExpirationDay();
							
							long diasMora = ChronoUnit.DAYS.between(fechaVencimiento, fechaNuevaFactura);
							
							if (diasMora > 0) {

								// Actualizamos el contrato con los dias de mora que se deben cobrar.

								int diasMoraActual = con.getDaysPastDue();

								con.setDaysPastDue((int) (diasMoraActual + diasMora));
								contractsService.save(con);
							}
						});
					}

					// Sacamos la fecha actual
					Calendar fecha = Calendar.getInstance();
					int expDay = con.getExpirationDay();


					LocalDate expirationDay = LocalDate.of(yearActual, MonthFActurar, expDay);

					Configurations config = configurationService.findById((long) 1);

					Double cobroNews = (double) 0;
					Double cobroMora = (double) 0;
					Double cobroDetalle = (double) 0;
					Double descuentoNews = (double) 0;
					int total = 0;

					
					// Clculamos el total del detalle de la factura
					for (ContractDetails cd2 : con.getContractDetails()) {

						// calculamos el precio unitario segun los dias a facturar
						Double precioUnitarioDiario = cd2.getUnitPrice() / 30;
						Double diasfacturar = (double) 0;

						if (monthStart == MonthFActurar && year == yearActual ) {
							diasfacturar = (double) (30 - day);
						} else {
							diasfacturar = (double) 30;
						}

						Double precioUnitario = precioUnitarioDiario * diasfacturar;
						Double impuestos = ((precioUnitario * cd2.getAmount()) * cd2.getTaxes()) / 100;

						cobroDetalle += (precioUnitario + impuestos);

					}

					// Guardamos El total de las novedades novedades
					for (ContractNews cn2 : con.getContractNews()) {

						Double precioUnitario = (cn2.getUnitPrice() / cn2.getNumberOfMonths());
						Double impuestos = ((precioUnitario * cn2.getAmount()) * cn2.getTaxes()) / 100;

						if (cn2.getNews().getTipo().equals("Descuento")) {

							descuentoNews += (precioUnitario + impuestos);

						} else {
							cobroNews += (precioUnitario + impuestos);
						}

					}

					// Calculamos la mora defacturas anteriores
					int diasMoraFact = con.getDaysPastDue();
					if (diasMoraFact > 0) {

						Double precioUnitario = diasMoraFact * 100.0;
						Double impuestos = (precioUnitario * 10.5) / 100;
						cobroMora = precioUnitario + impuestos;
					}

					
					// armamos el objeto de la factura 
					Invoices inv = new Invoices();
					DecimalFormat df = new DecimalFormat("0000000");
					String ref =  Integer.toString((Integer.parseInt(config.getConsecutiveRefPago())+ 1));
					ref = df.format(Integer.parseInt(ref));
					String cons = Integer.toString(Integer.parseInt(config.getConsecutiveFactura()) + 1);
					cons = df.format(Integer.parseInt(cons));
					inv.setConsecutive(cons);
					inv.setRefPago(ref);
					inv.setEnvoicesMonth(data.get("invoiceMonth").toString());
					inv.setEnvoicesState("Abierta");
					// sta fecha debe de ser la fecha actual en la que se genero la factura
					inv.setInvoiceDate(LocalDate.parse(formateador.format(ahora), DateTimeFormatter.BASIC_ISO_DATE));
					
					total = (int) ((cobroDetalle + cobroNews + cobroMora) - descuentoNews);
					
					inv.setTotal(total);
					inv.setExpirationDay(expirationDay);
					inv.setContrato(con);
					inv.setRecurringInvoice(true);

					config.setConsecutiveFactura(cons);
					config.setConsecutiveRefPago(ref);

					configurationService.save(config);
					Invoices invo = invoiceServices.save(inv);


					// Guardamos las novedades
					for (ContractNews cn2 : con.getContractNews()) {
						// Actualizamos las veces que se ha cobrado la novedad
						cn2.setBilledMonths(cn2.getBilledMonths() + 1);
						contractsNewsService.save(cn2);

						// Guardar el tipo de novedad.
						InvoiceNews inNews = new InvoiceNews();

						if (cn2.getNews().getTipo().equals("Descuento")) {
							Double a = (double) -1;
							inNews.setUnitPrice((cn2.getUnitPrice() / cn2.getNumberOfMonths()) * a);
						} else {
							inNews.setUnitPrice(cn2.getUnitPrice() / cn2.getNumberOfMonths());
						}

						inNews.setAmount(cn2.getAmount());
						inNews.setInvoices(invo);
						inNews.setTaxes(cn2.getTaxes());
						inNews.setTipo("Novedad -" + cn2.getNews().getTipo());
						inNews.setName(cn2.getNews().getName());
						inNews.setObservations(cn2.getObservations());
						invoiceNewsService.save(inNews);

					}

					Double vContrato = (double) 0;

					// guardamos el detalle del contrato
					for (ContractDetails cd2 : con.getContractDetails()) {

						// calculamos el precio unitario segun los dias a facturar
						Double precioUnitarioDiario = cd2.getUnitPrice() / 30;
						Double diasfacturar = (double) 0;

						if (monthStart == MonthFActurar && year == yearActual) {
							diasfacturar = (double) (30 - day);
						} else {
							diasfacturar = (double) 30;
						}

						Double precioUnitario = precioUnitarioDiario * diasfacturar;

						InvoiceNews inDetail = new InvoiceNews();
						inDetail.setAmount(cd2.getAmount());
						inDetail.setInvoices(invo);
						inDetail.setTaxes(cd2.getTaxes());
						inDetail.setTipo("Producto");
						inDetail.setUnitPrice(precioUnitario);
						inDetail.setName(cd2.getProducts().getName());
						inDetail.setObservations(cd2.getObservations());
						
						Double Suma = (precioUnitario * cd2.getAmount());
						
						Double impuesto = Suma * cd2.getTaxes();
						
						vContrato += Suma + impuesto;

						invoiceNewsService.save(inDetail);
					}

					// Verificamos si el contrato tiene dias de mora pendientes de cobrar y se
					// genera la novedad de cobro

					int diasMora = con.getDaysPastDue();
					if (diasMora > 0) {
						
						Double anual = calcular();
						double dias = 365;
						Double pDia = anual / dias;

						Double valorDia = (vContrato * pDia);
						LocalDate monthAux = expirationDay.minusMonths(1);
						InvoiceNews inDetail = new InvoiceNews();
						inDetail.setAmount(diasMora);
						inDetail.setInvoices(invo);
						inDetail.setTaxes(10.5);
						inDetail.setTipo("Novedad - Cobro");
						inDetail.setUnitPrice(valorDia);
						inDetail.setName("Intereses por mora "+ monthOfYear(monthAux.getMonthValue()));
						inDetail.setObservations("Cobro de intereses por pago tardío de la factura");
						invoiceNewsService.save(inDetail);
						
						InvoiceNews inIvaMora = new InvoiceNews();
						inIvaMora.setAmount(1);
						inIvaMora.setInvoices(invo);
						inIvaMora.setTaxes(0.0);
						inIvaMora.setTipo("Novedad - Cobro");
						inIvaMora.setUnitPrice(valorDia * diasMora * 19 / 100);
						inIvaMora.setName("Iva intereses por mora " + monthOfYear(monthAux.getMonthValue()));
						inIvaMora.setObservations("Cobro de iva de intereses por pago tardío de la factura");
						invoiceNewsService.save(inIvaMora);
						
						invo.setTotal((int) (invo.getTotal() + inIvaMora.getUnitPrice() + (inDetail.getUnitPrice() * inDetail.getAmount())));
						invoiceServices.save(invo);
					}

					// Actualizamos el contrato para que quede en 0 los dias de mora
					con.setDaysPastDue(0);
					contractsService.save(con);					
				}

			}

		});

		Date ahora = new Date();
		SimpleDateFormat formateador = new SimpleDateFormat("yyyyMMdd");
		LocalDate fecha = LocalDate.parse(formateador.format(ahora), DateTimeFormatter.BASIC_ISO_DATE);
		String invoiceMonth = data.get("invoiceMonth").toString();

		// consultamos el conteo de facturas generadas para ser devuelto al front
		List<Invoices> listFacturas = invoiceServices.findAllInvoicesRecurrentes(fecha, invoiceMonth);
		int cant = listFacturas.size();

		response.put("mensaje", "¡" + cant + " Facturas recurrentes creadas exitosamente!");
		response.put("data", cant);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);

	}

	@Transactional
	@PreAuthorize("hasAuthority('INVOICES_WRITE')")
	@PostMapping(path = "/generateRecurrente", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> generateRecurrente(@RequestBody Map<String, Object> data)
			throws JRException, FileNotFoundException {

		Map<String, Object> response = new HashMap<>();

		Date ahora = new Date();
		SimpleDateFormat formateador = new SimpleDateFormat("yyyyMMdd");

		String envoicesMonth = data.get("envoicesMonth").toString();
		LocalDate fecha = LocalDate.parse(formateador.format(ahora), DateTimeFormatter.BASIC_ISO_DATE);

		// se consultan las facturas generadas para el periodo espesificado
		
		List<Invoices> listFacturas = invoiceServices.findAllInvoicesRecurrentes(fecha, envoicesMonth);
		int cant = listFacturas.size();
		String reportPath = Constant.ruta + File.separator;
		List<InputStream> pdfs = new ArrayList<InputStream>();
		String pdfPath = Constant.RUTAPDF;
		// se abre la conexion del jaspper
		JasperReport jasperReport = JasperCompileManager.compileReport(pdfPath + "invoice-copia(3).jrxml");
		listFacturas.forEach(factura -> {
			try {
				
				String dirPath = generatePDFMeto(factura, jasperReport);

				// Unir los PDF
				pdfs.add(new FileInputStream(reportPath + dirPath));

			} catch (JRException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		});

		// guardamos en la tabla de facturas recurrentes
		Integer idEmployee = Integer.parseInt(data.get("idEmployee").toString());
		String month = data.get("month").toString();
		String year = data.get("year").toString();

		SimpleDateFormat formateador2 = new SimpleDateFormat("yyyy-MM-dd");
		// donde se va a guardar el nuevo reporte
		String rutaFact = "FacturasRecurrentes" + File.separator + year + File.separator;

		File dir = new File(reportPath + rutaFact);
		if (!dir.exists()) {
			dir.mkdirs();
		}
		
		String fileName = rutaFact + "Mes-" + month + ".pdf";
		FileOutputStream output = new FileOutputStream(reportPath + fileName);

		// metodo para unir los pdfs en uno solo
		concatPDFs(pdfs, output, true);

		Employee employee = employeeService.findById(idEmployee);

		RecurringInvoices rec = null;

		rec = recurringInvoicesService.findByInvoices(employee, year, month);

		if (rec == null) {

			RecurringInvoices recurring = new RecurringInvoices();

			recurring.setIdEmployee(employee);
			recurring.setMonth(month);
			recurring.setYear(year);
			recurring.setInvoicesGenerated(cant);
			recurring.setFile(fileName);

			recurringInvoicesService.save(recurring);
		}

		response.put("mensaje", "Documento generado exitosamente");
		response.put("data", fileName);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);

	}

	public static void concatPDFs(List<InputStream> streamOfPDFFiles, OutputStream outputStream, boolean paginate) {

		Document document = new Document();
		try {
			List<InputStream> pdfs = streamOfPDFFiles;
			List<PdfReader> readers = new ArrayList<PdfReader>();
			int totalPages = 0;
			Iterator<InputStream> iteratorPDFs = pdfs.iterator();

			while (iteratorPDFs.hasNext()) {
				InputStream pdf = iteratorPDFs.next();
				PdfReader pdfReader = new PdfReader(pdf);
				readers.add(pdfReader);
				totalPages += pdfReader.getNumberOfPages();
			}

			PdfWriter writer = PdfWriter.getInstance(document, outputStream);

			document.open();
			PdfContentByte cb = writer.getDirectContent();

			PdfImportedPage page;
			int currentPageNumber = 0;
			int pageOfCurrentReaderPDF = 0;
			Iterator<PdfReader> iteratorPDFReader = readers.iterator();

			while (iteratorPDFReader.hasNext()) {
				PdfReader pdfReader = iteratorPDFReader.next();

				while (pageOfCurrentReaderPDF < pdfReader.getNumberOfPages()) {

					Rectangle rectangle = pdfReader.getPageSizeWithRotation(1);
					document.setPageSize(rectangle);
					document.newPage();

					pageOfCurrentReaderPDF++;
					currentPageNumber++;
					page = writer.getImportedPage(pdfReader, pageOfCurrentReaderPDF);
					switch (rectangle.getRotation()) {
					case 0:
						cb.addTemplate(page, 1f, 0, 0, 1f, 0, 0);
						break;
					case 90:
						cb.addTemplate(page, 0, -1f, 1f, 0, 0, pdfReader.getPageSizeWithRotation(1).getHeight());
						break;
					case 180:
						cb.addTemplate(page, -1f, 0, 0, -1f, 0, 0);
						break;
					case 270:
						cb.addTemplate(page, 0, 1.0F, -1.0F, 0, pdfReader.getPageSizeWithRotation(1).getWidth(), 0);
						break;
					default:
						break;
					}
					if (paginate) {
						cb.beginText();
						cb.getPdfDocument().getPageSize();
						cb.endText();
					}
				}
				pageOfCurrentReaderPDF = 0;
			}
			outputStream.flush();
			document.close();
			outputStream.close();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (document.isOpen())
				document.close();
			try {
				if (outputStream != null)
					outputStream.close();
			} catch (IOException ioe) {
				ioe.printStackTrace();
			}
		}
	}

	
	// Consultar las facturas recurrentes generadas
	@PreAuthorize("hasAuthority('INVOICES_READ')")
	@PostMapping(path = "/recurrentesPagesAll", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getAllRecurringPagesAll(@RequestBody RecurringInvoicesDTO invoicesPageDto) {
		Map<String, Object> response = new HashMap<>();


		
		@SuppressWarnings("unchecked")
		List<RecurringInvoices> page = (List<RecurringInvoices>) recurringInvoicesService.findAll(
				invoicesPageDto.getYear(), invoicesPageDto.getMonth(), invoicesPageDto.getInvoicesGenerated(),
				invoicesPageDto.getInvoicesGeneratedMax());

		List<RecurringInvoicesDTO> invoicesDTO = new ArrayList<>();

		for (RecurringInvoices invoice : page) {

				invoicesDTO.add(RecurringInvoicesDTO.builder().year(invoice.getYear())
						.month(invoice.getMonth())
						.file(invoice.getFile())
						.id(invoice.getId())
						.invoicesGenerated(invoice.getInvoicesGenerated())
						.build());

		}

		response.put("values", invoicesDTO);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);	
	}

	
	@PreAuthorize("hasAuthority('INVOICES_READ')")
	@PostMapping(path = "/recurrentesPages", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getAllRecurringPages(@RequestBody RecurringInvoicesDTO invoicesPageDto) {
		Map<String, Object> response = new HashMap<>();
		
		// SE CONSULTA LA INFORMACION PAGINADA Y SE ARMA LA LISTA CON LA INFORMACION
		Page<RecurringInvoices> page = recurringInvoicesService.findAll(invoicesPageDto.getPage() -1, invoicesPageDto.getLimit(),
				invoicesPageDto.getYear(), invoicesPageDto.getMonth(), invoicesPageDto.getInvoicesGenerated(),
				invoicesPageDto.getInvoicesGeneratedMax());

		List<RecurringInvoicesDTO> invoicesDTO = new ArrayList<>();

		for (RecurringInvoices invoice : page.getContent()) {

				invoicesDTO.add(RecurringInvoicesDTO.builder().year(invoice.getYear())
						.month(invoice.getMonth())
						.invoicesGenerated(invoice.getInvoicesGenerated())
						.file(invoice.getFile())
						.id(invoice.getId())
						.build());

		}

		PaginationData data = PaginationData.builder().pages(page.getTotalPages()).total(page.getTotalElements())
				.values(invoicesDTO).build();
		return new ResponseEntity<PaginationData>(data, HttpStatus.OK);				
	}

	
	
	@PreAuthorize("hasAuthority('INVOICES_READ')")
	@GetMapping(path = "/recurrentes", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getAllRecurring() {
		Map<String, Object> response = new HashMap<>();

		List<RecurringInvoices> recurrentes = recurringInvoicesService.findAll();

		response.put("mensaje", "Listado de aacturas recurrentes.");
		response.put("data", recurrentes);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}

	
	@PreAuthorize("hasAuthority('INVOICES_WRITE')")
	@GetMapping(path = "/intereces", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> intereces() {
		Map<String, Object> response = new HashMap<>();

		Double anual = calcular();

		response.put("mensaje", "los intereces mensuales son: ");
		response.put("data", anual);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);

	}

	public Double calcular() {

		Configurations config = configurationService.findById((long) 1);

		Double tasaUsuara = config.getTasaUsura() / 100;

		Double base = 1 + tasaUsuara;

		Double n = 0.0833333333333333;

		Double resultadoExpo = (Double) Math.pow(base, n);

		Double valor1 = resultadoExpo - 1;

		Double anual = 12 * valor1;

		return anual;
	}

	@PreAuthorize("hasAuthority('INVOICES_READ')")
	@PostMapping(path = "pages", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getAll(@RequestBody InvoicesPageDto invoicesPageDto) {
		
		// SE CONSULTA LA INFORMACION PAGINADA  DE LAS FACTURAS Y SE ARMA LA LISTA 
		Page<Invoices> page = invoiceServices.findAll(invoicesPageDto.getPage() -1, invoicesPageDto.getLimit(),
				invoicesPageDto.getStartDate(), invoicesPageDto.getEndDate(), invoicesPageDto.getConsecutive(),
				invoicesPageDto.getNames(), invoicesPageDto.getEnvoicesState(), invoicesPageDto.getMinTotal(),
				invoicesPageDto.getMaxTotal());

		List<InvoicesDTO> invoicesDTO = new ArrayList<>();

		for (Invoices invoice : page.getContent()) {

			List<Payments> pago = null;
			pago = paymentsService.findByInvoices(invoice);

			Double pagado = (double) 0;

			// SE VALIDA SI LA FACTURA TIENE PAGOS
			if(!pago.isEmpty()) {
				for(Payments pagos: pago) {
					pagado += pagos.getValuePaid();
				}
			}

			double saldo = invoice.getTotal() - pagado;
			
			// SE VALIDA SI LA FACTURA ES A UN CONTRATO O A UN ASOCIADO
		  if(invoice.getContrato() != null) {
				
				invoicesDTO.add(InvoicesDTO.builder().id(invoice.getId()).consecutive(invoice.getConsecutive())
						.total(invoice.getTotal()).envoicesMonth(invoice.getEnvoicesMonth())
						.envoicesState(invoice.getEnvoicesState()).invoiceDate(invoice.getInvoiceDate())
						.associateName(invoice.getContrato().getIdClient().getIdAssociated().getFirstName()
								.concat(" ".concat(invoice.getContrato().getIdClient().getIdAssociated().getLastName())))
						.email(invoice.getContrato().getIdClient().getIdAssociated().getEmail())
						.cancel(invoice.isCancel())
						.reasonCancel(invoice.getReasonCancel())
						.observationCancel(invoice.getObservationCancel())
						.contract(invoice.getContrato().getId()).jsonTotales(invoice.getTotal()).pago(pagado).saldo(saldo)
						.build());
				
			}else {
				
				invoicesDTO.add(InvoicesDTO.builder().id(invoice.getId()).consecutive(invoice.getConsecutive())
						.total(invoice.getTotal()).envoicesMonth(invoice.getEnvoicesMonth())
						.envoicesState(invoice.getEnvoicesState()).invoiceDate(invoice.getInvoiceDate())
						.associateName(invoice.getAssociate().getFirstName()
					    .concat(" ".concat(invoice.getAssociate().getLastName())))
						.email(invoice.getAssociate().getEmail())
						.cancel(invoice.isCancel())
						.reasonCancel(invoice.getReasonCancel())
						.observationCancel(invoice.getObservationCancel())
						.jsonTotales(invoice.getTotal()).pago(pagado).saldo(saldo)
						.build());
			}

		}

		PaginationData data = PaginationData.builder().pages(page.getTotalPages()).total(page.getTotalElements())
				.values(invoicesDTO).build();

		return new ResponseEntity<PaginationData>(data, HttpStatus.OK);
	}
	
	@PreAuthorize("hasAuthority('INVOICES_READ')")
	@PostMapping(path = "pagesall", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getAllpages(@RequestBody InvoicesPageDto invoicesPageDto) {
		Map<String, Object> response = new HashMap<>();

		// SE TRAE TODA LA INFORMACION DE LAS FACTURAS

		List<Invoices> page = (List<Invoices>) invoiceServices.findAll(
				invoicesPageDto.getStartDate(), invoicesPageDto.getEndDate(), invoicesPageDto.getConsecutive(),
				invoicesPageDto.getNames(), invoicesPageDto.getEnvoicesState(), invoicesPageDto.getMinTotal(),
				invoicesPageDto.getMaxTotal());

		List<InvoicesDTO> invoicesDTO = new ArrayList<>();

		for (Invoices invoice : page) {

			List<Payments> pago = null;
			pago = paymentsService.findByInvoices(invoice);

			Double pagado = (double) 0;

            if(!pago.isEmpty()) {
                for(Payments pagos: pago) {
                    pagado += pagos.getValuePaid();
                }
			}

			double saldo = invoice.getTotal() - pagado;
			
			// SE VALIDA SI LA FACTURA ES A UN ASOCIADO O A UN CONTRATO
		  if(invoice.getContrato() != null) {
				
				invoicesDTO.add(InvoicesDTO.builder().id(invoice.getId()).consecutive(invoice.getConsecutive())
						.total(invoice.getTotal()).envoicesMonth(invoice.getEnvoicesMonth())
						.envoicesState(invoice.getEnvoicesState()).invoiceDate(invoice.getInvoiceDate())
						.associateName(invoice.getContrato().getIdClient().getIdAssociated().getFirstName()
								.concat(" ".concat(invoice.getContrato().getIdClient().getIdAssociated().getLastName())))
						.cancel(invoice.isCancel())
						.reasonCancel(invoice.getReasonCancel())
						.observationCancel(invoice.getObservationCancel())
						.contract(invoice.getContrato().getId()).jsonTotales(invoice.getTotal()).pago(pagado).saldo(saldo)
						.build());
				
			}else {
				
				invoicesDTO.add(InvoicesDTO.builder().id(invoice.getId()).consecutive(invoice.getConsecutive())
						.total(invoice.getTotal()).envoicesMonth(invoice.getEnvoicesMonth())
						.envoicesState(invoice.getEnvoicesState()).invoiceDate(invoice.getInvoiceDate())
						.associateName(invoice.getAssociate().getFirstName()
					    .concat(" ".concat(invoice.getAssociate().getLastName())))
						.cancel(invoice.isCancel())
						.reasonCancel(invoice.getReasonCancel())
						.observationCancel(invoice.getObservationCancel())
						.jsonTotales(invoice.getTotal()).pago(pagado).saldo(saldo)
						.build());
			}

		}

		response.put("values", invoicesDTO);

		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}
	
	@PreAuthorize("hasAuthority('INVOICES_READ')")
	@GetMapping(path = "associateByInvoice/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> associateByInvoice(@PathVariable Long id) {

		Invoices invoice = null;
		Associated assoc = null;
		Map<String, Object> response = new HashMap<>();
		
		try {

			invoice = invoiceServices.findById(id);

		} catch (DataAccessException e) {
			response.put("mensaje", "Error al realizar la consulta en la base de datos");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		if (invoice == null) {
			response.put("mensaje", "La factura ID: ".concat(id.toString().concat(" no existe en la base de datos!")));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}
		
		// se valida si la factura es a un contrato o a un asociado para sacar informacion de la persona
		if(invoice.getContrato() != null) {
			assoc = invoice.getContrato().getIdClient().getIdAssociated();
		}else {
			assoc = invoice.getAssociate();
		}
			

		response.put("data", assoc);
		response.put("refPago", invoice.getRefPago());
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}
	
}
