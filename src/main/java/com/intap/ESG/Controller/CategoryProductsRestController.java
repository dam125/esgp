package com.intap.ESG.Controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import com.intap.ESG.Models.CategoryProducts;
import com.intap.ESG.Models.Products;
import com.intap.ESG.Service.ICategoryProductsService;
import com.intap.ESG.Service.IProductsService;




@RestController
@RequestMapping(path = "category")
public class CategoryProductsRestController {
	
	@Autowired
	private ICategoryProductsService categoryProductsDao;
	
	@Autowired
	private IProductsService productsService;
	
	@PreAuthorize("hasAuthority('INVENTORY_READ')")
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?>  findAll(){
		Map<String, Object> response = new HashMap<>();
		
		List<CategoryProducts> categoryProducts = (List<CategoryProducts>) categoryProductsDao.findAll();
		
		response.put("mensaje", "¡Listado de categorías!");
		response.put("data", categoryProducts);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}
	
	@PreAuthorize("hasAuthority('INVENTORY_READ')")
	@GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> findById(@PathVariable Long id) {
		
		CategoryProducts categoryProducts = null;
		Map<String, Object> response = new HashMap<>();
		
		try {
			
			categoryProducts = categoryProductsDao.findById(id);	
			
		} catch(DataAccessException e) {
			response.put("mensaje", "Error al realizar la consulta en la base de datos");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		if(categoryProducts == null) {
			response.put("mensaje", "¡la categoría ID: ".concat(id.toString().concat(" no existe en la base de datos!")));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}

			response.put("data", categoryProducts);
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}
	
	@PreAuthorize("hasAuthority('INVENTORY_WRITE')")
	@PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> create(@Valid @RequestBody CategoryProducts category) {
		
		Map<String, Object> response = new HashMap<>();
		
		CategoryProducts cat = categoryProductsDao.save(category);
		
		response.put("mensaje", "¡La categoría ha sido creada con éxito!");
		response.put("data", cat);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);

	}
	
	@PreAuthorize("hasAuthority('INVENTORY_WRITE')")
	@PutMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> update(@PathVariable Long id, @RequestBody CategoryProducts category) {
		Map<String, Object> response = new HashMap<>();
		
		CategoryProducts cat = categoryProductsDao.findById(id);
		
		cat.setName(category.getName());
		
		categoryProductsDao.save(cat);
		
		response.put("mensaje", "¡La categoría ha sido actualizada con éxito!");
		response.put("data", cat);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}
	
	@PreAuthorize("hasAuthority('INVENTORY_DELETE')")
	@DeleteMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> delete(@PathVariable Long id) {
		Map<String, Object> response = new HashMap<>();
		
		CategoryProducts cat = categoryProductsDao.findById(id);
		
		List<Products> pro = productsService.findByategoryProducts(cat);
		Integer cant = pro.size();
		if (cant > 0) {
			
			response.put("mensaje", "La categoría '" +cat.getName()
					+ "' no se puede eliminar ya que está asignada a uno o más productos");
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
			

		} else {
			
			categoryProductsDao.delete(id);
			response.put("mensaje", "Categoría eliminada exitosamente");
		}

		

		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}

}
