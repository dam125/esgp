package com.intap.ESG.Controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.intap.ESG.Models.Taxes;
import com.intap.ESG.Service.ITaxesService;

@RestController
@RequestMapping("/taxes")
public class TaxesRestController {
	
	
	@Autowired
	private ITaxesService taxesService;

	
	@PreAuthorize("hasAuthority('INVENTORY_READ')")
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?>  findAll(){
		Map<String, Object> response = new HashMap<>();
		
		List<Taxes> products =  taxesService.findAll();
		
		response.put("mensaje", "Listado de impuestos.");
		response.put("data", products);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}

}
