package com.intap.ESG.Controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.intap.ESG.Models.Associated;
import com.intap.ESG.Models.CategoryProducts;
import com.intap.ESG.Models.Client;
import com.intap.ESG.Models.Configurations;
import com.intap.ESG.Models.ContractDetails;
import com.intap.ESG.Models.Employee;
import com.intap.ESG.Models.Partner;
import com.intap.ESG.Models.Products;
import com.intap.ESG.Models.Provider;
import com.intap.ESG.Models.DTO.AssocietedPageData;
import com.intap.ESG.Models.DTO.PaginationData;
import com.intap.ESG.Models.DTO.ProductsDTO;
import com.intap.ESG.Models.DTO.ProductsPageDto;
import com.intap.ESG.Service.ICategoryProductsService;
import com.intap.ESG.Service.IConfigurationsService;
import com.intap.ESG.Service.IContractDetailsService;
import com.intap.ESG.Service.IProductsService;

@RestController
@RequestMapping(path = "product")
public class ProductsRestController {
	
	@Autowired
	private IProductsService productsService;
	
	@Autowired
	private ICategoryProductsService categoryProductsDao;
	
	@Autowired
	private IContractDetailsService contractDetailsService;
	
	@Autowired
	private IConfigurationsService configurationService;
	
	@PreAuthorize("hasAuthority('INVENTORY_READ')")
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?>  findAll(){
		Map<String, Object> response = new HashMap<>();
		
		List<Products> products =  productsService.findAll();
		
		response.put("mensaje", "Listado de Productos.");
		response.put("data", products);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}
	
	@PreAuthorize("hasAuthority('INVENTORY_READ')")
	@PostMapping(path = "/findPager", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> findPager(@RequestBody ProductsPageDto productsPageDto) {
		List<Products> pages =  (List<Products>) productsService.findAll( productsPageDto.getName(), productsPageDto.getReference(),
				productsPageDto.getType(), productsPageDto.getInventory(), 
				productsPageDto.getEnabled(), productsPageDto.getMaxvalue(), productsPageDto.getMinvalue(), productsPageDto.getCategoryProductsId());
		List<Map<String, Object>> data = pages.stream().map(products -> {

			Map<String, Object> d = new HashMap<>();	
			
			ProductsDTO pro = new ProductsDTO();
			
			pro.setName(products.getName());
			pro.setReference(products.getReference());
			pro.setCode("ESG" + StringUtils.leftPad(products.getCode().toString(), 3, "0"));
			pro.setCreationDate(products.getCreateAt().toString());
			pro.setType(products.getType());
			pro.setInventory(products.inventory);
			pro.setTaxes(products.getTaxes());
			pro.setUnitPrice(products.getUnitPrice());
			pro.setCategory(products.getCategoryProducts().getName());
			d.put("product", pro);
			
			return d;
		}).collect(Collectors.toList());
		
	

		return new ResponseEntity<Object>(data, HttpStatus.OK);

	}
	
	
	@PreAuthorize("hasAuthority('INVENTORY_READ')")
	@PostMapping(path = "pages", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<PaginationData>  findAll(@RequestBody ProductsPageDto productsPageDto){
			
		Page<Products> page =  productsService.findAll(productsPageDto.getPage() -1, productsPageDto.getLimit(), 
				productsPageDto.getName(), productsPageDto.getReference(), productsPageDto.getType(), productsPageDto.getInventory(), 
				productsPageDto.getEnabled(), productsPageDto.getMaxvalue(), productsPageDto.getMinvalue(), productsPageDto.getCategoryProductsId());
		
		
		PaginationData data = PaginationData.builder()
				.pages(page.getTotalPages())
				.total(page.getTotalElements())
				.values(page.getContent())
				.build();

		return new ResponseEntity<PaginationData>(data, HttpStatus.OK);
	}
	
	@PreAuthorize("hasAuthority('INVENTORY_READ')")
	@GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> findById(@PathVariable Long id) {
		
		Products products = null;
		Map<String, Object> response = new HashMap<>();
		
		try {
			
			products = productsService.findById(id);	
			
		} catch(DataAccessException e) {
			response.put("mensaje", "Error al realizar la consulta en la base de datos");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		if(products == null) {
			response.put("mensaje", "El producto ID: ".concat(id.toString().concat(" no existe en la base de datos!")));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}

			response.put("data", products);
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}
	
	@PreAuthorize("hasAuthority('INVENTORY_WRITE')")
	@PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> create(@RequestBody Map<String, Object> product) {
		
		
		Long codepasar = Long.parseLong((String)product.get("code"));
		Products produc = productsService.finByCode(codepasar);
		
		if(produc == null) {
			Map<String, Object> response = new HashMap<>();
			CategoryProducts cat = categoryProductsDao.findById(Long.parseLong(product.get("categoryProducts").toString()));
			
			Products pro = new Products();
			pro.setCode(Long.parseLong(product.get("code").toString()));
			pro.setName(product.get("name").toString());
			pro.setReference(product.get("reference").toString());
			pro.setType(product.get("type").toString());
			pro.setInventory(Boolean.parseBoolean(product.get("inventory").toString()));
			pro.setEnabled(Boolean.parseBoolean(product.get("enabled").toString()));
			pro.setUnitPrice(Double.parseDouble(product.get("unit_Price").toString()));
			pro.setTaxes(Double.parseDouble(product.get("taxes").toString()));
			pro.setCategoryProducts(cat);
			
			Products prod = productsService.save(pro);
			
			Configurations config = configurationService.findById((long) 1);
			
			int codes =  Integer.parseInt(config.getConsecutiveProducts()) +1;
			config.setConsecutiveProducts(Integer.toString(codes));
			configurationService.save(config);
			
			
			response.put("mensaje", "¡El producto ha sido creado con éxito!");
			response.put("data", prod);
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
		}else {
			Map<String, Object> response = new HashMap<>();
			response.put("mensaje", "El consecutivo ya se encuentra asignado a otro producto");
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}
		


	}
	
	@PreAuthorize("hasAuthority('INVENTORY_WRITE')")
	@PutMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> update(@PathVariable Long id, @RequestBody Map<String, Object> product) {
		Map<String, Object> response = new HashMap<>();
		
		
        Long codepasar = Long.parseLong((String)product.get("code"));
		Products produc = productsService.finByCode(codepasar);
		
		if(produc != null && id != produc.getId()) {
			response.put("mensaje", "El consecutivo ya se encuentra asignado a otro producto");
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
			
		}
		
		Products pro = productsService.findById(id);
		CategoryProducts cat = categoryProductsDao.findById(Long.parseLong(product.get("categoryProducts").toString()));
		
		pro.setCode(Long.parseLong(product.get("code").toString()));
		pro.setName(product.get("name").toString());
		pro.setReference(product.get("reference").toString());
		pro.setType(product.get("type").toString());
		pro.setInventory(Boolean.parseBoolean(product.get("inventory").toString()));
		pro.setEnabled(Boolean.parseBoolean(product.get("enabled").toString()));
		pro.setUnitPrice(Double.parseDouble(product.get("unit_Price").toString()));
		pro.setTaxes(Double.parseDouble(product.get("taxes").toString()));
		pro.setCategoryProducts(cat);
		
		
		productsService.save(pro);
		
		contractDetailsService.UpdatePriceByProduct(pro, pro.getUnitPrice());
		
		
		
		response.put("mensaje", "¡El producto ha sido actualizado con éxito!");
		response.put("data", pro);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}
	
	@PreAuthorize("hasAuthority('INVENTORY_WRITE')")
	@PutMapping(path = "ChangeStatus/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?>ChageStatus(@PathVariable Long id, @RequestBody Map<String, Object> prod){
		Map<String, Object> response = new HashMap<>();
		
		Products product = productsService.findById(id);
		
		product.setEnabled(Boolean.parseBoolean(prod.get("enabled").toString()));
		
		productsService.save(product);
		
		response.put("mensaje", "¡El estado del producto ha sido actualizado con éxito!");
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}
	
	@PreAuthorize("hasAuthority('INVENTORY_DELETE')")
	@DeleteMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> delete(@PathVariable Long id) {
		Map<String, Object> response = new HashMap<>();
		
		List<ContractDetails> contr = null;
		
		Products product = productsService.findById(id);
		
		contr = contractDetailsService.findByProduct(product);
		Integer cant = contr.size();
		if (cant > 0) {
			
			response.put("mensaje", "El Producto '" + product.getName()
					+ "' no se puede eliminar ya que está asociado a un contrato");
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
			

		} else {
	
			productsService.delete(id);
			response.put("mensaje", "¡Producto eliminado exitosamente!");

		}
	
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}

}
