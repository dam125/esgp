package com.intap.ESG.Controller;

import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.MailException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.intap.ESG.Dao.IRoleDao;
import com.intap.ESG.Models.Associated;
import com.intap.ESG.Models.AssociatesType;
import com.intap.ESG.Models.Client;
import com.intap.ESG.Models.Configurations;
import com.intap.ESG.Models.Contracts;
import com.intap.ESG.Models.Employee;
import com.intap.ESG.Models.EnabledReason;
import com.intap.ESG.Models.FilesClient;
import com.intap.ESG.Models.FilesEmployee;
import com.intap.ESG.Models.FilesPartner;
import com.intap.ESG.Models.FilesProvider;
import com.intap.ESG.Models.Partner;
import com.intap.ESG.Models.Provider;
import com.intap.ESG.Models.RecoverPassword;
import com.intap.ESG.Models.Role;
import com.intap.ESG.Models.UserModel;
import com.intap.ESG.Models.DTO.AssociatesDTO;
import com.intap.ESG.Models.DTO.AssocietedPageData;
import com.intap.ESG.Models.DTO.PaginationData;
import com.intap.ESG.Models.DTO.UserDTO;
import com.intap.ESG.Service.IAssociatedService;
import com.intap.ESG.Service.IAssociatesTypeService;
import com.intap.ESG.Service.IClientService;
import com.intap.ESG.Service.IConfigurationsService;
import com.intap.ESG.Service.IContractsService;
import com.intap.ESG.Service.IEmployeeService;
import com.intap.ESG.Service.IEnabledReasonService;
import com.intap.ESG.Service.IFilesClientService;
import com.intap.ESG.Service.IFilesEmployeeService;
import com.intap.ESG.Service.IFilesPartnerService;
import com.intap.ESG.Service.IFilesProviderService;
import com.intap.ESG.Service.IPartnerSrevice;
import com.intap.ESG.Service.IProviderService;
import com.intap.ESG.Service.IRecoverPasswordService;
import com.intap.ESG.Service.ISendEmailService;
import com.intap.ESG.Service.IUserService;
import com.intap.ESG.Service.impl.UploadFileServiceImpl;
import com.intap.ESG.util.Constant;

@RestController
@RequestMapping("/associates")
public class AssociatedRestController {

	@Autowired
	private IUserService userService;


	@Autowired
	private IAssociatedService associatedService;

	@Autowired
	private IEmployeeService employeeService;

	@Autowired
	private IClientService clientService;

	@Autowired
	private IProviderService providerService;

	@Autowired
	private IPartnerSrevice partnerSrevice;

	@Autowired
	private IFilesClientService filesClientService;

	@Autowired
	private IFilesEmployeeService filesEmployeeService;

	@Autowired
	private IFilesPartnerService filesPartnerService;

	@Autowired
	private IFilesProviderService filesProviderService;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private IAssociatesTypeService associatesTypeService;

	@Autowired
	private IRoleDao roleDao;

	@Autowired
	private IContractsService contractsService;
	
	@Autowired
	private IConfigurationsService configurationService;

	@Autowired
	private UploadFileServiceImpl upload;

	@Autowired
	private IEnabledReasonService enabledReasonService;

	@Autowired
	private IRecoverPasswordService recoverPasswordService;

	@Autowired
	private ISendEmailService sendEmail;

	@PreAuthorize("hasAuthority('USER_READ')")
	@GetMapping(path = "/findAdress/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> FindAssociateAdress(@PathVariable Integer id) {

		Map<String, Object> response = new HashMap<>();

		Client client = clientService.findById(id);

		response.put("data", client.getIdAssociated().getAddress());
		response.put("city", client.getIdAssociated().getCity());
		response.put("neighborhood", client.getIdAssociated().getNeighborhood());

		return new ResponseEntity<Object>(response, HttpStatus.OK);

	}
	
	
	@PreAuthorize("hasAuthority('USER_READ')")
	@GetMapping(path = "/GetUsersFilter/{value}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> GetUsersFilter(@PathVariable String value) {

		Map<String, Object> response = new HashMap<>();
		List<AssociatesDTO> client = new ArrayList<AssociatesDTO>();
		List<Associated> clientesActivos = associatedService.getUsersFilter(value);

		clientesActivos.forEach(cliente -> {
			client.add(AssociatesDTO.builder().id(cliente.getId())
					.fullName(cliente.getFirstName() + " " + cliente.getLastName())
					.enabledClient(cliente.enabled)
					.identificationNumber(cliente.getIdentificationNumber())
					.neighborhood_Client(cliente.getNeighborhood()).build());
		});

		
		response.put("mensaje", "Listado de clientes activos!");
		response.put("data", client);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}
	
	
	@PreAuthorize("hasAuthority('SETUP_WRITE')")
	@GetMapping(path = "/findConfig")
	public ResponseEntity<?> findConfig() {

		Map<String, Object> response = new HashMap<>();

		Configurations Config = configurationService.findById((long) 1);

		response.put("data", Config);
		return new ResponseEntity<Object>(response, HttpStatus.OK);

	}
	
	@PreAuthorize("hasAuthority('SETUP_WRITE')")
	@PostMapping(path = "/saveConfig")
	public ResponseEntity<?> saveConfig(@RequestParam(value = "consecutiveContract", required = false) String consecutive_Contract,
			@RequestParam(value = "consecutiveInvoice", required = false) String consecutive_Invoice,
			@RequestParam(value = "consecutiveInvoiceStart", required = false) String consecutive_InvoiceStart,
			@RequestParam(value = "consecutiveInvoiceEnd", required = false) String consecutive_InvoiceEnd,
			@RequestParam(value = "consecutiveInvoiceDate", required = false) String consecutive_InvoiceDate,
			@RequestParam(value = "resolutionconsecutiveInvoice", required = false) String resolutionconsecutiveInvoice,
			@RequestParam(value = "consecutivePayment", required = false) String consecutive_Payment,
			@RequestParam(value = "consecutiveProducts", required = false) String consecutive_Products,
			@RequestParam(value = "consecutiveRefPaymente", required = false) String consecutiveRef_Paymente,
			@RequestParam(value = "usuryRate", required = false) String usury_Rate,
			@RequestParam(value = "textInvoices", required = false) String text_Invoices,
			@RequestParam(value = "urlpay", required = false) String url_pay) {

		Map<String, Object> response = new HashMap<>();

		Configurations Config = configurationService.findById((long) 1);
		
		Config.setConsecutiveContrato(consecutive_Contract);
		Config.setConsecutiveFactura(consecutive_Invoice);
		Config.setConsecutiveFacturaStart(consecutive_InvoiceStart);
		Config.setConsecutiveFacturaEnd(consecutive_InvoiceEnd);
		Config.setConsecutiveFacturaDate(LocalDate.parse(consecutive_InvoiceDate, DateTimeFormatter.BASIC_ISO_DATE));
		Config.setConsecutivePaymemt(consecutive_Payment);
		Config.setConsecutiveProducts(consecutive_Products);
		Config.setConsecutiveRefPago(consecutiveRef_Paymente);
		Config.setResolutionconsecutiveInvoice(resolutionconsecutiveInvoice);
		Config.setTasaUsura(Double.parseDouble(usury_Rate));
		Config.setTextoFactura(text_Invoices);
		Config.setUrlPayment(url_pay);
		
		configurationService.save(Config);

		response.put("data", Config);
		response.put("mensaje", "La configuración fue actualizada con exito.");
		return new ResponseEntity<Object>(response, HttpStatus.CREATED);

	}

	@PreAuthorize("hasAuthority('USER_READ')")
	@GetMapping(path = "/EditAssociate/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> FindAssociateEdit(@PathVariable Integer id) {
		Map<String, Object> response = new HashMap<>();
		Map<String, Object> data = new HashMap<>();

		Associated aso = null;
		Employee employee = null;
		Client client = null;
		Provider provider = null;
		Partner partner = null;

		aso = associatedService.findById(id);

		employee = employeeService.findByIdAssociated(aso);
		client = clientService.findByIdAssociated(aso);
		provider = providerService.findByIdAssociated(aso);
		partner = partnerSrevice.findByIdAssociated(aso);

		Long idrol = null;

		if (employee != null) {
			idrol = employee.getUserId().getRole().getId();
		}

		data.put("associado", aso);
		data.put("employee", employee);
		data.put("client", client);
		data.put("provider", provider);
		data.put("partner", partner);
		data.put("rolvalue", idrol);

		response.put("data", data);

		return new ResponseEntity<Object>(response, HttpStatus.OK);

	}

	@PreAuthorize("hasAuthority('USER_DELETE')")
	@DeleteMapping(path = "/{idAssociated}")
	public ResponseEntity<?> DeleteAssociate(@PathVariable Integer idAssociated) {

		Map<String, Object> response = new HashMap<>();
		// Consultamos la informacion del asociado y la informacion adicional

		List<FilesEmployee> fileEmployee = null;
		List<FilesClient> fileClient = null;
		List<FilesProvider> fileProvider = null;
		List<FilesPartner> filePartner = null;

		Employee employee = null;
		Client client = null;
		Provider provider = null;
		Partner partner = null;
		Associated associated = null;

		associated = associatedService.findById(idAssociated);

		if (associated == null) {

			response.put("mensaje", "El asociado con el ID: " + idAssociated + " No existe");
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);

		}

		employee = employeeService.findByIdAssociated(associated);
		client = clientService.findByIdAssociated(associated);
		provider = providerService.findByIdAssociated(associated);
		partner = partnerSrevice.findByIdAssociated(associated);

		// Verificamos el tipo de usuario a liminar.

		List<Contracts> contr = null;

		contr = contractsService.findByAssociates(client);
		Integer cant = contr.size();
		if (cant > 0) {

			response.put("mensaje", "El Asociado/Cliente no se puede eliminar ya que tiene contratos asociados");
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);

		} else {

			// 2. Eliminar Empleado
			if (employee != null) {

				// 1. Eliminar los archivos de los usuarios

				fileEmployee = filesEmployeeService.findByIdEmployee(employee);

				if (fileEmployee.size() > 0) {

					fileEmployee.forEach(file -> {

						String url = file.getUrl();

						// Eliminamos el archivo(Hoja de vida o rut) del servidor
						if (url != null) {
							File oldFichero = new File(url);
							oldFichero.delete();
						}

					});

				}

				UserModel userModel = employee.getUserId();

				employeeService.delete(employee.getId());

				// 3. Eliminar usuario
				userService.delete(userModel);

			}

			if (client != null) {

				fileClient = filesClientService.findByIdClient(client);

				if (fileClient.size() > 0) {

					fileClient.forEach(file -> {

						String url = file.getUrl();

						// Eliminamos el archivo(Hoja de vida o rut) del servidor
						if (url != null) {
							File oldFichero = new File(url);
							oldFichero.delete();
						}
					});

				}

				// 4. Eliminar Cliente
				clientService.delete(client.getId());
			}

			if (provider != null) {

				fileProvider = filesProviderService.findByIdProvider(provider);

				if (fileProvider.size() > 0) {

					fileProvider.forEach(file -> {

						String url = file.getUrl();

						// Eliminamos el archivo(Hoja de vida o rut) del servidor
						if (url != null) {
							File oldFichero = new File(url);
							oldFichero.delete();
						}
					});

				}

				// 5. Eliminar Proveedor
				providerService.delete(provider.getId());

			}

			if (partner != null) {

				filePartner = filesPartnerService.findByIdPartner(partner);

				if (filePartner.size() > 0) {

					filePartner.forEach(file -> {

						String url = file.getUrl();

						// Eliminamos el archivo(Hoja de vida o rut) del servidor
						if (url != null) {
							File oldFichero = new File(url);
							oldFichero.delete();
						}
					});

				}

				// 6. Eliminar Socio
				partnerSrevice.delete(partner.getId());

			}

			if (associated != null) {
				// 7. Eliminar Asociado
				associatedService.delete(associated.getId());
			}

		}

		response.put("mensaje", "El usuario '" + associated.getFirstName() + " " + associated.getLastName()
				+ "' eliminado exitosamente.");
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);

	}

	@PreAuthorize("hasAuthority('USER_WRITE')")
	@PutMapping(path = "/ChangeStatusAll")
	public ResponseEntity<?> ChageStatus(@RequestBody Map<String, Object> data) {
		Map<String, Object> response = new HashMap<>();
		Associated associate = null;

		EnabledReason enabledReason = null;
		String enabledReason2 = data.get("enabledReason").toString();

		Boolean enabled = Boolean.parseBoolean(data.get("enabled").toString());
		LocalDate dateEnabled = LocalDate.parse(data.get("dateEnabled").toString(), DateTimeFormatter.BASIC_ISO_DATE);
		String observation = data.get("enabledObservation").toString();

		associate = associatedService.findById(Integer.parseInt(data.get("id").toString()));

		if (associate == null) {
			response.put("mensaje",
					"El asociado con el ID " + data.get("id").toString() + " no existe en la base de datos.");
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}

		Employee employee = null;
		Client client = null;
		Provider provider = null;
		Partner partner = null;
		UserModel user = null;
		employee = employeeService.findByIdAssociated(associate);
		client = clientService.findByIdAssociated(associate);
		provider = providerService.findByIdAssociated(associate);
		partner = partnerSrevice.findByIdAssociated(associate);

		// cambiar estado al asociado
		associate.setEnabled(enabled);
		associate.setDateEnabled(dateEnabled);
		associate.setEnabledReason(enabledReason2);
		associate.setEnabledObservation(observation);

		associatedService.save(associate);

		// cambiamos el estado en el empleado

		if (employee != null) {
			employee.setEnabled(enabled);
			employee.setDateEnabled(dateEnabled);
			employee.setEnabledReason(enabledReason2);
			employee.setEnabledObservation(observation);

			employeeService.save(employee);

			// Cambiamos el estado en la tabla Usuarios para que no se pueda loguear
			user = employee.getUserId();

			user.setEnabled(enabled);
			userService.SaveUser(user);
		}

		// Cambiamos el estado en el cliente

		if (client != null) {
			client.setEnabled(enabled);
			client.setDateEnabled(dateEnabled);
			client.setEnabledReason(enabledReason2);
			client.setEnabledObservation(observation);

			clientService.save(client);
		}

		// cambiamos el estado en el proveedor

		if (provider != null) {
			provider.setEnabled(enabled);
			provider.setDateEnabled(dateEnabled);
			provider.setEnabledReason(enabledReason2);
			provider.setEnabledObservation(observation);

			providerService.save(provider);
		}

		// Cambiamos el estado en el Socio

		if (partner != null) {
			partner.setEnabled(enabled);
			partner.setDateEnabled(dateEnabled);
			partner.setEnabledReason(enabledReason2);
			partner.setEnabledObservation(observation);

			partnerSrevice.save(partner);
		}

		return new ResponseEntity<Object>(HttpStatus.CREATED);
	}

	@PreAuthorize("hasAuthority('USER_WRITE')")
	@PutMapping(path = "/ChangeStatusEmployee")
	public ResponseEntity<?> ChageStatusEmployee(@RequestBody Map<String, Object> data) {
		Map<String, Object> response = new HashMap<>();
		Employee employee = null;
		Associated associate = null;
		UserModel user = null;

		EnabledReason enabledReason = null;
		String enabledReason2 = data.get("enabledReason").toString();

		Boolean enabled = Boolean.parseBoolean(data.get("enabled").toString());
		LocalDate dateEnabled = LocalDate.parse(data.get("dateEnabled").toString(), DateTimeFormatter.BASIC_ISO_DATE);
		String observation = data.get("enabledObservation").toString();

		associate = associatedService.findById(Integer.parseInt(data.get("id").toString()));

		if (associate == null) {
			response.put("mensaje",
					"El associado con el ID " + data.get("id").toString() + " no existe en la base de datos.");
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}

		employee = employeeService.findByIdAssociated(associate);

		employee.setEnabled(enabled);
		employee.setDateEnabled(dateEnabled);
		employee.setEnabledReason(enabledReason2);
		employee.setEnabledObservation(observation);

		employeeService.save(employee);

		// Cambiamos el estado en la tabla Usuarios para que no se pueda loguear
		user = employee.getUserId();

		user.setEnabled(enabled);
		userService.SaveUser(user);

		return new ResponseEntity<Object>(HttpStatus.CREATED);
	}

	@PreAuthorize("hasAuthority('USER_WRITE')")
	@PutMapping(path = "/ChangeStatusClient")
	public ResponseEntity<?> ChageStatusClient(@RequestBody Map<String, Object> data) {
		Map<String, Object> response = new HashMap<>();
		Client client = null;
		Associated associate = null;

		EnabledReason enabledReason = null;
		String enabledReason2 = data.get("enabledReason").toString();

		Boolean enabled = Boolean.parseBoolean(data.get("enabled").toString());
		LocalDate dateEnabled = LocalDate.parse(data.get("dateEnabled").toString(), DateTimeFormatter.BASIC_ISO_DATE);
		String observation = data.get("enabledObservation").toString();

		associate = associatedService.findById(Integer.parseInt(data.get("id").toString()));

		if (associate == null) {
			response.put("mensaje",
					"El associado con el ID " + data.get("id").toString() + " no existe en la base de datos.");
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}

		client = clientService.findByIdAssociated(associate);

		client.setEnabled(enabled);
		client.setDateEnabled(dateEnabled);
		client.setEnabledReason(enabledReason2);
		client.setEnabledObservation(observation);

		clientService.save(client);

		return new ResponseEntity<Object>(HttpStatus.CREATED);
	}

	@PreAuthorize("hasAuthority('USER_WRITE')")
	@PutMapping(path = "/ChangeStatusProvider")
	public ResponseEntity<?> ChageStatusProvider(@RequestBody Map<String, Object> data) {
		Map<String, Object> response = new HashMap<>();
		Provider provider = null;
		Associated associate = null;

		EnabledReason enabledReason = null;
		String enabledReason2 = data.get("enabledReason").toString();

		Boolean enabled = Boolean.parseBoolean(data.get("enabled").toString());
		LocalDate dateEnabled = LocalDate.parse(data.get("dateEnabled").toString(), DateTimeFormatter.BASIC_ISO_DATE);
		String observation = data.get("enabledObservation").toString();

		associate = associatedService.findById(Integer.parseInt(data.get("id").toString()));

		if (associate == null) {
			response.put("mensaje",
					"El empleado con el ID " + data.get("id").toString() + " no existe en la base de datos.");
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}

		provider = providerService.findByIdAssociated(associate);

		provider.setEnabled(enabled);
		provider.setDateEnabled(dateEnabled);
		provider.setEnabledReason(enabledReason2);
		provider.setEnabledObservation(observation);

		providerService.save(provider);

		return new ResponseEntity<Object>(HttpStatus.CREATED);
	}

	@PreAuthorize("hasAuthority('USER_WRITE')")
	@PutMapping(path = "/ChangeStatusPartner")
	public ResponseEntity<?> ChageStatusPartner(@RequestBody Map<String, Object> data) {
		Map<String, Object> response = new HashMap<>();
		Partner partner = null;
		Associated associate = null;

		EnabledReason enabledReason = null;
		String enabledReason2 = data.get("enabledReason").toString();

		Boolean enabled = Boolean.parseBoolean(data.get("enabled").toString());
		LocalDate dateEnabled = LocalDate.parse(data.get("dateEnabled").toString(), DateTimeFormatter.BASIC_ISO_DATE);
		String observation = data.get("enabledObservation").toString();

		associate = associatedService.findById(Integer.parseInt(data.get("id").toString()));

		if (associate == null) {
			response.put("mensaje",
					"El empleado con el ID " + data.get("id").toString() + " no existe en la base de datos.");
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}

		partner = partnerSrevice.findByIdAssociated(associate);

		partner.setEnabled(enabled);
		partner.setDateEnabled(dateEnabled);
		partner.setEnabledReason(enabledReason2);
		partner.setEnabledObservation(observation);

		partnerSrevice.save(partner);

		return new ResponseEntity<Object>(HttpStatus.CREATED);
	}

	@PreAuthorize("hasAuthority('USER_WRITE')")
	@PutMapping(path = "/ChangeStatusBasic")
	public ResponseEntity<?> ChageStatusBasica(@RequestBody Map<String, Object> data) {
		Map<String, Object> response = new HashMap<>();
		Associated associate = null;

		EnabledReason enabledReason = null;
		String enabledReason2 = data.get("enabledReason").toString();

		Boolean enabled = Boolean.parseBoolean(data.get("enabled").toString());
		LocalDate dateEnabled = LocalDate.parse(data.get("dateEnabled").toString(), DateTimeFormatter.BASIC_ISO_DATE);
		String observation = data.get("enabledObservation").toString();

		associate = associatedService.findById(Integer.parseInt(data.get("idAssociate").toString()));

		if (associate == null) {
			response.put("mensaje",
					"El empleado con el ID " + data.get("id").toString() + " no existe en la base de datos.");
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}

		associate.setEnabled(enabled);
		associate.setDateEnabled(dateEnabled);
		associate.setEnabledReason(enabledReason2);
		associate.setEnabledObservation(observation);

		associatedService.save(associate);

		return new ResponseEntity<Object>(HttpStatus.CREATED);
	}

	@PostMapping("/GetUserInfo")
	public ResponseEntity<?> GetUserInfo(@RequestBody UserDTO user) {
		Map<String, Object> response = new HashMap<>();

		Optional<UserModel> userinfo = userService.findUserByUser(user.getUsername());
		AssociatesDTO associated = null;
		UserModel userfind = new UserModel();

		userfind = userinfo.get();
		Employee employee = employeeService.findByUserId(userfind);

		if (employee != null) {

			if (employee.enabled) {

				associated = AssociatesDTO.builder().id(employee.getIdAssociated().getId()).idEmployee(employee.getId())
						.address(employee.getIdAssociated().getAddress()).enabled(employee.getIdAssociated().enabled)
						.enabledEmployee(employee.enabled).firstName(employee.getIdAssociated().getFirstName())
						.lastName(employee.getIdAssociated().lastName).email(employee.getIdAssociated().getEmail())
						.identificationNumber(employee.getIdAssociated().getIdentificationNumber())
						.city(employee.getIdAssociated().getCity())
						.phoneNumber(employee.getIdAssociated().getPhoneNumber())
						.CellPhoneNumber(employee.getIdAssociated().getCellPhoneNumber())
						.birthdate(employee.getIdAssociated().getBirthdate().toString())
						.neighborhood_Client(employee.getIdAssociated().getNeighborhood())
						.city(employee.getIdAssociated().getCity()).build();

				response.put("messaje", "Assocaites list");
				response.put("associate", associated);

				return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
			} else {
				response.put("messaje", "Usuario Inactivo");

				return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
			}

		} else {
			response.put("messaje", "Usuario o contraseña incorrecta");
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
		}

	}

	@PreAuthorize("hasAuthority('USER_READ')")
	@GetMapping(path = "getClientEnabled", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> findAll() {
		Map<String, Object> response = new HashMap<>();
		List<AssociatesDTO> client = new ArrayList<AssociatesDTO>();
		List<Client> clientesActivos = clientService.findAllEnabled();

		clientesActivos.forEach(cliente -> {
			client.add(AssociatesDTO.builder().id(cliente.getIdAssociated().getId()).idClient(cliente.getId())
					.fullName(cliente.getIdAssociated().getFirstName() + " " + cliente.getIdAssociated().getLastName())
					.enabledClient(cliente.enabled)
					.identificationNumber(cliente.getIdAssociated().getIdentificationNumber())
					.neighborhood_Client(cliente.getIdAssociated().getNeighborhood()).build());
		});

		response.put("mensaje", "Listado de clientes activos!");
		response.put("data", client);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}
	
	@PreAuthorize("hasAuthority('USER_READ')")
	@GetMapping(path = "findAllInfo", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> findAllInfo() {
		Map<String, Object> response = new HashMap<>();
		List<AssociatesDTO> client = new ArrayList<AssociatesDTO>();
		List<Associated> clientesActivos = clientService.findAllInfo();

		clientesActivos.forEach(cliente -> {
			client.add(AssociatesDTO.builder().id(cliente.getId())
					.fullName(cliente.getFirstName() + " " + cliente.getLastName())
					.enabledClient(cliente.enabled)
					.identificationNumber(cliente.getIdentificationNumber())
					.neighborhood_Client(cliente.getNeighborhood()).build());
		});

		response.put("mensaje", "Listado de clientes activos!");
		response.put("data", client);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}


	@PreAuthorize("hasAuthority('USER_READ')")
	@GetMapping(path = "getEmployeeEnabled", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> findEmployees() {
		Map<String, Object> response = new HashMap<>();
		List<AssociatesDTO> employee = new ArrayList<AssociatesDTO>();
		List<Employee> employeeActivos = employeeService.findAllEnabled();

		employeeActivos.forEach(employe -> {
			employee.add(AssociatesDTO.builder().id(employe.getIdAssociated().getId())
					.enabled(employe.getIdAssociated().enabled).idEmployee(employe.getId())
					.fullName(employe.getIdAssociated().getFirstName() + " " + employe.getIdAssociated().getLastName())
					.enabledEmployee(employe.enabled).build());
		});

		response.put("mensaje", "Listado de empleados activos!");
		response.put("data", employee);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}

	@PreAuthorize("hasAuthority('USER_READ')")
	@GetMapping("/GetUsers/{id}")
	public ResponseEntity<?> GetUsers(@PathVariable Integer id) {

		Map<String, Object> response = new HashMap<>();
		List<Associated> Lista = associatedService.findWithoutActualUser(id);
		ArrayList<AssociatesDTO> asociateslist = new ArrayList<AssociatesDTO>();

		for (Associated data : Lista) {

			Employee employee = null;
			Client client = null;
			Provider provider = null;
			Partner partner = null;
			List<String> associateType = new ArrayList<String>();
			List<Object> typeAsso = new ArrayList<Object>();

			employee = employeeService.findByIdAssociated(data);
			client = clientService.findByIdAssociated(data);
			provider = providerService.findByIdAssociated(data);
			partner = partnerSrevice.findByIdAssociated(data);

			AssociatesDTO associate = new AssociatesDTO();
			associate.setId(data.getId());
			associate.setFullName(data.getFirstName() + " " + data.getLastName());
			associate.setFirstName(data.getFirstName());
			associate.setLastName(data.getLastName());
			associate.setAddress(data.getAddress());
			associate.setEnabled(data.enabled);
			associate.setEmail(data.getEmail());
			associate.setIdentificationType(data.getIdentificationType());
			associate.setIdentificationNumber(data.getIdentificationNumber());
			associate.setCity(data.getCity());
			associate.setPhoneNumber(data.getPhoneNumber());
			associate.setCellPhoneNumber(data.getCellPhoneNumber());
			associate.setBirthdate(data.getBirthdate().toString());
			associate.setNeighborhood_Client(data.getNeighborhood());
			associate.setCreationDateAssociated(data.getCreationDate());

			if (associate.enabled == true) {
				associateType.add("Basica[Habilitado]");
			} else {
				associateType.add("Basica[Inhabilitado]");
			}

			if (employee != null) {

				associate.setIdEmployee(employee.getId());
				associate.setEnabledEmployee(employee.enabled);
				associate.setIdRol(employee.userId.getRole().getId());
				associate.setIdUser(employee.userId.getId());
				associate.setPositionEmployee(employee.getPosition());
				associate.setBranchEmployee(employee.getBranch());
				associate.setObservationsEmployee(employee.getObservations());
				associate.setFileEmployee(employee.getFile());
				associate.setCreationDataEmployee(employee.getCreationDate());

				if (employee.enabled == true) {
					associateType.add("Empleado[Habilitado]");
				} else {
					associateType.add("Empleado[Inhabilitado]");
				}

				Map<String, Object> type = new HashMap<>();

				type.put("type", "Empleado");
				type.put("status", employee.enabled);

				typeAsso.add(type);

			}

			if (client != null) {
				associate.setIdClient(client.getId());
				associate.setEnabledClient(client.enabled);
				associate.setCellPhoneNumber2_Client(client.getCellPhoneNumber2());
				associate.setReferenceFamily_Client(client.getReferenceFamily());
				associate.setContacNumber_Client(client.getContacNumber());

				if (client.enabled == true) {
					associateType.add("Cliente[Habilitado]");
				} else {
					associateType.add("Cliente[Inhabilitado]");
				}

				Map<String, Object> type = new HashMap<>();

				type.put("type", "Cliente");
				type.put("status", client.enabled);

				typeAsso.add(type);

			}

			if (provider != null) {
				associate.setIdProvider(provider.getId());
				associate.setEnabledProvider(provider.enabled);
				associate.setBusinessName(provider.getBusinessName());
				associate.setEconomicActivityProvider(provider.getEconomicActivity());
				associate.setPaymentConditionProvider(provider.getPaymentCondition());
				associate.setCreationDateProvider(provider.getCreationDate());
				associate.setTypeServiceProvider(provider.getTypeService());
				associate.setCIIUCode(provider.getCIIUCode());
				associate.setCountry(provider.getCountry());
				associate.setCreationDateProvider(provider.getCreationDate());

				if (provider.enabled == true) {
					associateType.add("Proveedor[Habilitado]");
				} else {
					associateType.add("Proveedor[Inhabilitado]");
				}

				Map<String, Object> type = new HashMap<>();

				type.put("type", "Proveedor");
				type.put("status", associate.enabled);
				typeAsso.add(type);
			}

			if (partner != null) {
				associate.setIdPartner(partner.getId());
				associate.setEnabledPartner(partner.enabled);
				associate.setCreationDataPartner(partner.getCreationDate());
				if (partner.enabled == true) {
					associateType.add("Socio[Habilitado]");
				} else {
					associateType.add("Socio[Inhabilitado]");
				}

				Map<String, Object> type = new HashMap<>();

				type.put("type", "Socio");
				type.put("status", partner.enabled);
				typeAsso.add(type);
			}

			Map<String, Object> type = new HashMap<>();
			Map<String, Object> type2 = new HashMap<>();

			type.put("type", "General");
			type.put("status", data.enabled);
			typeAsso.add(type);

			type2.put("type", "Todos");
			type2.put("status", data.enabled);
			typeAsso.add(type2);

			associate.setAssociate_type_name(associateType);
			associate.setType(typeAsso);

			asociateslist.add(associate);
		}

		response.put("mensaje", "Assocaites list");
		response.put("users", asociateslist);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}

	@PreAuthorize("hasAuthority('USER_READ')")
	@PostMapping(path = "/findall", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> findAll(@RequestBody AssocietedPageData associetedPageData) {
	
		List<Associated> pages = (List<Associated>) associatedService.findAll(associetedPageData.getUserId(), associetedPageData.getNames(),
				associetedPageData.getIdentificationNumber(), associetedPageData.getCellPhoneNumber(),
				associetedPageData.getNeighborhood(), associetedPageData.getCity(), associetedPageData.getEmail(),
				associetedPageData.getTypeAssociated());

			List<Map<String, Object>> data = pages.stream().map(associated -> {

			Map<String, Object> d = new HashMap<>();

			Employee employee = null;
			Client client = null;
			Provider provider = null;
			Partner partner = null;

			employee = employeeService.findByIdAssociated(associated);
			client = clientService.findByIdAssociated(associated);
			provider = providerService.findByIdAssociated(associated);
			partner = partnerSrevice.findByIdAssociated(associated);

			Long idrol = null;

			if (employee != null) {
				idrol = employee.getUserId().getRole().getId();
			}

			d.put("associado", associated);
			d.put("employee", employee);
			d.put("client", client);
			d.put("provider", provider);
			d.put("partner", partner);
			d.put("rolvalue", idrol);

			return d;
		}).collect(Collectors.toList());

		return new ResponseEntity<Object>(data, HttpStatus.OK);

	}

	@PreAuthorize("hasAuthority('USER_READ')")
	@PostMapping("/GetUsers/pages")
	public ResponseEntity<?> GetUsers(@RequestBody AssocietedPageData associetedPageData) {

		Page<Associated> Lista = associatedService.findAll(associetedPageData.getPage() - 1,
				associetedPageData.getLimit(), associetedPageData.getUserId(), associetedPageData.getNames(),
				associetedPageData.getIdentificationNumber(), associetedPageData.getCellPhoneNumber(),
				associetedPageData.getNeighborhood(), associetedPageData.getCity(), associetedPageData.getEmail(),
				associetedPageData.getTypeAssociated());
		ArrayList<AssociatesDTO> asociateslist = new ArrayList<AssociatesDTO>();

		for (Associated data : Lista.getContent()) {

			Employee employee = null;
			Client client = null;
			Provider provider = null;
			Partner partner = null;
			List<String> associateType = new ArrayList<String>();
			List<Object> typeAsso = new ArrayList<Object>();

			employee = employeeService.findByIdAssociated(data);
			client = clientService.findByIdAssociated(data);
			provider = providerService.findByIdAssociated(data);
			partner = partnerSrevice.findByIdAssociated(data);

			AssociatesDTO associate = new AssociatesDTO();
			associate.setId(data.getId());
			associate.setFullName(data.getFirstName() + " " + data.getLastName());
			associate.setFirstName(data.getFirstName());
			associate.setLastName(data.getLastName());
			associate.setAddress(data.getAddress());
			associate.setEnabled(data.enabled);
			associate.setEmail(data.getEmail());
			associate.setIdentificationType(data.getIdentificationType());
			associate.setIdentificationNumber(data.getIdentificationNumber());
			associate.setCity(data.getCity());
			associate.setPhoneNumber(data.getPhoneNumber());
			associate.setCellPhoneNumber(data.getCellPhoneNumber());
			associate.setBirthdate(data.getBirthdate().toString());
			associate.setNeighborhood_Client(data.getNeighborhood());
			associate.setCreationDateAssociated(data.getCreationDate());

			if (associate.enabled == true) {
				associateType.add("Basica[Habilitado]");
			} else {
				associateType.add("Basica[Inhabilitado]");
			}

			if (employee != null) {

				associate.setIdEmployee(employee.getId());
				associate.setEnabledEmployee(employee.enabled);
				associate.setIdRol(employee.userId.getRole().getId());
				associate.setIdUser(employee.userId.getId());
				associate.setPositionEmployee(employee.getPosition());
				associate.setBranchEmployee(employee.getBranch());
				associate.setObservationsEmployee(employee.getObservations());
				associate.setFileEmployee(employee.getFile());
				associate.setCreationDataEmployee(employee.getCreationDate());

				if (employee.enabled == true) {
					associateType.add("Empleado[Habilitado]");
				} else {
					associateType.add("Empleado[Inhabilitado]");
				}

				Map<String, Object> type = new HashMap<>();

				type.put("type", "Empleado");
				type.put("status", employee.enabled);

				typeAsso.add(type);

			}

			if (client != null) {
				associate.setIdClient(client.getId());
				associate.setEnabledClient(client.enabled);
				associate.setCellPhoneNumber2_Client(client.getCellPhoneNumber2());
				associate.setReferenceFamily_Client(client.getReferenceFamily());
				associate.setContacNumber_Client(client.getContacNumber());

				if (client.enabled == true) {
					associateType.add("Cliente[Habilitado]");
				} else {
					associateType.add("Cliente[Inhabilitado]");
				}

				Map<String, Object> type = new HashMap<>();

				type.put("type", "Cliente");
				type.put("status", client.enabled);

				typeAsso.add(type);

			}

			if (provider != null) {
				associate.setIdProvider(provider.getId());
				associate.setEnabledProvider(provider.enabled);
				associate.setBusinessName(provider.getBusinessName());
				associate.setEconomicActivityProvider(provider.getEconomicActivity());
				associate.setPaymentConditionProvider(provider.getPaymentCondition());
				associate.setCreationDateProvider(provider.getCreationDate());
				associate.setTypeServiceProvider(provider.getTypeService());
				associate.setCIIUCode(provider.getCIIUCode());
				associate.setCountry(provider.getCountry());
				associate.setCreationDateProvider(provider.getCreationDate());

				if (provider.enabled == true) {
					associateType.add("Proveedor[Habilitado]");
				} else {
					associateType.add("Proveedor[Inhabilitado]");
				}

				Map<String, Object> type = new HashMap<>();

				type.put("type", "Proveedor");
				type.put("status", associate.enabled);
				typeAsso.add(type);
			}

			if (partner != null) {
				associate.setIdPartner(partner.getId());
				associate.setEnabledPartner(partner.enabled);
				associate.setCreationDataPartner(partner.getCreationDate());
				if (partner.enabled == true) {
					associateType.add("Socio[Habilitado]");
				} else {
					associateType.add("Socio[Inhabilitado]");
				}

				Map<String, Object> type = new HashMap<>();

				type.put("type", "Socio");
				type.put("status", partner.enabled);
				typeAsso.add(type);
			}

			Map<String, Object> type = new HashMap<>();
			Map<String, Object> type2 = new HashMap<>();

			type.put("type", "General");
			type.put("status", data.enabled);
			typeAsso.add(type);

			type2.put("type", "Todos");
			type2.put("status", data.enabled);
			typeAsso.add(type2);

			associate.setAssociate_type_name(associateType);
			associate.setType(typeAsso);
			asociateslist.add(associate);
		}

		PaginationData data = PaginationData.builder().pages(Lista.getTotalPages()).total(Lista.getTotalElements())
				.values(asociateslist).build();

		return new ResponseEntity<PaginationData>(data, HttpStatus.OK);
	}

	@Transactional
	@PreAuthorize("hasAuthority('USER_WRITE')")
	@PostMapping(path = "/save")
	public ResponseEntity<?> Save(@RequestParam(value = "fileEmployee", required = false) MultipartFile fileEmployee,
			@RequestParam(value = "fileProvider", required = false) MultipartFile fileProvider,
			@RequestParam(value = "fileClient", required = false) MultipartFile fileClient,
			@RequestParam(value = "isEmployee", required = false) String isEmployee,
			@RequestParam(value = "isClient", required = false) String isClient,
			@RequestParam(value = "isProvider", required = false) String isProvider,
			@RequestParam(value = "isPartner", required = false) String isPartner,
			@RequestParam(value = "rolPartner", required = false) String rolPartner,
			@RequestParam(value = "identificationType", required = false) String identificationType,
			@RequestParam(value = "phoneNumber", required = false) String phoneNumber,
			@RequestParam(value = "address", required = false) String address,
			@RequestParam(value = "identificationNumber", required = false) String identificationNumber,
			@RequestParam(value = "CellPhoneNumber", required = false) String CellPhoneNumber,
			@RequestParam(value = "birthdate", required = false) String birthdate,
			@RequestParam(value = "first_Name", required = false) String firstName,
			@RequestParam(value = "email", required = false) String email,
			@RequestParam(value = "lastName", required = false) String lastName,
			@RequestParam(value = "city", required = false) String city,
			@RequestParam(value = "creationDate_Client", required = false) String creationDate_Client,
			@RequestParam(value = "neighborhood_Client", required = false) String neighborhood_Client,
			@RequestParam(value = "CellPhoneNumber2_Client", required = false) String CellPhoneNumber2_Client,
			@RequestParam(value = "ReferenceFamily_Client", required = false) String ReferenceFamily_Client,
			@RequestParam(value = "ContacNumber_Client", required = false) String ContacNumber_Client,
			@RequestParam(value = "PositionEmployee", required = false) String PositionEmployee,
			@RequestParam(value = "branchEmployee", required = false) String branchEmployee,
			@RequestParam(value = "dependencyEmployee", required = false) String dependencyEmployee,
			@RequestParam(value = "ObservationsEmployee", required = false) String ObservationsEmployee,
			@RequestParam(value = "PasswordEmployee", required = false) String PasswordEmployee,
			@RequestParam(value = "economicActivityProvider", required = false) String economicActivityProvider,
			@RequestParam(value = "PaymentConditionProvider", required = false) String PaymentConditionProvider,
			@RequestParam(value = "creationDateProvider", required = false) String creationDateProvider,
			@RequestParam(value = "TypeServiceProvider", required = false) String TypeServiceProvider,
			@RequestParam(value = "providerContactName", required = false) String providerContactName,
			@RequestParam(value = "providerContactCellphone", required = false) String providerContactCellphone,
			@RequestParam(value = "CIIUCode", required = false) String CIIUCode,
			@RequestParam(value = "Country", required = false) String Country,
			@RequestParam(value = "businessNameProvider", required = false) String businessName,
			@RequestParam(value = "id_rol", required = false) Long idRol,
			@RequestParam(value = "creationDate_employee", required = false) String creationDate_employee,
			@RequestParam(value = "creationDate_associated", required = false) String creationDate_associated,
			@RequestParam(value = "creationDate_partner", required = false) String creationDate_partner)
			throws IOException {

		Map<String, Object> response = new HashMap<>();
		Associated associatefind = null;
		associatefind = associatedService.findByIdentificationNumber(identificationNumber);

		// Validamos que no se repita el numero de documento
		if (associatefind != null) {
			response.put("mensaje", "El número de identificación digitado ya se encuentra asignado a un asociado.");
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);

		}

		Associated associted = new Associated();

		associted.setFirstName(firstName);
		associted.setLastName(lastName);
		associted.setIdentificationType(identificationType);
		associted.setIdentificationNumber(identificationNumber);
		associted.setCellPhoneNumber(CellPhoneNumber);
		associted.setPhoneNumber(phoneNumber);
		associted.setAddress(address);
		associted.setNeighborhood(neighborhood_Client);
		associted.setCity(city);
		associted.setEmail(email);
		associted.setBirthdate(LocalDate.parse(birthdate, DateTimeFormatter.BASIC_ISO_DATE));
		associted.setEnabled(true);
		associted.setCreationDate(LocalDate.parse(creationDate_associated, DateTimeFormatter.BASIC_ISO_DATE));

		Associated ass = associatedService.save(associted);

		// validamos que tipo de usuarios es

		if (isEmployee != null && isEmployee.equals("true")) {
			// Aditional info Employee

			// 1. Creamos el usuario para iniciar al sistema

			Role rol = this.roleDao.findById(idRol).get();

			UserModel user = new UserModel();
			user.setAccountExpired(true);
			user.setAccountLocked(true);
			user.setCredentialsExpired(true);
			user.setEnabled(true);
			user.setPassword(passwordEncoder.encode(PasswordEmployee));
			user.setUsername(identificationNumber);
			user.setRole(rol);

			// Salvamos el usuarios
			UserModel userM = userService.SaveUser(user);

			Employee employee = new Employee();

			// documento del empleado
			if (fileEmployee != null) {
				// enviamos a guardar elarchivo al servidor
				String nombreArchivo = firstName + lastName + "-" + identificationNumber;
				employee.setFile(upload.copiar(fileEmployee, nombreArchivo, "HVEmpleado"));
			}

			employee.setPosition(PositionEmployee);
			employee.setBranch(branchEmployee);
			employee.setObservations(ObservationsEmployee);
			employee.setDependency(dependencyEmployee);
			employee.setEnabled(true);
			employee.setIdAssociated(ass);
			employee.setUserId(userM);
			employee.setCreationDate(LocalDate.parse(creationDate_employee, DateTimeFormatter.BASIC_ISO_DATE));

			employeeService.save(employee);

		}

		if (isClient != null && isClient.equals("true")) {

			// Aditional info Client
			Client client = new Client();
			
			if (fileClient != null) {
			
				String nombreArchivo = firstName + lastName + "-" + identificationNumber;
				client.setFile(upload.copiar(fileClient, nombreArchivo, "ContCliente"));
			}

			
			
			// llenamos la informacion adicional del asociado
			

			client.setCreationDate(LocalDate.parse(creationDate_Client, DateTimeFormatter.BASIC_ISO_DATE));
			client.setCellPhoneNumber2(CellPhoneNumber2_Client);
			client.setReferenceFamily(ReferenceFamily_Client);
			client.setContacNumber(ContacNumber_Client);
			client.setEnabled(true);
			client.setIdAssociated(ass);

			clientService.save(client);

		}

		if (isProvider != null && isProvider.equals("true")) {

			// llenamos la informacion adicional del asociado
			Provider provider = new Provider();

			provider.setEconomicActivity(economicActivityProvider);
			provider.setPaymentCondition(PaymentConditionProvider);
			provider.setCreationDate(LocalDate.parse(creationDateProvider, DateTimeFormatter.BASIC_ISO_DATE));
			provider.setTypeService(TypeServiceProvider);
			provider.setCIIUCode(CIIUCode);
			provider.setCountry(Country);
			provider.setBusinessName(businessName);
			provider.setContactName(providerContactName);
			provider.setContactCellphone(providerContactCellphone);
			provider.setEnabled(true);
			provider.setIdAssociated(ass);

			// documento del empleado

			if (fileProvider != null) {

				// enviamos a guardar elarchivo al servidor
				String nombreArchivo = businessName + "-" + identificationNumber;
				provider.setFile(upload.copiar(fileProvider, nombreArchivo, "RutProvedor"));

			}

			providerService.save(provider);

		}
		if (isPartner != null && isPartner.equals("true")) {

			// llenamos la informacion adicional del asociado
			Partner partner = new Partner();

			partner.setIdAssociated(ass);
			partner.setEnabled(true);
			partner.setRol(rolPartner);
			partner.setCreationDate(LocalDate.parse(creationDate_partner, DateTimeFormatter.BASIC_ISO_DATE));
			partnerSrevice.save(partner);

		}

		response.put("mensaje", "¡Asociado agregado exitosamente!");

		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);

	}

	@Transactional
	@PreAuthorize("hasAuthority('USER_WRITE')")
	@PutMapping(path = "/update")
	public ResponseEntity<?> Update(@RequestParam(value = "fileEmployee", required = false) MultipartFile fileEmployee,
			@RequestParam(value = "validFileEmployee", required = false) Boolean ValidFileEmployee,
			@RequestParam(value = "fileProvider", required = false) MultipartFile fileProvider,
			@RequestParam(value = "validFileProvider", required = false) Boolean ValidFileProvider,
			@RequestParam(value = "fileClient", required = false) MultipartFile fileClient,
			@RequestParam(value = "validFileClient", required = false) Boolean ValidFileClient,
			@RequestParam(value = "isEmployee", required = false) String isEmployee,
			@RequestParam(value = "isClient", required = false) String isClient,
			@RequestParam(value = "isProvider", required = false) String isProvider,
			@RequestParam(value = "isPartner", required = false) String isPartner,
			@RequestParam(value = "rolPartner", required = false) String rolPartner,
			@RequestParam(value = "identificationType", required = false) String identificationType,
			@RequestParam(value = "phoneNumber", required = false) String phoneNumber,
			@RequestParam(value = "address", required = false) String address,
			@RequestParam(value = "identificationNumber", required = false) String identificationNumber,
			@RequestParam(value = "CellPhoneNumber", required = false) String CellPhoneNumber,
			@RequestParam(value = "birthdate", required = false) String birthdate,
			@RequestParam(value = "first_Name", required = false) String firstName,
			@RequestParam(value = "email", required = false) String email,
			@RequestParam(value = "lastName", required = false) String lastName,
			@RequestParam(value = "city", required = false) String city,
			@RequestParam(value = "creationDate_Client", required = false) String creationDate_Client,
			@RequestParam(value = "neighborhood_Client", required = false) String neighborhood_Client,
			@RequestParam(value = "CellPhoneNumber2_Client", required = false) String CellPhoneNumber2_Client,
			@RequestParam(value = "ReferenceFamily_Client", required = false) String ReferenceFamily_Client,
			@RequestParam(value = "ContacNumber_Client", required = false) String ContacNumber_Client,
			@RequestParam(value = "PositionEmployee", required = false) String PositionEmployee,
			@RequestParam(value = "branchEmployee", required = false) String branchEmployee,
			@RequestParam(value = "dependencyEmployee", required = false) String dependencyEmployee,
			@RequestParam(value = "ObservationsEmployee", required = false) String ObservationsEmployee,
			@RequestParam(value = "PasswordEmployee", required = false) String PasswordEmployee,
			@RequestParam(value = "economicActivityProvider", required = false) String economicActivityProvider,
			@RequestParam(value = "PaymentConditionProvider", required = false) String PaymentConditionProvider,
			@RequestParam(value = "creationDateProvider", required = false) String creationDateProvider,
			@RequestParam(value = "TypeServiceProvider", required = false) String TypeServiceProvider,
			@RequestParam(value = "providerContactName", required = false) String providerContactName,
			@RequestParam(value = "providerContactCellphone", required = false) String providerContactCellphone,
			@RequestParam(value = "CIIUCode", required = false) String CIIUCode,
			@RequestParam(value = "Country", required = false) String Country,
			@RequestParam(value = "businessNameProvider", required = false) String businessName,
			@RequestParam(value = "id", required = false) Integer id,
			@RequestParam(value = "id_rol", required = false) Long idRol,
			@RequestParam(value = "creationDate_employee", required = false) String creationDate_employee,
			@RequestParam(value = "creationDate_associated", required = false) String creationDate_associated,
			@RequestParam(value = "creationDate_partner", required = false) String creationDate_partner)
			throws IOException {

		Map<String, Object> response = new HashMap<>();
		Associated associatefind = null;
		Associated associted = associatedService.findById(id);

		if (associted == null) {
			response.put("mensaje", "No existe un asociado con el ID " + id);
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}
		
		if(!identificationNumber.equals(associted.getIdentificationNumber())) {		
			associatefind = associatedService.findByIdentificationNumber(identificationNumber);
	
			// Validamos que no se repita el numero de documento
			if (associatefind != null) {
				response.put("mensaje", "El número de identificación digitado ya se encuentra asignado a un asociado.");
				return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
	
			}
		}

		associted.setFirstName(firstName);
		associted.setLastName(lastName);
		associted.setIdentificationType(associted.getIdentificationType());
		associted.setIdentificationNumber(associted.getIdentificationNumber());
		associted.setCellPhoneNumber(CellPhoneNumber);
		associted.setPhoneNumber(phoneNumber);
		associted.setAddress(address);
		associted.setNeighborhood(neighborhood_Client);
		associted.setCity(city);
		associted.setEmail(email);
		associted.setBirthdate(LocalDate.parse(birthdate, DateTimeFormatter.BASIC_ISO_DATE));
		associted.setCreationDate(LocalDate.parse(creationDate_associated, DateTimeFormatter.BASIC_ISO_DATE));

		Associated ass = associatedService.save(associted);

		// Empleado
		if (isEmployee != null && isEmployee.equals("true")) {

			Employee employee = null;
			employee = employeeService.findByIdAssociated(associted);

			if (employee != null) {

				Role rol = this.roleDao.findById(idRol).get();

				UserModel user = employee.getUserId();
				user.setRole(rol);
				// Salvamos el usuarios
				UserModel userM = userService.SaveUser(user);
				if(!ValidFileEmployee) {
					String rutaActual = Constant.ruta + employee.getFile();
					File oldFichero = new File(rutaActual);
					oldFichero.delete();
					employee.setFile(null);
				}else {
				// documento del empleado
					if (fileEmployee != null) {
						// Elimina mos el anterrir archivo
						if (employee.getFile() != null) {
							String rutaActual = Constant.ruta + employee.getFile();
							File oldFichero = new File(rutaActual);
							oldFichero.delete();
						}
	
						// enviamos a guardar elarchivo al servidor
						String nombreArchivo = firstName + lastName + "-" + associted.getIdentificationNumber();
						employee.setFile(upload.copiar(fileEmployee, nombreArchivo, "HVEmpleado"));
	
					}
				}

				employee.setPosition(PositionEmployee);
				employee.setBranch(branchEmployee);
				employee.setObservations(ObservationsEmployee);
				employee.setDependency(dependencyEmployee);
				employee.setIdAssociated(ass);
				employee.setUserId(userM);

				employeeService.save(employee);

			} else {

				// 1. Creamos el usuario para iniciar al sistema

				Role rolNew = this.roleDao.findById(idRol).get();

				UserModel userNew = new UserModel();
				userNew.setAccountExpired(true);
				userNew.setAccountLocked(true);
				userNew.setCredentialsExpired(true);
				userNew.setEnabled(true);
				userNew.setPassword(passwordEncoder.encode(PasswordEmployee));
				userNew.setUsername(associted.getIdentificationNumber());
				userNew.setRole(rolNew);

				// Salvamos el usuarios
				UserModel userMNew = userService.SaveUser(userNew);

				Employee employeeNew = new Employee();

				// documento del empleado
				if (fileEmployee != null) {
					// enviamos a guardar elarchivo al servidor
					String nombreArchivo = firstName + lastName + "-" + associted.getIdentificationNumber();
					employeeNew.setFile(upload.copiar(fileEmployee, nombreArchivo, "HVEmpleado"));
				}

				
				
				employeeNew.setPosition(PositionEmployee);
				employeeNew.setBranch(branchEmployee);
				employeeNew.setObservations(ObservationsEmployee);
				employeeNew.setDependency(dependencyEmployee);
				employeeNew.setEnabled(true);
				employeeNew.setIdAssociated(ass);
				employeeNew.setUserId(userMNew);
				employeeNew.setCreationDate(LocalDate.parse(creationDate_employee, DateTimeFormatter.BASIC_ISO_DATE));

				employeeService.save(employeeNew);
			}

		}

		// Cliente
		if (isClient != null && isClient.equals("true")) {

			Client client = null;
			client = clientService.findByIdAssociated(associted);
			
			// Aditional info Client

			if (client != null) {
				if(!ValidFileClient) {
					String rutaActual = Constant.ruta + client.getFile(); 
					File oldFichero = new File(rutaActual);
					oldFichero.delete();
					client.setFile(null);
					
				}else {		
					if (fileClient != null) {
										
						if (client.getFile() != null) {
								String rutaActual = Constant.ruta + client.getFile(); 
								File oldFichero = new File(rutaActual);
								oldFichero.delete();
						}
	
						// enviamos a guardar elarchivo al servidor
						String nombreArchivo = firstName + lastName + "-" + associted.getIdentificationNumber();
						client.setFile(upload.copiar(fileClient, nombreArchivo, "ContCliente"));
					
						
					}
				}

				client.setCreationDate(LocalDate.parse(creationDate_Client, DateTimeFormatter.BASIC_ISO_DATE));
				client.setCellPhoneNumber2(CellPhoneNumber2_Client);
				client.setReferenceFamily(ReferenceFamily_Client);
				client.setContacNumber(ContacNumber_Client);
				client.setIdAssociated(ass);

				clientService.save(client);

			} else {
				Client clientNew = new Client();
				
				if (fileClient != null) {
					
					

					if (clientNew.getFile() != null) {
							String rutaActual = Constant.ruta + client.getFile(); 
							File oldFichero = new File(rutaActual);
							oldFichero.delete();
					}					

					// enviamos a guardar elarchivo al servidor
					String nombreArchivo = firstName + lastName + "-" + associted.getIdentificationNumber();
					clientNew.setFile(upload.copiar(fileClient, nombreArchivo, "ContCliente"));
				
					
				}	
							
				clientNew.setCreationDate(LocalDate.parse(creationDate_Client, DateTimeFormatter.BASIC_ISO_DATE));
				clientNew.setCellPhoneNumber2(CellPhoneNumber2_Client);
				clientNew.setReferenceFamily(ReferenceFamily_Client);
				clientNew.setContacNumber(ContacNumber_Client);
				clientNew.setEnabled(true);
				clientNew.setIdAssociated(ass);

				clientService.save(clientNew);
			}

		}

		// Proveedor
		if (isProvider != null && isProvider.equals("true")) {

			Provider provider = null;
			provider = providerService.findByIdAssociated(associted);

			if (provider != null) {

				provider.setEconomicActivity(economicActivityProvider);
				provider.setPaymentCondition(PaymentConditionProvider);
				provider.setCreationDate(LocalDate.parse(creationDateProvider, DateTimeFormatter.BASIC_ISO_DATE));
				provider.setTypeService(TypeServiceProvider);
				provider.setCIIUCode(CIIUCode);
				provider.setCountry(Country);
				provider.setBusinessName(businessName);
				provider.setContactName(providerContactName);
				provider.setContactCellphone(providerContactCellphone);
				provider.setIdAssociated(ass);

				// documento del empleado

				if(!ValidFileProvider) {
					String rutaActual = Constant.ruta + provider.getFile();
					File oldFichero = new File(rutaActual);
					oldFichero.delete();
					provider.setFile(null);
				}else {
				
					if (fileProvider != null) {
	
						// Eliminamos el anterrir archivo
						if (provider.getFile() != null) {
							String rutaActual = Constant.ruta + provider.getFile();
							File oldFichero = new File(rutaActual);
							oldFichero.delete();
						}
	
						// enviamos a guardar elarchivo al servidor
						String nombreArchivo = businessName + "-" + associted.getIdentificationNumber();
						provider.setFile(upload.copiar(fileProvider, nombreArchivo, "RutProvedor"));
	
					}
				}

				providerService.save(provider);

			} else {

				Provider providerNew = new Provider();

				providerNew.setEconomicActivity(economicActivityProvider);
				providerNew.setPaymentCondition(PaymentConditionProvider);
				providerNew.setCreationDate(LocalDate.parse(creationDateProvider, DateTimeFormatter.BASIC_ISO_DATE));
				providerNew.setTypeService(TypeServiceProvider);
				providerNew.setCIIUCode(CIIUCode);
				providerNew.setCountry(Country);
				providerNew.setBusinessName(businessName);
				providerNew.setContactName(providerContactName);
				providerNew.setContactCellphone(providerContactCellphone);
				providerNew.setEnabled(true);
				providerNew.setIdAssociated(ass);

				// documento del empleado

				if (fileProvider != null) {
					// enviamos a guardar elarchivo al servidor
					String nombreArchivo = businessName + "-" + associted.getIdentificationNumber();
					providerNew.setFile(upload.copiar(fileProvider, nombreArchivo, "RutProvedor"));

				}

				providerService.save(providerNew);

			}

		}

		// Socio
		if (isPartner != null && isPartner.equals("true")) {

			Partner partner = null;
			partner = partnerSrevice.findByIdAssociated(associted);

			if (partner != null) {

				partner.setIdAssociated(ass);
				partner.setRol(rolPartner);

				partnerSrevice.save(partner);

			} else {

				Partner partnerNew = new Partner();

				partnerNew.setIdAssociated(ass);
				partnerNew.setEnabled(true);
				partnerNew.setRol(rolPartner);
				partnerNew.setCreationDate(LocalDate.parse(creationDate_partner, DateTimeFormatter.BASIC_ISO_DATE));

				partnerSrevice.save(partnerNew);
			}

		}

		response.put("mensaje", "¡Asociado actualizado exitosamente!");
		response.put("Asso",ass);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);

	}

	@PreAuthorize("hasAuthority('USER_WRITE')")
	@PostMapping(path = "/SaveProfile")
	public ResponseEntity<?> SaveProfile(@RequestBody Map<String, Object> associate) throws IOException {

		Map<String, Object> response = new HashMap<>();

		Associated associted = associatedService.findById((Integer) associate.get("id"));

		associted.setId((Integer) associate.get("id"));
		associted.setFirstName(associate.get("firstName").toString());
		associted.setLastName(associate.get("lastName").toString());
		associted.setCellPhoneNumber(associate.get("cellPhoneNumber").toString());
		associted.setEmail(associate.get("email").toString());
		associted.setAddress(associate.get("address").toString());
		// otros datos que estaban pendientes
		associted
				.setBirthdate(LocalDate.parse(associate.get("birthdate").toString(), DateTimeFormatter.BASIC_ISO_DATE));
		associted.setPhoneNumber(associate.get("phoneNumber").toString());
		associted.setNeighborhood(associate.get("neighborhood").toString());
		associted.setCity(associate.get("city").toString());

		associatedService.save(associted);

		response.put("mensaje",
				"¡" + associted.getFirstName() + " " + associted.getLastName() + " editado exitosamente!");
		response.put("data", associted);

		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);

	}

	@PreAuthorize("hasAuthority('USER_READ')")
	@GetMapping(path = "/enabledReason/{associateName}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> enabledreason(@PathVariable String associateName) {

		Map<String, Object> response = new HashMap<>();
		List<EnabledReason> enabledReasons = null;
		AssociatesType type = null;

		type = associatesTypeService.findByName(associateName);

		if (type == null) {
			response.put("mensaje", "No existe ningún tipo de asociado con el nombre " + associateName);
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}

		enabledReasons = enabledReasonService.findByAssociatesType(type);

		if (enabledReasons == null) {
			response.put("mensaje", "El no existen razones de retiro para el tipo de asociado "
					+ associatesTypeService.getClass().getName());
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}

		response.put("data", enabledReasons);

		return new ResponseEntity<Object>(response, HttpStatus.OK);

	}

	@PreAuthorize("hasAuthority('PASSWORD_WRITE')")
	@PutMapping(path = "/changePassword", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> ChangePassword(@RequestBody Map<String, Object> employee) {

		Map<String, Object> response = new HashMap<>();

		Employee employe = null;

		employe = employeeService.findById(Integer.parseInt(employee.get("idEmployee").toString()));

		if (employe == null) {
			response.put("mensaje", "No existe un empleado con el ID '" + employee.get("idEmployee").toString() + "'");
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}

		UserModel user = employe.getUserId();
		user.setPassword(passwordEncoder.encode(employee.get("newPassword").toString()));

		userService.SaveUser(user);

		response.put("mensaje", "¡Contraseña actualizada correctamente!");
		response.put("data", employe);

		return new ResponseEntity<Object>(response, HttpStatus.OK);

	}

	@PreAuthorize("hasAuthority('DOCUMENT_WRITE')")
	@PutMapping(path = "/changeDocument", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> ChangeDocument(@RequestBody Map<String, Object> asociate) {

		Map<String, Object> response = new HashMap<>();

		Associated associate = null;

		associate = associatedService.findById(Integer.parseInt(asociate.get("idAssociate").toString()));

		if (associate == null) {
			response.put("mensaje", "No existe un asociado con el ID '" + asociate.get("idAssociate").toString() + "'");
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}
		
		if(!asociate.get("documentNumber").toString().equals(associate.getIdentificationNumber())) {		
			Associated associatefind = associatedService.findByIdentificationNumber(asociate.get("documentNumber").toString());
	
			// Validamos que no se repita el numero de documento
			if (associatefind != null) {
				response.put("mensaje", "El número de identificación digitado ya se encuentra asignado a un asociado.");
				return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
	
			}
		}

		associate.setIdentificationType(asociate.get("documentType").toString());
		associate.setIdentificationNumber(asociate.get("documentNumber").toString());
		associatedService.save(associate);

		// Verifcamos si el asociado es cliente para cambiar la cedula en en loguin.
		Employee employee = null;
		employee = employeeService.findByIdAssociated(associate);

		if (employee != null) {
			UserModel user = employee.getUserId();
			user.setUsername(asociate.get("documentNumber").toString());
			userService.SaveUser(user);
		}

		response.put("mensaje", "¡Contraseña actualizada correctamente!");
		response.put("data", associate);

		return new ResponseEntity<Object>(response, HttpStatus.OK);

	}

	// Metodos para recuperar la contraseña
	@PostMapping("/sendMailRecovery")
	public ResponseEntity<?> sendMail(@RequestBody Map<String, Object> data) throws Exception {

		Map<String, Object> response = new HashMap<>();
		String nombreUsuario = data.get("userName").toString();

		UserModel usuario = null;
		try {

			usuario = userService.findUserByUser(nombreUsuario).get();

			if (usuario == null) {
				response.put("mensaje", "El usuario digitado no existe en el sistema");
				return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
			}

			Integer codigo = recoverPasswordService.generateTokenUsuario(usuario);

			Employee empleado = employeeService.findByUserId(usuario);

			String to = empleado.getIdAssociated().getEmail();
			String subject = "Restablecimiento de contraseña ESG MANAGER";
			String text = "Hemos recibido una solicitud para el restablecimiento de tu contraseña del aplicativo ESG MANAGER, "
					+ "\npara ello debes usar el siguiente codigo: " + "\n " + codigo + ""
					+ "\n \nTen en cuenta que este codigo caducara en 3 horas. Transcurrido este tiempo, tendrás que  volver a realizar la solicitud de restablecimiento de contraseña.";

			if (to == null) {
				response.put("mensaje",
						"No se puede recuperar contraseña ya que el usuario digitado no tiene un correo electrónico asociado.");
				return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
			}
			sendEmail.sendEmail(to, subject, text);

		} catch (MailException e) {
			response.put("mensaje", "Error al enviar el correo electronico");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		response.put("mensaje", "Se ha enviado un email con los pasos para restablecer la contraseña.");
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}

	@PostMapping("/validateRecovery")
	public ResponseEntity<?> sendPassRecoveryMovil(@RequestBody Map<String, Object> data) throws Exception {
		Map<String, Object> response = new HashMap<>();
		RecoverPassword recuperarPass = null;
		try {
			recuperarPass = recoverPasswordService.findByCodigo(Integer.parseInt(data.get("code").toString()));

			if (recuperarPass == null) {
				response.put("mensaje", "Error: Codigo no valido");
				return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
			}
			if (recuperarPass.getFechauso() != null) {
				response.put("mensaje", "El codigo digitado ya fue utilizado!");
				return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
			}

			String nombreUsuario = data.get("userName").toString();
			UserModel usuario = null;
			usuario = userService.findUserByUser(nombreUsuario).get();
			Employee empleado = employeeService.findByUserId(usuario);
			Employee empleado2 = employeeService.findByUserId(recuperarPass.getIdusuario());

			if (empleado.getId() == empleado2.getId()) {

				long milliseconds = Calendar.getInstance().getTime().getTime() - recuperarPass.getFechacrea().getTime();
				int hours = (int) ((milliseconds / (1000 * 60 * 60)) % 24);

				if (hours > 3) {
					response.put("mensaje",
							"El codigo se encuentra vencido, para recuperar la contraseña debe generar otro codigo!");
					return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
				}

				// Actualizamos la fecha de uso del codigo
				recuperarPass.setFechauso(Calendar.getInstance().getTime());
				recoverPasswordService.save(recuperarPass);

				usuario.setPassword(passwordEncoder.encode(data.get("newPassword").toString()));
				userService.SaveUser(usuario);

			} else {
				response.put("mensaje", "Lo sentimos, este codigo no se te ha sido asignado.!");
				return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);

			}

		} catch (DataAccessException e) {
			response.put("mensaje", "Error al actualizar la contraseña en la base de datos");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		response.put("mensaje", "La contraseña ha sido actualizado con éxito!");
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
	}

}
