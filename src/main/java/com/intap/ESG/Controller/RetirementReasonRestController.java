package com.intap.ESG.Controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.intap.ESG.Models.RetirementReason;
import com.intap.ESG.Service.RetirementReasonService;

@RestController
@RequestMapping(path = "retirementReason")
public class RetirementReasonRestController {
	
	@Autowired
	private RetirementReasonService retirementReasonService;
	
	@PreAuthorize("hasAuthority('CONTRACTS_READ')")
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?>  findAll(){
		Map<String, Object> response = new HashMap<>();
		
		List<RetirementReason> categoryProducts =  retirementReasonService.findAll();
		
		response.put("mensaje", "¡Listado de razones de retiro!");
		response.put("data", categoryProducts);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}

}
