package com.intap.ESG.Controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.intap.ESG.Models.ContractNews;
import com.intap.ESG.Models.Contracts;
import com.intap.ESG.Models.News;
import com.intap.ESG.Service.IContractNewsService;
import com.intap.ESG.Service.IContractsService;
import com.intap.ESG.Service.INewsService;

@RestController
@RequestMapping(path = "newsContract")
public class NewsContractRestController {
	
	@Autowired
	private IContractNewsService contractNewsService;
	
	@Autowired
	private IContractsService contractsService;
	
	@Autowired
	private INewsService newsService;
	
	@PreAuthorize("hasAuthority('CONTRACTS_READ')")
	@GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> findById(@PathVariable Long id) {
		
		Contracts contract = null;
		Map<String, Object> response = new HashMap<>();
		
		try {
			
			contract = contractsService.findById(id);	
			
		} catch(DataAccessException e) {
			response.put("mensaje", "Error al realizar la consulta en la base de datos");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		if(contract == null) {
			response.put("mensaje", "El contrato ID: ".concat(id.toString().concat(" no existe en la base de datos!")));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}
		
		List<ContractNews> cn = contractNewsService.findByContracts(contract);
		

			response.put("data", cn);
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}
	
	@PreAuthorize("hasAuthority('CONTRACTS_WRITE')")
	@PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> create(@RequestBody Map<String, Object> dato) {
		
		Map<String, Object> response = new HashMap<>();
		News news=null;
		Contracts contractSave = null;
		ContractNews cNews = new ContractNews();
		
		news = newsService.findById(Long.parseLong(dato.get("newsId").toString()));
		contractSave = contractsService.findById(Long.parseLong(dato.get("contractId").toString()));
		
		cNews.setTaxes(Double.parseDouble(dato.get("taxes").toString()));
		cNews.setUnitPrice(Double.parseDouble(dato.get("unitPrice").toString()));
		cNews.setAmount(Integer.parseInt(dato.get("amount").toString()));
		cNews.setObservations(dato.get("Observations").toString());
		
		cNews.setNumberOfMonths(Integer.parseInt(dato.get("numberOfMonths").toString()));
		cNews.setContracts(contractSave);
		cNews.setNews(news);
		cNews.setBilledMonths(0);
		
		ContractNews n =contractNewsService.save(cNews);
		
		response.put("mensaje", "¡La novedad ha sido creada con éxito!");
		response.put("data", n);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);

	}
	
	
	@PreAuthorize("hasAuthority('CONTRACTS_WRITE')")
	@PutMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> update(@PathVariable Long id,@RequestBody Map<String, Object> dato) {
		
		Map<String, Object> response = new HashMap<>();
		News news=null;
		Contracts contractSave = null;
		ContractNews cNews = contractNewsService.findById(id);
		
		news = newsService.findById(Long.parseLong(dato.get("newsId").toString()));
		contractSave = contractsService.findById(Long.parseLong(dato.get("contractId").toString()));
		
		cNews.setTaxes(Double.parseDouble(dato.get("taxes").toString()));
		cNews.setUnitPrice(Double.parseDouble(dato.get("unitPrice").toString()));
		cNews.setAmount(Integer.parseInt(dato.get("amount").toString()));
		cNews.setObservations(dato.get("Observations").toString());
		cNews.setNumberOfMonths(Integer.parseInt(dato.get("numberOfMonths").toString()));
		cNews.setContracts(contractSave);
		cNews.setNews(news);
		
		ContractNews n =contractNewsService.save(cNews);
		
		response.put("mensaje", "¡La novedad ha sido actualizada con éxito!");
		response.put("data", n);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);

	}
	
	@PreAuthorize("hasAuthority('CONTRACTS_WRITE')")
	@PostMapping(path = "/saveSeveralContracts", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> saveSeveralContracts(@RequestBody Map<String, Object> dato) {
		
		Map<String, Object> response = new HashMap<>();
		ArrayList<?> contractsIds1 = null;
		News news=null;
		Contracts contractSave = null;
		List<ContractNews> allSaved = new ArrayList<>();
		
		contractsIds1 = (ArrayList<?>) dato.get("data");
		JSONArray contractsData = new JSONArray(contractsIds1);
		
		news = newsService.findById(Long.parseLong(dato.get("newsId").toString()));
		

		for (int i = 0; i < contractsData.length(); i++) {
			ContractNews cNews = new ContractNews();
			
			
			JSONObject data = contractsData.getJSONObject(i);
			contractSave = contractsService.findById(data.getLong("contractId"));
			
			cNews.setTaxes(Double.parseDouble(dato.get("taxes").toString()));
			cNews.setUnitPrice(Double.parseDouble(data.get("unitPrice").toString()));
			cNews.setAmount(Integer.parseInt(dato.get("amount").toString()));
			cNews.setObservations(dato.get("Observations").toString());
			cNews.setNumberOfMonths(Integer.parseInt(dato.get("numberOfMonths").toString()));
			cNews.setContracts(contractSave);
			cNews.setNews(news);
			cNews.setBilledMonths(0);
			
			ContractNews n =contractNewsService.save(cNews);
			
			allSaved.add(n);
			
		}
		
		
		
		response.put("mensaje", "¡Las novedades han sido creadas con éxito!");
		response.put("data", allSaved);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);

	}
	
	@PreAuthorize("hasAuthority('CONTRACTS_DELETE')")
	@DeleteMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> delete(@PathVariable Long id) {
		Map<String, Object> response = new HashMap<>();

			contractNewsService.delete(id);

		
		response.put("data", "¡Novedad eliminado exitosamente");
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}
}
