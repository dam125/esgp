package com.intap.ESG.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import com.intap.ESG.Models.DTO.ErrorData;
import com.intap.ESG.exception.ValidationException;



/**
 * 
 * @author Lisbey Urrea
 *
 */
public class ValidationFields<T> {

	public void validate(T t, String exception) {		
		List<ErrorData> errors = new ArrayList<ErrorData>();
		ValidatorFactory factory = javax.validation.Validation.buildDefaultValidatorFactory();
		Validator validator = factory.getValidator();

		Set<ConstraintViolation<T>> constraints = validator.validate(t);
		if (!constraints.isEmpty()) {
			constraints.forEach(constraint -> {
				System.err.println(constraint.getConstraintDescriptor());
				errors.add(ErrorData.builder().message(constraint.getMessage())
						.property(constraint.getPropertyPath().toString()).build());
			});

			throw new ValidationException(exception, errors);
		}
	}

}
