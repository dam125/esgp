package com.intap.ESG.util;

import java.util.Properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import com.intap.ESG.Dao.IConfiguracionesEmailDao;
import com.intap.ESG.Models.ConfiguracionesEmail;


public class PropertiesEmail {
	
	@Autowired
	IConfiguracionesEmailDao configuracionesEmailDao;
	
	public static Properties properties(ConfiguracionesEmail confEmail) {
		
		Properties props = System.getProperties();
	    props.put("mail.defaultEncoding", confEmail.getDefaultEncoding());
	    props.put("mail.smtp.host", confEmail.getHost());  
	    props.put("mail.smtp.port",confEmail.getPort()); 
	    props.put("mail.smtp.user", confEmail.getUsername());
	    props.put("mail.smtp.clave", confEmail.getPassword());    
	    props.put("mail.smtp.auth", confEmail.getMailSmtpAuth());   
	    props.put("mail.smtp.starttls.enable", confEmail.getMailSmtpStarttlsEnable()); 
	    props.put("mail.smtp.starttls.enable", confEmail.getMailSmtpStarttlsEnable()); 
	    props.put("mail.smtp.ssl.trust", confEmail.getMailSmtpSslTrust()); 
	    props.put("mail.smtp.connectiontimeout", confEmail.getMailSmtpConnectiontimeout()); 
	    props.put("mail.smtp.timeou", confEmail.getMailSmtpTimeout()); 
	    props.put("mail.smtp.writetimeout", confEmail.getMailSmtpWritetimeout()); 
	    
    
		return props;
	}
	
}
