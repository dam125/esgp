/**
 *
 */
package com.intap.ESG.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

import com.google.common.base.Predicate;
import com.google.common.base.Predicates;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.OAuthBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.GrantType;
import springfox.documentation.service.ResourceOwnerPasswordCredentialsGrant;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.service.SecurityScheme;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;


//@SwaggerDefinition
//@Configuration
public class SwaggerConfiguration extends WebMvcConfigurationSupport {

	@Value("${host.base.address}")
	private String baseAddress;

	@Bean
	public Docket usersApi() {
		return new Docket(DocumentationType.SWAGGER_2).forCodeGeneration(true).apiInfo(usersApiInfo()).select()
				.apis(RequestHandlerSelectors.any()).paths(regex()).build()
				.securitySchemes(Arrays.asList(securityScheme())).securityContexts(Arrays.asList(securityContext()));
	}

	private ApiInfo usersApiInfo() {
		return new ApiInfoBuilder().title("OTP Servicios").version("2.0").license("Apache License Version 2.0").build();
	}

	private Predicate<String> regex() {
		List<Predicate<String>> predicates = new ArrayList<Predicate<String>>();
		predicates.add(PathSelectors.regex("/authorities.*"));
		predicates.add(PathSelectors.regex("/roles.*"));
		/*predicates.add(PathSelectors.regex("/cities.*"));
		predicates.add(PathSelectors.regex("/countries.*"));
		predicates.add(PathSelectors.regex("/states.*"));
		predicates.add(PathSelectors.regex("/users.*")); */


		return Predicates.or(predicates);
	}

	@Override
	protected void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("swagger-ui.html").addResourceLocations("classpath:/META-INF/resources/");

		registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");
	}

	private SecurityScheme securityScheme() {
		GrantType grantType = new ResourceOwnerPasswordCredentialsGrant(this.baseAddress + "oauth/token");

		SecurityScheme oauth = new OAuthBuilder().name("spring_oauth").grantTypes(Arrays.asList(grantType))
				.scopes(Arrays.asList(scopes())).build();
		return oauth;
	}

	private AuthorizationScope[] scopes() {
		AuthorizationScope[] scopes = { new AuthorizationScope("read", "for read operations"),
				new AuthorizationScope("write", "for write operations") };
		return scopes;
	}

	private SecurityContext securityContext() {
		return SecurityContext.builder()
				.securityReferences(Arrays.asList(new SecurityReference("spring_oauth", scopes()))).forPaths(regex())
				.build();
	}

}
