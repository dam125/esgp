/**
 *
 */
package com.intap.ESG.util;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;

/**
 * @author Leonardo Castro
 *
 */
public final class SortUtil {

	/**
	 * - = descendente
	 * + = acsendente
	 * @param value
	 * @return
	 */
	public static List<Order> sort(String value) {
		List<Order> orders = new ArrayList<Sort.Order>();

		if(value != null && !value.isEmpty()) {
			String[] values = value.split(",");

			for (int i = 0; i < values.length; i++) {
				String field = values[i];

				Order order = null;

				if (field.startsWith("-")) {
					order = new Order( Direction.DESC,  field.substring(1));
				} else {
					order = new Order( Direction.ASC,  field);
				}

				orders.add(order);
			}

		}

		return orders;
	}

}
