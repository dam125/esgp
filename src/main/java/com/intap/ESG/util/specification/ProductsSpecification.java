/**
 * 
 */
package com.intap.ESG.util.specification;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.intap.ESG.Models.CategoryProducts;
import com.intap.ESG.Models.Products;

import lombok.AllArgsConstructor;

/**
 * @author Leonardo
 *
 */
@SuppressWarnings("serial")
@AllArgsConstructor
public class ProductsSpecification implements Specification<Products> {

	private String name;
	private String reference;
	private String type;
	private Boolean inventory;
	private Boolean enabled;
	private Integer minTotal;
	private Integer maxTotal;
	private Long categoryProductsId;

	@Override
	public Predicate toPredicate(Root<Products> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
		Predicate p = criteriaBuilder.conjunction();

		List<javax.persistence.criteria.Predicate> predicates = new ArrayList<javax.persistence.criteria.Predicate>();

		if (name != null && !name.isEmpty()) {
			predicates.add(criteriaBuilder.like(criteriaBuilder.upper(root.get("name")), "%" + this.name.toUpperCase() + "%"));
		}
		
		if (reference != null && !reference.isEmpty()) {
			predicates.add(criteriaBuilder.like(criteriaBuilder.upper(root.get("reference")), "%" + this.reference.toUpperCase() + "%"));
		}
		
		if (type != null && !type.isEmpty()) {
			predicates.add(criteriaBuilder.like(criteriaBuilder.upper(root.get("type")), "%" + this.type.toUpperCase() + "%"));
		}
		
		if (this.inventory != null ) {
			predicates.add(criteriaBuilder.equal(root.get("inventory"), this.inventory));
		}
		
		if(this.minTotal != null) {
			predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get("unitPrice"), this.minTotal));
		}
		
		if (maxTotal != null) {
			predicates.add(criteriaBuilder.lessThanOrEqualTo(root.get("unitPrice"), this.maxTotal));
		}
		
		if (this.enabled != null ) {
			predicates.add(criteriaBuilder.equal(root.get("enabled"), this.enabled));
		}
		
		if (this.categoryProductsId != null ) {
			predicates.add(criteriaBuilder.equal(root.get("categoryProducts"), CategoryProducts.builder().id(this.categoryProductsId).build()));
		}

		if (predicates.size() > 0) {
			p.getExpressions().add(criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()])));
		}

		return p;
	}

}
