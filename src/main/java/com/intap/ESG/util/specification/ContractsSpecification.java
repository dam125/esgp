 package com.intap.ESG.util.specification;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;

import org.springframework.data.jpa.domain.Specification;

import com.intap.ESG.Models.ContractDetails;
import com.intap.ESG.Models.ContractStatus;
import com.intap.ESG.Models.Contracts;
import com.intap.ESG.Models.Invoices;

import ch.qos.logback.core.net.server.Client;
import lombok.AllArgsConstructor;

@SuppressWarnings("serial")
@AllArgsConstructor
public class ContractsSpecification implements Specification<Contracts> {

	private String nameCliente;
	private String consecutive;
	private String cc;
	private String ip;
	private Integer fopen;
	private Integer fclose;
	private String mac;
	private LocalDate fechaAfiliacion;
	private String barrio;
	private String telefono;
	private String direccio;
	private Number valor;
	private Boolean activo;
	private String estado;
	private String subsidiado;
	private LocalDate fechaFac;
	private String serialEquipo;
	private Integer saldomin;
	private Integer saldomax;
	
	@Override
	public Predicate toPredicate(Root<Contracts> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
		Predicate p = criteriaBuilder.conjunction();
		
		List<javax.persistence.criteria.Predicate> predicates = new ArrayList<javax.persistence.criteria.Predicate>();
		
		if(nameCliente != null && !nameCliente.isEmpty()) {
			
			Path<Client> client = root.get("idClient");
			
			predicates.add(criteriaBuilder.or(
					criteriaBuilder.like(
							criteriaBuilder.upper(
									criteriaBuilder.concat( client.get("idAssociated").get("firstName"),
									criteriaBuilder.concat(" ", 
											client.get("idAssociated").get("lastName")))),
							
							"%" + nameCliente.trim().toUpperCase() + "%"),
					
					criteriaBuilder.like(criteriaBuilder.upper(client.get("idAssociated").get("firstName")),
							"%" + nameCliente.trim().toUpperCase() + "%"),
					
					criteriaBuilder.like(criteriaBuilder.upper(client.get("idAssociated").get("lastName")),
							"%" + nameCliente.trim().toUpperCase() + "%")));
		}
		
		if(consecutive != null && !consecutive.isEmpty()) {
			predicates.add(criteriaBuilder.like(root.get("consecutive"), "%" + consecutive.trim() + "%"));
		}
		
		if(cc != null && !cc.isEmpty()) {
			
			Path<Client> client = root.get("idClient");
			
			predicates.add(criteriaBuilder.like(criteriaBuilder.upper(client.get("idAssociated").get("identificationNumber")),"%" + cc.trim().toUpperCase() + "%"));
		}
		
		if(ip != null && !ip.isEmpty()) {
			predicates.add(criteriaBuilder.like(criteriaBuilder.upper(root.get("ipClient")), "%" + ip.trim().toUpperCase() + "%"));
		}
		
		if(fopen != null) {
			Subquery<Long> sub = query.subquery(Long.class);
			Root<Invoices> subRoot = sub.from(Invoices.class);
			sub.select(criteriaBuilder.count(subRoot.get("envoicesState")));
			sub.where(criteriaBuilder.equal(subRoot.get("envoicesState"),"Abierta"), criteriaBuilder.equal(subRoot.get("contrato"), root.get("id")));
			sub.groupBy(subRoot.get("envoicesState"));
					
			predicates.add(criteriaBuilder.and(criteriaBuilder.equal(sub, fopen)));	
		}
		
		if(saldomin != null && saldomax != null ) {
			Subquery<Integer> sub = query.subquery(Integer.class);
			Root<Invoices> subRoot = sub.from(Invoices.class);
			sub.select(criteriaBuilder.sum((subRoot.get("total"))));
			sub.where(criteriaBuilder.equal(subRoot.get("envoicesState"),"Abierta")					
					,criteriaBuilder.equal(subRoot.get("contrato"), root.get("id")));
			sub.groupBy(subRoot.get("envoicesState"));
			
			predicates.add(criteriaBuilder.greaterThanOrEqualTo(sub, this.saldomin));
			predicates.add(criteriaBuilder.lessThanOrEqualTo(sub, this.saldomax));
	
		}
		
		
		if(fclose != null) {
			Subquery<Long> sub = query.subquery(Long.class);
			Root<Invoices> subRoot = sub.from(Invoices.class);
			sub.select(criteriaBuilder.count(subRoot.get("envoicesState")));
			sub.where(criteriaBuilder.equal(subRoot.get("envoicesState"),"Cerrada"), criteriaBuilder.equal(subRoot.get("contrato"), root.get("id")));
			sub.groupBy(subRoot.get("envoicesState"));
			
			
			predicates.add(criteriaBuilder.and(criteriaBuilder.equal(sub, fclose)));
		}
		
		if(mac != null && !mac.isEmpty()) {
			predicates.add(criteriaBuilder.equal(root.get("macClient"), mac));
		}
		
		if(fechaAfiliacion != null) {
			predicates.add(criteriaBuilder.equal(root.get("startDate"), fechaAfiliacion));
		}
		
		if(barrio != null && !barrio.isEmpty()) {
			predicates.add(criteriaBuilder.like(criteriaBuilder.upper(root.get("neighborhood")), "%" + barrio.trim().toUpperCase() + "%"));
		}
		
		if(telefono != null && !telefono.isEmpty()) {
			Path<Client> client = root.get("contrato").get("idClient");
			predicates.add(criteriaBuilder.equal(client.get("idAssociated").get("CellPhoneNumber"), telefono));
		}
		
		if(direccio != null && !direccio.isEmpty()) {
			predicates.add(criteriaBuilder.like(criteriaBuilder.upper(root.get("address")), "%" + direccio.trim().toUpperCase() + "%"));
		}
		
		if(valor != null) {
			Subquery<Double> sub = query.subquery(Double.class);
			Root<ContractDetails> subRoot = sub.from(ContractDetails.class);
			Expression<Double> unit = criteriaBuilder.prod(subRoot.<Double>get("unitPrice"), subRoot.<Double>get("amount"));
			Expression<Double> taxes = criteriaBuilder.prod(unit, subRoot.<Double>get("taxes"));
			Expression<Number> quot = criteriaBuilder.quot(taxes, 100);
			Expression<Double> sum = criteriaBuilder.sum(unit, quot).as(Double.class);
			sub.select(criteriaBuilder.sum(sum));
			sub.where(criteriaBuilder.equal(subRoot.get("contracts"), root.get("id")));
			sub.groupBy(subRoot.get("contracts"));
			
			
			predicates.add(criteriaBuilder.and(criteriaBuilder.equal(sub, valor)));
		}
		
		if(activo != null) {
			predicates.add(criteriaBuilder.equal(root.get("enabled"), activo));
		}
		
		if(estado != null && !estado.isEmpty()) {
			Subquery<Long> sub = query.subquery(Long.class);
			Root<ContractStatus> subRoot = sub.from(ContractStatus.class);
			sub.select(criteriaBuilder.max(subRoot.get("createAt")));
			sub.where(criteriaBuilder.equal(subRoot.get("contract"), root.get("id")));
			sub.groupBy(subRoot.get("contract"));
			
			Path<ContractStatus> cs = root.join("contractStatus");
			predicates.add(criteriaBuilder.and(criteriaBuilder.equal(cs.get("status").get("name"), estado),criteriaBuilder.equal(sub, cs.get("createAt"))));
		}
		
		if(subsidiado != null && !subsidiado.isEmpty()) {
			predicates.add(criteriaBuilder.like(criteriaBuilder.upper(root.get("type")), subsidiado.toUpperCase()+"%"));
		}
		
		if(fechaFac != null) {
			predicates.add(criteriaBuilder.equal(root.get("billingStarDate"), fechaFac));
		}
		
		if(serialEquipo != null && !serialEquipo.isEmpty()) {
			predicates.add(criteriaBuilder.equal(root.get("serialEquipment"), serialEquipo));
		}
		
		if (predicates.size() > 0) {
			p.getExpressions().add(criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()])));
		}
		
		return p;
	}
}
