package com.intap.ESG.util.specification;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;
import com.intap.ESG.Models.News;

import lombok.AllArgsConstructor;

@SuppressWarnings("serial")
@AllArgsConstructor
public class NewsSpecification implements Specification<News> {



	
	private String name;
	
    private String tipo;
    
	private Double valor;
	
	private Double valorMax;
	
	private Boolean porcentaje;
	
	private Double valorPorcentaje;
	
	private Boolean estado;
	private static final long serialVersionUID = 1L;

	@Override
	public Predicate toPredicate(Root<News> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
		Predicate p = criteriaBuilder.conjunction();
		List<javax.persistence.criteria.Predicate> predicates = new ArrayList<javax.persistence.criteria.Predicate>();

		
		if (name != null && !name.isEmpty()) {
			predicates.add(criteriaBuilder.like(criteriaBuilder.upper(root.get("name")), "%" + this.name.toUpperCase() + "%"));
		}
		
		if (tipo != null && !tipo.isEmpty()) {
			predicates.add(criteriaBuilder.like(criteriaBuilder.upper(root.get("tipo")), "%" + this.tipo.toUpperCase() + "%"));
		}
	
		if (valor != null) {
			
			predicates.add(criteriaBuilder.or(criteriaBuilder.greaterThanOrEqualTo(root.get("valor"), this.valor)));
		
		}
		
		if(this.valorMax != null && valor != null) {
			predicates.add(criteriaBuilder.or(criteriaBuilder.lessThanOrEqualTo(root.get("valor"), this.valorMax)));
		}
		

		if (valorPorcentaje != null ) {
			predicates.add(criteriaBuilder.or(criteriaBuilder.greaterThanOrEqualTo(root.get("valorPorcentaje"), this.valorPorcentaje)));
		}
		
		
		if (valorMax != null && valorPorcentaje != null ) {
			predicates.add(criteriaBuilder.or(criteriaBuilder.lessThanOrEqualTo(root.get("valorPorcentaje"), this.valorMax)));
		}
		
		
		
		if (this.porcentaje != null ) {
			predicates.add(criteriaBuilder.equal(root.get("porcentaje"), this.porcentaje));
		}
		
		if (this.estado != null ) {
			predicates.add(criteriaBuilder.equal(root.get("enabled"), this.estado));
		}
		
		if (predicates.size() > 0) {
			p.getExpressions().add(criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()])));
		}
	
		return p;

	}
	
	

}
