package com.intap.ESG.util.specification;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;


import com.intap.ESG.Models.RecurringInvoices;

import lombok.AllArgsConstructor;


@AllArgsConstructor
public class RecurinInvoicesSpecification implements Specification<RecurringInvoices> {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String year;
	
	private String month;
	
	private Integer invoicesGenerated;
	private Integer invoicesGeneratedMax;

	@Override
	public Predicate toPredicate(Root<RecurringInvoices> root, CriteriaQuery<?> query,
			CriteriaBuilder criteriaBuilder) {
		Predicate p = criteriaBuilder.conjunction();

		List<javax.persistence.criteria.Predicate> predicates = new ArrayList<javax.persistence.criteria.Predicate>();
		
		if (year != null && !year.isEmpty()) {
			predicates.add(criteriaBuilder.equal(root.get("year"),this.year));
		}
		
		if (month != null && !month.isEmpty()) {
			predicates.add(criteriaBuilder.like(root.get("month"), "%" + this.month + "%"));
		}
		
		if(invoicesGenerated != null ) {
			predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get("invoicesGenerated"), this.invoicesGenerated));
		}
		
		
		if (invoicesGeneratedMax != null) {
			predicates.add(criteriaBuilder.lessThanOrEqualTo(root.get("invoicesGenerated"), this.invoicesGeneratedMax));
		}
		if (predicates.size() > 0) {
			p.getExpressions().add(criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()])));
		}

		return p;
	}

}
