/**
 * 
 */
package com.intap.ESG.util.specification;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.intap.ESG.Models.Associated;
import com.intap.ESG.Models.Invoices;

import lombok.AllArgsConstructor;

/**
 * @author Leonardo
 *
 */
@SuppressWarnings("serial")
@AllArgsConstructor
public class InvoicesSpecification implements Specification<Invoices> {

	private String startDate;
	private String endDate;
	
	private String consecutive;
	private String names;
	private String envoicesState;
	
	private Integer minTotal;
	private Integer maxTotal;
	
	@Override
	public Predicate toPredicate(Root<Invoices> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
		Predicate p = criteriaBuilder.conjunction();

		List<javax.persistence.criteria.Predicate> predicates = new ArrayList<javax.persistence.criteria.Predicate>();

		
		if (startDate != null && !startDate.isEmpty()) {
			predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get("envoicesMonth"), startDate));
		}
		
		if (endDate != null && !endDate.isEmpty()) {
			predicates.add(criteriaBuilder.lessThanOrEqualTo(root.get("envoicesMonth"), endDate));
		}
		
		if(consecutive != null && !consecutive.isEmpty()) {
			predicates.add(criteriaBuilder.like(root.get("consecutive"), "%" + this.consecutive + "%"));
//			predicates.add(criteriaBuilder.equal(root.get("consecutive"), consecutive));
		}
		
		if(envoicesState != null && !envoicesState.isEmpty()) {
			predicates.add(criteriaBuilder.equal(root.get("envoicesState"), envoicesState));
		}
		
		if(names != null && !names.isEmpty()) {
			
			@SuppressWarnings("unchecked")
			Path<Associated> contrato = (Path<Associated>) root.fetch("contrato", JoinType.LEFT).fetch("idClient", JoinType.LEFT).fetch("idAssociated", JoinType.LEFT);
			
			@SuppressWarnings("unchecked")
			Path<Associated> associated = (Path<Associated>) root.fetch("associate", JoinType.LEFT);
			
			predicates.add(criteriaBuilder.or(
					criteriaBuilder.like(
							criteriaBuilder.upper(
									criteriaBuilder.concat(contrato.get("firstName"),
									criteriaBuilder.concat(" ", 
											contrato.get("lastName")))),
							
							"%" + names.trim().toUpperCase() + "%"),
					
					criteriaBuilder.like(criteriaBuilder.upper(contrato.get("firstName")),
							"%" + names.trim().toUpperCase() + "%"),
					
					criteriaBuilder.like(criteriaBuilder.upper(contrato.get("lastName")),
							"%" + names.trim().toUpperCase() + "%"),
					criteriaBuilder.like(
							criteriaBuilder.upper(
									criteriaBuilder.concat( associated.get("firstName"),
									criteriaBuilder.concat(" ", 
											associated.get("lastName")))),
							
							"%" + names.trim().toUpperCase() + "%"),
					
					criteriaBuilder.like(criteriaBuilder.upper(associated.get("firstName")),
							"%" + names.trim().toUpperCase() + "%"),
					criteriaBuilder.like(criteriaBuilder.upper(associated.get("lastName")),
							"%" + names.trim().toUpperCase() + "%"))
				);
		
		}
		
		
		
		if(this.minTotal != null) {
			predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get("total"), this.minTotal));
		}
		
		if (maxTotal != null) {
			predicates.add(criteriaBuilder.lessThanOrEqualTo(root.get("total"), this.maxTotal));
		}
		
		if (predicates.size() > 0) {
			p.getExpressions().add(criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()])));
		}

		return p;
	}

}
