/**
 * 
 */
package com.intap.ESG.util.specification;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.intap.ESG.Models.Associated;
import com.intap.ESG.Models.DTO.TypeAssociatedFilter;

import lombok.AllArgsConstructor;

/**
 * @author Leonardo
 *
 */
@SuppressWarnings("serial")
@AllArgsConstructor
public class AssocietedSpecification implements Specification<Associated> {

	private String names;
	private String identificationNumber;
	private String CellPhoneNumber;
	private String neighborhood;
	private String city;
	private String email;
	private List<TypeAssociatedFilter> typeAssociated;
	private Integer userId;
	
	@Override
	public Predicate toPredicate(Root<Associated> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
		Predicate p = criteriaBuilder.conjunction();
		
	

		List<javax.persistence.criteria.Predicate> predicates = new ArrayList<javax.persistence.criteria.Predicate>();

		predicates.add(criteriaBuilder.notEqual(root.get("id"), userId));
		
		if (names != null && !names.isEmpty()) {

			predicates.add(criteriaBuilder.or(
					criteriaBuilder.like(
							criteriaBuilder.upper(
									criteriaBuilder.concat(root.get("firstName"),
									criteriaBuilder.concat(" ", 
											root.get("lastName")))),
							
							"%" + names.trim().toUpperCase() + "%"),
					
					criteriaBuilder.like(criteriaBuilder.upper(root.get("firstName")),
							"%" + names.trim().toUpperCase() + "%"),
					
					criteriaBuilder.like(criteriaBuilder.upper(root.get("lastName")),
							"%" + names.trim().toUpperCase() + "%")));
		}

		if (identificationNumber != null && !identificationNumber.isEmpty()) {
			predicates.add(criteriaBuilder.like(root.get("identificationNumber"), "%"+ this.identificationNumber + "%"));
		}

		if (CellPhoneNumber != null && !CellPhoneNumber.isEmpty()) {
			predicates.add(criteriaBuilder.like(root.get("CellPhoneNumber"), "%"+ this.CellPhoneNumber.trim()+ "%"));
		}

		if (neighborhood != null && !neighborhood.isEmpty()) {
			predicates.add(criteriaBuilder.like(criteriaBuilder.upper(root.get("neighborhood")),
					"%" + this.neighborhood.trim().toUpperCase() + "%"));
		}

		if (city != null && !city.isEmpty()) {
			predicates.add(criteriaBuilder.like(criteriaBuilder.upper(root.get("city")),
					"%" + this.city.trim().toUpperCase() + "%"));
		}

		if (email != null && !email.isEmpty()) {
			predicates.add(criteriaBuilder.like(criteriaBuilder.upper(root.get("email")),
					"%" + this.email.trim().toUpperCase() + "%"));
		}

		if (typeAssociated != null && !typeAssociated.isEmpty()) {
			
			List<javax.persistence.criteria.Predicate> typePredicates = typeAssociated.stream().map(data -> {
				javax.persistence.criteria.Predicate typePr = null;
				switch (data.getType()) {
				case 0:
					
					if (data.getStatus() != null && data.getStatus() != 0) {
						if (data.getStatus() == 1) {
							typePr = criteriaBuilder.isTrue(root.get("enabled"));
						} else if (data.getStatus() == 2) {
							typePr = criteriaBuilder.isFalse(root.get("enabled"));
						}
					} else {
						typePr = criteriaBuilder.or(criteriaBuilder.isTrue(root.get("enabled")), typePr = criteriaBuilder.isFalse(root.get("enabled")));
					}
					
					break;
				case 1:
					root.join("employee", JoinType.LEFT);
					typePr = generartedPredicateTypeAssociated(root, criteriaBuilder, data, typePr, "employee");
					break;
				case 2:
					root.join("client", JoinType.LEFT);
					typePr = generartedPredicateTypeAssociated(root, criteriaBuilder, data, typePr, "client");
					break;
				case 3:
					root.join("provider", JoinType.LEFT);
					typePr = generartedPredicateTypeAssociated(root, criteriaBuilder, data, typePr, "provider");
					break;
				case 4:
					root.join("partner", JoinType.LEFT);
					typePr = generartedPredicateTypeAssociated(root, criteriaBuilder, data, typePr, "partner");
					break;
				default:
					break;
				}

				return typePr;
			}).collect(Collectors.toList());

			predicates.add(criteriaBuilder.or(typePredicates.toArray(new Predicate[predicates.size()])));
		}

		if (predicates.size() > 0) {
			p.getExpressions().add(criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()])));
		}

		return p;
	}

	private javax.persistence.criteria.Predicate generartedPredicateTypeAssociated(Root<Associated> root,
			CriteriaBuilder cb, TypeAssociatedFilter typeAssociated, javax.persistence.criteria.Predicate predicate, String type) {
		if (typeAssociated.getStatus() != null && typeAssociated.getStatus() != 0) {
			if (typeAssociated.getStatus() == 1) {
				predicate = cb.and(cb.isNotNull(root.get(type).get("id")), 
						cb.isTrue(root.get(type).get("enabled")));
			} else if (typeAssociated.getStatus() == 2) {
				predicate = cb.and(cb.isNotNull(root.get(type)), 
						cb.isFalse(root.get(type).get("enabled")));
			}
		} else {
			predicate = cb.isNull(root.get(type));
		}
		return predicate;
	}

}
