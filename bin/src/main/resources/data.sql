﻿-- auth_client_details
INSERT INTO oauth_client_details (client_id, access_token_validity, additional_information, authorities, authorized_grant_types, autoapprove, client_secret, refresh_token_validity, resource_ids, scope, web_server_redirect_uri) 
VALUES ('intap', 86400, NULL, 'USER', 'password,authorization_code,refresh_token,implicit', 'true', /*123456*/ '$2a$10$xLB5nL5Z1qo3YAeZJHXJh.o0WXtvJ7kUQ/uxKObpUAVja/F8terru', NULL, 'oauth2-resource', 'read,write', NULL);


-- tIPOS DE ASOSIADOS
insert into associates_type(id,name) values(1,'Empleado');
insert into associates_type(id,name) values(2,'Cliente');
insert into associates_type(id,name) values(3,'Proveedor');
insert into associates_type(id,name) values(4,'Socio');
insert into associates_type(id,name) values(5,'General');
insert into associates_type(id,name) values(6,'Todos');


INSERT INTO enabled_reason ( id,name, associate_type) VALUES (1,'Prueba Empleado 1', 1);
INSERT INTO enabled_reason ( id,name, associate_type) VALUES (2,'Prueba Empleado 2', 1);
INSERT INTO enabled_reason ( id,name, associate_type) VALUES (3,'Prueba Empleado 3', 1);
INSERT INTO enabled_reason ( id,name, associate_type) VALUES (4,'Prueba Cliente 1', 2);
INSERT INTO enabled_reason ( id,name, associate_type) VALUES (5,'Prueba Cliente 2', 2);
INSERT INTO enabled_reason ( id,name, associate_type) VALUES (6,'Prueba Cliente 3', 2);
INSERT INTO enabled_reason ( id,name, associate_type) VALUES (7,'Prueba Proveedor 1', 3);
INSERT INTO enabled_reason ( id,name, associate_type) VALUES (8,'Prueba Proveedor 2', 3);
INSERT INTO enabled_reason ( id,name, associate_type) VALUES (9,'Prueba Proveedor3', 3);
INSERT INTO enabled_reason ( id,name, associate_type) VALUES (10,'Prueba Socio 1', 4);
INSERT INTO enabled_reason ( id,name, associate_type) VALUES (11,'Prueba Socio2', 4);
INSERT INTO enabled_reason ( id,name, associate_type) VALUES (12,'Prueba Socio3', 4);
INSERT INTO enabled_reason ( id,name, associate_type) VALUES (13,'General 1', 5);
INSERT INTO enabled_reason ( id,name, associate_type) VALUES (14,'General 2', 5);
INSERT INTO enabled_reason ( id,name, associate_type) VALUES (15,'General 3', 5);
INSERT INTO enabled_reason ( id,name, associate_type) VALUES (16,'Prueba Todos1', 6);
INSERT INTO enabled_reason ( id,name, associate_type) VALUES (17, 'Prueba Todos2', 6);
INSERT INTO enabled_reason ( id,name, associate_type) VALUES (18,'Prueba Todos3', 6);






-- Configuraciones de los consecutivos de factura
INSERT INTO configurations (id, consecutive_contrato, consecutive_factura, consecutive_ref_pago, consecutive_paymemt,consecutive_products, texto_factura) 
VALUES (1, '1', '1', '1', '1','1','este texto debe ir en la factura');

-- Configuraciones de envio de correo eletronico
INSERT INTO configuraciones_email(id, default_encoding, host, port, username, password, mail_smtp_auth, mail_smtp_starttls_enable, mail_smtp_starttls_required, mail_smtp_ssl_trust, mail_smtp_connectiontimeout, mail_smtp_timeout, mail_smtp_writetimeout)
VALUES (1, 'UTF-8', 'smtp.gmail.com', 587, 'facturaesg@gmail.com', 'esg123456', true, true, false, 'smtp.gmail.com', 5000, 3000, 5000);



-- Authorities
INSERT INTO authorities (authority, description) VALUES ('USER_READ', 'Consulta Asociados');
INSERT INTO authorities (authority, description) VALUES ('USER_WRITE', 'Crea y Actualiza Asociados');
INSERT INTO authorities (authority, description) VALUES ('USER_DELETE', 'Elimina Asociados');
INSERT INTO authorities (authority, description) VALUES ('SECURITY2_READ', 'Consulta roles');
INSERT INTO authorities (authority, description) VALUES ('SECURITY2_WRITE', 'Crea y Actualiza roles');
INSERT INTO authorities (authority, description) VALUES ('SECURITY2_DELETE', 'Elimina roles');
INSERT INTO authorities (authority, description) VALUES ('INVENTORY_READ', 'Consulta productos y categorias');
INSERT INTO authorities (authority, description) VALUES ('INVENTORY_WRITE', 'Crea y actualiza productos y categorias');
INSERT INTO authorities (authority, description) VALUES ('INVENTORY_DELETE', 'Elimina productos y categorias');
INSERT INTO authorities (authority, description) VALUES ('CONTRACTS_READ', 'Consulta contratos');
INSERT INTO authorities (authority, description) VALUES ('CONTRACTS_WRITE', 'Crea y actualiza contratos');
INSERT INTO authorities (authority, description) VALUES ('CONTRACTS_DELETE', 'Elimina contratos');
INSERT INTO authorities (authority, description) VALUES ('NEWS_READ', 'Consulta novedades');
INSERT INTO authorities (authority, description) VALUES ('NEWS_WRITE', 'Crea y actualiza novedades');
INSERT INTO authorities (authority, description) VALUES ('NEWS_DELETE', 'Elimina novedades');
INSERT INTO authorities (authority, description) VALUES ('LOGIN', 'Puede ingresar a la plataforma');
INSERT INTO authorities (authority, description) VALUES ('INVOICES_WRITE', 'Crea y actualiza facturas');
INSERT INTO authorities (authority, description) VALUES ('INVOICES_DELETE', 'Elimina facturas');
INSERT INTO authorities (authority, description) VALUES ('INVOICES_READ', 'Consulta facturas');
INSERT INTO authorities (authority, description) VALUES ('PASSWORD_WRITE', 'Cambiar contraseña');
INSERT INTO authorities (authority, description) VALUES ('DOCUMENT_WRITE', 'Cambiar Número de Documento');
INSERT INTO authorities (authority, description) VALUES ('PAYMENTS_READ', 'Visualizar pagos');
INSERT INTO authorities (authority, description) VALUES ('PAYMENTS_WRITE', 'Crear pagos');
INSERT INTO authorities (authority, description) VALUES ('PAYMENTS_DELETE', 'Eliminar pagos');

-- Roles
INSERT INTO roles (id, name) VALUES (1, 'SUPER USUARIO');

-- seguridad
INSERT INTO roles_authorities (role_id, authority_id) VALUES (1, 'USER_READ');
INSERT INTO roles_authorities (role_id, authority_id) VALUES (1, 'USER_WRITE');
INSERT INTO roles_authorities (role_id, authority_id) VALUES (1, 'USER_DELETE');
INSERT INTO roles_authorities (role_id, authority_id) VALUES (1, 'SECURITY2_READ');
INSERT INTO roles_authorities (role_id, authority_id) VALUES (1, 'SECURITY2_WRITE');
INSERT INTO roles_authorities (role_id, authority_id) VALUES (1, 'SECURITY2_DELETE');
INSERT INTO roles_authorities (role_id, authority_id) VALUES (1, 'INVENTORY_READ');
INSERT INTO roles_authorities (role_id, authority_id) VALUES (1, 'INVENTORY_WRITE');
INSERT INTO roles_authorities (role_id, authority_id) VALUES (1, 'INVENTORY_DELETE');
INSERT INTO roles_authorities (role_id, authority_id) VALUES (1, 'CONTRACTS_READ');
INSERT INTO roles_authorities (role_id, authority_id) VALUES (1, 'CONTRACTS_WRITE');
INSERT INTO roles_authorities (role_id, authority_id) VALUES (1, 'CONTRACTS_DELETE');
INSERT INTO roles_authorities (role_id, authority_id) VALUES (1, 'NEWS_READ');
INSERT INTO roles_authorities (role_id, authority_id) VALUES (1, 'NEWS_WRITE');
INSERT INTO roles_authorities (role_id, authority_id) VALUES (1, 'NEWS_DELETE');
INSERT INTO roles_authorities (role_id, authority_id) VALUES (1, 'LOGIN');
INSERT INTO roles_authorities (role_id, authority_id) VALUES (1, 'INVOICES_READ');
INSERT INTO roles_authorities (role_id, authority_id) VALUES (1, 'INVOICES_WRITE');
INSERT INTO roles_authorities (role_id, authority_id) VALUES (1, 'INVOICES_DELETE');
INSERT INTO roles_authorities (role_id, authority_id) VALUES (1, 'PASSWORD_WRITE');
INSERT INTO roles_authorities (role_id, authority_id) VALUES (1, 'PAYMENTS_WRITE');
INSERT INTO roles_authorities (role_id, authority_id) VALUES (1, 'PAYMENTS_READ');
INSERT INTO roles_authorities (role_id, authority_id) VALUES (1, 'PAYMENTS_DELETE');
INSERT INTO roles_authorities (role_id, authority_id) VALUES (1, 'DOCUMENT_WRITE');


-- users
INSERT INTO users (id, account_expired, account_locked, create_at, credenttials_expired, enabled, password, updated_at, user_name, role_id) 
VALUES (1,true ,true , NOW(), true, true, '$2a$10$xLB5nL5Z1qo3YAeZJHXJh.o0WXtvJ7kUQ/uxKObpUAVja/F8terru', null, '123456789', 1);
 
 --Asociado
 INSERT INTO associated(id, enabled, first_name, identification_number,birthdate, identification_type, last_name) VALUES (1, true, 'Super', 123456789, '1990-06-08','CC', 'Usuario' );

 -- empleado
 INSERT INTO employee(id, date_enabled, enabled, id_associated, user_id) VALUES (1, now(), true, 1,1);

 --Cliente
--  INSERT INTO client(id, enabled, id_associated) VALUES (1, true, 1);

--Taxes
INSERT INTO taxes (id, name, valor) VALUES (1, 'Sin impuestos', 0);
INSERT INTO taxes (id, name, valor) VALUES (2, 'Iva', 19);
INSERT INTO taxes (id, name, valor) VALUES (3, 'Rete Fuente', 8.5);

--Razones de retiro (actualizar con las reales)
INSERT INTO retirement_reason (id, name) VALUES (1, 'Razón de retiro No 1');
INSERT INTO retirement_reason (id, name) VALUES (2, 'Razón de retiro No 2');
INSERT INTO retirement_reason (id, name) VALUES (3, 'Razón de retiro No 3');
INSERT INTO retirement_reason (id, name) VALUES (4, 'Razón de retiro No 4');
INSERT INTO retirement_reason (id, name) VALUES (5, 'Razón de retiro No 5');

--Estados de los contratos
INSERT INTO status (id,description, name) VALUES (1,'Manual', 'CONECTADO');
INSERT INTO status (id,description, name) VALUES (2,'Automatico', 'SUSPENDER');
INSERT INTO status (id,description, name) VALUES (3,'Manual', 'SUSPENDIDO');
INSERT INTO status (id,description, name) VALUES (4,'Automatico', 'RECONECTAR');
INSERT INTO status (id,description, name) VALUES (5,'Automatico', 'CARTERA');
INSERT INTO status (id,description, name) VALUES (6,'Manual', 'AL DIA');


--Ciudades del valle
INSERT INTO cities (id,name) VALUES (1,'ALCALA');
INSERT INTO cities (id,name) VALUES (2,'ANDALUCIA');
INSERT INTO cities (id,name) VALUES (3,'ANSERMA NUEVO');
INSERT INTO cities (id,name) VALUES (4,'ARGELIA');
INSERT INTO cities (id,name) VALUES (5,'BOLIVAR');
INSERT INTO cities (id,name) VALUES (6,'BUENAVENTURA');
INSERT INTO cities (id,name) VALUES (7,'BUGA');
INSERT INTO cities (id,name) VALUES (8,'BUGALAGRANDE');
INSERT INTO cities (id,name) VALUES (9,'CAICEDONIA');
INSERT INTO cities (id,name) VALUES (10,'CALI');
INSERT INTO cities (id,name) VALUES (11,'CALIMA - EL DARIEN');
INSERT INTO cities (id,name) VALUES (12,'CANDELARIA');
INSERT INTO cities (id,name) VALUES (13,'CARTAGO');
INSERT INTO cities (id,name) VALUES (14,'DAGUA');
INSERT INTO cities (id,name) VALUES (15,'EL AGUILA');
INSERT INTO cities (id,name) VALUES (16,'EL CAIRO');
INSERT INTO cities (id,name) VALUES (17,'EL CERRITO');
INSERT INTO cities (id,name) VALUES (18,'EL DOVIO');
INSERT INTO cities (id,name) VALUES (19,'FLORIDA');
INSERT INTO cities (id,name) VALUES (20,'GINEBRA GUACARI');
INSERT INTO cities (id,name) VALUES (21,'JAMUNDI');
INSERT INTO cities (id,name) VALUES (22,'LA CUMBRE');
INSERT INTO cities (id,name) VALUES (23,'LA UNION');
INSERT INTO cities (id,name) VALUES (24,'LA VICTORIA');
INSERT INTO cities (id,name) VALUES (25,'OBANDO');
INSERT INTO cities (id,name) VALUES (26,'PALMIRA');
INSERT INTO cities (id,name) VALUES (27,'PRADERA');
INSERT INTO cities (id,name) VALUES (28,'RESTREPO');
INSERT INTO cities (id,name) VALUES (29,'RIOFRIO');
INSERT INTO cities (id,name) VALUES (30,'ROLDANILLO');
INSERT INTO cities (id,name) VALUES (31,'SAN PEDRO');
INSERT INTO cities (id,name) VALUES (32,'SEVILLA');
INSERT INTO cities (id,name) VALUES (33,'TORO');
INSERT INTO cities (id,name) VALUES (34,'TRUJILLO');
INSERT INTO cities (id,name) VALUES (35,'TULUA');
INSERT INTO cities (id,name) VALUES (36,'ULLOA');
INSERT INTO cities (id,name) VALUES (37,'VERSALLES');
INSERT INTO cities (id,name) VALUES (38,'VIJES');
INSERT INTO cities (id,name) VALUES (39,'YOTOCO');
INSERT INTO cities (id,name) VALUES (40,'YUMBO');
INSERT INTO cities (id,name) VALUES (41,'ZARZAL');

-- Index
CREATE INDEX contract_status_id_contract ON contract_status (contract);
CREATE INDEX contract_details_id_contract ON contract_details (contracts_id);
CREATE INDEX contract_news_id_contract ON contract_news (contracts_id);
CREATE INDEX contracs_consecutive ON contracts (consecutive);
CREATE INDEX contracs_client ON contracts (id_client);
CREATE INDEX contracs_ip_client ON contracts (ip_client);
CREATE INDEX contracs_enabled ON contracts (enabled);
CREATE INDEX invoices_state ON invoices (envoices_state);
CREATE INDEX invoices_contract ON invoices (id_contrato);
CREATE INDEX invoices_date ON invoices (invoice_date);
CREATE INDEX invoices_consecutive ON invoices (consecutive);
CREATE INDEX taxes_id ON taxes (id);
CREATE INDEX retirement_reason_id ON retirement_reason (id);
CREATE INDEX cities_id ON cities (id);
CREATE INDEX client_enabled ON client (enabled);
CREATE INDEX client_id_associated ON client (id_associated);
CREATE INDEX status_id ON status (id);

-- Restar sequences
ALTER SEQUENCE associates_type_id_seq RESTART WITH 7;
ALTER SEQUENCE enabled_reason_sequence RESTART WITH 19;
ALTER SEQUENCE configuration_sequence RESTART WITH 2;
ALTER SEQUENCE configurarion_email_sequence RESTART WITH 2;
ALTER SEQUENCE roles_sequence RESTART WITH 2;
ALTER SEQUENCE users_sequence RESTART WITH 2;
ALTER SEQUENCE associated_seq RESTART WITH 2;
ALTER SEQUENCE employee_sequence RESTART WITH 2;
ALTER SEQUENCE taxes_sequence RESTART WITH 4;
ALTER SEQUENCE retirement_reason_sequence RESTART WITH 6;
ALTER SEQUENCE stratus_sequence RESTART WITH 7;
ALTER SEQUENCE cities_sequence RESTART WITH 42;